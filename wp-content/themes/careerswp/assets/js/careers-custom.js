(function($){

	"use strict";

	/*banner-inline search start*/
	$("select[name=looking_for]").change( function() {
		// Get value
        var select_val = $( this ).val();

        if ( 'job' === select_val ) {
            $( "select[name=industry]" ).show();
            $( 'input[name=skills]' ).hide();
            $( '.salary-search-reange-label' ).hide();
        } else if ( 'candidate' === select_val ) {
        	$( "select[name=industry]" ).hide();
        	$( 'input[name=skills]' ).show();
        	$( '.salary-search-reange-label' ).show();
        }
	}).change();


	$('.banner-inline-search-form').submit(function(e){

		
		var searchFormData = $(this).serializeArray();

		
		var dataObj = {};

		$(searchFormData).each(function(i, field){
		  dataObj[field.name] = field.value;
		});

		console.log(dataObj.salary);

		var location = dataObj.location.split(',');

		var length = location.length;

		//console.log(location);

		var country   = location[length-1],
			state = location[length-2];

		var activeClass = $('.header-search-bar').hasClass('active');

		console.log(activeClass);
		
		if( dataObj.looking_for === 'job' ) {
			if( activeClass != true ) {
				if( 
					( typeof(state) == 'undefined' || state === null ) && 
					( dataObj.industry == "" ) 
				) {
					window.location.href = dataObj.job_search_page_id;
				} 
				else if( 
					( state != "" ) && 
					( dataObj.industry == "" ) 
				) {
					window.location.href = dataObj.job_search_page_id+'#?'+'location='+state;
				} else if( 
					( typeof(state) == 'undefined' || state === null ) && 
					( dataObj.industry != "" ) 
				) {
					window.location.href = dataObj.job_search_page_id+'#?'+'industry='+dataObj.industry;	
				} else {
					window.location.href = dataObj.job_search_page_id+'#?'+'location='+state+'&'+'industry='+dataObj.industry;
				}
			} else {
				if( 
					( typeof(state) == 'undefined' || state === null ) && 
					( dataObj.industry == "" ) 
				) {
					window.location.href = dataObj.job_search_page_id+'#?postday%5Bmin%5D='+0+'&postday%5Bmax%5D='+dataObj.days+'&salary%5Bmin%5D='+1+'&salary%5Bmax%5D='+dataObj.salary;
				} 
				else if( 
					( state != "" ) && 
					( dataObj.industry == "" ) 
				) {
					window.location.href = dataObj.job_search_page_id+'#?'+'location='+state+'&postday%5Bmin%5D='+0+'&postday%5Bmax%5D='+dataObj.days+'&salary%5Bmin%5D='+1+'&salary%5Bmax%5D='+dataObj.salary;
				} else if( 
					( typeof(state) == 'undefined' || state === null ) && 
					( dataObj.industry != "" ) 
				) {
					window.location.href = dataObj.job_search_page_id+'#?'+'industry='+dataObj.industry+'&postday%5Bmin%5D='+0+'&postday%5Bmax%5D='+dataObj.days+'&salary%5Bmin%5D='+1+'&salary%5Bmax%5D='+dataObj.salary;
				} else {
					window.location.href = dataObj.job_search_page_id+'#?'+'location='+state+'&'+'industry='+dataObj.industry+'&postday%5Bmin%5D='+0+'&postday%5Bmax%5D='+dataObj.days+'&salary%5Bmin%5D='+1+'&salary%5Bmax%5D='+dataObj.salary;
				}
			}
		} else if( dataObj.looking_for === 'candidate' ) {
			if( activeClass != true ) {
				if( 
					( typeof(state) == 'undefined' || state === null ) && 
					( dataObj.skills == "" ) 
				) {
					window.location.href = dataObj.candidate_search_page_id;
				}
				else if( 
					( state != "" ) && 
					( dataObj.skills == "" ) 
				) {
					window.location.href = dataObj.candidate_search_page_id+'#?'+'location='+state+'&'+'skills=';
				} else if( 
					( typeof(state) == 'undefined' || state === null ) && 
					( dataObj.skills != "" ) 
				) {
					window.location.href = dataObj.candidate_search_page_id+'#?'+'skills='+dataObj.skills;
				} else {
					window.location.href = dataObj.candidate_search_page_id+'#?'+'location='+state+'&'+'skills='+dataObj.skills;
				}
			} else {
				if( 
					( typeof(state) == 'undefined' || state === null ) && 
					( dataObj.skills == "" ) 
				) {
					window.location.href = dataObj.candidate_search_page_id+'#?'+'postday%5Bmin%5D='+0+'&postday%5Bmax%5D='+dataObj.days+'&salary%5Bmin%5D='+1+'&salary%5Bmax%5D='+dataObj.salary;
				}
				else if( 
					( state != "" ) && 
					( dataObj.skills == "" ) 
				) {
					window.location.href = dataObj.candidate_search_page_id+'#?'+'location='+state+'&'+'skills='+'&postday%5Bmin%5D='+0+'&postday%5Bmax%5D='+dataObj.days+'&salary%5Bmin%5D='+1+'&salary%5Bmax%5D='+dataObj.salary;
				} else if( 
					( typeof(state) == 'undefined' || state === null ) && 
					( dataObj.skills != "" ) 
				) {
					window.location.href = dataObj.candidate_search_page_id+'#?'+'skills='+dataObj.skills+'&postday%5Bmin%5D='+0+'&postday%5Bmax%5D='+dataObj.days+'&salary%5Bmin%5D='+1+'&salary%5Bmax%5D='+dataObj.salary;
				} else {
					window.location.href = dataObj.candidate_search_page_id+'#?'+'location='+state+'&'+'skills='+dataObj.skills+'&postday%5Bmin%5D='+0+'&postday%5Bmax%5D='+dataObj.days+'&salary%5Bmin%5D='+1+'&salary%5Bmax%5D='+dataObj.salary;
				}
			}
		}
		


		return false;

	});


	/*banner-inline header search end*/




})(jQuery);
