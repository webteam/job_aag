<?php
/*
* Template Name: Home With Slider
* Description: A Homepage Template for careers theme
*/
get_header('slider');?>

<div id="page-content">
      <div class="container">
          <div class="row">
          	<div class="col-sm-9 page-content">
          <?php get_template_part( 'framework/template/homepage/latest-jobs', '' ); ?>

          <?php get_template_part( 'framework/template/homepage/featured-jobs', '' ); ?>

          <?php get_template_part( 'framework/template/homepage/partners', '' ); ?>

    </div> <!-- end .page-content -->
    <div class="col-sm-3 page-sidebar">
        <aside>
             <?php get_sidebar('ads-sidebar'); ?>

            <div class="white-container">
              <?php get_sidebar('homepage-widget'); ?>
            </div>
          </aside>
      </div> <!-- end .page-sidebar -->

        <div class="col-sm-6 page-sidebar">
             <?php get_sidebar(''); ?>
            </div>
         
      <div class="col-sm-6 page-sidebar">
             <?php get_sidebar('sidebar-bottom-right'); ?>
      </div> <!-- end .page-sidebar -->
          
    </div>
    

  </div> <!-- end .container -->

    <?php /* 成功例はまだないのでコメントアウトします。
          *  get_template_part( 'framework/template/homepage/success-stories', '' ); 
          */ ?>

</div> <!-- end #page-content -->

<?php get_footer(); ?>