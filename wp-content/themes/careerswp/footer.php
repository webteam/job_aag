<?php global $careers_option_data; ?>

<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-md-4">
					<div class="widget">
						<div class="widget-content">
							<?php if( !empty($careers_option_data['uou-site-logo']['url'])) : ?>
								<img class="logo" src="<?php echo esc_url($careers_option_data['uou-site-logo']['url']); ?>" alt="<?php bloginfo('name') ?> | <?php bloginfo('description'); ?>">
							<?php else : ?>
								<h3><?php bloginfo('name') ?> | <small><?php bloginfo('description'); ?></small></h3>
							<?php endif; ?>
							<?php if(isset($careers_option_data['uou-footer-text']) && $careers_option_data['uou-footer-text'] ) : ?>
								<p><?php _e($careers_option_data['uou-footer-text']); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>

				<div class="col-sm-3 col-md-4">
					<div class="widget">
						<div class="widget-content">
							<div class="row">
								<div class="col-xs-6 col-sm-12 col-md-6">
									<?php get_sidebar('footer-widget-1') ?>
								</div>

								<div class="col-xs-6 col-sm-12 col-md-6">
									<?php get_sidebar('footer-widget-2') ?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-3 col-md-2">
					<?php get_sidebar('footer-widget-3') ?>
				</div>

				<div class="col-sm-3 col-md-2">
					<?php get_sidebar('footer-widget-4') ?>
				</div>
			</div>
		</div>

		<div class="copyright">
			<div class="container">
				<p>&copy; <?php _e('Copyright', 'careers') ?> <?php echo date('Y'); ?> <a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo('name') ?></a><?php _e(' | All Rights Reserved | Powered by ', 'careers'); ?><a href="<?php echo esc_url( __( 'http://themeforest.net/user/uouapps/', 'careers' ) ); ?>" target="_blank"><?php _e('UOU Apps', 'careers') ?></a></p>
				<ul class="footer-social">
					<?php if(isset($careers_option_data['uou-facebook-link']) && $careers_option_data['uou-facebook-link'] ) : ?>
					<li><?php echo ($careers_option_data['uou-facebook-link']) ? '<a href="'.esc_url($careers_option_data['uou-facebook-link'] ).'" title="Facebook" class="fa fa-facebook" target="_blank"></a>' : '' ?></li>
					<?php endif; ?>
					<?php if(isset($careers_option_data['uou-twitter-link']) && $careers_option_data['uou-twitter-link'] ) : ?>
					<li><?php echo ($careers_option_data['uou-twitter-link']) ? '<a href="'.esc_url($careers_option_data['uou-twitter-link'] ).'" title="twitter" class="fa fa-twitter" target="_blank"></a>' : '' ?></li>
					<?php endif; ?>
					<?php if(isset($careers_option_data['uou-linkedin-link']) && $careers_option_data['uou-linkedin-link'] ) : ?>
					<li><?php echo ($careers_option_data['uou-linkedin-link']) ? '<a href="'.esc_url($careers_option_data['uou-linkedin-link'] ).'" title="linkedin" class="fa fa-linkedin" target="_blank"></a>' : '' ?></li>
					<?php endif; ?>
					<?php if(isset($careers_option_data['uou-google-link']) && $careers_option_data['uou-google-link'] ) : ?>
					<li><?php echo ($careers_option_data['uou-google-link']) ? '<a href="'.esc_url($careers_option_data['uou-google-link'] ).'" title="google plus" class="fa fa-google-plus" target="_blank"></a>' : '' ?></li>
					<?php endif; ?>
					<?php if(isset($careers_option_data['uou-pinterest-link']) && $careers_option_data['uou-pinterest-link'] ) : ?>
					<li><?php echo ($careers_option_data['uou-pinterest-link']) ? '<a href="'.esc_url($careers_option_data['uou-pinterest-link'] ).'" title="pinterest" class="fa fa-pinterest" target="_blank"></a>' : '' ?></li>
					<?php endif; ?>
					<?php if(isset($careers_option_data['uou-dribbble-link']) && $careers_option_data['uou-dribbble-link'] ) : ?>
					<li><?php echo ($careers_option_data['uou-dribbble-link']) ? '<a href="'.esc_url($careers_option_data['uou-dribbble-link'] ).'" title="dribbble" class="fa fa-dribbble" target="_blank"></a>' : '' ?></li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</footer> <!-- end #footer -->

</div> <!-- end #main-wrapper -->

<?php if(isset($careers_option_data['uou-custom-css'])) { echo '<style>' . $careers_option_data['uou-custom-css'] . '</style>'; } ?>
<?php if(isset($careers_option_data['uou-custom-js'])) { echo '<script>' . $careers_option_data['uou-custom-js'] . '</script>'; } ?>

<?php wp_footer(); ?>
</body>
</html>