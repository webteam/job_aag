<?php

$args = array(
	'post_type' => 'job',
	'posts_per_page' => -1
);
$posts = get_posts( $args );


$jobs = array();

	
	foreach ($posts as  $post) {
		$post_id = $post->ID;


		// Company
		$company_id   = get_post_meta( $post_id, 'company_id', true );
		$company_post =  get_post( $company_id, ARRAY_A);
		$company_name = $company_post['post_title'];
		$company_url  = home_url( '/company/' .$company_post['post_name']);


		// Job expiry
		$date        = get_post_meta( $post_id, 'expire', true );
		$date_format = get_option( 'date_format' );
		$expire = date( $date_format, strtotime( $date ) );

		$locations = get_the_terms( $post_id, 'location' );
		if(isset($locations) && is_array($locations)){
			foreach ($locations as $location) {
			  if($location->parent != 0) {
		        $arr[] = $location;
		      }
		    }
		    if( !empty( $arr[0] ) && !empty($arr[1]) ) {
			    if($arr[0]->term_id == $arr[1]->parent) {
			      $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
			    } else {
			      $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
			    }
			} else {
	          $state_country = $arr[0]->name;
	        }
		}

		if( ! empty( $company_id )) {

			$thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $company_id ) );
			$thumb_url = $company_url;

		}

		$jobs[] = array(

			'id'                => $post_id,
			'title'             => $post->post_title,
			'excerpt'           => uou_get_post_excerpt($post->post_content),
			'content'           => apply_filters('the_content', $post->post_content),
			'post_date'         => $post->post_date,
			'post_author'       => $post->post_author,
			'guid'              => $post->guid,
			'comment_count'     => $post->comment_count,
			'location'          => $state_country,
			'application_email' => get_post_meta($post_id, 'application_email', true),
			'post_day'          => get_the_time('j', $post_id),
			'post_month'        => get_the_time('M', $post_id),
			'expire'            => $expire,
			'company_id'        => $company_id,
			'company_name'      => $company_name,
			'thumb_src'         => (!empty($thumb_src)) ? $thumb_src : careers_theme_profile_placeholder_image_url(),
			'thumb_url'         => $thumb_url,
			'isBookmarked'		=> isBookmarked($post_id, 'bookmarked_job'),
			'hr_id'             => get_post_meta($post_id, 'hr_id', true),
			'recruiter_id'      => get_post_meta($post_id, 'recruiter_id', true)

		);

		$arr = array();

	}

	wp_localize_script( 'ng-controllers', 'listing', array( 'jobs' => $jobs ) );


?>


<div class="pt30 pb30">
	<div>
		
		<div ng-controller="jobListing">
			<div class="white-container candidates-search clearfix">
				<div class="col-sm-6">
					<input type="text" class="form-control" placeholder="Job title" ng-model="search.title">
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" placeholder="場所" ng-model="search.location">
				</div>
			</div>
			<div class="title-lines">
				<h3 class="mt0"><?php _e('Available Jobs', 'careers') ?></h3>
			</div>

			<div class="jobs-item with-thumb" ng-repeat="job in jobs | filter:search:strict">
				<div class="thumb"><a href="{{ job.thumb_url }}"><img ng-src="{{ job.thumb_src }}" alt=""></a></div>
				<div class="clearfix visible-xs"></div>
				<div class="date">{{ job.post_day }} <span>{{ job.post_month }}</span></div>
				<h6 class="title"><a href="{{ job.guid }}">{{ job.title }}</a></h6>
				<span class="meta" ng-bind-html="renderHtml(job.location)"></span>

				<ul class="top-btns">
					<li><a href="#" class="btn btn-gray fa fa-plus toggle"></a></li>
					<li>
						<a href="" ng-if="job.isBookmarked" class="btn btn-gray fa fa-star" ng-click="saveAsBookmark(job)"></a>
						<a href="" ng-if="!job.isBookmarked" class="btn btn-gray fa fa-star-o" ng-click="saveAsBookmark(job)"></a>
					</li>
					<li><a href="{{ job.guid }}" class="btn btn-gray fa fa-link"></a></li>
				</ul>

				<p class="description">{{job.excerpt}}</p>

				<div class="content" ng-bind-html="renderHtml(job.content)">
				</div>

			</div>

		</div>
	</div>
</div>

