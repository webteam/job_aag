<?php
/*
* Template Name: Home With Boxes
* Description: A Homepage Template for careers theme
*/
get_header('boxes'); ?>

<div id="page-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 page-content">

        <?php get_template_part( 'framework/template/homepage/find-jobs-per', '' ); ?>

        <?php get_template_part( 'framework/template/homepage/featured-jobs', '' ); ?>

        <?php get_template_part( 'framework/template/homepage/partners', '' ); ?>

	       <?php get_template_part( 'framework/template/homepage/pricing-tables', '' ); ?>
      </div> <!-- end .page-content -->

      <div class="col-sm-4 page-sidebar">
        <aside>
          <?php get_template_part( 'framework/template/homepage/social-share', '' ); ?>

            <?php get_sidebar('ads-sidebar'); ?>

            <div class="white-container">
              <?php get_sidebar('homepage-widget'); ?>
            </div>
          </aside>
      </div> <!-- end .page-sidebar -->
    </div>
  </div> <!-- end .container -->

  <?php get_template_part( 'framework/template/homepage/success-stories', '' ); ?>

</div> <!-- end #page-content -->

<?php get_footer(); ?>