<?php
/*
* Template Name: 404
* Description: 404 Page
*/
?>
<?php get_header(); ?>
<h2 class="center">Error 404 - Not Found</h2>

<div class="center"><a href="javascript: history.go(-1)"><?php _e('前のページに戻る', TEXTDOMAIN); ?></a></div>
<div class="back-home　center"><a class="btn btn-default btn-block" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e('ホームへ戻る', TEXTDOMAIN); ?></a></div>

<?php get_footer(); ?>