<?php
/*
 * Template Name: Page without title & sidebar
 * Description: Page tempalte for show no title
 */
get_header(); ?>
  <div id="page-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 page-content">
          <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
            <article class="blog-post blog-post-single white-container">
            
              <?php if (current_user_can('edit_post', $post->ID)) {
                  edit_post_link(__('Edit This', 'new-item'), '<p class="page-admin-edit-this">', '</p>');
                } 
              ?>

              <div class="content">
                <?php the_content(); ?>
              </div>

              <!-- Displaying post pagination links in case we have multiple page posts -->
              <?php wp_link_pages('before=<div class="post-pagination">&after=</div>&pagelink=Page %'); ?>

            </article> <!-- end .blog-post -->
          <?php endwhile; else : ?>
            <article>
              <h1><?php _e( 'No Posts were Found!', 'careers' ); ?></h1>
            </article>
          <?php endif; ?>
        </div> <!-- end .page-content -->
      </div>
    </div> <!-- end .container -->
  </div>

<?php get_footer(); ?>