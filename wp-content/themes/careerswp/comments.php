<?php
/**
 * The template for displaying comments.
 *
 * @author 		faysal
 * @package 	Careers
 * @version     1.0
 */

if ( post_password_required() )
	return;
?>
<div id="comment" class="comments-section mt60">

	<div class="title-lines-left">
		<h4>Comments (<?php printf( _n('%', get_comments_number(), 'uou-pc')) ?>)</h4>
	</div>
	<?php if ( have_comments() ) : ?>
		<ul class="comments">
			<?php wp_list_comments(array( 'callback' => 'uou_comment' )); ?>
		</ul>
		<nav class="navigation comment-navigation" role="navigation">
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'uou-pc' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'uou-pc' ) ); ?></div>
		</nav><!-- .comment-navigation -->
	<?php endif; ?>

	<?php if ( comments_open() ) : ?>

		<h5>Leave a Reply</h5>

		<?php

			$req = get_option( 'require_name_email' );
			$aria_req = ( $req ? " required" : '' );

			//Custom Fields
			$fields =  array(
			'author'=> '<div class="row"><div class="col-md-4">
			<input type="text" name="author" class="form-control" placeholder="名前 *"' . $aria_req . '>
			</div>',
			'email' => '<div class="col-md-4">
			<input type="email" name="email" class="form-control" placeholder="メールアドレス *"' . $aria_req . '>
			</div>',
			'url' 	=> '<div class="col-md-4">
			<input type="url" name="url" class="form-control" placeholder="ウェブサイト">
			</div></div>',
			);

			//Comment Form Args
			$comments_args = array(
			'fields' => $fields,
			'title_reply'=>'',
			'comment_field' => '<textarea name="comment" rows="5" class="form-control" placeholder="ご意見を記入してください..."></textarea>',
			'label_submit' => __('Post Comment','uou-pc'),
			'class_submit' => '',
			'comment_notes_after' => '<button type="submit" class="btn btn-default"><i class="fa fa-pencil-square-o"></i>コメントする</button>'
			);

			// Show Comment Form
			comment_form($comments_args);

		?>
	<?php endif; ?>
</div>
