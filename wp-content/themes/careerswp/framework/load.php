<?php


	
	define('UOU', get_template_directory_uri().'/framework/');
	define('UOU_LIB', get_template_directory_uri().'/framework/lib/');
	define('UOU_EX', get_template_directory_uri().'/framework/vendor/');
	define('UOU_JS', get_template_directory_uri().'/assets/js/' );	
	define('UOU_CSS', get_template_directory_uri().'/assets/css/' );
	define('NO_UOU_CSS', get_template_directory_uri().'/assets/css-plugin/' );
	define('NO_UOU_JS', get_template_directory_uri().'/assets/js-plugin/' );
	
	







	#vendor loading
  
	// REDUX 

	if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/vendor/ReduxFramework/ReduxCore/framework.php' ) ) {
	    require_once( dirname( __FILE__ ) . '/vendor/ReduxFramework/ReduxCore/framework.php' );
	}
	if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/vendor/ReduxFramework/config/config.php' ) ) {
	    require_once( dirname( __FILE__ ) . '/vendor/ReduxFramework/config/config.php' );
	}

	// load plugins

	require_once('vendor/plugins/class-tgm-plugin-activation.php');

	require_once('vendor/plugins/load-plugin.php');








	#lib loading


	// for bbpress

	// require_once('lib/bbpress.php');
	

	// stylesheet & scripts 
	
	require_once('lib/style.php');
	require_once('lib/script.php');
	
	// uct_breadcrumbs
	require_once('lib/uct-breadcrumbs.php');


	// for theme function

	require_once('lib/uou-function.php');



	// for ajaxing 

	// require_once('lib/redq-ajax-posting.php');
	// require_once('lib/redq-hooks.php');

	

