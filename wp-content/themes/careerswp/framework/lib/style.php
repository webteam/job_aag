<?php

function uou_style(){
    global $comparis_option_data;

  	wp_register_style('uou-bootstrap', UOU_CSS.'bootstrap.css', array(), false, $media = 'all');
  	wp_enqueue_style('uou-bootstrap');

  	wp_register_style('font-awesome', UOU_CSS.'font-awesome.min.css', array(), false, $media = 'all');
  	wp_enqueue_style('font-awesome');

    wp_deregister_style( 'mediaelement' );
    wp_deregister_style( 'wp-mediaelement' );

    wp_register_style('wp-mediaelement', UOU_CSS.'mediaelementplayer.css', array(), false, $media = 'all');
    wp_enqueue_style('wp-mediaelement');

    wp_register_style('uou-flexslider', UOU_CSS.'flexslider.css', array(), false, $media = 'all');
    wp_enqueue_style('uou-flexslider');

  	wp_register_style('uou-style', UOU_CSS.'style.css', array(), false, $media = 'all');
  	wp_enqueue_style('uou-style');

  	wp_register_style('uou-responsive', UOU_CSS.'responsive.css', array(), false, $media = 'all');
  	wp_enqueue_style('uou-responsive');

    $change_style = 'color/'.$comparis_option_data['uou-select-stylesheet'];

    if( ! empty( $comparis_option_data['uou-select-stylesheet'] ) ){
        wp_register_style('skin-style', UOU_CSS.$change_style, array(), false, $media = 'all');
        wp_enqueue_style('skin-style');
    }

    wp_register_style('uou-main-style', get_bloginfo('stylesheet_url'), array(), false, $media = 'all');
    wp_enqueue_style('uou-main-style');

    wp_register_style('uou-theme-font', 'http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700', array(), false, $media = 'all');
    wp_enqueue_style('uou-theme-font');



}

add_action( 'wp_enqueue_scripts','uou_style', 99 );
