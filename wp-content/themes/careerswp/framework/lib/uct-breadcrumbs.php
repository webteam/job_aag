<?php
/**
 * Careers: Breadcrumbs Functions
 *
 * @author 		UouApps
 * @category 	Core
 * @package 	careers/lib
 * @version 	1.0.0
 */
function uct_breadcrumbs() {
    global $post, $wp_query, $author;

	$delimiter   = '  '; // delimiter between crumbs

	$wrap_before = '<ul class="breadcrumbs">';
	$wrap_after = '</ul>';


	$before = '<li>';
	$after = '</li>';
	$home = _x( 'Home', 'breadcrumb', 'careers' );
	$link = '<a href="%1$s">%2$s</a>';


	$prepend      = '';

	if ( ( ! is_front_page() && ! ( is_post_type_archive() ) ) || is_paged() ) {

	    echo apply_filters('the_excerpt', $wrap_before);

	    if ( ! empty( $home ) ) {
	        echo apply_filters('the_excerpt', $before . '<a class="home" href="' . apply_filters( 'uct_breadcrumb_home_url', home_url() ) . '">' . $home . '</a>' . $after . $delimiter);
	    }


	    if ( is_home() ) {

	        echo apply_filters('the_excerpt', $before . single_post_title('', false) . $after);

	    } elseif ( is_category() ) {

	        $cat_obj = $wp_query->get_queried_object();
	        $this_category = get_category( $cat_obj->term_id );

	        if ( 0 != $this_category->parent ) {
	            $parent_category = get_category( $this_category->parent );
	            if ( ( $parents = get_category_parents( $parent_category, TRUE, $after . $delimiter . $before ) ) && ! is_wp_error( $parents ) ) {
	                echo apply_filters('the_excerpt', $before . rtrim( $parents, $after . $delimiter . $before ) . $after . $delimiter);
	            }
	        }

	        echo apply_filters('the_excerpt', $before . single_cat_title( '', false ) . $after);

	    }  elseif ( is_day() ) {

	        echo apply_filters('the_excerpt', $before . '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a>' . $after . $delimiter);
	        echo apply_filters('the_excerpt', $before . '<a href="' . get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) . '">' . get_the_time( 'F' ) . '</a>' . $after . $delimiter);
	        echo apply_filters('the_excerpt', $before . get_the_time( 'd' ) . $after);

	    } elseif ( is_month() ) {

	        echo apply_filters('the_excerpt', $before . '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a>' . $after . $delimiter);
	        echo apply_filters('the_excerpt', $before . get_the_time( 'F' ) . $after);

	    } elseif ( is_year() ) {

	        echo apply_filters('the_excerpt', $before . get_the_time( 'Y' ) . $after);

	    } elseif ( is_single() && ! is_attachment() ) {
			if ( 'post' != get_post_type() ) {

	            $post_type = get_post_type_object( get_post_type() );
	            $slug = $post_type->rewrite;
	           	echo apply_filters('the_excerpt', $before . '<a href="' . get_post_type_archive_link( get_post_type() ) . '">' . $post_type->labels->singular_name . '</a>' . $after . $delimiter);
	            echo apply_filters('the_excerpt', $before . get_the_title() . $after);

	        }
	        else {

	            echo apply_filters('the_excerpt', $before . get_the_title() . $after);
	        }

	    } elseif ( is_404() ) {

	        echo apply_filters('the_excerpt', $before . __( 'Error 404', 'careers' ) . $after);

	    } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' ) {

	        $post_type = get_post_type_object( get_post_type() );

	        if ( $post_type ) {
	            echo apply_filters('the_excerpt', $before . $post_type->labels->singular_name . $after);
	        }

	    } elseif ( is_attachment() ) {

	        $parent = get_post( $post->post_parent );
	        $cat = get_the_category( $parent->ID );
	        $cat = $cat[0];
	        if ( ( $parents = get_category_parents( $cat, TRUE, $after . $delimiter . $before ) ) && ! is_wp_error( $parents ) ) {
	            echo apply_filters('the_excerpt', $before . rtrim( $parents, $after . $delimiter . $before ) . $after . $delimiter);
	        }
	        echo apply_filters('the_excerpt', $before . '<a href="' . get_permalink( $parent ) . '">' . $parent->post_title . '</a>' . $after . $delimiter);
	        echo apply_filters('the_excerpt', $before . get_the_title() . $after);

	    } elseif ( is_page() && ! $post->post_parent ) {

	        echo apply_filters('the_excerpt', $before . get_the_title() . $after);

	    } elseif ( is_page() && $post->post_parent ) {

	        $parent_id  = $post->post_parent;
	        $breadcrumbs = array();

	        while ( $parent_id ) {
	            $page          = get_post( $parent_id );
	            $breadcrumbs[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
	            $parent_id     = $page->post_parent;
	        }

	        $breadcrumbs = array_reverse( $breadcrumbs );

	        foreach ( $breadcrumbs as $crumb ) {
	            echo apply_filters('the_excerpt', $before . $crumb . $after . $delimiter);
	        }

	        echo apply_filters('the_excerpt', $before . get_the_title() . $after);

	    } elseif ( is_search() ) {

	        echo apply_filters('the_excerpt', $before . __( 'Search results for &ldquo;', 'careers' ) . get_search_query() . '&rdquo;' . $after);

	    } elseif ( is_tag() ) {

	            echo apply_filters('the_excerpt', $before . __( 'Posts tagged &ldquo;', 'careers' ) . single_tag_title( '', false ) . '&rdquo;' . $after);

	    } elseif ( is_author() ) {

	        $userdata = get_userdata( $author );
	        echo apply_filters('the_excerpt', $before . __( 'Author:', 'careers' ) . ' ' . $userdata->display_name . $after);

	    }

	    if ( get_query_var( 'paged' ) ) {
	        echo ' (' . __( 'ページ', 'careers' ) . ' ' . get_query_var( 'paged' ) . ')';
	    }

	    echo apply_filters('the_excerpt', $wrap_after);

	}

}
