<?php

/***********************************************************************************************/
/* Add Sidebar Support */
/***********************************************************************************************/
function uou_widgets_init() {
    register_sidebar(
		array(
			'name' => __('Main Sidebar', 'careers'),
			'id' => 'main-sidebar',
			'description' => __('The main sidebar area', 'careers'),
			'before_widget' => '<div class="widget">',
			'after_widget' => '</div> <!-- end .widget -->',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6>'
		)
	);
	register_sidebar(
		array(
			'name' => __('Footer widget 1', 'careers'),
			'id' => 'footer-widget-1',
			'description' => __('The footer widget area 1', 'careers'),
			'before_widget' => '<div class="widget footer-widget">',
			'after_widget' => '</div> <!-- end .widget -->',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6>'
		)
	);
	register_sidebar(
		array(
			'name' => __('Footer widget 2', 'careers'),
			'id' => 'footer-widget-2',
			'description' => __('The footer widget area 2', 'careers'),
			'before_widget' => '<div class="widget footer-widget">',
			'after_widget' => '</div> <!-- end .widget -->',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6>'
		)
	);
	register_sidebar(
		array(
			'name' => __('Footer widget 3', 'careers'),
			'id' => 'footer-widget-3',
			'description' => __('The footer widget area 3', 'careers'),
			'before_widget' => '<div class="widget footer-widget">',
			'after_widget' => '</div></div> <!-- end .widget -->',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6><div class="widget-content">'
		)
	);
	register_sidebar(
		array(
			'name' => __('Footer widget 4', 'careers'),
			'id' => 'footer-widget-4',
			'description' => __('The footer widget area 4', 'careers'),
			'before_widget' => '<div class="widget footer-widget">',
			'after_widget' => '</div></div> <!-- end .widget -->',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6><div class="widget-content">'
		)
	);

  register_sidebar(
    array(
      'name' => __('Ads Sidebar', 'careers'),
      'id' => 'ads-sidebar',
      'description' => __('The Ads widget area', 'careers'),
      'before_widget' => '<div class="widget sidebar-widget text-center">',
      'after_widget' => '</div> <!-- end .widget -->',
      'before_title' => '',
      'after_title' => ''
    )
  );

  register_sidebar(
    array(
      'name' => __('Homepage Widget', 'careers'),
      'id' => 'homepage-widget',
      'description' => __('Homepage widget area', 'careers'),
      'before_widget' => '<div class="widget sidebar-widget">',
      'after_widget' => '</div><!-- end .widget -->',
      'before_title' => '<h6 class="widget-title">',
      'after_title' => '</h6>'
    )
  );

  register_sidebar(
  array(
      'name' => __('bottom-left-sidebar', 'careers'),
      'id' => 'bottom-left-sidebar',
      'description' => __('The bottom left sidebar area', 'careers'),
      'before_widget' => '<div class="widget sidebar-widget">',
      'after_widget' => '</div></div> <!-- end .widget -->',
      'before_title' => '',
      'after_title' => ''
    )
  );

 register_sidebar(
  array(
      'name' => __('bottom-right-sidebar', 'careers'),
      'id' => 'bottom-right-sidebar',
      'description' => __('The bottom right sidebar area', 'careers'),
      'before_widget' => '<div class="widget sidebar-widget">',
      'after_widget' => '</div><!-- end .widget -->',
      'before_title' => '<h6 class="widget-title">',
      'after_title' => '</h6>'
    )
  );

 register_sidebar(
  array(
     'name' => 'test_widget01',
     'id' => 'test_widget01',
     'description' => '説明1',
     'before_widget' => '<div>',
     'after_widget' => '</div>',
     'before_title' => '<h3>',
     'after_title' => '</h3>'
    )
  );

}
add_action( 'widgets_init', 'uou_widgets_init' );

/***********************************************************************************************/
/* uou_comment */
/***********************************************************************************************/
function uou_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li id="li-comment-<?php comment_ID(); ?>">
    <div <?php comment_class( 'media' ); ?> id="comment-<?php comment_ID(); ?>">
      <?php echo apply_filters('the_excerpt', get_avatar( $comment, 60 )); ?>

      <div class="meta">
        <?php printf( __( '%s', 'careers' ), get_comment_author_link() ) ?>
        on <?php printf( __( '%1$s', 'careers' ),  get_comment_date() ) ?> - <?php printf( __( '%s', 'careers' ),  get_comment_time() ) ?> -

        <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ) ?>
        <?php edit_comment_link( __( '(編集)', 'careers' ), '  ', '' ) ?>
      </div>

      <div class="content">
        <p>
          <?php comment_text() ?>
          <?php if ( $comment->comment_approved == '0' ) : ?>
            <em><?php _e( 'あなたのコメントは管理者の承認待ちです.', 'careers' ) ?></em>
          <?php endif; ?>
        </p>
      </div>
    </div>
  </li>
<?php
}

/***********************************************************************************************/
/* custom menu walker class */
/***********************************************************************************************/
class uou_walker extends Walker_Nav_Menu
{

  public static function fallback( $args ) {
    if ( current_user_can( 'manage_options' ) ) {

      extract( $args );

      $fb_output = null;

      if ( $container ) {
        $fb_output = '<' . $container;

        if ( $container_id )
          $fb_output .= ' id="' . $container_id . '"';

        if ( $container_class )
          $fb_output .= ' class="' . $container_class . '"';

        $fb_output .= '>';
      }

      $fb_output .= '<ul';

      if ( $menu_id )
        $fb_output .= ' id="' . $menu_id . '"';

      if ( $menu_class )
        $fb_output .= ' class="' . $menu_class . '"';

      $fb_output .= '>';
      $fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">' . __('メニューを追加する', 'careers') . '</a></li>';
      $fb_output .= '</ul>';

      if ( $container )
        $fb_output .= '</' . $container . '>';

      echo apply_filters('the_excerpt', $fb_output);
    }
  }


  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"e-sub-menu\">\n";
  }



  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0) {
    global $wp_query,$wpdb;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

    $class_names = $value = '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    $has_children = $wpdb->get_var( $wpdb->prepare(
      "SELECT COUNT(meta_id) FROM {$wpdb->prefix}postmeta 
      WHERE meta_key='%s' AND meta_value='%d'",
      '_menu_item_menu_item_parent', $item->ID) );

    if ( $has_children > 0 ) {
      array_push( $classes, "has-submenu" );
    }
    //current-menu-ancestor

    if (in_array('current-menu-item', $classes, true) || in_array('current_page_item', $classes, true) || in_array('current-menu-ancestor', $classes, true) ) {
      $classes = array_diff($classes, array('current-menu-item', 'current_page_item', 'active'));

      array_push( $classes, "m-active" );
    }


    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
    $class_names = ' class="'. esc_attr( $class_names ) . '"';



    $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

    if($item->hash == 1) {
      $attributes .= ' href="'.get_site_url().'/#'. esc_attr( $item->subtitle ).'"'; 
    } else{
      $attributes .= ! empty( $item->url )        ? ' href="'. esc_attr( $item->url) .'"' : ''; 
    }
         

    $prepend = '';
    $append = '';
    $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

    if($depth != 0) {
      $description = $append = $prepend = "";
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
    $item_output .= $description.$args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}


/***********************************************************************************************/
/* WPML LANGUAGE SETUP */
/***********************************************************************************************/

function wpml_languages(){

    global $comparis_option_data;

      $languages = icl_get_languages('skip_missing=0');

      $wpml_select = $comparis_option_data['uou-wpml-select'];

      ?>
      <div class="header-language">
        <ul>
          <?php if($wpml_select == "name") : ?>
            
            <?php foreach ($languages as $language) : ?>
                <li <?php if($language['active']) : ?> class="active" <?php endif; ?>><a href="<?php echo esc_url($language['url']); ?>"><?php echo esc_attr($language['translated_name']); ?></a></li>
            <?php endforeach; ?>

          <?php elseif ($wpml_select == "code") : ?>
            
            <?php foreach ($languages as $language) : ?>
              <li <?php if($language['active']) : ?> class="active" <?php endif; ?>><a href="<?php echo esc_url($language['url']); ?>"><?php echo esc_attr($language['language_code']); ?></a></li>
            <?php endforeach; ?>

          <?php else : ?>

            <?php foreach ($languages as $language) : ?>
                <li <?php if($language['active']) : ?> class="active" <?php endif; ?>><a href="<?php echo esc_url($language['url']); ?>" style="width:45px;"><img src="<?php echo esc_attr($language['country_flag_url']); ?>"></a></li>
            <?php endforeach; ?>

          <?php endif; ?>
      </ul>
    </div> <!-- end .header-language -->
<?php
}


/* -------------------------------------------------------------------------
    START USER REGISTRATION
------------------------------------------------------------------------- */

function registration_user_data(){

    global $wpdb, $user_ID;

    if($user_ID){
        header('Location:'.home_url());
    }else{

        $error = array();

        if(isset($_REQUEST)){

            $form_data = $_POST['form_data'];
            parse_str($form_data,$user_info);
            
            $username = $user_info['username'];
            $email = $user_info['email'];
            $password = $user_info['password'];

            /*check username*/

            if ( strpos($username, ' ') !== false ) { 
              $errors['username'] = "ユーザー名にスペースを入れないでください";
            }

            if(empty($username)) { 
                $errors['username'] = "ユーザー名を入力してください";
            } elseif( username_exists( $username ) ) {
                $errors['username'] = "ユーザー名を登録済みです。他のユーザー名を入力してください";
            }

            /*check email*/

            if(!is_email($email)){
                $error['email'] = "メールアドレス正しくありません";
            }elseif(email_exists($email)){
                $error['email'] = " 登録済みのメールアドレスです! 他のメールアドレスを利用下さい ";
            }

            if(0 == count($error)){

                $new_user = wp_create_user($username,$password,$email);
                echo '登録完了しました';

            }else{

                echo json_encode($error); 
            }

        }

    }

    die();

}

add_action('wp_ajax_nopriv_registration_user_data','registration_user_data');
add_action('wp_ajax_registration_user_data','registration_user_data');

add_action('wp_head','wrr_ajaxurl');
function wrr_ajaxurl() {
    ?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
<?php
}

function flush_rules(){
flush_rewrite_rules();
}
add_action('init','flush_rules');


function uou_pagination() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    $pages = paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $wp_query->max_num_pages,
                'prev_next' => false,
                'type'  => 'array',
                'prev_next'   => TRUE,
                'prev_text'    => '&laquo;',
                'next_text'    => '&raquo;',
            ) );
    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul class="pagination pull-right">';
        foreach ( $pages as $page ) {
                echo "<li>$page</li>";
        }
       echo '</ul>';
    }
}


function uou_wp_title( $title, $sep, $default ) {
  $name = get_bloginfo('name');
  if($default == 'right') {

    return $title . ' ' . $name;

  } else {
    
    return ' ' . $title;
    
  }
 
}
add_filter( 'wp_title', 'uou_wp_title', 15, 3 );

/***********************************************************************************************/
/* WPML LANGUAGE SETUP */
/***********************************************************************************************/
function uou_wpml_languages(){
  global $careers_option_data;
  $languages = icl_get_languages('skip_missing=0');
  $wpml_select = $careers_option_data['uou-wpml-select'];
  ?>

    <?php
    if($wpml_select == "name"){ ?>

      <div class="header-language clearfix wpml_name">
      <ul>
      <?php
      foreach ($languages as $language) {
        if($language['active']){
          ?>
          <li class="active">
            <a href="<?php echo esc_url( $language['url'] ); ?>">
              <?php echo esc_attr( $language['translated_name'] ); ?>
            </a>
          </li>
      <?php
        }
      }
      ?>

        <?php foreach ($languages as $language) {
          
          if( !$language['active'] ){
            ?>
            <li><a href="<?php echo esc_url( $language['url'] ); ?>"><?php echo esc_attr($language['translated_name']); ?></a></li>
            <?php
          }
        }
        ?>

      </ul>
  <?php }elseif ($wpml_select == "code") { ?>

    <div class="header-language clearfix">
    <ul>
    <?php
    foreach ($languages as $language) {
        if($language['active']){
          ?>

          <li class="active">
            <a href="<?php echo esc_url( $language['url'] ); ?>">
              <?php echo esc_attr( $language['language_code'] ); ?>
            </a>
          </li>
      <?php
        }
      }
      ?>

        <?php foreach ($languages as $language) {
          
          if( !$language['active'] ){
            ?>
            <li><a href="<?php echo esc_url( $language['url'] ); ?>"><?php echo esc_attr($language['language_code']); ?></a></li>
            <?php
          }
        }
        ?>

      </ul>
  <?php
    }else{ ?>

      <div class="header-language clearfix">
      <ul>
      <?php
      foreach ($languages as $language) {
        if($language['active']){
          ?>
          <li class="active">
            <a href="<?php echo esc_url( $language['url'] ); ?>">
              <img src="<?php echo esc_url( $language['country_flag_url'] ); ?>">
            </a>
          </li>
      <?php
        }
      }
      ?>

        <?php foreach ($languages as $language) {
          // print_r($language);
          if( !$language['active'] ){
            ?>
            <li><a href="<?php echo esc_url( $language['url'] ); ?>" style="width:45px;"><img src="<?php echo esc_url( $language['country_flag_url'] ); ?>"></a></li>
            <?php
          }
        }
        ?>

      </ul>
    <?php
    } ?>
  </div> <!-- end .header-language -->
<?php
}


/***********************************************************************************************/
/*  Define Constants */
/***********************************************************************************************/
define('THEMEROOT', get_stylesheet_directory_uri());
define('IMAGES', THEMEROOT.'/assets/img/');

function uou_getTaxonomyObj($post_id, $taxonomy){
  $objectArray = array();
  $results = get_the_terms( $post_id, $taxonomy );
  if(isset($results) && is_array($results)){
    foreach ($results as $result) {
      $objectArray[] = array( 
        'id' => $result->term_id, 
        'name' => $result->name, 
        'slug'=> $result->slug);
    }
  }
  return $objectArray;
}





/***********************************************************************************************/
/* Add Menus */
/***********************************************************************************************/
function uou_register_my_menus(){
  register_nav_menus(
    array(
      'primary_navigation' => __('Primary Navigation', 'uou-pc')
    )
  );
}
add_action('init', 'uou_register_my_menus');


/***********************************************************************************************/
/* Add Theme Support for Post Formats, Post Thumbnails and Automatic Feed Links */
/***********************************************************************************************/

// Register Theme Features
function uou_theme_features()  {
  global $wp_version;

  add_theme_support( 'post-formats', array( 'aside', 'audio', 'image', 'link', 'quote', 'status', 'video' ) );
  // add_theme_support('post-formats', array('link', 'image', 'quote', 'video', 'audio'));

  add_theme_support( 'post-thumbnails', array( 'post', 'team', 'partner', 'success' ) );
  set_post_thumbnail_size(198, 198, true);
  add_image_size( 'custom-blog-image', 690, 216 );
  add_image_size( 'partner-thumb', 163, 37 );
  add_image_size( 'success-thumb', 100, 100 );
  add_image_size( 'small-thumb', 80, 80 );

  add_theme_support( 'automatic-feed-links' );

  add_theme_support( 'custom-header' );

  add_theme_support( 'custom-background' );
 
  add_theme_support( 'post-thumbnails' ); 
  if ( ! isset( $content_width ) ) {
      $content_width = 690;
  }

  load_theme_textdomain( 'careers', get_template_directory() . '/languages' );
  // add_theme_support( 'title-tag' );

}

// Hook into the 'after_setup_theme' action
add_action( 'after_setup_theme', 'uou_theme_features' );


function careers_theme_profile_placeholder_image() {
  
  echo apply_filters( 'careers_theme_profile_placeholder_image_html', sprintf( '<img src="%s" alt="Placeholder" />', careers_theme_profile_placeholder_image_url() ) );
}

function careers_theme_profile_placeholder_image_url() {
  return IMAGES . 'userpic.gif';
}


function gplus_count( $url ) {
      /* get source for custom +1 button */
      $contents = file_get_contents( 'https://plusone.google.com/_/+1/fastbutton?url=' .  $url );
   
      /* pull out count variable with regex */
      preg_match( '/window\.__SSR = {c: ([\d]+)/', $contents, $matches );
   
      /* if matched, return count, else zed */
      if( isset( $matches[0] ) ) 
          return (int) str_replace( 'window.__SSR = {c: ', '', $matches[0] );
      return 0;
  }

function truncate($string, $length, $dots = "...") {
  return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

function uou_get_post_excerpt( $text, $length = 240 ) {
  $string = strip_tags($text);

  return (strlen($string) > $length) ? substr($string, 0, $length) : $string;
}

function careers_set_post_views($postID) {
  $count_key = 'careers_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count=='') {
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
  } else {
      $count++;
      update_post_meta($postID, $count_key, $count);
  }
}
// To keep the count accurate, lets get rid of prefetching








class Uou_Walker_Category extends Walker_Category {

        function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
                extract($args);
                $cat_name = esc_attr( $category->name );
                $cat_name = apply_filters( 'list_cats', $cat_name, $category );
                $link = '<a href="' . esc_url( get_term_link($category) ) . '" ng-click="setFilterItem(\''.$category->taxonomy.'\', \''.$category->name.'\', \'list\' )"';
                if ( $use_desc_for_title == 0 || empty($category->description) )
                        $link .= 'title="' . esc_attr( sprintf(__( 'View all posts filed under %s', 'careers' ), $cat_name) ) . '"';
                else
                        $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
                $link .= '>';

                $count = ' <span>(' . intval($category->counts) . ')</span>';
                $link .= $cat_name . $count . '</a>';
                if ( !empty($feed_image) || !empty($feed) ) {
                        $link .= ' ';
                        if ( empty($feed_image) )
                                $link .= '(';
                        $link .= '<a href="' . esc_url( get_term_feed_link( $category->term_id, $category->taxonomy, $feed_type ) ) . '"';
                        if ( empty($feed) ) {
                                $alt = ' alt="' . sprintf(__( 'Feed for all posts filed under %s', 'careers' ), $cat_name ) . '"';
                        } else {
                                $title = ' title="' . $feed . '"';
                                $alt = ' alt="' . $feed . '"';
                                $name = $feed;
                                $link .= $title;
                        }
                        $link .= '>';
                        if ( empty($feed_image) )
                                $link .= $name;
                        else
                                $link .= "<img src='$feed_image'$alt$title" . ' />';
                        $link .= '</a>';
                        if ( empty($feed_image) )
                                $link .= ')';
                }
                // if ( !empty($show_count) )
                //         $link .= ' (' . intval($category->count) . ')';
                if ( 'list' == $args['style'] ) {
                        $output .= "\t<li";
                        $class = 'has-submenu cat-item cat-item-' . $category->term_id;


                        // YOUR CUSTOM CLASS
                        if ($depth)
                            $class .= ' sub-'.sanitize_title_with_dashes($category->name);


                        if ( !empty($current_category) ) {
                                $_current_category = get_term( $current_category, $category->taxonomy );
                                if ( $category->term_id == $current_category )
                                        $class .=  ' current-cat';
                                elseif ( $category->term_id == $_current_category->parent )
                                        $class .=  ' current-cat-parent';
                        }
                        $output .=  ' class="' . $class . '"';
                        $output .= ">$link\n";
                } else {
                        $output .= "\t$link<br />\n";
                }
        } // function start_el

} // class Uou_Walker_Category



/**
 * Display or retrieve the HTML list of categories.
 *
 * The list of arguments is below:
 *     'show_option_all' (string) - Text to display for showing all categories.
 *     'orderby' (string) default is 'ID' - What column to use for ordering the
 * categories.
 *     'order' (string) default is 'ASC' - What direction to order categories.
 *     'show_count' (bool|int) default is 0 - Whether to show how many posts are
 * in the category.
 *     'hide_empty' (bool|int) default is 1 - Whether to hide categories that
 * don't have any posts attached to them.
 *     'use_desc_for_title' (bool|int) default is 1 - Whether to use the
 * category description as the title attribute.
 *     'feed' - See {@link get_categories()}.
 *     'feed_type' - See {@link get_categories()}.
 *     'feed_image' - See {@link get_categories()}.
 *     'child_of' (int) default is 0 - See {@link get_categories()}.
 *     'exclude' (string) - See {@link get_categories()}.
 *     'exclude_tree' (string) - See {@link get_categories()}.
 *     'echo' (bool|int) default is 1 - Whether to display or retrieve content.
 *     'current_category' (int) - See {@link get_categories()}.
 *     'hierarchical' (bool) - See {@link get_categories()}.
 *     'title_li' (string) - See {@link get_categories()}.
 *     'depth' (int) - The max depth.
 *
 * @since 2.1.0
 *
 * @param string|array $args Optional. Override default arguments.
 * @return false|null|string HTML content only if 'echo' argument is 0.
 */
function uou_list_categories( $args = '' ) {
  $defaults = array(
    'show_option_all' => '', 'show_option_none' => __('No categories', 'careers'),
    'orderby' => 'name', 'order' => 'ASC',
    'style' => 'list',
    'show_count' => 0, 'hide_empty' => 1,
    'use_desc_for_title' => 1, 'child_of' => 0,
    'feed' => '', 'feed_type' => '',
    'feed_image' => '', 'exclude' => '',
    'exclude_tree' => '', 'current_category' => 0,
    'hierarchical' => true, 'title_li' => __( 'Categories', 'careers' ),
    'echo' => 1, 'depth' => 0,
    'post_type' => 'post',
    'taxonomy' => 'category'
  );

  $r = wp_parse_args( $args, $defaults );

  if ( !isset( $r['pad_counts'] ) && $r['show_count'] && $r['hierarchical'] )
    $r['pad_counts'] = true;

  if ( true == $r['hierarchical'] ) {
    $r['exclude_tree'] = $r['exclude'];
    $r['exclude'] = '';
  }

  if ( ! isset( $r['class'] ) )
    $r['class'] = ( 'category' == $r['taxonomy'] ) ? 'categories' : $r['taxonomy'];

  if ( ! taxonomy_exists( $r['taxonomy'] ) ) {
    return false;
  }

  $show_option_all = $r['show_option_all'];
  $show_option_none = $r['show_option_none'];

  // $categories = get_categories( $r );

  $taxonomy = $r['taxonomy'];
  $post_type = $r['post_type'];
  
  global $wpdb;
  $categories = $wpdb->get_results( $wpdb->prepare( "
      SELECT tr.*, tm.*, tmr.*, count(*) AS counts FROM {$wpdb->terms} tr
      INNER JOIN {$wpdb->term_taxonomy} tm
        ON ( tm.term_id = tr.term_id )
      INNER JOIN {$wpdb->term_relationships} tmr
        ON ( tmr.term_taxonomy_id = tm.term_taxonomy_id )
      INNER JOIN {$wpdb->posts} p
        ON ( p.ID = tmr.object_id )
      WHERE ( tm.taxonomy = '%s'
        AND p.post_type = '%s' )
      GROUP BY name HAVING COUNT(name) > '%d'
  ", $taxonomy, $post_type, 0 ));


  $output = '';
  // if ( $r['title_li'] && 'list' == $r['style'] ) {
  //   $output = '<li class="' . esc_attr( $r['class'] ) . '">' . $r['title_li'] . '<ul>';
  // }
  if ( empty( $categories ) ) {
    if ( ! empty( $show_option_none ) ) {
      if ( 'list' == $r['style'] ) {
        $output .= '<li class="cat-item-none">' . $show_option_none . '</li>';
      } else {
        $output .= $show_option_none;
      }
    }
  } else {
    if ( ! empty( $show_option_all ) ) {
      $posts_page = ( 'page' == get_option( 'show_on_front' ) && get_option( 'page_for_posts' ) ) ? get_permalink( get_option( 'page_for_posts' ) ) : home_url( '/' );
      $posts_page = esc_url( $posts_page );
      if ( 'list' == $r['style'] ) {
        $output .= "<li class='cat-item-all'><a href='$posts_page'>$show_option_all</a></li>";
      } else {
        $output .= "<a href='$posts_page'>$show_option_all</a>";
      }
    }

    if ( empty( $r['current_category'] ) && ( is_category() || is_tax() || is_tag() ) ) {
      $current_term_object = get_queried_object();
      if ( $current_term_object && $r['taxonomy'] === $current_term_object->taxonomy ) {
        $r['current_category'] = get_queried_object_id();
      }
    }

    if ( $r['hierarchical'] ) {
      $depth = $r['depth'];
    } else {
      $depth = -1; // Flat.
    }
    $output .= walk_category_tree( $categories, $depth, $r );
  }

  if ( $r['title_li'] && 'list' == $r['style'] )
    $output .= '</ul></li>';

  /**
   * Filter the HTML output of a taxonomy list.
   *
   * @since 2.1.0
   *
   * @param string $output HTML output.
   * @param array  $args   An array of taxonomy-listing arguments.
   */
  $html = apply_filters( 'uou_list_categories', $output, $args );

  if ( $r['echo'] ) {
    echo apply_filters('the_excerpt', $html);
  } else {
    return $html;
  }
}