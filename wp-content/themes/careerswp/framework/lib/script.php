<?php

function uou_scripts() {

	wp_enqueue_script( 'jquery' );

	wp_register_script( 'uou-fitvids', UOU_JS.'jquery.fitvids.js', array('jquery'), true, false);
	wp_enqueue_script( 'uou-fitvids' );

	wp_register_script( 'ba-outside-events', UOU_JS.'jquery.ba-outside-events.min.js', array('jquery'), true, false);
	wp_enqueue_script( 'ba-outside-events' );

	wp_register_script( 'uou-media-match', UOU_JS.'media.match.min.js', array('jquery'), true, false);
	wp_enqueue_script( 'uou-media-match' );

	wp_register_script( 'uou-responsive-tabs', UOU_JS.'jquery.responsive-tabs.js', array('jquery'), true, false);
	wp_enqueue_script( 'uou-responsive-tabs' );

	wp_register_script( 'uou-inview', UOU_JS.'jquery.inview.min.js', array('jquery'), true, false);
	wp_enqueue_script( 'uou-inview' );

	wp_register_script( 'uou-flexslider', UOU_JS.'jquery.flexslider-min.js', array('jquery'), true, false);
	wp_enqueue_script( 'uou-flexslider' );

	wp_register_script( 'uou-google', 'https://maps.google.com/maps/api/js?sensor=false', array('jquery'), true, false);
    wp_enqueue_script( 'uou-google' );

	wp_register_script( 'uou-maplace', UOU_JS.'maplace.min.js', array('jquery'), true, false);
    wp_enqueue_script( 'uou-maplace' );

    wp_register_script( 'typeahead.bundle', UOU_JS.'typeahead.bundle.js', array('jquery'), $ver = false, true );
	wp_enqueue_script('typeahead.bundle');

	wp_register_script( 'careers_custom', UOU_JS.'careers-custom.js', array('jquery'), $ver = false, true );
	wp_enqueue_script('careers_custom');

	wp_enqueue_script('jquery-ui-slider');


	wp_register_script( 'uou-script', UOU_JS.'script.js', array('jquery', 'jquery-ui-slider', 'uou-inview', 'uou-inview', 'uou-media-match', 'uou-flexslider', 'uou-responsive-tabs'), true, true);
	wp_enqueue_script( 'uou-script' );
	




	/*-------------------------------------------------------------------------
	  GOOGLE MAP START
	------------------------------------------------------------------------- */

	$args = array('post_type' => 'job','posts_per_page' => '-1');

	$my_query = new WP_Query( $args );

	$marker_content_prev = array();

	$search_location = array();

	foreach ($my_query->posts as $key => $value) {

		$state_country = '';

		$post_id = $my_query->posts[$key]->ID;
		$lat = get_post_meta( $post_id, '_job_latitude');
		$lng = get_post_meta( $post_id, '_job_longitude');	

		$locations = get_the_terms( $post_id, 'location' );

        //print_r($locations);

        $arr = array(); 

        foreach ($locations as $location) {
          if($location->parent != 0) {
            $arr[] = $location;
          }
        }

       
        if(sizeof($arr)>1){

            if($arr[0]->term_id == $arr[1]->parent) {
              $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
              $residence_location = $arr[0]->name;
            } else {
              $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
              $residence_location = $arr[1]->name;
            }

        }else{
            $state_country   = $arr[0]->name;
        }


        array_push($search_location, html_entity_decode($state_country));








		

		/*$icon_id = get_post_meta($post_id,'_concierge_company_location_icon');
		$icon = wp_get_attachment_image_src( $icon_id[0] );
	*/
		$post_title = $my_query->posts[$key]->post_title;
		$post_permalink = $my_query->posts[$key]->guid;
		$content = $my_query->posts[$key]->post_content;
		$trimmed_content = wp_trim_words( $content, 5, '<a href="'. $post_permalink .'"> Read More</a>'  );
		

		if(!empty($lat) && !empty($lng) ){

			$marker_content_prev[$key]['lat'] = floatval($lat[0]);
	    	$marker_content_prev[$key]['lon'] = floatval($lng[0]);

	    	$marker_content_prev[$key]['id'] = (string)$post_id;
	    	$marker_content_prev[$key]['zoom'] = 8;

	    	if(isset($icon) && !empty($icon)){
				$marker_content_prev[$key]['icon'] = $icon[0];
			}			
			$marker_content_prev[$key]['title'] = $post_title;
			$marker_content_prev[$key]['html'] = '<div class="post-'.$post_id.' map-product"><h5><a href ="'.$post_permalink.'">'.$post_title.'</a></h5><p>'.$trimmed_content.'</p></div>';

			$marker_content_prev[$key]['icon'] = get_stylesheet_directory_uri() . '/assets/img/marker.png';




		}
	    
	    
		

	}


	$marker_content = array();

	foreach ($marker_content_prev as $keys => $values) {
		array_push($marker_content, $values);
	}


	
	$search_location = array_unique($search_location);




	$args = array('post_type' => 'candidate','posts_per_page' => '-1');

	$my_query = new WP_Query( $args );

	$candidate_search_location = array();

	foreach ($my_query->posts as $key => $value) {

		$state_country = '';

		$post_id = $my_query->posts[$key]->ID;

		$locations = get_the_terms( $post_id, 'location' );

        //print_r($locations);

        $arr = array(); 
        if(isset($locations) && is_array($locations)){
	        foreach ($locations as $location) {
	          if($location->parent != 0) {
	            $arr[] = $location;
	          }
	        }

	        if(sizeof($arr)>1){

	            if($arr[0]->term_id == $arr[1]->parent) {
	              $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
	              $residence_location = $arr[0]->name;
	            } else {
	              $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
	              $residence_location = $arr[1]->name;
	            }

	        }else{
	            $state_country   = $arr[0]->name;
	        }

	        array_push($candidate_search_location, html_entity_decode($state_country));
    	}
       
	}

	
	$search_location = array_unique (array_merge ($search_location, $candidate_search_location));







	wp_localize_script( 'uou-script', 'marker_location', $marker_content);
	wp_localize_script( 'uou-script', 'search_location', $search_location);




	/*-------------------------------------------------------------------------
	  GOOGLE MAP START
	------------------------------------------------------------------------- */










	wp_localize_script( 'uou-script', 'nonce_contact_form', wp_create_nonce( 'nonce-contact-form' ) );





}


add_action( 'wp_enqueue_scripts', 'uou_scripts' );


function uou_admin_load_scripts() {
	// wp_register_script( 'uou-admin', UOU_JS . 'admin.js', array('jquery'), $ver = false, true );
	// wp_enqueue_script( 'uou-admin' );

}
add_action('admin_enqueue_scripts', 'uou_admin_load_scripts') ;



