<article <?php post_class('blog-post white-container'); ?> id="post-<?php the_ID(); ?>">
  <?php echo do_shortcode(get_the_content()); ?>
  <h1 class="h2"><?php the_title(); ?></h1>
  <div class="meta">
    <span class="author"><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></span>
    <span class="date"><i class="fa fa-clock-o"></i><a href="<?php the_permalink(); ?>"> <?php the_time(get_option('date_format')); ?></a></span>
    <span class="category"><i class="fa fa-folder-open"></i> <?php the_category(',&nbsp; '); ?></span>
    <?php if (comments_open() && !post_password_required()) { ?>
    <span class="comments"><i class="fa fa-comments"></i> <?php comments_popup_link('0 Comments', '1 Comment', '% Comments', ''); ?></span>
    <?php } ?>
  </div>

  <?php the_excerpt(); ?>

</article>