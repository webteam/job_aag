<article <?php post_class('blog-post blog-post-single white-container'); ?> id="post-<?php the_ID(); ?>">
  <div class="meta">
    <span class="author"><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></span>
    <span class="date"><i class="fa fa-clock-o"></i><a href="<?php the_permalink(); ?>"> <?php the_time(get_option('date_format')); ?></a></span>
    <span class="category"><i class="fa fa-folder-open"></i> <?php the_category(',&nbsp; '); ?></span>
    <?php if (comments_open() && !post_password_required()) { ?>
    <span class="comments"><i class="fa fa-comments"></i> <?php comments_popup_link('0 Comments', '1 Comment', '% Comments', ''); ?></span>
    <?php } ?>
    <?php if (current_user_can('edit_post', $post->ID)) { ?>
    <span class="edit-post"><i class="fa fa-pencil"></i> <?php edit_post_link(__('Edit', 'uou-pc'), '', ''); ?></span>
    <?php } ?>
  </div>

  <?php echo do_shortcode(get_the_content()); ?>

  <?php get_template_part( 'framework/template/format/posts-pagination', '' ); ?>

  <div class="post-comments">
    <?php wp_reset_query(); if (comments_open()) : comments_template('', true ); endif; ?>
  </div>
</article>

<?php get_template_part( 'framework/template/format/pages-pagination', '' ); ?>