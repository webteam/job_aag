<ul class="pager">
    <li class="previous"><?php previous_post_link('%link', '&larr; Older', true); ?></li>
    <li class="next"><?php next_post_link('%link', 'Newer &rarr;', true); ?></li>
</ul>