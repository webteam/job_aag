<article <?php post_class('blog-post white-container'); ?> id="post-<?php the_ID(); ?>">

  <?php if (has_post_thumbnail()) : ?>
		<?php
			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
			$thumb_url = $thumb_url_array[0];
		?>

    <!-- <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> -->

		<a href="<?php the_permalink(); ?>"><img class="post-image" src="<?php echo esc_url($thumb_url); ?>"></a>
  <?php endif; ?>

  <h1 class="h2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

  <div class="meta">
    <span class="author"><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></span>
    <span class="date"><i class="fa fa-clock-o"></i> <a href="<?php the_permalink(); ?>"><?php the_time(get_option('date_format')); ?></a></span>
    <span class="category"><i class="fa fa-folder-open"></i> <?php the_category(',&nbsp; '); ?></span>
    <?php if (comments_open() && !post_password_required()) { ?>
    <span class="comments"><i class="fa fa-comments"></i> <?php comments_popup_link('0 Comments', '1 Comment', '% Comments', ''); ?></span>
    <?php } ?>
  </div>

  <p><?php echo apply_filters('the_excerpt', get_the_excerpt()); ?></p>

  <a href="<?php the_permalink(); ?>" class="btn btn-default read-more"><?php _e('Read More', 'uou-pc'); ?></a>
</article>