<article <?php post_class('blog-post blog-post-single white-container'); ?> id="post-<?php the_ID(); ?>">

	<?php if (has_post_thumbnail()) : ?>

		<?php
			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
			$thumb_url = $thumb_url_array[0];
		?>

		<a href="<?php the_permalink(); ?>"><img class="post-image" src="<?php echo esc_url($thumb_url); ?>"></a>
		
	<?php endif; ?>

	<h1 class="h2"><?php the_title(); ?></h1>

	<div class="meta">
			<span class="author"><i class="fa fa-user"></i> <?php the_author_posts_link(); ?>
			 @ - <a href="<?php the_permalink(); ?>"><?php the_time(get_option('time_format')); ?> , <?php the_time(get_option('date_format')); ?></a></span>
			<span class="category"><i class="fa fa-folder-open"></i> <?php the_category(',&nbsp; '); ?></span>
			<?php if (comments_open() && !post_password_required()) { ?>
		    <span class="comments"><i class="fa fa-comments"></i> <?php comments_popup_link('0 Comments', '1 Comment', '% Comments', ''); ?></span>
		    <?php } ?>
			<?php if (current_user_can('edit_post', $post->ID)) { ?>
		    <span class="edit-post"><i class="fa fa-pencil"></i> <?php edit_post_link(__('Edit', 'uou-pc'), '', ''); ?></span>
		    <?php } ?>
	</div>

	<div class="content">
		<?php echo do_shortcode(get_the_content()); ?>
	</div>

	<?php if(has_tag()) : ?>
		<h6 class="tags"><?php _e('Tags: ', 'uou-pc'); ?><?php the_tags('', ', ' ,''); ?></h6>
	<?php endif; ?>

	<?php get_template_part( 'framework/template/format/posts-pagination', '' ); ?>


	<div class="comments-section">

		<?php wp_reset_query(); if (comments_open()) : comments_template('', true ); endif; ?>

	</div>
</article>
<?php get_template_part( 'framework/template/format/pages-pagination', '' ); ?>


