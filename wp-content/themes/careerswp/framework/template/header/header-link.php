<?php if(!is_user_logged_in()) { ?>
				<!-- Header Register -->
				<div class="header-register">
					<a href="#" class="btn btn-link"><?php _e('登録', 'careers'); ?></a>
					<div>
						<span class="success"></span>
						<div ng-controller="userRegisterController" ng-cloak>
						    <form name="userForm" ng-submit="submitForm()" novalidate >
						        <div class="alert alert-info ng-alert" ng-show="response_errors">
						            <button href="#" type="button" class="close" ng-click="response_errors=false">&times;</button>
						            <ul>
						                  <li ng-repeat="(k,v) in response_errors">{{v}}</li>
						            </ul>
						        </div>
						        <div class="alert alert-info ng-alert" ng-show="saved">
						            <button href="#" type="button" class="close" ng-click="saved=false">&times;</button>
						            <p><?php _e('登録を完了しました', 'careers'); ?></p>
						        </div>
						       
						        <div class="form-group"   show-errors>
						            
						            <input type="email" ng-model="user.email" name="email" class="form-control" id="email" placeholder="メールアドレスを入力する" required />
						            <p ng-if="userForm.email.$error.required" class="help-block"><?php _e('正しいメールアドレスを入力してください', 'careers'); ?></p>
						        </div>
						        <div class="form-group"   show-errors>
						            
						            <input type="text" ng-model="user.username" name="username" class="form-control" id="username" placeholder="ユーザー名" required />
						            <p ng-if="userForm.username.$error.required" class="help-block"><?php _e('ユーザー名を入力してください', 'careers'); ?></p>
						        </div>
						        <div class="form-group"   show-errors>
						            
						            <input type="password" ng-model="user.password" name="password" class="form-control" id="password" placeholder="パスワード" required />
						            <p ng-if="userForm.password.$error.required" class="help-block"><?php _e('パスワードを入力してください', 'careers'); ?></p>
						        </div>

						        <div class="form-group">
						            
						            <select class="form-control" ng-model="user.registration_type" name="registration_type" ng-options="s.name for s in accountType"></select>
						        </div>
						        <div class="form-group"   show-errors>
						            
						            <input type="text" ng-model="user.name" name="name" class="form-control" placeholder="名前" required />
						            <p ng-if="userForm.name.$error.required" class="help-block"><?php _e('会社/代理店/候補名が必要です。', 'careers'); ?></p>
						        </div>
						        <button type="submit" class="btn btn-default" ng-disabled="userForm.$invalid"><?php _e('送信', 'careers'); ?></button>
						    </form>
						</div>
					</div>
				</div> <!-- end .header-register -->
				<?php } ?>
				<a href="<?php echo esc_url(site_url('/bookmarks')); ?>" class="btn btn-link bookmarks"><?php _e('ブックマーク', 'careers') ?></a>
				<!-- Header Login -->
				<div class="header-login">
					<?php $current_user = wp_get_current_user(); ?>

					<?php if(is_user_logged_in()){ ?>

						<a href="<?php echo home_url(); ?>" class="btn btn-link"><?php _e('ようこそ, ', 'careers') ?><?php echo esc_attr($current_user->user_login); ?></a>
						<div>
							<a href="<?php echo wp_logout_url( home_url() ); ?>"><?php _e('ログアウト', 'careers') ?></a>
						</div>
					<?php }else{ ?>

						<a href="#" class="btn btn-link"><?php _e('ログイン', 'careers'); ?></a>
						<div ng-controller="userLoginController" ng-cloak>
							<div class="alert alert-danger status" role="alert" style="display:none;">
							  	<a href="#" class="alert-link"></a>
							</div>
							<form name="userLoginForm" ng-submit="submitForm()" novalidate >
								
						        <div class="form-group"   show-errors>
						            
						            <input type="text" ng-model="user.username" name="username" class="form-control" id="username" placeholder="ユーザー名" required />
						            <p ng-if="userLoginForm.username.$error.required" class="help-block"><?php _e('ユーザー名を入力してください', 'careers') ?></p>
						        </div>
								<div class="form-group"   show-errors>
						           	
						            <input type="password" ng-model="user.password" name="password" class="form-control" id="password" placeholder="パスワードを入力する" required />
						            <p ng-if="userLoginForm.password.$error.required" class="help-block"><?php _e('正しいパスワードを入力してください', 'careers') ?></p>
						        </div>
								
								
								

								<input class="btn btn-default submit_button" id="uc-login" type="submit" value="Login" name="送信">
									
									<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

								<a class="lost btn btn-link" href="<?php echo wp_lostpassword_url(); ?>"><?php _e('パスワード忘れました', 'careers') ?></a>
							</form>	
						</div>	

					<?php } ?>
				</div> <!-- end .header-login -->