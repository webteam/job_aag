<?php 
	global $careers_option_data;

	if(isset($careers_option_data['career-select-search-page-for-job'])){
		$search_page_for_job = $careers_option_data['career-select-search-page-for-job'];
	}

	if(isset($careers_option_data['career-select-search-page-for-candidate'])){
		$search_page_for_candidate = $careers_option_data['career-select-search-page-for-candidate'];
	}


?>
<div class="header-search-bar">
	<div class="container">
		<form class="default-form banner-inline-search-form">
			<div class="basic-form clearfix">
				<a href="#" class="toggle"><span></span></a>

				<input type="hidden" id="job-search-page-id" name="job_search_page_id" value="<?php echo esc_url(site_url("/$search_page_for_job/")); ?> " >
				<input type="hidden" id="candidate-search-page-id" name="candidate_search_page_id" value="<?php echo esc_url(site_url("/$search_page_for_candidate/")); ?> " >


				<div class="hsb-input-1">

					<select name="looking_for" class="form-control">
						<option value="job"><?php _e("検索項目は...", 'careers') ?></option>
						<option value="job"><?php _e("仕事", 'careers') ?></option>
						<option value="candidate"><?php _e("候補者</option", 'careers') ?>>								
					</select>
					
				</div>

				<div class="hsb-text-1"><?php _e('で', 'careers') ?></div>

				<div class="hsb-container">
					<div class="hsb-input-2">
						<input type="text" name="location" class="form-control job typeahead" placeholder="住所">
					</div>

					<div class="hsb-input-2">
						<input type="text" name="skills" class="form-control" placeholder="スキル">
					</div>
					
					<div class="hsb-select">
						<?php
							$industries = $wpdb->get_results( $wpdb->prepare( "
							      SELECT DISTINCT tr.* FROM {$wpdb->terms} tr
							      INNER JOIN {$wpdb->term_taxonomy} tm
							        ON ( tm.term_id = tr.term_id )
							      INNER JOIN {$wpdb->term_relationships} tmr
							        ON ( tmr.term_taxonomy_id = tm.term_taxonomy_id )
							      INNER JOIN {$wpdb->posts} p
							        ON ( p.ID = tmr.object_id )
							      WHERE (tm.parent != '%d'
							        AND tm.taxonomy = '%s'
							        AND p.post_type = '%s' )
							      GROUP BY name HAVING COUNT(name) > '%d'
							  ", 0, 'industry', 'job', 0 ));
						?>
						<select name="industry" class="form-control">
							<option value=""><?php _e('業界を選択する', 'careers') ?></option>
							<?php foreach ($industries as $industry) : ?>
								<option value="<?php echo urlencode($industry->name) ?>"><?php echo esc_attr($industry->name); ?></option>	
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="hsb-submit">
					<input type="submit" class="btn btn-default btn-block" value="検索">
				</div>
			</div>

			<div class="advanced-form">
				<div class="container">

					<div class="row salary-search-reange">
						<label class="col-md-3"><span class="salary-search-reange-label"><?php _e('予想', 'careers') ?></span> <?php _e('給与', 'careers') ?></label>

						<div class="col-md-9">
							<div class="range-slider advanced-salary">
								<div class="slider" data-min="150000" data-max="300000" data-current="180000"></div>
								<div class="last-value">&lt; <span><?php _e('180000', 'careers') ?></span></div>
								<input type="hidden" name="salary" id="salary" value="1000">
							</div>
						</div>
					</div>

					<div class="row">
						<label class="col-md-3"><?php _e('公開日',' careers') ?></label>

						<div class="col-md-9">
							<div class="range-slider advanced-days">
								<div class="slider" data-min="1" data-max="60" data-current="30"></div>
								<div class="last-value">&lt; <span><?php _e('30', 'careers') ?></span></div>
								<input type="hidden" name="days" id="days" value="30">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div> <!-- end .header-search-bar -->