<?php
/**
 * Nav Main Menu
 *
 * @author 		UouApps
 * @package 	Careeers/Framework/Templates/header
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	$defaults = array(
		'theme_location'  => 'primary_navigation',
		'menu'            => '',
		'container'       => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'menu primary-nav',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'       => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="m-custom-list %2$s">%3$s</ul>',
		'depth'           => 3,
		'walker'          => new uou_walker()
	);

	if ( has_nav_menu('primary_navigation') ) {
		wp_nav_menu( $defaults );
	} else {
		wp_nav_menu( array(
			'menu'            => '',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'menu primary-nav',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'       => 'uou_walker::fallback',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="m-custom-list %2$s">%3$s</ul>',
			'depth'           => 3,
			'walker'          => new uou_walker()
		) );
	}
?>