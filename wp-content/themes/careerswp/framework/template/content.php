<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

    <?php get_template_part('framework/template/format/content', get_post_format()); ?>

    <?php endwhile; else : ?>

    <article class="blog-post">

        <h2><?php _e( '投稿は見つかりませんでした！', 'uou-pc' ); ?></h2>

    </article>
<?php endif; ?>

<?php
	if ($wp_query->max_num_pages > 1) :
		uou_pagination();
	endif;
?>