<div class="widget sidebar-widget white-container social-widget">
            <h5 class="widget-title"><?php _e('共有する', 'careers') ?></h5>

            <div class="widget-content">
              <div class="row row-p5">
                <div class="col-xs-6 col-md-3 share-box facebook">
                  <div class="count"><?php _e('0', 'careers') ?></div>
                  <a href="//www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><?php _e('Facebook', 'careers') ?></a>
                </div>

                <div class="col-xs-6 col-md-3 share-box twitter">
                  <div class="count"></div>
                  <a href="//twitter.com/share?url=<?php the_permalink(); ?>" target="_blank"><?php _e('Twitter', 'careers') ?></a>
                </div>

                <div class="col-xs-6 col-md-3 share-box google">
                  <div class="count"><?php echo gplus_count(get_the_permalink()) ?></div>
                  <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><?php _e('Google +', 'careers') ?></a>
                </div>

                <div class="col-xs-6 col-md-3 share-box linkedin">
                  <div class="count"></div>
                  <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank"><?php _e('LinkedIn', 'careers') ?></a>
                </div>
              </div>
            </div>
          </div>

          <script type="text/javascript">
            ;(function($) {

              "use strict";

              var permalink = '<?php the_permalink(); ?>';

              var getTwitterCount = function () {
                $.getJSON('http://urls.api.twitter.com/1/urls/count.json?url='+permalink+'&callback=?', function(data){
                  // console.log(data);
                  var twitterShares = data.count;
                  $('.twitter .count').text(twitterShares);
                });
              };

              getTwitterCount();

              var getFacebookCount = function () {
                $.getJSON('http://graph.facebook.com/?ids='+permalink+'&callback=?', function(data){
                  // console.log(data);
                  var facebookShares = data[permalink].shares;
                  $('.facebook .count').text(facebookShares);
                });
              };

              getFacebookCount();

              var getLinkedInCount = function () {
                $.getJSON('http://www.linkedin.com/countserv/count/share?url='+permalink+'&lang=en_US&callback=?', function(data){
                // console.log(data);
                var linkedinShares = data.count;
                  $('.linkedin .count').text(linkedinShares);
                });
              };

              getLinkedInCount();

            }(jQuery));

          </script>