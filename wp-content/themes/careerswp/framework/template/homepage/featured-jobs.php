<?php
          $featured_jobs = get_posts(array(
            'post_type' => 'job',
            'meta_query' => array(
              array(
                'key' => 'featured',
                'value' => 'true',
                'meta_compare' => '='
              )
            )
          ));
        ?>

        <?php if( !empty( $featured_jobs ) ) : ?>

        <div class="title-lines">
          <h3><?php _e('おすすめ求人', 'careers'); ?></h3>
        </div>

        <div class="latest-jobs-section white-container">
          <div class="flexslider clearfix">
            <ul class="slides">

              <?php foreach( $featured_jobs as $featured_job ) : ?>
                <li>
                  <?php
                    $job_id     = $featured_job->ID;
                    $agency_id  = get_post_meta( $job_id, 'agency_id', true );
                    $company_id = get_post_meta( $job_id, 'company_id', true );
                    
                    $locations  = get_the_terms( $job_id, 'location' );
                    $arr        = array();

                    foreach ($locations as $location) {
                      if($location->parent != 0) {
                        $arr[] = $location;
                      }
                    }

                    if($arr[0]->term_id == $arr[1]->parent) {
                      $country = $arr[0]->name;
                      $state   = $arr[1]->name;
                    } else {
                      $state   = $arr[0]->name;
                      $country = $arr[1]->name;
                    }
                    // Agency / Company thumb
                    if( empty( $company_id ) && ! empty( $agency_id ) ) {

                      $thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $agency_id ) );
                      // $thumb_url = $agency_url;
                      if(empty($thumb_src)) {
                        $thumb_src = careers_theme_profile_placeholder_image_url();
                      }

                    } else {

                      $thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $company_id ) );
                      // $thumb_url = $company_url;
                      if(empty($thumb_src)) {
                        $thumb_src = careers_theme_profile_placeholder_image_url();
                      }

                    }

                  ?>
                  <div class="image">
                    <img src="<?php echo esc_url($thumb_src); ?>" alt="">
                    <a href="<?php echo esc_url($featured_job->guid); ?>" class="btn btn-default fa fa-link"></a>
                  </div>

                  <div class="content">
                    <h6><?php echo esc_attr($featured_job->post_title); ?></h6>
                    <span class="location"><?php echo esc_attr($state); ?>, <?php echo esc_attr($country); ?></span>
                    <p><?php echo esc_attr(uou_careers_get_post_excerpt($featured_job->post_content, 20)); ?><a href="<?php echo esc_url($featured_job->guid); ?>" class="read-more"><?php _e('Read More', 'careers'); ?></a></p>
                  </div>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div> <!-- end .latest-jobs-section -->

        <?php endif; ?>