<div class="title-lines">
          <h3><?php _e('当社の価格', 'careers') ?></h3>
        </div>

        <?php $general = get_option('general'); ?>

        <?php $subscription_plans = json_decode(get_option('subscription_plans')); ?>

        <div class="pricing-tables tables-3">

          <?php foreach($subscription_plans as $plan) : ?>

            <?php
              $plan_title = $plan->title;
              $plan_member_quota = ( empty($plan->member_quota) ) ? 'NO' : $plan->member_quota;
              $plan_job_quota = $plan->job_quota;
              $plan_blog_quota = $plan->blog_quota;
              $plan_feature_job_quota = $plan->feature_job_quota;
              $plan_price = $plan->membership_price;
            ?>

            <div class="pricing-tables-column">
              <div class="white-container">
                <h5 class="title"><?php echo esc_attr($plan_title); ?></h5>

                <span class="price"><?php echo esc_attr($general['uou_currency_sign']) ?><?php echo esc_attr($plan_price); ?></span>

                <ul class="features">
                  <li><?php echo esc_attr($plan_member_quota); ?> <?php _e('メンバー数', 'careers') ?></li>
                  <li><?php echo esc_attr($plan_job_quota); ?> <?php _e('求人広告', 'careers') ?></li>
                  <li><?php echo esc_attr($plan_feature_job_quota); ?> <?php _e('おすすめ求人', 'careers') ?></li>
                  <li><?php echo esc_attr($plan_blog_quota); ?> <?php _e('ブログの投稿', 'careers') ?></li>
                </ul>

                <a href="<?php echo esc_url(site_url('/register')); ?>" class="btn btn-default"><?php _e('選ぶ', 'careers') ?></a>
              </div>
            </div>

          <?php endforeach; ?>

        </div> <!-- end .pricing-tables -->