<?php
  $jobs = get_posts(array(
    'post_type' => 'job',
    'posts_per_page'   => -1,
  ));
  if( !empty($jobs) ) :

  global $careers_option_data;
  if(isset($careers_option_data['career-select-search-page-for-job'])){
    $search_page_for_job = $careers_option_data['career-select-search-page-for-job'];
  }
  $location_map = array();
  
  foreach ($jobs as $job) :
    $job_id = $job->ID;
    $marker = get_stylesheet_directory_uri() . '/assets/img/marker.png';
    $lat = get_post_meta($job_id , '_job_latitude', true);
    $lon = get_post_meta($job_id , '_job_longitude', true);
    $title = get_the_title($job_id);
    $permalink = get_the_permalink($job_id);

    if(!empty($lat) && !empty($lon)) {
      $location_map[] = array(
        'lat'   => $lat,
        'lon'   => $lon,
        'title' => $title,
        'html'  => '<a href=' . esc_url($permalink) . '><strong>' . $title . '</strong></a>',
        'icon'  => $marker
      );
    }

  endforeach;


global $wpdb;

  $region = $wpdb->get_results( $wpdb->prepare( "
      SELECT tr.*, count(*) AS jobs_count FROM {$wpdb->terms} tr
      INNER JOIN {$wpdb->term_taxonomy} tm
        ON ( tm.term_id = tr.term_id )
      INNER JOIN {$wpdb->term_relationships} tmr
        ON ( tmr.term_taxonomy_id = tm.term_taxonomy_id )
      INNER JOIN {$wpdb->posts} p
        ON ( p.ID = tmr.object_id )
      WHERE (tm.parent = '%d'
        AND tm.taxonomy = '%s'
        AND p.post_type = '%s' )
      GROUP BY name HAVING COUNT(name) > '%d'
  ", 0, 'location', 'job', 0 ));

if(!empty($region) && count($region) > 0) {
  $region_count = ceil((count($region)) / 2);
  $regions = array_chunk($region, $region_count, false);
} else {
  $regions = array_chunk($region, 1, false);
}

$country = array();

  $sql = array();
  $test = array();
  foreach ($region as $reg) {
    $sql[] = $reg->term_id;
  }


$country = $wpdb->get_results($wpdb->prepare("
  SELECT tr.*, count(*) AS jobs_count FROM {$wpdb->terms} tr
   INNER JOIN {$wpdb->term_taxonomy} tm
     ON ( tm.term_id = tr.term_id )
   INNER JOIN {$wpdb->term_relationships} trm
     ON ( trm.term_taxonomy_id = tm.term_taxonomy_id )
   INNER JOIN {$wpdb->posts} p
     ON ( p.ID = trm.object_id )

   WHERE tm.parent IN ( ".implode(', ', array_fill(0, count($sql), '%s'))." ) 
    AND tm.taxonomy = '%s' AND p.post_type = '%s'
   GROUP BY name HAVING COUNT(name) > '%d'
   ", array_merge($sql, array('location', 'job', 0))));


if(!empty($country) && count($country) > 0) {
  $country_count = ceil((count($country)) / 2);
  $countries = array_chunk($country, $country_count, false);
} else {
  $countries = array_chunk($country, 1, false);
}


$type = $wpdb->get_results( $wpdb->prepare( "
      SELECT tr.*, count(*) AS jobs_count FROM {$wpdb->terms} tr
      INNER JOIN {$wpdb->term_taxonomy} tm
        ON ( tm.term_id = tr.term_id )
      INNER JOIN {$wpdb->term_relationships} tmr
        ON ( tmr.term_taxonomy_id = tm.term_taxonomy_id )
      INNER JOIN {$wpdb->posts} p
        ON ( p.ID = tmr.object_id )
      WHERE (tm.parent = '%d'
        AND tm.taxonomy = '%s'
        AND p.post_type = '%s' )
      GROUP BY name HAVING COUNT(name) > '%d'
  ", 0, 'job_type', 'job', 0 ));


if(!empty($type) && count($type) > 0) {
  $type_count = ceil((count($type)) / 2);
  $types = array_chunk($type, $type_count, false);
} else {
  $types = array_chunk($type, 1, false);
}

$args = array(
          'orderby' => 'name',
          'parent' => 0,
          'taxonomy'                 => 'industry',
        );
$industry = get_categories( $args );

if(!empty($industry) && count($industry) > 0) {
  $industry_count = ceil((count($industry)) / 2);
  $industries = array_chunk($industry, $industry_count, false);
} else {
  $industries = array_chunk($industry, 1, false);
}
        ?>

        <?php if( !empty( $jobs ) ) : ?>
        <div class="title-lines">
          <h3 class="mt0"><?php _e('目的別お仕事探す', 'careers'); ?></h3>
        </div>

        <div class="find-job-tabs responsive-tabs">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#find-job-tabs-map"><?php _e('地域', 'careers'); ?></a></li>
              <li><a href="#find-job-tabs-industry"><?php _e('産業', 'careers'); ?></a></li>
              <li><a href="#find-job-tabs-role"><?php _e('職種', 'careers'); ?></a></li>
              <li><a href="#find-job-tabs-country"><?php _e('国', 'careers'); ?></a></li>
            </ul>

            <div class="tab-content">
              <div class="tab-pane active" id="find-job-tabs-map">
                <div id="find-job-map-tab" class="p5"></div>

                <hr class="m0 primary">

                <div class="row p30">
                  <?php foreach($regions as $region) : ?>
                    <div class="col-sm-6">
                      <ul class="filter-list">
                        <?php foreach ($region as $r) : ?>
                          <li><a href="<?php echo esc_url(site_url("/$search_page_for_job/#?location=".urlencode($r->name))); ?>"><?php echo esc_attr($r->name); ?> <span>(<?php echo esc_attr($r->jobs_count); ?>)</span></a></li>
                        <?php endforeach; ?>
                      </ul>
                    </div>

                  <?php endforeach; ?>
                </div>
              </div>

              <div class="tab-pane" id="find-job-tabs-industry">
                <div class="row p30">
                  <?php foreach ($industries as $industry) : ?>
                    <div class="col-sm-6">
                      <?php foreach ($industry as $i) : ?>
                      <h6 class="mt0"><?php echo esc_attr($i->name); ?></h6>

                      <?php

                        $args = array(
                          'show_option_all'    => '',
                          'orderby'            => 'name',
                          'order'              => 'ASC',
                          'style'              => 'post',
                          'child_of'           => $i->term_id,
                          'taxonomy'           => 'industry',
                        );

                        $in_child = get_categories($args);

                      ?>



                      <ul class="filter-list">
                      <?php foreach ($in_child as $child) : ?>
                        <li><a href="<?php echo esc_url(site_url("/$search_page_for_job/#?industry=".urlencode($child->name))); ?>"><?php echo esc_attr($child->name); ?> <span>(<?php echo esc_attr($child->count); ?>)</span></a></li>
                        <?php endforeach; ?>
                      </ul>

                      <?php endforeach; ?>

                    </div>

                  <?php endforeach ?>
                </div>
              </div>

              <div class="tab-pane" id="find-job-tabs-role">
                <div class="p30">
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, harum, optio, repudiandae voluptatum illum et ipsam quisquam at dolore illo eaque odio inventore quos esse reiciendis laudantium nobis aperiam iure!</p> -->
                  <div class="row">
                    <?php foreach ($types as $type) : ?>
                      <div class="col-sm-6">
                        <ul class="filter-list">
                        <?php foreach ($type as $t) : ?>
                          <li><a href="<?php echo esc_url(site_url("/$search_page_for_job/#?job_type=".urlencode($t->name))); ?>"><?php echo esc_attr($t->name); ?> <span>(<?php echo esc_attr($t->jobs_count); ?>)</span></a></li>
                        <?php endforeach; ?>
                        </ul>
                      </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

              <div class="tab-pane" id="find-job-tabs-country">
                <div class="row p30">
                  <?php foreach($countries as $country) : ?>
                    <div class="col-sm-6">
                      <ul class="country-list">
                        <?php foreach ($country as $c): ?>
                          <li><a href="<?php echo esc_url(site_url("/$search_page_for_job/#?location=".urlencode($c->name))); ?>"><img src="<?php print IMAGES; ?>flag-icons/<?php echo esc_attr($c->name); ?>.png" alt=""> <?php echo esc_attr($c->name); ?> <span>(<?php echo esc_attr($c->jobs_count); ?>)</span></a></li>
                        <?php endforeach ?>

                      </ul>
                    </div>
                  <?php endforeach; ?>

                </div>
                        </div>
            </div>
          </div> <!-- end .find-job-tabs -->


        <?php endif; ?>

        <script>

          new Maplace({
            map_div: '#find-job-map-tab',
            controls_type: 'list',
            map_options: {
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              scrollwheel: false,
              zoom: 14
            },
            locations: <?php echo json_encode($location_map); ?>
          }).Load();
        </script>

<?php endif; ?>