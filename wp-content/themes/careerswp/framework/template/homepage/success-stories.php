<?php
    $success_stories = get_posts(array(
    'post_type' => 'success',
    'posts_per_page'   => -1,
    ));
    ?>

    <?php if( ! empty( $success_stories ) ) : ?>

      <div class="success-stories-section">
        <div class="container">
          <h5 class="mt10"><?php _e('成功事例', 'careers'); ?></h5>

          <div>
            <div class="flexslider">
              <ul class="slides">
                <?php foreach($success_stories as $success_story) : ?>
                  <?php if( has_post_thumbnail( $success_story->ID ) ) : ?>
                    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($success_story->ID), 'success-thumb' );
                    $url = $thumb['0']; ?>
                    <li>
                      <a href="#">
                        <img class="thumb" src="<?php echo esc_url($url); ?>" alt="">
                      </a>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>