<?php
        $partners = get_posts(array(
          'post_type' => 'partner',
          'posts_per_page'   => -1,
        ));
        ?>

        <?php if( ! empty( $partners ) ) : ?>

          <div class="title-lines">
            <h3><?php _e('ビジネスパートナー', 'careers'); ?></h3>
          </div>

          <div class="our-partners-section white-container">
            <ul class="clearfix">
              <?php foreach($partners as $partner) : ?>
                <?php if( has_post_thumbnail( $partner->ID ) ) : ?>
                  <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($partner->ID), 'partner-thumb' );
                  $url = $thumb['0']; ?>
                  <li>
                    <div class="css-table">
                      <div class="css-table-cell">
                        <a href="#"><img src="<?php echo esc_url($url); ?>" alt=""></a>
                      </div>
                    </div>
                  </li>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          </div> <!-- end .our-partners-section -->

          <?php endif; ?>