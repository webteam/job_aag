<?php
          $latest_jobs = get_posts(array(
            'post_type' => 'job',
          ));
        ?>

        <?php if( !empty( $latest_jobs ) ) : ?>

        <div class="title-lines">
          <h3><?php _e('最新の求人', 'careers'); ?></h3>
        </div>

        <div class="latest-jobs-section white-container">
          <div class="flexslider clearfix">
            <ul class="_slides">

              <?php foreach( $latest_jobs as $latest_job ) : ?>
                <li>
                  <?php
                    $job_id     = $latest_job->ID;
                    $agency_id  = get_post_meta( $job_id, 'agency_id', true );
                    $company_id = get_post_meta( $job_id, 'company_id', true );
                    
                    $locations  = get_the_terms( $job_id, 'location' );
                    $arr        = array();

                    foreach ($locations as $location) {
                      if($location->parent != 0) {
                        $arr[] = $location;
                      }
                    }

                    if($arr[0]->term_id == $arr[1]->parent) {
                      $country = $arr[0]->name;
                      $state   = $arr[1]->name;
                    } else {
                      $state   = $arr[0]->name;
                      $country = $arr[1]->name;
                    }
                    // Agency / Company thumb
                    if( empty( $company_id ) && ! empty( $agency_id ) ) {

                      $thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $agency_id ) );
                      // $thumb_url = $agency_url;
                      if(empty($thumb_src)) {
                        $thumb_src = careers_theme_profile_placeholder_image_url();
                      }

                    } else {

                      $thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $company_id ) );
                      // $thumb_url = $company_url;
                      if(empty($thumb_src)) {
                        $thumb_src = careers_theme_profile_placeholder_image_url();
                      }

                    }

                  ?>
                  <!--<div class="image">
                    <img src="<?php echo esc_url($thumb_src); ?>" alt="">
                    <a href="<?php echo esc_url($latest_job->guid); ?>" class="btn btn-default fa fa-link"></a>
                  </div>-->

                  <div class="content">
                    <h6><?php echo esc_attr($latest_job->post_title); ?></h6>
                    <span class="location"><?php echo esc_attr($state); ?>, <?php echo esc_attr($country); ?></span>
                    <p><?php echo esc_attr(uou_careers_get_post_excerpt($latest_job->post_content, 5)); ?><a href="<?php echo esc_url($latest_job->guid); ?>" class="read-more"><?php _e('もっと読む', 'careers'); ?></a></p>
                  </div>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div> <!-- end .latest-jobs-section -->

        <?php endif; ?>