<?php

/**
 *  Careers admin panel settings .   
 * @since       Version 1.0.0
 */



if (!class_exists('uou_admin_config')) {

    class uou_admin_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {


            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }



        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => __('Section via hook', 'careers'),
                'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redq'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }


        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../options/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../options/patterns/';
            $sample_patterns        = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode('.', $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'redq'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                <?php endif; ?>

                <h4><?php echo esc_attr($this->theme->display('Name')); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(__('By %s', 'redq'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(__('Version %s', 'redq'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . __('Tags', 'redq') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo esc_attr($this->theme->display('Description')); ?></p>
            <?php
            if ($this->theme->parent()) {
                printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'redq'), $this->theme->parent()->display('Name'));
            }
            ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $sampleHTML = '';
            if (file_exists(dirname(__FILE__) . '/info-html.html')) {
                /** @global WP_Filesystem_Direct $wp_filesystem  */
                global $wp_filesystem;
                if (empty($wp_filesystem)) {
                    require_once(ABSPATH . '/wp-admin/includes/file.php');
                    WP_Filesystem();
                }
                $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
            }








            $this->sections[] = array(
                'title'     => __('Home Settings', 'careers'),
           //     'desc'      => __('Redux Framework was created with the developer in mind. It allows for any theme developer to have an advanced theme panel with most of the features a developer would need. For more information check out the Github repo at: <a href="https://github.com/ReduxFramework/Redux-Framework">https://github.com/ReduxFramework/Redux-Framework</a>', 'redq'),
                'icon'      => 'el-icon-home',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(







                    array(
                        'id'        => 'uou-favicon',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Custom favicon', 'careers'),
                        'compiler'  => 'true',
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('Upload custom favicon.', 'careers'),
                        'default'   => '',

                    ),


                    array(
                        'id'        => 'uou-favicon-iphone',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Custom favicon for iphone', 'careers'),
                        'compiler'  => 'true',
                         'default'   => '',
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('Upload custom favicon for iphone.', 'careers'),

                    ),


                    array(
                        'id'        => 'uou-favicon-ipad',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Custom favicon for ipad', 'careers'),
                        'compiler'  => 'true',
                         'default'   => '',
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('Upload custom favicon for ipad', 'careers'),

                    ),


                    array(
                        'id'        => 'uou-custom-css',
                        'type'      => 'ace_editor',
                        'title'     => __('Custom CSS ', 'careers'),
                        'subtitle'  => __('Paste your custom CSS code here.', 'careers'),
                        'mode'      => 'css',
                        'theme'     => 'monokai',
                         'default'   => '',
                         'output' => true,

                    ),


                    array(
                        'id'        => 'uou-custom-js',
                        'type'      => 'ace_editor',
                        'title'     => __('Tracking code', 'careers'),
                        'subtitle'  => __('Paste your JS code here.', 'careers'),
                        'mode'      => 'javascript',
                        'theme'     => 'chrome',
                         'default'   => '',

                    ),





                ),
            );





            $this->sections[] = array(
                'icon'      => 'el-icon-paper-clip',
                'title'     => __('Header Settings', 'careers'),
                'fields'    => array(

                    array(
                        'id'        => 'uou-site-logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Site Logo', 'careers'),
                        'compiler'  => 'true',
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'subtitle'      => __('Upload your site logo', 'careers'),
                        'desc'      => __('The image size will be <span style="color:red;">190x57</span>', 'careers'),

                    ),

                )
            );





            $this->sections[] = array(
                'icon' => 'el-icon-braille',
                'title' => __('Top Nav Options', 'careers'),
                'fields' => array(


                    array(
                        'id'        => 'uou-top-language',
                        'type'      => 'switch',
                        'desc'     => __('This select type "ON" will only work if WPML activated in your theme', 'careers'),

                        'title'     => __('Show Language ', 'careers'),


                        'default'   => true,
                    ),

                    array(
                        'id'       => 'uou-wpml-select',
                        'type'     => 'select',
                        'title'    => __('WPML language show type', 'careers'),
                        'subtitle' => __('Select the type how you want to show language selector', 'careers'),
                        'desc'     => __('This select type will only work if WPML activated in your theme', 'careers'),
                        // Must provide key => value pairs for select options
                        'options'  => array(
                            'code' => 'Language Code',
                            'name' => 'Language Name',
                            'flag' => 'Flag'
                        ),
                        'default'  => 'name',
                    ),


                )
            );


            /*
            |--------------------------------------------------------------------------
            | Start search page Options settings
            |--------------------------------------------------------------------------
            |
            | 
            |
            */    
                    $get_all_pages = get_pages(array(
                        'meta_key' => '_wp_page_template',
                        'meta_value' => 'page-job-search.php'
                    ));
                  

                    $all_pages = array();

                    //print_r($get_all_pages);

                    if(isset($get_all_pages) && !empty($get_all_pages)){

                        foreach ($get_all_pages as $key => $value) {
                          $all_pages[$value->post_name] = $value->post_title;    
                        }

                    }


                    $get_candidate_pages = get_pages(array(
                        'meta_key' => '_wp_page_template',
                        'meta_value' => 'page-fullwidth.php'
                    ));
                  

                    $candidate_pages = array();

                    //print_r($get_candidate_pages);

                    if(isset($get_candidate_pages) && !empty($get_candidate_pages)){

                        foreach ($get_candidate_pages as $key => $value) {
                          $candidate_pages[$value->post_name] = $value->post_title;    
                        }

                    }


                    $this->sections[] = array(
                        'icon'      => 'el-icon-search-alt',
                        'title'     => __('Search Page Options', 'uoulib'),
                        'fields'    => array(
                            array(
                                'id'        => 'career-select-search-page-for-job',
                                'type'      => 'select',
                                'title'     => __('Search Page For Job', 'uoulib'),
                                'subtitle'  => __('When you select your search page the advance property search form will be appear in top header and home page. and the search result will show in this page', 'uoulib'),
                                'options'   => $all_pages,
                                
                            ),
                            array(
                                'id'        => 'career-select-search-page-for-candidate',
                                'type'      => 'select',
                                'title'     => __('Search Page For Candidate', 'uoulib'),
                                'subtitle'  => __('When you select your search page the advance property search form will be appear in top header and home page. and the search result will show in this page', 'uoulib'),
                                'options'   => $candidate_pages,
                                
                            )

                        )
                    );

            /*
            |--------------------------------------------------------------------------
            | End Footer Options settings
            |--------------------------------------------------------------------------
            |
            | 
            |
            */





            $this->sections[] = array(
                'icon'  => 'el-icon-link',
                'title' => __('Social Options', 'careers'),
                'fields' => array(
                    array(
                        'id'        => 'uou-facebook-link',
                        'type'      => 'text',                    
                        'title'     => __('Facebook', 'careers'),
                         'default'   => '',



                    ),
                    array(
                        'id'        => 'uou-twitter-link',
                        'type'      => 'text',                    
                        'title'     => __('Twitter', 'careers'),
                         'default'   => '',

                    ),
                    array(
                        'id'        => 'uou-google-link',
                        'type'      => 'text',                    
                        'title'     => __('Google', 'careers'),
                         'default'   => '',

                    ),
                    array(
                        'id'        => 'uou-linkedin-link',
                        'type'      => 'text',                    
                        'title'     => __('Linkedin', 'careers'),
                         'default'   => '',

                    ),
                    array(
                        'id'        => 'uou-pinterest-link',
                        'type'      => 'text',                    
                        'title'     => __('Pinterest', 'careers'),
                         'default'   => '',

                    ),
                    array(
                        'id'        => 'uou-dribbble-link',
                        'type'      => 'text',                    
                        'title'     => __('Dribbble', 'careers'),
                         'default'   => '',

                    ),

                )
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-photo',
                'title'     => __('Footer Options', 'careers'),
                'fields'    => array(

                    array(
                        'id'        => 'uou-footer-text',
                        'type'      => 'textarea',
                        'title'     => __('Site Footer text ', 'careers'),
                        'default'   => ''
                    )

                )
            );
            $this->sections[] = array(
                'icon'      => 'el-icon-fontsize',
                'title'     => __('Typeography', 'careers'),
                'fields'    => array(
                    array(
                        'id'            => 'uou-body-typography',
                        'type'          => 'typography',
                        'title'         => __('Body', 'careers'),
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => true,    // Select a backup non-google font in addition to a google font
                        'font-style'    => false, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => false, // Only appears if google is true and subsets not set to false
                        'font-size'     => true,
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        => array('body'), // An array of CSS selectors to apply this font style to dynamically
                        'units'         => 'px', // Defaults to px
                        'default'   => '',

                    ),



                    array(
                        'id'            => 'uou-header-typography',
                        'type'          => 'typography',
                        'title'         => __('Header', 'careers'),
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => true,    // Select a backup non-google font in addition to a google font
                        'font-style'    => false, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => false, // Only appears if google is true and subsets not set to false
                        'font-size'     => false,
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        => array('h1','h2','h3','h4','h5'), // An array of CSS selectors to apply this font style to dynamically

                        'units'         => 'px', // Defaults to px
                        'default'   => '',
                    ),
                )
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-website',
                'title'     => __('Styling Options', 'careers'),
                'fields'    => array(
                    array(
                        'id'        => 'uou-select-layout',
                        'type'      => 'select',
                        'title'     => __('Theme Layout', 'careers'),
                        'subtitle'  => __('Select your themes alternative layout.', 'careers'),
                        'options'   => array(
                            'boxed-layout' => 'Boxed Layout',
                        ),
                        'default'   => '',
                    ),
                    array(
                        'id'        => 'uou-select-stylesheet',
                        'type'      => 'select',
                        'title'     => __('Theme Stylesheet', 'careers'),
                        'subtitle'  => __('Select your themes alternative color scheme.', 'careers'),
                        'options'   => array(
                            'green.css' => 'Greeen',
                            'orange.css' => 'Orange',
                            'violet.css' => 'violet'
                        ),
                        'default'   => '',
                    ),
                    array(
                        'id'        => 'uou-body-color',
                        'type'      => 'color',
                        'output'    => array('body'),
                        'title'     => __('Body Color', 'careers'),
                        'default'  => '#303c42'
                    ),
                    array(
                        'id'        => 'uou-background',
                        'type'      => 'background',
                        'output'    => array('body'),
                        'mode'      => 'background',
                        'title'     => __('Body Background', 'careers'),
                        'subtitle'  => __('Body background with image, color, etc.', 'careers'),
                        'default'  => array(
                            'background-color' => '#ffffff'
                        )
                    ),

                    // array(
                    //     'id'            => 'opt-color-scheme',
                    //     'type'          => 'color_scheme',
                    //     'title'         => 'Color Schemes',
                    //     'subtitle'      => 'Save and load color schemes',
                    //     'output'        => true,
                    //     'compiler'      => true,
                    //     'simple'        => false,
                    //     'default'       => array(
                    //         array(
                    //             'id'        => 'body-text', // ID
                    //             'title'     => 'body text', // Display text
                    //             'color'     => '#fdfdfd',   // Default colour
                    //             'alpha'     => 1,           // Default alpha
                    //             'selector'  => 'body',      // CSS selector
                    //             'mode'      => 'color',     // CSS mode
                    //             'important' => true         // CSS important
                    //         ),
                    //         array(
                    //             'id'        => 'body-background',
                    //             'title'     => 'body background',
                    //             'color'     => '#ededed', 
                    //             'alpha'     => .5,
                    //             'selector'  => 'body',
                    //             'mode'      => 'background-color',
                    //             'important' => false
                    //         ),
                    //     )
                    // ),



                    

                )
            );




            $theme_info  = '<div class="redux-framework-section-desc">';
            $theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . __('<strong>Theme URL:</strong> ', 'redq') . '<a href="' . $this->theme->get('ThemeURI') . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-author">' . __('<strong>Author:</strong> ', 'redq') . $this->theme->get('Author') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-version">' . __('<strong>Version:</strong> ', 'redq') . $this->theme->get('Version') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
            $tabs = $this->theme->get('Tags');
            if (!empty($tabs)) {
                $theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . __('<strong>Tags:</strong> ', 'redq') . implode(', ', $tabs) . '</p>';
            }
            $theme_info .= '</div>';

            if (file_exists(dirname(__FILE__) . '/../README.md')) {
                $this->sections['theme_docs'] = array(
                    'icon'      => 'el-icon-list-alt',
                    'title'     => __('Documentation', 'careers'),
                    'fields'    => array(
                        array(
                            'id'        => '17',
                            'type'      => 'raw',
                            'markdown'  => true,
                            'content'   => file_get_contents(dirname(__FILE__) . '/../README.md')
                        ),
                    ),
                );
            }
            

            $this->sections[] = array(
                'title'     => __('Import / Export', 'careers'),
                'desc'      => __('Import and Export your Redux Framework settings from file, text or URL.', 'redq'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
                    ),
                ),
            );                     
                    



            if (file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
                $tabs['docs'] = array(
                    'icon'      => 'el-icon-book',
                    'title'     => __('Documentation', 'careers'),
                    'content'   => nl2br(file_get_contents(trailingslashit(dirname(__FILE__)) . 'README.html'))
                );
            }
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => __('Theme Information 1', 'careers'),
                'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'careers')
            );

            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-2',
                'title'     => __('Theme Information 2', 'careers'),
                'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'careers')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'careers');
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'careers_option_data',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => false,                    // Show the sections below the admin menu item or not
                'menu_title'        => __('Careers WP', 'careers'),
                'page_title'        => __('Careers WP', 'careers'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => 'AIzaSyBj4Lx93qjbVn9_p3Kqx178aDp4G6YrFeg', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                
                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => 'careers_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE


            );


            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            $this->args['share_icons'][] = array(
                'url'   => 'https://github.com/faysalhaque',
                'title' => 'Visit my GitHub profile',
                'icon'  => 'el-icon-github'
                //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
            );
            $this->args['share_icons'][] = array(
                'url'   => 'https://www.facebook.com/faysal.haque',
                'title' => 'Follow me on Facebook',
                'icon'  => 'el-icon-facebook'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'https://twitter.com/faysal_haque',
                'title' => 'Follow me on Twitter',
                'icon'  => 'el-icon-twitter'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'https://www.linkedin.com/pub/faysal-haque/4a/8/61b',
                'title' => 'Find me on LinkedIn',
                'icon'  => 'el-icon-linkedin'
            );

            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }

            } else {

            }


        }

    }
    
    global $reduxConfig;
    $reduxConfig = new uou_admin_config();
}


if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;


if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';


        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;
