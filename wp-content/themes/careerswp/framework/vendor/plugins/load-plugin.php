<?php



add_action( 'tgmpa_register', 'uou_theme_register_required_plugins' );



function uou_theme_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(





        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),

        // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'Careers - Complete job management app on top of WordPress', // The plugin name.
            'slug'               => 'uou-careers', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/framework/ext/plugins/uou-careers.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
        // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'Careers Theme helper - Plugin extension for Careers theme', // The plugin name.
            'slug'               => 'careers-theme-helper', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/framework/ext/plugins/careers-theme-helper.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),

        // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'Zilla Shortcodes Modified', // The plugin name.
            'slug'               => 'zilla-shortcodes-modified', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/framework/ext/plugins/zilla-shortcodes-modified.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),

    );


    $config = array(
        'id'           => 'uou',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'uou-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'careers' ),
            'menu_title'                      => __( 'Install Plugins', 'careers' ),
            'installing'                      => __( 'Installing Plugin: %s', 'careers' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'careers' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'careers' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'careers' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'careers' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'careers' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'careers' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'careers' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'careers' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'careers' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'careers' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'careers' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'careers' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'careers' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'careers' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}