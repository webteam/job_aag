<?php global $careers_option_data; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset') ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- favicon  -->
	<?php if($careers_option_data['uou-favicon']): ?>
		<link rel="shortcut icon" href="<?php echo esc_url($careers_option_data['uou-favicon']['url']); ?>" type="image/x-icon" />
	<?php endif; ?>

	<!-- For iPhone -->
	<?php if($careers_option_data['uou-favicon-iphone']): ?>
		<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url($careers_option_data['uou-favicon-iphone']['url']); ?>">
	<?php endif; ?>


	<?php if($careers_option_data['uou-favicon-ipad']): ?>
	<!-- For iPad -->
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo esc_url($careers_option_data['uou-favicon-ipad']['url']); ?>">
	<?php endif; ?>

	<!-- end of favicon  -->

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<!--[if lt IE 9]>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/html5shiv.js"></script>
	<![endif]-->
	<!--[if IE 9]>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/js/media.match.min.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<?php $body_class = $careers_option_data['uou-select-layout']; ?>
<body <?php body_class("$body_class"); ?> ng-app="userModule">
<div id="main-wrapper">

	<header id="header" class="header-style-1">
		<div class="header-top-bar">
			<div class="container">

				<?php get_template_part( 'framework/template/header/wpml-lang', '' ); ?>

				<?php get_template_part( 'framework/template/header/header-link', '' ); ?>

			</div> <!-- end .container -->
		</div> <!-- end .header-top-bar -->

		<div class="header-nav-bar">
			<div class="container">

				<!-- Logo -->
				<div class="css-table logo">
					<div class="css-table-cell">
						<a href="<?php echo esc_url( home_url() ); ?>">
							<?php if( !empty($careers_option_data['uou-site-logo']['url'])) : ?>
								<img src="<?php echo esc_url($careers_option_data['uou-site-logo']['url']); ?>" alt="<?php bloginfo('name') ?> | <?php bloginfo('description'); ?>">
							<?php else : ?>
								<h5><?php bloginfo('name') ?> | <small><?php bloginfo('description'); ?></small></h5>
							<?php endif; ?>
						</a> <!-- end .logo -->
					</div>
				</div>

				<!-- Mobile Menu Toggle -->
				<a href="#" id="mobile-menu-toggle"><span></span></a>

				<!-- Primary Nav -->
				<nav>

					<?php get_template_part( 'framework/template/header/main-menu', '' ); ?>

				</nav>
			</div> <!-- end .container -->

			<div id="mobile-menu-container" class="container">
				<div class="login-register"></div>
				<div class="menu"></div>
			</div>
		</div> <!-- end .header-nav-bar -->


		


		<?php get_template_part( 'framework/template/header/search-bar', '' ); ?>

			<?php
				$job_posts = get_posts(array(
					'posts_per_page' => -1,
					'post_type' => 'job'
				));

				$count_string = count($job_posts);
				$count_length = strlen((string)$count_string);
				$new_string = sprintf( "%08d", $count_string);
				$arr = str_split($new_string);
				$output_string = '';

				for ($i=0; $i < 8 - $count_length; $i++) { 
					$output_string .= '<li class="zero">0</li>';
				}

				for ($j=8 - $count_length; $j < 8 ; $j++) { 
					$output_string .= '<li>'.$arr[$j].'</li>';
				}

			?>
			<div class="header-banner">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="header-banner-box register">
								<div class="counter-container">
									<ul class="counter clearfix">
										<?php echo apply_filters('the_excerpt', $output_string); ?>
									</ul>

									<div><span><?php _e('ジョブ', 'careers'); ?></span></div>
								</div>

								<a href="<?php echo esc_url(site_url('/register')); ?>" class="btn btn-default"><?php _e('今すぐ登録', 'careers'); ?></a>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="header-banner-box post-job">
								<img src="<?php print IMAGES; ?>verified.png" alt="">

								<a href="<?php echo esc_url(site_url('/login')); ?>" class="btn btn-red"><?php _e('求人を投稿', 'careers'); ?></a>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- end .header-banner -->

	</header> <!-- end #header -->
