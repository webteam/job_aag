<aside>
	<div class="white-container mb0">
		<div class="widget sidebar-widget jobs-search-widget">
			<h5 class="widget-title"><?php _e('地域で検索', 'careers') ?></h5>
			<div class="widget-content">
				<input type="text" class="form-control mt10" ng-model="search.state_country" placeholder="検索条件"></br>
				<input type="submit" class="btn btn-default" value="検索">
			</div>


			<h5><?php _e('掲載日', 'careers') ?></h5>
			<div class="range-slider clearfix" id="postday">
				<div class="slider" data-min="1" data-max="60"></div>
				<div class="first-value" ><span><?php _e('1', 'careers') ?></span> <?php _e('days', 'careers') ?></div>
				<input class="min" type="hidden" value="" ng-click="setFilterItem('postday',this, 'slider' )" />
				<div class="last-value"><span><?php _e('60', 'careers') ?></span> <?php _e('days', 'careers') ?></div>
				<input class="max" type="hidden" value="" ng-click="setFilterItem('postday', this, 'slider' )" />
			</div>

		</div>
	</div>
</aside>