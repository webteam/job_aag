<?php
/*
 * Template Name: About Us Page
 * Description: An About Us Page Template for careers theme
 */
get_header(); ?>

	<div id="page-content">
		<div class="container">
			<div class="row">
				<?php if( have_posts() ) : ?><?php the_post(); ?>

					<div class="col-sm-8 page-content">
						<div class="white-container mb0">

							<?php the_content(); ?>

							<?php
								$team_posts = get_posts(array(
			  					'post_type' => 'team',
			  					'posts_per_page'   => -1,
			  				));
			  			?>
				  		
				  		<?php if( ! empty( $team_posts ) ) : ?>
							
								<h3 class="bottom-line">チームメンバー</h3>

								<div class="row">
									
									<?php foreach($team_posts as $team) : ?>
										
										<div class="col-sm-6 col-md-4">
											<div class="our-team-item">
												<div class="img">
													<?php if( has_post_thumbnail( $team->ID ) ) : ?>

														<?php echo apply_filters('the_excerpt', get_the_post_thumbnail( $team->ID ));  ?>
													
													<?php else : ?>
														<?php echo careers_theme_profile_placeholder_image(); ?>
													<?php endif; ?>
												</div>

												<h6><?php echo esc_attr(get_the_title($team->ID)); ?> <span><?php echo esc_attr(get_post_meta( $team->ID, '_meta_box_team_designation', true )); ?></span></h6>
											</div>
										</div>

									<?php endforeach; ?>

								</div>

							<?php endif; ?>
						</div>
					</div>

					<div class="col-sm-4 page-sidebar">
						<aside>
							<?php
								global $post;
								
								$pages = get_pages(array("child_of"=>$post->ID));
								if ($pages) {
									array_unshift($pages, $post);
								}
							?>
							<div class="widget sidebar-widget white-container links-widget">
								<ul>
									<?php foreach ($pages as $page) : ?>
										<li class="<?php echo ($post->ID === $page->ID) ? 'active' : '' ?>"><a href="<?php echo esc_url(get_the_permalink($page->ID)); ?>"><?php echo esc_attr($page->post_title); ?></a></li>
									<?php endforeach ?>
								</ul>
							</div>
						</aside>
					</div>

				<?php endif; ?>
			</div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->
<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>