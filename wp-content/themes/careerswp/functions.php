<?php

require_once('framework/load.php');

add_action( "wp_ajax_nopriv_uc_send_mail", 'uc_send_mail' );
add_action( "wp_ajax_uc_send_mail", 'uc_send_mail' );

function uc_send_mail() {

	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// check ajax refer
		check_ajax_referer( 'nonce-contact-form', 'security' );
		$to      = $_POST["to"];
		$from    = $_POST["from"];
		$subject = get_bloginfo('name') . ': ' . $_POST["subject"];
		$message = $_POST["msg"];

		$headers[] = 'From: '.$from;
		
		// Start output buffering to grab smtp debugging output
		ob_start();

		// Send the test mail
		$result = wp_mail( $to, $subject, $message, $headers );

		$smtp_debug = ob_get_clean();

		echo json_encode('Email sent');

		wp_die();   
	}
}