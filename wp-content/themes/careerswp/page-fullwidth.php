<?php
/*
 * Template Name: Full Width Page without title
 * Description: A Full Width Page Template for careers theme
 */
get_header(); ?>
		<div id="page-content">
            <div class="container">
            	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
            		
            		<?php the_content(); ?>            	
            	<?php endwhile; endif ?>
            </div> <!-- end .container -->
        </div> <!-- end #page-content -->

<?php get_footer(); ?>