<aside>
	<div class="white-container mb0">
		<div class="widget sidebar-widget jobs-search-widget">
			<h5 class="widget-title"><?php _e('検索', 'careers') ?></h5>

			<div class="widget-content">

				<span><?php _e("キーワード検索", 'careers') ?></span>

				<input type="text" class="form-control mt15 mb15" ng-model="search.title" placeholder="職種">

				<input type="text" class="form-control mt10" ng-model="search.state_country" placeholder="住所">
			</div>
		</div>

		<div class="widget sidebar-widget jobs-filter-widget">

			<div class="widget-content">

				<h6><?php _e('職種別', 'careers') ?></h6>

				<div>
					<ul class="filter-list">
						<?php 
						    $args = array(
								'show_option_all'    => '',
								'orderby'            => 'name',
								'order'              => 'ASC',
								'style'              => 'list',
								'show_count'         => 1,
								'hide_empty'         => 1,
								'use_desc_for_title' => 1,
								'child_of'           => 0,
								'feed'               => '',
								'feed_type'          => '',
								'feed_image'         => '',
								'exclude'            => '',
								'exclude_tree'       => '',
								'include'            => '',
								'hierarchical'       => 1,
								'title_li'           => __( 'Job Types', 'careers' ),
								'show_option_none'   => '',
								'number'             => null,
								'echo'               => 1,
								'depth'              => 0,
								'current_category'   => 0,
								'pad_counts'         => 0,
								'post_type'          => 'job',
								'taxonomy'           => 'job_type',
								'walker'             => new Uou_Walker_Category(),
						    );

						    uou_list_categories( $args );
						?>
					</ul>

					<a href="#" class="toggle"></a>
				</div>
				<br/>

				<h6><?php _e('掲載日', 'careers') ?></h6>
				<br/>

				<div class="range-slider clearfix" id="postday">
					<div class="slider" data-min="1" data-max="60"></div>
					<div class="first-value" ><span><?php _e('1', 'careers') ?></span> <?php _e('days', 'careers') ?></div>
					<input class="min" type="hidden" value="" ng-click="setFilterItem('postday',this, 'slider' )" />
					<div class="last-value"><span><?php _e('60', 'careers') ?></span> <?php _e('days', 'careers') ?></div>
					<input class="max" type="hidden" value="" ng-click="setFilterItem('postday', this, 'slider' )" />
				</div>

				<h6><?php _e('給与', 'careers') ?></h6>
				<br/>

				<div class="range-slider clearfix" id="salary">
					<div class="slider" data-min="150000" data-max="3000000"></div>
					<div class="first-value" >￥ <span>150000</span></div>
					<input class="min" type="hidden" value="" ng-click="setFilterItem('salary',this, 'slider' )" />
					<div class="last-value">￥ <span><?php _e('1500000', 'careers') ?></span></div>
					<input class="max" type="hidden" value="" ng-click="setFilterItem('salary', this, 'slider' )" />
				
				</div>

			</div>
		</div>
	</div>

</aside>