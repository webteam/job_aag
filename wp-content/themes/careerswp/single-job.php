<?php get_header(); ?>

<?php $general = get_option('general'); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); $post_id = get_the_ID(); ?>

<?php
	
	// job terms
	$locations           = get_the_terms( $post_id, 'location' );
	$industries          = get_the_terms( $post_id, 'industry' );
	$roles               = get_the_terms( $post_id, 'role' );
	$careers_level       = get_the_terms( $post_id, 'career_level' );
	$job_types           = get_the_terms( $post_id, 'job_type' );
	$years_of_experience = get_the_terms( $post_id, 'years_of_experience' );
	$degrees             = get_the_terms( $post_id, 'degree' );
	$genders             = get_the_terms( $post_id, 'gender' );
	$nationalities       = get_the_terms( $post_id, 'nationality' );

	$address             = get_post_meta( $post_id, 'address_line', true);
	$zip             = get_post_meta( $post_id, 'zip_code', true);
	$isBookMarked = isBookmarked($post_id, 'bookmarked_job');
	$arr = array();

	if(isset($locations) && is_array($locations)){
		foreach ($locations as $location) {
	      if($location->parent != 0) {
	        $arr[] = $location;
	      }
	    }
	    if( !empty( $arr[0] ) && !empty($arr[1]) ) {
		    if($arr[0]->term_id == $arr[1]->parent) {
		      $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
		    } else {
		      $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
		    }
	    } else {
          $state_country = $arr[0]->name;
        }
	}

    if( !empty( $address ) && !empty( $state_country ) && !empty( $zip ) ) {
		$j_location          = $address . ', ' . $state_country . ', ' . $zip;
	}
	
	$company_id      = get_post_meta( $post_id, 'company_id', true );
	
	$recruiter_id    = ( !empty( $company_id ) ) ? $company_id : 0;
	$recruiter_key   = ( !empty( $company_id ) ) ? 'company' : '';
	
	$monthly_salary  = get_post_meta( $post_id, 'monthly_salary', true );
	
	$joining_date    = date( 'M j, Y', strtotime( get_post_meta( $post_id, 'joining', true ) . "+1 days" ) );
	
	$recruiter_post  = get_post( $recruiter_id );
	
	$facebook        = get_post_meta($recruiter_id , '_'.$recruiter_key.'_facebook' , true );
	$twitter         = get_post_meta($recruiter_id , '_'.$recruiter_key.'_twitter' , true );
	$google          = get_post_meta($recruiter_id , '_'.$recruiter_key.'_google' , true );
	$linkedin        = get_post_meta($recruiter_id , '_'.$recruiter_key.'_linkedin' , true );
	$youtube         = get_post_meta($recruiter_id , '_'.$recruiter_key.'_youtube' , true );
	$instagram       = get_post_meta($recruiter_id , '_'.$recruiter_key.'_instagram' , true );
	$recruiter_email = get_post_meta($recruiter_id , $recruiter_key.'_email' , true );
	
	$location_map    = array();
	$marker          = get_stylesheet_directory_uri() . '/assets/img/marker.png';

	$lat = get_post_meta($post_id , '_job_latitude', true);
	$lon = get_post_meta($post_id , '_job_longitude', true);

	if( !empty($lat) && !empty($lon)) {
		$location_map[] = array(
			'lat'   => $lat,
			'lon'   => $lon,
			'title' => get_the_title($post_id),
			'html'  => '<a href=' . esc_url(get_the_permalink($post_id)) . '><strong>' . get_the_title($post_id) . '</strong></a>',
			'icon'  => $marker
		);
	}


	// Company thumb
	if( ! empty( $company_id ) ) {
		$thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $company_id ) );
	}

	if( empty($thumb_src)) {
		$thumb_src = careers_theme_profile_placeholder_image_url();
	}


	$rc_address           = get_post_meta($recruiter_id , '_company_contact_address' , true );
	$rc_zip               = get_post_meta($recruiter_id , '_company_contact_post_code' , true );
	$rc_locations         = get_the_terms( $recruiter_id, 'location' );

	$arr1 = array();

	if(isset($rc_locations) && is_array($rc_locations)){
		foreach ($rc_locations as $rc_location) {
	      if($rc_location->parent != 0) {
	        $arr1[] = $rc_location;
	      }
	    }
	    if( !empty( $arr1[0] ) && !empty($arr1[1]) ) {
		    if($arr1[0]->term_id == $arr1[1]->parent) {
		      $state_country   = $arr1[1]->name . ', ' . $arr1[0]->name;
		    } else {
		      $state_country   = $arr1[0]->name . ', ' . $arr1[1]->name;
		    }
	    } else {
          $state_country = $arr1[0]->name;
        }
	}

    if( !empty( $address ) && !empty( $state_country ) && !empty( $zip ) ) {
		$rc_address          = $address . ', ' . $state_country . ', ' . $zip;
	}

	$jobs = array();

	wp_localize_script( 'ng-controllers', 'listing', array('isBookmarked'=>$isBookMarked ) );
?>


	<div id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 page-sidebar">
					<aside>
						<div class="widget sidebar-widget white-container candidates-single-widget">
							<div class="widget-content">
								<div id="jobs-single-page-map" class="white-container"></div>

								<h5 class="bottom-line"><?php _e('仕事内容', 'careers'); ?></h5>

								<table>
									<tbody>
										<tr>
											<td><?php _e('ID', 'careers'); ?></td>
											<td>#<?php echo esc_attr($post_id); ?></td>
										</tr>

										<tr>
											<td><?php _e('住所', 'careers'); ?></td>
											<td>
												<?php if( !empty($j_location) ) : ?>
													<?php echo esc_attr( $j_location ); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('産業', 'careers'); ?></td>
											<td>
												<?php if( !empty( $industries ) ) : ?>
													<?php foreach ($industries as $industry) : ?>
														<?php echo esc_attr($industry->name); ?>
													<?php endforeach; ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('職種', 'careers'); ?></td>
											<td>
												<?php if( !empty( $job_types ) ) : ?>
													<?php foreach ($job_types as $job_type) : ?>
														<?php echo esc_attr($job_type->name); ?>
													<?php endforeach; ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('ロール', 'careers'); ?></td>
											<td>
												<?php if( !empty( $roles ) ) : ?>
													<?php foreach ($roles as $role) : ?>
														<?php echo esc_attr($role->name); ?>
													<?php endforeach; ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('参加日', 'careers'); ?></td>
											<td>
												<?php if( !empty( $joining_date ) ) : ?>
													<?php echo esc_attr($joining_date); ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('雇用状況', 'careers'); ?></td>
											<td>-</td>
										</tr>

										<tr>
											<td><?php _e('月給', 'careers'); ?></td>
											<td>
												<?php if( !empty( $monthly_salary ) ) : ?>
													<?php echo esc_attr($general['uou_currency_sign']) ?> <?php echo esc_attr($monthly_salary); ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>
									</tbody>
								</table>

								<h5 class="bottom-line"><?php _e('候補', 'careers'); ?></h5>

								<table>
									<tbody>
										<tr>
											<td><?php _e('キャリアレベル', 'careers'); ?></td>
											<td>
												<?php if( !empty( $careers_level ) ) : ?>
													<?php foreach ($careers_level as $career_level) : ?>
														<?php echo esc_attr($career_level->name); ?>
													<?php endforeach; ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('経験年数', 'careers'); ?></td>
											<td>
												<?php if( !empty( $years_of_experience ) ) : ?>
													<?php foreach ($years_of_experience as $exp) : ?>
														<?php echo esc_attr($exp->name); ?>
													<?php endforeach; ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('性別', 'careers'); ?></td>
											<td>
												<?php if( !empty( $genders ) ) : ?>
													<?php foreach ($genders as $gender) : ?>
														<?php echo esc_attr($gender->name); ?>
													<?php endforeach; ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('国籍', 'careers'); ?></td>
											<td>
												<?php if( !empty( $nationalities ) ) : ?>
													<?php foreach ($nationalities as $nationality) : ?>
														<?php echo esc_attr($nationality->name); ?>
													<?php endforeach; ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('学位', 'careers'); ?></td>
											<td>
												<?php if( !empty( $degrees ) ) : ?>
													<?php foreach ($degrees as $degree) : ?>
														<?php echo esc_attr($degree->name); ?>
													<?php endforeach; ?>
												<?php else : ?>
													-
												<?php endif; ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</aside>
				</div> <!-- end .page-sidebar -->

				<div class="col-sm-8 page-content">

					<div class="jobs-item jobs-single-item">
						<div class="thumb"><img src="<?php echo esc_url($thumb_src); ?>" alt=""></div>
						<div class="clearfix visible-xs"></div>
						<div class="date"><?php the_time('j');?> <span><?php the_time('M'); ?></span></div>
						<h6 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
						<span class="meta"><?php echo esc_attr( $state_country ); ?></span>
						<ul class="top-btns" ng-controller="jobListing">
							<li>
								<a href="" ng-if="isBookmarked" class="btn btn-gray fa fa-star" ng-click="saveAsBookmark(<?php echo esc_attr($post_id); ?>)"></a>
								<a href="" ng-if="!isBookmarked" class="btn btn-gray fa fa-star-o" ng-click="saveAsBookmark(<?php echo esc_attr($post_id); ?>)"></a>
							</li>
						</ul>

						<?php the_content() ?>

						<?php $required_skilllist = json_decode(get_post_meta($post_id, 'required_skilllist', true)); ?>

						<?php if ($required_skilllist) : ?>
							<h5><?php _e('必要なスキル', 'careers'); ?></h5>

							<?php foreach ($required_skilllist as $required_skilll) : ?>
								<div class="progress-bar toggle" data-progress="<?php echo esc_attr($required_skilll->level); ?>">
									<a href="#" class="progress-bar-toggle"></a>
									<h6 class="progress-bar-title"><?php echo esc_attr($required_skilll->name); ?></h6>
									<div class="progress-bar-inner"><span style="width: <?php echo esc_attr($required_skilll->level); ?>%;"></span></div>
									<div class="progress-bar-content">
										<?php echo esc_attr($required_skilll->desc); ?>
									</div>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>

						<?php $additional_skilllist = json_decode(get_post_meta($post_id, 'additional_skilllist', true)); ?>
						
						<?php if ($additional_skilllist) : ?>
							<h5><?php _e('追加要件', 'careers'); ?></h5>

							<ul class="additional-requirements clearfix">
								<?php foreach ($additional_skilllist as $additional_skilll) : ?>
									<li><?php echo esc_attr($additional_skilll->name); ?></li>
								<?php endforeach; ?>
							</ul>
							
						<?php endif; ?>


						<hr>

						<div class="clearfix">
							<a href="#" class="btn btn-default pull-left" data-toggle="modal" data-target="#applyModal"><?php _e('この求人に応募する', 'careers'); ?></a>

							<!-- Modal -->
							<div class="modal fade" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="applyModalLabel" aria-hidden="true">
							  	<div class="modal-dialog">
							    	<div class="modal-content">
								      	<div class="modal-header">
								        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								        	<h4 class="modal-title" id="myModalLabel"><?php _e('この求人に応募します', 'careers'); ?></h4>
								      	</div>

								      	<div class="modal-body">
								      		<?php if ( is_user_logged_in() ) : ?>
								      			<?php $current_user = wp_get_current_user(); ?>
									      		<?php if (array_shift($current_user->roles) === 'candidate') : ?>
								      				<?php if (comments_open()) : comments_template('', true ); endif; ?>
								      			<?php else : ?>
													<?php _e('候補者のみ仕事を適用することができます', 'careers'); ?>
								      			<?php endif; ?>
								      		<?php else : ?>
								      			<?php _e('適用する前に、候補者としてログインする必要があります', 'careers'); ?>
								      			<?php echo apply_filters( 'the_excerpt', sprintf( '<a href="%s">%s</a>', esc_url(site_url('/login')), __( 'ログイン', 'careers' ) ) ); ?>
								      		<?php endif; ?>
									    </div>
							    	</div>
							  	</div>
							</div>

							<ul class="social-icons pull-right">
								<li><span><?php _e('Share', 'careers'); ?></span></li>
								<li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>" class="btn btn-gray fa fa-facebook" target="_blank"></a></li>
								<li><a href="http://twitthis.com/twit?url=<?php the_permalink(); ?>" class="btn btn-gray fa fa-twitter" target="_blank"></a></li>
								<li><a href="https://plus.google.com/share?url=<?php the_permalink();?>" class="btn btn-gray fa fa-google-plus" target="_blank"></a></li>
							</ul>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="title-lines">
						<h3 class="mt0"><?php _e('リクルーターについて', 'careers'); ?></h3>
					</div>

					<div class="about-candidate-item">
						<div class="thumb"><img src="<?php echo esc_url($thumb_src); ?>" alt=""></div>

						<h6 class="title"><a href="<?php echo esc_url(home_url( '/'.$recruiter_key.'/' .$recruiter_post->post_name)); ?>"><?php echo esc_attr( $recruiter_post->post_title ); ?></a></h6>
						<span class="meta"><?php if( !empty( $rc_address ) ) echo esc_attr( $rc_address ); ?></span>

						<ul class="social-icons clearfix">
							<?php if( !empty($facebook) ): ?><li><a href="<?php echo esc_url($facebook); ?>" class="btn btn-gray fa fa-facebook"></a></li><?php endif; ?>
							<?php if( !empty($twitter) ): ?><li><a href="<?php echo esc_url($twitter); ?>" class="btn btn-gray fa fa-twitter"></a></li><?php endif; ?>
							<?php if( !empty($google) ): ?><li><a href="<?php echo esc_url($google); ?>" class="btn btn-gray fa fa-google-plus"></a></li><?php endif; ?>
							<?php if( !empty($youtube) ): ?><li><a href="<?php echo esc_url($youtube);  ?>" class="btn btn-gray fa fa-youtube"></a></li><?php endif; ?>
							<?php if( !empty($instagram) ): ?><li><a href="<?php echo esc_url($instagram); ?>" class="btn btn-gray fa fa-instagram"></a></li><?php endif; ?>
							<?php if( !empty($linkedin) ): ?><li><a href="<?php echo esc_url($linkedin); ?>" class="btn btn-gray fa fa-linkedin"></a></li><?php endif; ?>
						</ul>

						<ul class="list-unstyled">
							<li><strong><?php _e('連絡先:', 'careers'); ?></strong> <?php echo esc_attr(get_post_meta($recruiter_id, '_'.$recruiter_key.'_contact_phone', true)); ?></li>
							<li><strong><?php _e('ウェブサイト:', 'careers'); ?></strong> <a href="<?php echo esc_url(get_post_meta($recruiter_id, $recruiter_key.'_website', true)); ?>"><?php echo esc_attr(get_post_meta($recruiter_id, ''.$recruiter_key.'_website', true)); ?></a></li>
						</ul>

						<a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal"><?php _e('メッセージを送る', 'careers'); ?></a>

						<!-- Modal -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  	<div class="modal-dialog">
						    	<div class="modal-content">
							      	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        	<h4 class="modal-title" id="myModalLabel"><?php _e('メッセージをお送りください', 'careers'); ?></h4>
							      	</div>
						      	
							      	<form method="post" action="">

								      	<div class="modal-body">
								        
											<input type="hidden" name="to" value="<?php echo esc_attr($recruiter_email); ?>"> 

											<div class="form-group">
											    <label for="from"><?php _e('メールアドレス', 'careers'); ?></label>
											    <input type="text" id="form" name="from" class="form-control"  placeholder="メールアドレスを入力してください">
											</div>

											<div class="form-group">
										    	<label for="subject"><?php _e('件名', 'careers'); ?></label>
										    	<input type="text" id="subject" name="subject" class="form-control"  placeholder="件名を入力してください ">
										  	</div>
											
											<div class="form-group">
										    	<label for="message"><?php _e('メッセージ', 'careers'); ?></label>
										    	<textarea name="message" id="message" rows="5" placeholder="メッセージを入力してください"></textarea>
										  	</div>
										
									    </div>
										
										<div class="modal-footer">
											<button type="button" class="btn btn-red" data-dismiss="modal"><?php _e('クローズ', 'careers'); ?></button>
											<?php wp_nonce_field('handle_contact_form', 'nonce_contact_form');?>
											<button type="submit" class="btn btn-default"><?php _e('送信', 'careers'); ?></button>
										</div>

							      	</form>
						    	</div>
						  	</div>
						</div>
					</div>
				</div> <!-- end .page-content -->
			</div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->
<?php careers_set_post_views($post_id); ?>

<?php endwhile; endif; ?>
<?php if ( !empty($location_map)) : ?>
<script>

	new Maplace({
	  map_div: '#jobs-single-page-map',
	  controls_type: 'list',
	  map_options: {
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    scrollwheel: false,
	    zoom: 14
	  },
	  locations: <?php echo json_encode($location_map); ?>
	}).Load();
</script>
<?php endif; ?>

<?php get_footer(); ?>