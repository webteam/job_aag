<?php

$args = array(
	'post_type' => 'company',
	'posts_per_page' => -1
);
$posts = get_posts( $args );


$companies = array();

foreach ($posts as  $post) {
	$post_id = $post->ID;
	$city_state = get_post_meta($post_id , '_company_contact_city_state' , true );
	$country    = get_post_meta($post_id , '_company_contact_country' , true );
	$location   = $city_state . ', ' . $country;

	$thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) );
	
	$companies[] = array(
		'id'          => $post_id,
		'title'       => $post->post_title,
		'content'     => $post->post_content,
		'thumb_src'   => (!empty($thumb_src)) ? $thumb_src : careers_theme_profile_placeholder_image_url(),
		'company_url' => home_url( '/company/' .$post->post_name),
		'post_day'    => get_the_time('j', $post_id),
		'post_month'  => get_the_time('M', $post_id),
		'location'    => $location,
		'isBookmarked'		=> isBookmarked($post_id, 'bookmarked_company')

	);
}

wp_localize_script( 'ng-controllers', 'listing', array( 'companies' => $companies ) );

?>
<div class="pt30 pb30">
	<div>
		<div ng-controller="companyListing">

			<div class="white-container candidates-search">
				<input type="text" class="form-control" placeholder="企業を検索..." ng-model="search">
			</div>

			<div class="title-lines">
				<h3 class="mt0">企業一覧</h3>
			</div>


				
			<div class="jobs-item with-thumb" ng-repeat="company in companies| filter:search">
				
				<div class="thumb"><a href="{{ company.company_url }}"><img src="{{ company.thumb_src }}" alt=""></a></div>
				<div class="clearfix visible-xs"></div>
				<div class="date">{{ company.post_day }} <span>{{ company.post_month }}</span></div>
				<h6 class="title"><a href="{{ company.company_url }}">{{ company.title }}</a></h6>
				<span class="meta">{{ company.location }}</span>
				
				<ul class="top-btns">
					<li>
						<a href="" ng-if="company.isBookmarked" class="btn btn-gray fa fa-star" ng-click="saveAsBookmark(company)"></a>
						<a href="" ng-if="!company.isBookmarked" class="btn btn-gray fa fa-star-o" ng-click="saveAsBookmark(company)"></a>
					</li>
					<li><a href="{{ company.company_url }}" class="btn btn-gray fa fa-link"></a></li>
				</ul>

				<p class="description" ng-bind-html="company.content | limitTo:230"></p>

				<div class="clearfix"></div>
			</div>
				
		</div>
	</div>
</div>