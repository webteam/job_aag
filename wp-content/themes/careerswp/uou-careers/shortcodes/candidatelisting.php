<?php

$args = array(
	'post_type' => 'candidate',
	'posts_per_page' => -1
);
$posts = get_posts( $args );


$candidates = array();
$state_country = '';
foreach ($posts as  $post) {
	$post_id = $post->ID;
	$arr = array();
	$locations = get_the_terms( $post_id, 'location' );
    $locationObj = array();
    $filter_location = '';
    $filter_industry_role = '';
    if(isset($locations) && is_array($locations)){
      	foreach ($locations as $location) {
	        if($location->parent != 0) {
	            $arr[] = $location;
	         }
	        $locationObj[] = array( 'id' => $location->term_id, 'name' => $location->name, 'slug'=> $location->slug);
	        $filter_location .= $location->name." ";
    	}

        if($arr[0]->term_id == $arr[1]->parent) {
          $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
        } else {
          $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
        }
    }
    
    $thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id, 'small-thumb' ) );

    if(empty($thumb_src)) {
    	$thumb_src = careers_theme_profile_placeholder_image_url();
    }

    $skills = json_decode( get_post_meta($post_id, 'skilllist', true) );
    
    $now = time();
    $postDate = strtotime($post->post_date);
	$candidates[] = array(

		'id'            => $post_id,
		'title'         => $post->post_title,
		'excerpt'       => uou_get_post_excerpt($post->post_content),
		'content'       => apply_filters('the_content', $post->post_content),
		'thumb_src'     => $thumb_src,
		'candidate_url' => home_url( '/candidate/' .$post->post_name),
		'post_day'      => get_the_time('j', $post_id),
		'post_month'    => get_the_time('M', $post_id),
		'location'      => $locationObj,
		'state_country' => $state_country,
		'salary'        => get_post_meta($post_id, '_expected_salary', true),
		'skills'        => $skills,
		'postday'       => floor(($now - $postDate)/86400),
		'facebook'      => esc_url('//www.facebook.com/sharer.php?u='.home_url( '/candidate/' .$post->post_name).'/'),
		'twitter'       => esc_url('//twitter.com/share?url='.home_url( '/candidate/' .$post->post_name).'/'),
		'google'        => esc_url('https://plus.google.com/share?url='.home_url( '/candidate/' .$post->post_name).'/'),
		'isBookmarked'		=> isBookmarked($post_id, 'bookmarked_candidate'),

	);
}

wp_localize_script( 'ng-controllers', 'listing', array( 'candidates' => $candidates ) );

?>
<div class="pt30 pb30 row">
	<div class="col-sm-12 page-content candidates-search-page">
		<div ng-controller="candidatelisting">
			
			<div class="white-container candidates-search clearfix">
				<div class="col-sm-6">
					<input type="text" class="form-control" placeholder="スキル" ng-model="search.skills">
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" placeholder="住所" ng-model="search.location">
				</div>
			</div>
			
			<div class="title-lines">
				<h3 class="mt0"><?php _e('候補者一覧', 'careers') ?></h3>
			</div>

				
			<!-- <div  ng-repeat="candidate in candidates | candidateFilter:filterItem | filter:search:strict"> -->
			<div class="candidates-item with-thumb" dir-paginate="candidate in candidates | dataFilter:filterItem | filter:search | itemsPerPage: pageSize: 'candidatelist'" current-page="currentPage" pagination-id="candidatelist">
				<div class="thumb"><a href="{{ candidate.candidate_url }}"><img ng-src="{{ candidate.thumb_src }}" alt=""></a></div>
				<div class="clearfix visible-xs"></div>
				<h6 class="title"><a href="{{ candidate.candidate_url }}">{{ candidate.title }}</a></h6>
				<span class="meta" ng-bind-html="renderHtml(candidate.state_country)"></span>

				<ul class="top-btns">
					<li><a href="#" class="btn btn-gray fa toggle fa-plus"></a></li>
					<li>
						<a href="" ng-if="candidate.isBookmarked" class="btn btn-gray fa fa-star" ng-click="saveAsBookmark(candidate)"></a>
						<a href="" ng-if="!candidate.isBookmarked" class="btn btn-gray fa fa-star" ng-click="saveAsBookmark(candidate)"></a>
					</li>
					<li><a href="{{ candidate.candidate_url }}" class="btn btn-gray fa fa-link"></a></li>
				</ul>

				<p class="description">
					{{ candidate.excerpt }}
				</p>

				<div class="clearfix"></div>

				<div class="content">
					<div ng-bind-html="renderHtml(candidate.content)"></div>

					<h5><?php _e('Skills', 'careers') ?></h5>

					<div class="progress-wrapper" ng-repeat="skill in candidate.skills">
						<div class="progress-bar toggle" data-progress="{{ skill.level }}">
							<a href="#" class="progress-bar-toggle"></a>
							<h6 class="progress-bar-title">{{ skill.name }}</h6>
							<div class="progress-bar-inner"><span></span></div>
							<div class="progress-bar-content">
								{{ skill.desc }}
							</div>
						</div>
					</div>

					<hr>

					<div class="clearfix">
						<ul class="social-icons pull-right">
							<li><span><?php _e('共有する', 'careers') ?></span></li>
							<li><a href="{{ candidate.facebook }}" target="_blank" class="btn btn-gray fa fa-facebook"></a></li>
							<li><a href="{{ candidate.twitter }}" target="_blank" class="btn btn-gray fa fa-twitter"></a></li>
							<li><a href="{{ candidate.google }}" target="_blank" class="btn btn-gray fa fa-google-plus"></a></li>
						</ul>
					</div>
				</div>
			</div>
			

			<div class="clearfix mb30">
	          <dir-pagination-controls boundary-links="true" pagination-id="candidatelist" on-page-change="pageChangeHandler(newPageNumber)" ></dir-pagination-controls>
	        </div>


		</div>
	</div>
</div>