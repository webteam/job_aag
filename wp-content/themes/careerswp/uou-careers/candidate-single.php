<?php global $careers_option_data; $general = get_option('general'); ?>
<?php if(isset($careers_option_data['career-select-search-page-for-candidate'])){ $search_page_for_candidate = $careers_option_data['career-select-search-page-for-candidate']; } ?>
<?php get_header(); ?>
	<div id="page-content">
        <div class="container">
    		<div class="row">
    			<?php  	if ( have_posts() ):  ?>
    				<?php the_post(); $post_id= $post->ID; $author_id= $post->post_author; ?>

						<?php

							$candidate_name = get_the_title($post_id);
							
							// Privacy check
							$hide_profile_pic = get_post_meta( $post_id, 'hide_profile_pic', true );
							$hide_user_email = get_post_meta( $post_id, 'hide_user_email', true );
							$hide_date_of_birth = get_post_meta( $post_id, 'hide_date_of_birth', true );

							$phone = get_post_meta( $post_id, 'phone', true );
							$fax = get_post_meta( $post_id, 'fax', true );
							$candidate_url = get_post_meta( $post_id, 'candidate_url', true );
							$date_of_birth = date("Y-m-d", strtotime('+1 day', strtotime(get_post_meta( $post_id, 'date_of_birth', true ))));

							$salary = get_post_meta( $post_id, '_expected_salary', true );

							$age = date_diff(date_create($date_of_birth), date_create('today'))->y;

							// Social media
							$facebook          = get_post_meta($post_id , '_candidate_facebook' , true );
							$twitter           = get_post_meta($post_id , '_candidate_twitter' , true );
							$google            = get_post_meta($post_id , '_candidate_google' , true );
							$linkedin          = get_post_meta($post_id , '_candidate_linkedin' , true );
							$youtube           = get_post_meta($post_id , '_candidate_youtube' , true );
							$instagram         = get_post_meta($post_id , '_candidate_instagram' , true );

							$career_levels = get_the_terms( $post_id, 'career_level' );
							$degree        = get_the_terms( $post_id, 'degree' );
							$experiance    = get_the_terms( $post_id, 'years_of_experience' );
							$gender        = get_the_terms( $post_id, 'gender' );
							$nationality   = get_the_terms( $post_id, 'nationality' );
							$locations     = get_the_terms( $post_id, 'location' );
							
							$thumb_src = wp_get_attachment_url( get_post_thumbnail_id($post_id) ); 

						    if(empty($thumb_src)) {
						    	$thumb_src = careers_theme_profile_placeholder_image_url();
						    }

							$arr           = array();

							if( !empty( $locations ) ) {
								foreach ($locations as $location) {
			                      if($location->parent != 0) {
			                        $arr[] = $location;
			                      }
			                    }

			                    if($arr[0]->term_id == $arr[1]->parent) {
			                      $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
			                    } else {
			                      $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
			                    }
							}

		                    $user = get_userdata( $author_id );
		                    
						?>
				<div class="col-sm-4 page-sidebar">
					<aside>
						<div class="widget sidebar-widget white-container candidates-single-widget">
							<div class="widget-content">
								<div class="thumb">
									<?php if( $hide_profile_pic !== 'true' ) : ?>
										<img src="<?php echo esc_url($thumb_src) ?>" alt="">
									<?php else : ?>
										<?php careers_theme_profile_placeholder_image() ?>
									<?php endif; ?>
								</div>

								<h5 class="bottom-line"><?php _e('候補の詳細', 'careers'); ?></h5>

								<table>
									<tbody>
										<tr>
											<td><?php _e('名前', 'careers'); ?></td>
											<td><?php echo esc_attr($candidate_name); ?></td>
										</tr>

										<tr>
											<td><?php _e('年齢', 'careers'); ?></td>
											<td>
												<?php if( empty( $hide_date_of_birth ) || $hide_date_of_birth === 'false' ) : ?>
													<?php echo esc_attr($age); ?> <?php _e('歳', 'careers'); ?>
												<?php else : ?>
													<?php _e('-', 'careers') ?>
												<?php endif ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('希望給与', 'careers'); ?></td>
											<td>
												<?php if( !empty( $salary ) ) : ?>
													<?php echo esc_attr($general['uou_currency_sign']) ?> <?php echo esc_attr($salary); ?>
												<?php else : ?>
													<?php _e('-', 'careers') ?>
												<?php endif ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('住所', 'careers'); ?></td>
											<td><?php if( isset( $state_country ) ) echo esc_attr($state_country); ?></td>
										</tr>

										<tr>
											<td><?php _e('経験', 'careers'); ?></td>
											<?php if( !empty( $experiance ) ) : foreach($experiance as $exp) : ?>
												<td><?php echo esc_attr($exp->name); ?></td>
											<?php endforeach; else :?>
												<td><?php _e('-', 'careers') ?></td>
											<?php endif; ?>
										</tr>

										<tr>
											<td><?php _e('性別', 'careers'); ?></td>
											<?php if( !empty( $gender) ) : foreach($gender as $gen) : ?>
												<td><?php echo esc_attr($gen->name); ?></td>
											<?php endforeach; else :?>
												<td><?php _e('-', 'careers') ?></td>
											<?php endif; ?>
										</tr>

										<tr>
											<td><?php _e('学位', 'careers'); ?></td>
											<?php if( !empty( $degree) ) : foreach($degree as $d) : ?>
												<td><?php echo esc_attr($d->name); ?></td>
											<?php endforeach; else : ?>
												<td><?php _e('-', 'careers') ?></td>
											<?php endif; ?>
										</tr>

										<tr>
											<td><?php _e('キャリアレベル', 'careers'); ?></td>
											<?php if( !empty( $career_levels) ) : foreach($career_levels as $career_level) : ?>
												<td><?php echo esc_attr($career_level->name); ?></td>
											<?php endforeach; else :?>
												<td><?php _e('-', 'careers') ?></td>
											<?php endif; ?>
										</tr>

										<tr>
											<td><?php _e('国籍', 'careers'); ?></td>
											<?php if( !empty( $nationality ) ) : foreach($nationality as $nat) : ?>
												<td><?php echo esc_attr($nat->name); ?></td>
											<?php endforeach; else :?>
												<td><?php _e('-', 'careers') ?></td>
											<?php endif; ?>
										</tr>

										<tr>
											<td><?php _e('電話番号', 'careers'); ?></td>
											<td>
												<?php if( !empty($phone)) : ?>
													<?php echo esc_attr($phone); ?>
												<?php else : ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('ファックス', 'careers'); ?></td>
											<td>
												<?php if( !empty($fax)) : ?>
													<?php echo esc_attr($fax); ?>
												<?php else : ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('メールアドレス', 'careers'); ?></td>
											<td>
												<?php if( empty( $hide_user_email ) || $hide_user_email === 'false' ) : ?>
													<a href="mailto:<?php echo esc_attr($user->user_email); ?>"><?php echo esc_attr($user->user_email); ?></a>
												<?php else : ?>
													<?php _e('-', 'careers') ?>
												<?php endif ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('ウェブサイト', 'careers'); ?></td>
											<td>
												<?php if( !empty($candidate_url)) : ?>
													<a href="<?php echo esc_url($candidate_url) ?>"><?php echo esc_attr($candidate_url) ?></a>
												<?php else : ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>
									</tbody>
								</table>

								<?php
									$expertise = (get_post_meta($post_id, 'expertise', true))/20;
									$knowledge = (get_post_meta($post_id, 'knowledge', true))/20;
									$quality = (get_post_meta($post_id, 'quality', true))/20;
									$price = (get_post_meta($post_id, 'price', true))/20;
									$services = (get_post_meta($post_id, 'services', true))/20;
								?>

								<h5 class="bottom-line"><?php _e('Professional Rating', 'careers'); ?></h5>

								<table>
									<tbody>
										<?php if ($expertise) : ?>
											<tr>
												<td><?php _e('特技', 'careers'); ?></td>
												<td>
													<ul class="stars">
														<?php for ($i=0; $i < $expertise; $i++) { ?>
															<li><i class="fa fa-star"></i></li>	
														<?php } ?>
														<?php if ($expertise < 5) : ?>
															<?php for ($j=0; $j < 5 - $expertise; $j++) { ?>
																<li><i class="fa fa-star-o"></i></li>
															<?php } ?>
														<?php endif; ?>
													</ul>
												</td>
											</tr>

										<?php endif; ?>

										<?php if ($knowledge) : ?>
											<tr>
												<td><?php _e('知識', 'careers'); ?></td>
												<td>
													<ul class="stars">
														<?php for ($i=0; $i < $knowledge; $i++) { ?>
															<li><i class="fa fa-star"></i></li>	
														<?php } ?>
														<?php if ($knowledge < 5) : ?>
															<?php for ($j=0; $j < 5 - $knowledge; $j++) { ?>
																<li><i class="fa fa-star-o"></i></li>
															<?php } ?>
														<?php endif; ?>
													</ul>
												</td>
											</tr>

										<?php endif; ?>

										<?php if ($quality) : ?>
											<tr>
												<td><?php _e('素質', 'careers'); ?></td>
												<td>
													<ul class="stars">
														<?php for ($i=0; $i < $quality; $i++) { ?>
															<li><i class="fa fa-star"></i></li>	
														<?php } ?>
														<?php if ($quality < 5) : ?>
															<?php for ($j=0; $j < 5 - $quality; $j++) { ?>
																<li><i class="fa fa-star-o"></i></li>
															<?php } ?>
														<?php endif; ?>
													</ul>
												</td>
											</tr>

										<?php endif; ?>

										<?php if ($price) : ?>
											<tr>
												<td><?php _e('値段', 'careers'); ?></td>
												<td>
													<ul class="stars">
														<?php for ($i=0; $i < $price; $i++) { ?>
															<li><i class="fa fa-star"></i></li>	
														<?php } ?>
														<?php if ($price < 5) : ?>
															<?php for ($j=0; $j < 5 - $price; $j++) { ?>
																<li><i class="fa fa-star-o"></i></li>
															<?php } ?>
														<?php endif; ?>
													</ul>
												</td>
											</tr>

										<?php endif; ?>

										<?php if ($services) : ?>
											<tr>
												<td><?php _e('サービス', 'careers'); ?></td>
												<td>
													<ul class="stars">
														<?php for ($i=0; $i < $services; $i++) { ?>
															<li><i class="fa fa-star"></i></li>	
														<?php } ?>
														<?php if ($services < 5) : ?>
															<?php for ($j=0; $j < 5 - $services; $j++) { ?>
																<li><i class="fa fa-star-o"></i></li>
															<?php } ?>
														<?php endif; ?>
													</ul>
												</td>
											</tr>

										<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>
					</aside>
				</div> <!-- end .page-sidebar -->

				<div class="col-sm-8 page-content">
					<div class="clearfix mb30 hidden-xs">
						<a href="<?php echo esc_url( site_url("/$search_page_for_candidate") ) ?>" class="btn btn-gray pull-left">リストへ戻る</a>
						<div class="pull-right">
							
							<?php if( get_previous_post() ) : ?>
								<?php ob_start(); ?>
									<?php previous_post_link( '%link', __( '前へ', 'careers' ) ,false ); ?>
								<?php $link = ob_get_clean();
								echo str_replace('<a ','<a class="btn btn-gray" ', $link); ?>
							<?php endif; ?>

							<?php if( get_next_post() ) : ?>
								<?php ob_start(); ?>
									<?php next_post_link( '%link', __( '次へ', 'careers' ) ,false ); ?>
								<?php $link = ob_get_clean();
								echo str_replace('<a ','<a class="btn btn-gray" ', $link); ?>
							<?php endif; ?>
						</div>
					</div>
								<div class="candidates-item candidates-single-item">
									<h6 class="title"><a href="<?php the_permalink(); ?>"><?php echo esc_attr($candidate_name); ?></a></h6>
									<span class="meta">
										<?php if ( empty( $hide_date_of_birth ) || $hide_date_of_birth === 'false' ) : ?>
											<?php echo esc_attr($age); ?> <?php _e('歳 - ', 'careers'); ?>
										<?php endif ?>
										<?php if( isset( $state_country ) ) echo esc_attr($state_country); ?>
									</span>

									<!-- <ul class="top-btns">
										<li><a href="#" class="btn btn-gray fa fa-star"></a></li>
									</ul> -->

									<ul class="social-icons clearfix">
										<?php if( !empty($facebook) ): ?><li><a href="<?php echo esc_url($facebook); ?>" class="btn btn-gray fa fa-facebook"></a></li><?php endif; ?>
										<?php if( !empty($twitter) ): ?><li><a href="<?php echo esc_url($twitter); ?>" class="btn btn-gray fa fa-twitter"></a></li><?php endif; ?>
										<?php if( !empty($google) ): ?><li><a href="<?php echo esc_url($google); ?>" class="btn btn-gray fa fa-google-plus"></a></li><?php endif; ?>
										<?php if( !empty($youtube) ): ?><li><a href="<?php echo esc_url($youtube);  ?>" class="btn btn-gray fa fa-youtube"></a></li><?php endif; ?>
										<?php if( !empty($instagram) ): ?><li><a href="<?php echo esc_url($instagram); ?>" class="btn btn-gray fa fa-instagram"></a></li><?php endif; ?>
										<?php if( !empty($linkedin) ): ?><li><a href="<?php echo esc_url($linkedin); ?>" class="btn btn-gray fa fa-linkedin"></a></li><?php endif; ?>
									</ul>

									<?php the_content(); ?>

									<ul class="list-unstyled">

										<?php if($experiance) : ?>
											<li><strong><?php _e('経験:', 'careers'); ?></strong> 
												<?php foreach($experiance as $exp) : ?>
													<?php echo esc_attr($exp->name); ?>
												<?php endforeach; ?>
											</li>
										<?php endif; ?>										

										<?php if($degree) : ?>
											<li><strong><?php _e('学位:', 'careers'); ?></strong> 
												<?php foreach($degree as $deg) : ?>
													<?php echo esc_attr($deg->name); ?>
												<?php endforeach; ?>
											</li>
										<?php endif; ?>
										
										<?php if($career_levels) : ?>
											<li><strong><?php _e('キャリアレベル:', 'careers'); ?></strong> 
												<?php foreach($career_levels as $career_level) : ?>
													<?php echo esc_attr($career_level->name); ?>
												<?php endforeach; ?>
											</li>
										<?php endif; ?>
									</ul>

									<?php $skilllist = json_decode(get_post_meta($post_id, 'skilllist', true)); ?>

									<?php if ( !empty( $skilllist ) ) : ?>
										<h5><?php _e('スキル', 'careers'); ?></h5>

										<?php foreach ($skilllist as $skill) : ?>
											<div class="progress-bar toggle" data-progress="<?php echo esc_attr($skill->level); ?>">
												<a href="#" class="progress-bar-toggle"></a>
												<h6 class="progress-bar-title"><?php echo esc_attr($skill->name); ?></h6>
												<div class="progress-bar-inner"><span></span></div>
												<div class="progress-bar-content">
													<?php echo esc_attr($skill->desc); ?>
												</div>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>

									<hr>

									<div class="clearfix">

										<a href="#" class="btn btn-default pull-left" data-toggle="modal" data-target="#contactModal"><i class="fa fa-envelope-o"></i> <?php _e('ご連絡ください', 'careers') ?></a>

										<!-- Modal -->
										<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
										  <div class="modal-dialog">
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										        <h4 class="modal-title" id="contactModalLabel"><?php _e('メッセージを送る', 'careers'); ?></h4>
										      </div>
										      <form method="post" action="">

											      <div class="modal-body">

													<input type="hidden" name="to" value="<?php echo esc_attr($user->user_email); ?>">

													<div class="form-group">
													    <label for="from"><?php _e('メールアドレス', 'careers'); ?></label>
													    <input type="text" id="form" name="from" required="true" class="form-control"  placeholder="メールアドレスを入力してください">
													</div>

													<div class="form-group">
												    	<label for="subject"><?php _e('件名', 'careers'); ?></label>
												    	<input type="text" id="subject" name="subject" required="true" class="form-control"  placeholder="件名を入力してください">
												  	</div>

													<div class="form-group">
												    	<label for="message"><?php _e('メッセージ', 'careers'); ?></label>
												    	<textarea name="message" class="form-control" id="message" rows="5" required="true" placeholder="メッセージを入力してください..."></textarea>
												  	</div>

											      </div>
											      <div class="modal-footer">
											      	<div class="spinner pull-left"></div>
											        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e('クローズ', 'careers'); ?></button>
											        <?php wp_nonce_field('handle_contact_form', 'nonce_contact_form');?>
											        <button type="submit" class="btn btn-default"><?php _e('送信', 'careers'); ?></button>
											      </div>

										      </form>
										    </div>
										  </div>
										</div>

										<ul class="social-icons pull-right">
											<li><span><?php _e('Share', 'careers'); ?></span></li>
											<li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>" class="btn btn-gray fa fa-facebook" target="_blank"></a></li>
											<li><a href="http://twitthis.com/twit?url=<?php the_permalink(); ?>" class="btn btn-gray fa fa-twitter" target="_blank"></a></li>
											<li><a href="https://plus.google.com/share?url=<?php the_permalink();?>" class="btn btn-gray fa fa-google-plus" target="_blank"></a></li>
										</ul>
									</div>
								</div> <!-- end .candidates-single-item -->
				</div> <!-- end .page-content -->
				<?php
					else:
					    echo 'no post was found';
					endif;
				?>
			</div> <!-- end .row -->
        </div> <!-- end .container -->
    </div> <!-- end #page-content -->
<?php

wp_reset_postdata();
//get_sidebar();
get_footer();
