<?php get_header(); ?>
	<div id="page-content">
        <div class="container">
    		<div class="row">
    			<?php  	if ( have_posts() ):  ?>
    				<?php the_post(); $post_id= $post->ID; $author_id= $post->post_author; ?>
    				<?php
							$foundation_year   = get_post_meta($post_id , '_company_contact_foundation_year' , true );
							$website           = get_post_meta($post_id , 'company_website' , true );
							$user = get_userdata( $author_id );
							$contact_email     = $user->user_email;
							$legal_entity      = get_post_meta($post_id , '_company_contact_legal_entity' , true );
							$contact_turnover  = get_post_meta($post_id , '_company_contact_turnover' , true );
							$total_employee    = get_post_meta($post_id , '_company_contact_employee_total' , true );
							$contact_phone     = get_post_meta($post_id , '_company_contact_phone' , true );
							$contact_fax       = get_post_meta($post_id , '_company_contact_fax' , true );
							$turnover          = get_post_meta($post_id , '_company_contact_turnover' , true );
							$employee_total    = get_post_meta($post_id , '_company_contact_employee_total' , true );
							
							$address           = get_post_meta($post_id , '_company_contact_address' , true );
							$zip               = get_post_meta($post_id , '_company_contact_post_code' , true );
							$locations         = get_the_terms( $post_id, 'location' );
							
							$arr = array();

							if(isset($locations) && is_array($locations)){
								foreach ($locations as $location) {
							      if($location->parent != 0) {
							        $arr[] = $location;
							      }
							    }
							    if( !empty( $arr[0] ) && !empty($arr[1]) ) {
								    if($arr[0]->term_id == $arr[1]->parent) {
								      $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
								    } else {
								      $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
								    }
							    } else {
						          $state_country = $arr[0]->name;
						        }
							}

							if( !empty( $address ) && !empty( $city_state ) && !empty($country) && !empty( $zip ) ) {
								$c_location          = $address . ', ' . $city_state . ', ' . $country . ', ' . $zip;
							}

							$opening_mon_fri   = get_post_meta($post_id , '_company_opening_hours_monday_friday' , true );
							$opening_sat       = get_post_meta($post_id , '_company_opening_hours_saturday' , true );
							$opening_sun       = get_post_meta($post_id , '_company_opening_hours_sunday' , true );

							// Social media
							$facebook          = get_post_meta($post_id , '_company_facebook' , true );
							$twitter           = get_post_meta($post_id , '_company_twitter' , true );
							$google            = get_post_meta($post_id , '_company_google' , true );
							$linkedin          = get_post_meta($post_id , '_company_linkedin' , true );
							$youtube           = get_post_meta($post_id , '_company_youtube' , true );
							$instagram         = get_post_meta($post_id , '_company_instagram' , true );
    				?>
				<div class="col-sm-4 page-sidebar">
					<aside>
						<div class="widget sidebar-widget white-container candidates-single-widget">
							<div class="widget-content">
								<div class="thumb">
									<?php (  has_post_thumbnail() ) ? the_post_thumbnail( 'full' ) : careers_theme_profile_placeholder_image(); ?>
								</div>

								<h5 class="bottom-line"><?php _e('会社概要', 'careers'); ?></h5>

								<table>
									<tbody>
										<tr>
											<td><?php _e('名前', 'careers'); ?></td>
											<td><?php the_title(); ?></td>
										</tr>

										<tr>
											<td><?php _e('設立年', 'careers'); ?></td>
											<td>
												<?php if( !empty( $foundation_year ) ) : ?>
													<?php echo esc_attr($foundation_year); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('住所', 'careers'); ?></td>
											<td>
												<?php if( !empty( $c_location ) ) : ?>
													<?php echo esc_attr($c_location); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('法人', 'careers'); ?></td>
											<td>
												<?php if( !empty( $legal_entity ) ) : ?>
													<?php echo esc_attr($legal_entity); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('売上高', 'careers'); ?></td>
											<td>
												<?php if( !empty( $turnover ) ) : ?>
													<?php echo esc_attr($turnover); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('従業員数', 'careers'); ?></td>
											<td>
												<?php if( !empty( $employee_total ) ) : ?>
													<?php echo esc_attr($employee_total); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('電話番号', 'careers'); ?></td>
											<td>
												<?php if( !empty( $contact_phone ) ) : ?>
													<?php echo esc_attr($contact_phone); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('ファクス', 'careers'); ?></td>
											<td>
												<?php if( !empty( $contact_fax ) ) : ?>
													<?php echo esc_attr($contact_fax); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('メールアドレス', 'careers'); ?></td>
											<td>
												<?php if( !empty( $contact_email ) ) : ?>
													<a href="mailto:<?php echo esc_attr($contact_email); ?>"><?php echo esc_attr($contact_email); ?></a>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('ウェブサイト', 'careers'); ?></td>
											<td>
												<?php if( !empty( $website ) ) : ?>
													<a href="<?php echo esc_url($website); ?>"><?php echo esc_attr($website); ?></a>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>
									</tbody>
								</table>

								<h5 class="bottom-line"><?php _e('営業時間', 'careers'); ?></h5>

								<table>
									<tbody>
										<tr>
											<td><?php _e('月曜〜金曜', 'careers'); ?></td>
											<td>
												<?php if( !empty( $opening_mon_fri ) ) : ?>
													<?php echo esc_attr($opening_mon_fri); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('土曜', 'careers'); ?></td>
											<td>
												<?php if( !empty( $opening_sat ) ) : ?>
													<?php echo esc_attr($opening_sat); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('日曜', 'careers'); ?></td>
											<td>
												<?php if( !empty( $opening_sun ) ) : ?>
													<?php echo esc_attr($opening_sun); ?>
												<?php else: ?>
													<?php _e('-', 'careers') ?>
												<?php endif; ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</aside>
				</div> <!-- end .page-sidebar -->

				<div class="col-sm-8 page-content">
								<div class="candidates-item candidates-single-item">
									<h6 class="title"><a href="#"><?php the_title(); ?></a></h6>
									<span class="meta">
										<?php if( !empty( $state_country) ) : ?>
											<?php echo esc_attr($state_country); ?>
										<?php else: ?>
											<?php _e('-', 'careers') ?>
										<?php endif; ?>
									</span>

									<ul class="social-icons clearfix">
										<?php if( !empty($facebook) ): ?><li><a href="<?php echo esc_url($facebook); ?>" class="btn btn-gray fa fa-facebook"></a></li><?php endif; ?>
										<?php if( !empty($twitter) ): ?><li><a href="<?php echo esc_url($twitter); ?>" class="btn btn-gray fa fa-twitter"></a></li><?php endif; ?>
										<?php if( !empty($google) ): ?><li><a href="<?php echo esc_url($google); ?>" class="btn btn-gray fa fa-google-plus"></a></li><?php endif; ?>
										<?php if( !empty($youtube) ): ?><li><a href="<?php echo esc_url($youtube);  ?>" class="btn btn-gray fa fa-youtube"></a></li><?php endif; ?>
										<?php if( !empty($instagram) ): ?><li><a href="<?php echo esc_url($instagram); ?>" class="btn btn-gray fa fa-instagram"></a></li><?php endif; ?>
										<?php if( !empty($linkedin) ): ?><li><a href="<?php echo esc_url($linkedin); ?>" class="btn btn-gray fa fa-linkedin"></a></li><?php endif; ?>
									</ul>

									<?php the_content(); ?>

									<hr>

									<div class="clearfix">
										<a href="#" class="btn btn-default pull-left" data-toggle="modal" data-target="#contactModal"><i class="fa fa-envelope-o"></i> ご連絡ください</a>

										<!-- Modal -->
										<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
										  <div class="modal-dialog">
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										        <h4 class="modal-title" id="contactModalLabel"><?php _e('メッセージを送りください', 'careers'); ?></h4>
										      </div>
										      <form method="post">

											      <div class="modal-body">

													<input type="hidden" name="to" value="<?php echo esc_attr($contact_email); ?>">

													<div class="form-group">
													    <label for="from"><?php _e('メールアドレス', 'careers'); ?></label>
													    <input type="text" id="form" name="from" required="true" class="form-control"  placeholder="メールアドレスを入力してください">
													</div>

													<div class="form-group">
												    	<label for="subject"><?php _e('件名', 'careers'); ?></label>
												    	<input type="text" id="subject" name="subject" required="true" class="form-control"  placeholder="件名を入力してください ">
												  	</div>

													<div class="form-group">
												    	<label for="message"><?php _e('メッセージ', 'careers'); ?></label>
												    	<textarea name="message" class="form-control" id="message" required="true" rows="5" placeholder="メッセージを入力してください..."></textarea>
												  	</div>

											      </div>
											      <div class="modal-footer">
											      	<div class="spinner pull-left"></div>
											        <button type="button" class="btn btn-red" data-dismiss="modal">クローズ</button>
											        <?php wp_nonce_field('handle_contact_form', 'nonce_contact_form');?>
											        <button type="submit" class="btn btn-default"><?php _e('送信', 'careers'); ?></button>
											      </div>

										      </form>
										    </div>
										  </div>
										</div>

										<ul class="social-icons pull-right">
											<li><span><?php _e('共有する', 'careers'); ?></span></li>
											<li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>" class="btn btn-gray fa fa-facebook" target="_blank"></a></li>
											<li><a href="http://twitthis.com/twit?url=<?php the_permalink(); ?>" class="btn btn-gray fa fa-twitter" target="_blank"></a></li>
											<li><a href="https://plus.google.com/share?url=<?php the_permalink();?>" class="btn btn-gray fa fa-google-plus" target="_blank"></a></li>
										</ul>
									</div>
								</div> <!-- end .candidates-single-item -->
				</div> <!-- end .page-content -->
				<?php
					else:
					    _e('会社は見つかりませんでした。', 'careers');
					endif;
				?>
			</div> <!-- end .row -->
        </div> <!-- end .container -->
    </div> <!-- end #page-content -->

<?php

wp_reset_postdata();
//get_sidebar();
get_footer();
