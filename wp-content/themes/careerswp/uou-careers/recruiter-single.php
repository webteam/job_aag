<?php get_header(); ?>
	<div id="page-content">
        <div class="container">
    		<div class="row">
    			<?php  	if ( have_posts() ):  ?>
    				<?php the_post(); $post_id= $post->ID; $author_id= $post->post_author; ?>
    				<?php
						$website          = get_post_meta(get_the_ID() , 'user_url' , true );
						$contact_email    = get_post_meta(get_the_ID() , 'contact_email' , true );
						$legal_entity     = get_post_meta(get_the_ID() , '_company_contact_legal_entity' , true );
						$contact_turnover = get_post_meta(get_the_ID() , '_company_contact_turnover' , true );
						$total_employee   = get_post_meta(get_the_ID() , '_company_contact_employee_total' , true );
						$contact_phone    = get_post_meta(get_the_ID() , '_company_contact_phone' , true );
						$contact_fax      = get_post_meta(get_the_ID() , '_company_contact_fax' , true );
						
						$address          = get_post_meta($post_id , '_company_contact_address' , true );
						$city_state       = get_post_meta($post_id , '_company_contact_city_state' , true );
						$country          = get_post_meta($post_id , '_company_contact_country' , true );
						$zip              = get_post_meta($post_id , '_company_contact_post_code' , true );
						$location         = $address . ', ' . $city_state . ', ' . $country . ', ' . $zip;
						
						$opening_mon_fri  = get_post_meta(get_the_ID() , '_company_opening_hours_monday_friday' , true );
						$opening_sat      = get_post_meta(get_the_ID() , '_company_opening_hours_saturday' , true );
						$opening_sun      = get_post_meta(get_the_ID() , '_company_opening_hours_sunday' , true );
						
						// Social media
						$facebook         = get_post_meta(get_the_ID() , '_recruiter_facebook' , true );
						$twitter          = get_post_meta(get_the_ID() , '_recruiter_twitter' , true );
						$google           = get_post_meta(get_the_ID() , '_recruiter_google' , true );
						$linkedin         = get_post_meta(get_the_ID() , '_recruiter_linkedin' , true );
						$youtube          = get_post_meta(get_the_ID() , '_recruiter_youtube' , true );
						$instagram        = get_post_meta(get_the_ID() , '_recruiter_instagram' , true );
    				?>
				<div class="col-sm-4 page-sidebar">
					<aside>
						<div class="widget sidebar-widget white-container candidates-single-widget">
							<div class="widget-content">
								<div class="thumb">
									<?php (  has_post_thumbnail() ) ? the_post_thumbnail( 'full' ) : careers_theme_profile_placeholder_image(); ?>
								</div>

								<h5 class="bottom-line"><?php _e('リクルーターの詳細', 'careers'); ?></h5>

								<table>
									<tbody>
										<tr>
											<td><?php _e('名前', 'careers'); ?></td>
											<td><?php the_title(); ?></td>
										</tr>

										<tr>
											<td><?php _e('住所', 'careers'); ?></td>
											<td><?php echo esc_attr($location); ?></td>
										</tr>

										<tr>
											<td><?php _e('電話番号', 'careers'); ?></td>
											<td><?php echo esc_attr($contact_phone); ?></td>
										</tr>

										<tr>
											<td><?php _e('ファクス', 'careers'); ?></td>
											<td><?php echo esc_attr($contact_fax); ?></td>
										</tr>

										<tr>
											<td><?php _e('メール', 'careers'); ?></td>
											<td><a href="mailto:<?php echo esc_attr($contact_email); ?>"><?php echo esc_attr($contact_email); ?></a></td>
										</tr>

										<tr>
											<td><?php _e('ウェブサイト', 'careers'); ?></td>
											<td><a href="<?php esc_url($website); ?>"><?php echo esc_attr($website); ?></a></td>
										</tr>
									</tbody>
								</table>

								<h5 class="bottom-line"><?php _e('営業時間', 'careers'); ?></h5>

								<table>
									<tbody>
										<tr>
											<td><?php _e('月曜〜金曜', 'careers'); ?></td>
											<td>
												<?php echo esc_attr($opening_mon_fri); ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('土曜', 'careers'); ?></td>
											<td>
												<?php echo esc_attr($opening_sat); ?>
											</td>
										</tr>

										<tr>
											<td><?php _e('日曜', 'careers'); ?></td>
											<td>
												<?php echo esc_attr($opening_sun); ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</aside>
				</div> <!-- end .page-sidebar -->

				<div class="col-sm-8 page-content">
					<div class="responsive-tabs">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profile"><?php _e('プロフィール', 'careers'); ?></a></li>
							<li><a href="#blog"><?php _e('ブログ', 'careers'); ?></a></li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane active" id="profile">
								<div class="candidates-item candidates-single-item">
									<h6 class="title"><a href="#"><?php the_title(); ?></a></h6>
									<span class="meta"><?php echo esc_attr($location); ?></span>

									<!-- <ul class="top-btns">
										<li><a href="#" class="btn btn-gray fa fa-star"></a></li>
									</ul> -->

									<ul class="social-icons clearfix">
										<?php if( !empty($facebook) ): ?><li><a href="<?php echo esc_url($facebook); ?>" class="btn btn-gray fa fa-facebook"></a></li><?php endif; ?>
										<?php if( !empty($twitter) ): ?><li><a href="<?php echo esc_url($twitter); ?>" class="btn btn-gray fa fa-twitter"></a></li><?php endif; ?>
										<?php if( !empty($google) ): ?><li><a href="<?php echo esc_url($google); ?>" class="btn btn-gray fa fa-google-plus"></a></li><?php endif; ?>
										<?php if( !empty($youtube) ): ?><li><a href="<?php echo esc_url($youtube);  ?>" class="btn btn-gray fa fa-youtube"></a></li><?php endif; ?>
										<?php if( !empty($instagram) ): ?><li><a href="<?php echo esc_url($instagram); ?>" class="btn btn-gray fa fa-instagram"></a></li><?php endif; ?>
										<?php if( !empty($linkedin) ): ?><li><a href="<?php echo esc_url($linkedin); ?>" class="btn btn-gray fa fa-linkedin"></a></li><?php endif; ?>
									</ul>

									<?php the_content(); ?>

									<hr>

									<div class="clearfix">
										<a href="#" class="btn btn-default pull-left" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope-o"></i> Contact Me</a>

										<!-- Modal -->
										<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										  <div class="modal-dialog">
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										        <h4 class="modal-title" id="myModalLabel"><?php _e('メッセージを送りください', 'careers'); ?></h4>
										      </div>
										      <form method="post" action="">

											      <div class="modal-body">

													<input type="hidden" name="to" value="<?php echo esc_attr($contact_email); ?>">

													<div class="form-group">
													    <label for="from"><?php _e('メールアドレス', 'careers'); ?></label>
													    <input type="text" id="form" name="from" class="form-control"  placeholder="メールアドレスを入力してください">
													</div>

													<div class="form-group">
												    	<label for="subject"><?php _e('件名', 'careers'); ?></label>
												    	<input type="text" id="subject" name="subject" class="form-control"  placeholder="件名を入力してください">
												  	</div>

													<div class="form-group">
												    	<label for="message"><?php _e('メッセージ', 'careers'); ?></label>
												    	<textarea name="message" id="message" rows="5" placeholder="メッセージを入力してください"></textarea>
												  	</div>

											      </div>
											      <div class="modal-footer">
											        <button type="button" class="btn btn-red" data-dismiss="modal">クローズ</button>
											        <?php wp_nonce_field('handle_contact_form', 'nonce_contact_form');?>
											        <button type="submit" class="btn btn-default"><?php _e('送信', 'careers'); ?></button>
											      </div>

										      </form>
										    </div>
										  </div>
										</div>

										<ul class="social-icons pull-right">
											<li><span><?php _e('共有する', 'careers'); ?></span></li>
											<li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>" class="btn btn-gray fa fa-facebook" target="_blank"></a></li>
											<li><a href="http://twitthis.com/twit?url=<?php the_permalink(); ?>" class="btn btn-gray fa fa-twitter" target="_blank"></a></li>
											<li><a href="https://plus.google.com/share?url=<?php the_permalink();?>" class="btn btn-gray fa fa-google-plus" target="_blank"></a></li>
										</ul>
									</div>
								</div> <!-- end .candidates-single-item -->
							</div>

							<div class="tab-pane" id="blog">
								<?php
									$candidate_blog_posts = get_posts(array(
					  					'post_type' => 'blog',
					  					'author'    => $author_id,
					  					'posts_per_page'   => -1,
					  				));

					  				if( ! empty( $candidate_blog_posts ) ) :

								  		foreach ($candidate_blog_posts as $blog_posts) : ?>
								  			<?php
								  				$blog_id = $blog_posts->ID;

								  				$date = get_the_time('j', $blog_id);
												$month = get_the_time('M', $blog_id);
												$thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id( $blog_id ) );
								  			?>

					  			  			<div class="jobs-item with-thumb">

					  			  				<?php if(!empty($thumbnail)) :?>
					  			  					<div class="thumb"><a href="<?php echo esc_url(get_the_permalink($blog_id)); ?>"><img class="post-image" src="<?php echo esc_url($thumbnail); ?>" alt=""></a></div>
					  			  				<?php endif; ?>
					  			  				<div class="clearfix visible-xs"></div>
												<div class="date"><?php echo esc_attr($date);?> <span><?php echo esc_attr($month); ?></span></div>

												<h4 class="title"><a href="<?php echo esc_url(get_the_permalink($blog_id)); ?>"><?php echo esc_attr($blog_posts->post_title); ?></a></h4>

												<span class="meta"></span>

												<ul class="top-btns">
													<li><a href="<?php echo esc_url(get_the_permalink($blog_id)); ?>" class="btn btn-gray fa fa-link"></a></li>
												</ul>

												<p class="description">
													<?php
														apply_filters('the_content', $blog_posts->post_content);
														$content =  apply_filters('the_content', $blog_posts->post_content);

														$trimmed_content = wp_trim_words( $content, 20 );

														echo esc_attr($trimmed_content);
													?>
												</p>

												<div class="clearfix"></div>

											</div>

								  		<?php endforeach; ?>

							  		<?php  else : ?>
							  			<article class="blog-post with-thumb no-posts">
							  				<h6 class="title"><?php _e('ブログの投稿は見つかりませんでした！', 'careers'); ?></h6>
							  			</article>
							  		<?php endif; ?>
							</div>
						</div> <!-- end .tab-content -->
					</div> <!-- end .responsive-tabs -->

				</div> <!-- end .page-content -->
				<?php
					else:
					    _e('ポストが見つかりませんでした', 'careers');
					endif;
				?>
			</div> <!-- end .row -->
        </div> <!-- end .container -->
    </div> <!-- end #page-content -->

<?php

wp_reset_postdata();
//get_sidebar();
get_footer();
