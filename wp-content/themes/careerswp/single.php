<?php get_header() ?>

<div id="page-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 page-content">
        
        <?php get_template_part( 'framework/template/content', 'single' ); ?>

      </div> <!-- end .page-content -->

      <div class="col-sm-4 page-sidebar">
        <aside>
          <div class="white-container mb0">
            <?php get_sidebar() ?>
          </div>
        </aside>
      </div> <!-- end .page-sidebar -->
    </div>
  </div> <!-- end .container -->
</div>

<?php get_footer() ?>