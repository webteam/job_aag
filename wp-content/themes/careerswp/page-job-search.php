<?php 
  $args = array(
    'post_type' => 'job',
    'posts_per_page' => -1
  );
  $posts = get_posts( $args );
  $jobs = array();
  foreach ($posts as  $post){
    $post_id = $post->ID;
    // Company
    $company_id   = get_post_meta( $post_id, 'company_id', true );
    $company_post =  get_post( $company_id, ARRAY_A);
    $company_name = $company_post['post_title'];
    $company_url  = home_url( '/company/' .$company_post['post_name']);


    // Job expiry
    $date        = get_post_meta( $post_id, 'expire', true );
    $date_format = get_option( 'date_format' );
    $expire = date( $date_format, strtotime( $date ) );
    $arr = array();
    $locations = get_the_terms( $post_id, 'location' );
    $locationObj = array();
    $filter_location = '';
    $filter_industry_role = '';
    if(isset($locations) && is_array($locations)){
      foreach ($locations as $location) {
        if($location->parent != 0) {
            $arr[] = $location;
          }
          $locationObj[] = array( 'id' => $location->term_id, 'name' => $location->name, 'slug'=> $location->slug);
          $filter_location .= $location->name." ";
        }
        if( !empty( $arr[0] ) && !empty($arr[1]) ) {
          if($arr[0]->term_id == $arr[1]->parent) {
            $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
          } else {
            $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
          }
        } else {
          $state_country = $arr[0]->name;
        }
    }
    $job_types = uou_getTaxonomyObj( $post_id, 'job_type' );
    
    $industrys = uou_getTaxonomyObj( $post_id, 'industry' );
    foreach ($industrys as $industry) {
        $filter_industry_role .= $industry['name']." ";
    }

    $roles = uou_getTaxonomyObj( $post_id, 'role' );
    foreach ($roles as $role) {
        $filter_industry_role .= $role['name']." ";
    }
    
    if( ! empty( $company_id )) {

      $thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $company_id ) );
      $thumb_url = $company_url;

    }
    $now = time();
    $postDate = strtotime($post->post_date);
    $jobs[] = array(

      'id'                => $post_id,
      'title'             => $post->post_title,
      'excerpt'           => uou_get_post_excerpt($post->post_content),
      'content'           => apply_filters('the_content', $post->post_content),
      'post_date'         => $post->post_date,
      'post_author'       => $post->post_author,
      'guid'              => $post->guid,
      'comment_count'     => $post->comment_count,
      'state_country'     => $state_country,
      'filter_location'   => $filter_location,
      'filter_industry_role'=>$filter_industry_role,
      'location'          => $locationObj,
      'industry'          => $industrys,
      'job_type'          => $job_types,
      'role'              => $roles,
      'application_email' => get_post_meta($post_id, 'application_email', true),
      'salary'            => get_post_meta($post_id, 'monthly_salary', true),
      'postday'           => floor(($now - $postDate)/86400),
      'post_month'        => get_the_time('M', $post_id),
      'post_day'          => get_the_time('j', $post_id),
      'expire'            => $expire,
      'company_id'        => $company_id,
      'company_name'      => $company_name,
      'thumb_src'         => (!empty($thumb_src)) ? $thumb_src : careers_theme_profile_placeholder_image_url(),
      'thumb_url'         => $thumb_url,
      'hr_id'             => get_post_meta($post_id, 'hr_id', true),
      'recruiter_id'      => get_post_meta($post_id, 'recruiter_id', true)
    );
    $state_country = "";
    $arr = array();

  }
//echo "<pre>";print_r($jobs);echo "</pre>";
  

  // $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  // $args = array( 
  //     // 'posts_per_page' => 1,
  //     'paged' => $paged, 
  //     'post_type' => 'job'
  // );
  // $jobs = new WP_Query($args);
?>


<?php
/*
* Template Name: Job Search page
* Description: A advaced filtering page template for job searching
*/
get_header(); 
wp_localize_script( 'ng-controllers', 'listing', array( 'jobs' => $jobs ) );
?>

<div id="page-content">
  <div class="container" ng-controller = "jobListing">
    <div class="row">
      <div class="col-sm-4 page-sidebar">
        <?php 
          get_template_part( 'framework/template/sidebar/jobs-sidebar', '' ); 
        ?>
      </div> <!-- end .page-sidebar -->

      <div class="col-sm-8 page-content">
        <div id="jobs-page-map" class="white-container"></div>

        <div class="title-lines">
          <h3 class="mt0"><?php _e('仕事一覧', 'careers') ?></h3>
        </div>

        <div class="clearfix mb30">
          <ul class="jobs-view-toggle pull-left">
            <li><a href="#" data-layout="with-thumb" class="btn btn-gray fa fa-th-list active"></a></li>
            <li><a href="#" data-layout="" class="btn btn-gray fa fa-list"></a></li>
            <li><a href="#" data-layout="compact" class="btn btn-gray fa fa-align-justify"></a></li>
          </ul>

          <select class="form-control pull-left" ng-model="sortKey">
            <option value="">並び替え</option>
            <option value="title">タイトル</option>
            <option value="salary">給与側</option>
            <option value="postday">投稿日側</option>
            <option value="state_country">都道府県</option>
          </select>

          <div class="clearfix">
            <dir-pagination-controls boundary-links="true" pagination-id="joblist" on-page-change="pageChangeHandler(newPageNumber)" ></dir-pagination-controls>
            
          </div>
        <div class="jobs-item with-thumb" dir-paginate="job in jobs | dataFilter:filterItem | filter:search | itemsPerPage: pageSize: 'joblist' | orderBy: sortKey" current-page="currentPage" pagination-id="joblist">
          <div class="thumb"><a href="{{ job.thumb_url }}">
            <img ng-src="{{ job.thumb_src }}" alt=""></a>
          </div>
          <div class="clearfix visible-xs"></div>
          <div class="date">{{ job.post_day }} <span>{{ job.post_month }}</span></div>
          <h6 class="title"><a href="{{ job.guid }}">{{ job.title }}</a></h6>
          <span class="meta" ng-bind-html="renderHtml(job.state_country)"></span>
          <ul class="top-btns">
            <li><a href="#" class="btn btn-gray fa fa-plus toggle"></a></li>
            <li><a href="" class="btn btn-gray fa fa-star" ng-click="saveAsBookmark(job.id)"></a></li>
            <li><a href="{{ job.guid }}" class="btn btn-gray fa fa-link"></a></li>
          </ul>
          <p class="description">{{job.excerpt}}</p>
          <div class="content" ng-bind-html="renderHtml(job.content)"></div>
        </div>
        <div class="clearfix">
          <dir-pagination-controls boundary-links="true" pagination-id="joblist" on-page-change="pageChangeHandler(newPageNumber)" ></dir-pagination-controls>
        </div>
      </div> <!-- end .page-content -->
    </div>
  </div> <!-- end .container -->

</div> <!-- end #page-content -->

<?php get_footer(); ?>