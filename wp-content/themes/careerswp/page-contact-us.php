<?php
/*
 * Template Name: Contact Us Page
 * Description: A Contact Us Page Template for careers theme
 */
get_header(); ?>
  <div id="page-content">
    <div class="container">
      <div class="row">
          <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
              <div class="col-sm-8 page-content">
                
                <div id="contact-page-map" class="white-container"></div>

                <div class="white-container mb0">
                  <div class="row">
                    <?php
                        $temp = get_post_meta($post->ID, '_meta_box_contact', true);
                        $count = count($temp);
                        $location = array();
                        $marker = IMAGES . '/marker.png';

                        for ($i = 0; $i < $count; $i++) {
                          extract($temp[$i]);
                          
                          $title = $temp[$i]['_title'];
                          $address = $temp[$i]['_address'];
                          $lat = $temp[$i]['_lat'];
                          $lon = $temp[$i]['_lon'];
                          $phone = $temp[$i]['_phone'];
                          $email = $temp[$i]['_email'];

                          $location[] = array(
                            'lat' => $lat,
                            'lon' => $lon,
                            'title' => $title,
                            'html' => '<strong>' . $title . '</strong><br>' . $address,
                            'icon' => $marker
                          );
                        ?>
                    <div class="col-sm-6">
                          <h5 class="bottom-line mt10"><?php echo esc_attr($title); ?></h5>
                          <address>
                            <?php echo apply_filters('the_excerpt', $address); ?>
                          </address>

                          <p>
                            <?php _e('電話番号: ', 'careers'); ?><a href="tel:<?php echo esc_attr($phone); ?>"><?php echo esc_attr($phone); ?></a> <br>
                            <?php _e('Eメール: ', 'careers'); ?><a href="mailto:<?php echo esc_attr($email); ?>"><?php echo esc_attr($email); ?></a>
                          </p>
                    </div>

                    <?php } ?>
                  </div>
                </div>
              
              </div> <!-- end .page-content -->

              <div class="col-sm-4 page-sidebar">
                <aside>
                  <div class="widget sidebar-widget white-container contact-form-widget">
                    <h5 class="widget-title">
                      <?php echo esc_attr(get_post_meta($post->ID, '_meta_box_contact_form_title', true)); ?>
                    </h5>

                    <div class="widget-content">
                      <p><?php echo esc_attr(get_post_meta($post->ID, '_meta_box_contact_form_description', true)); ?></p>

                      <?php echo do_shortcode(get_post_meta($post->ID, '_meta_box_contact_form_widget', true)); ?>
                    </div>
                  </div>
                </aside>
              </div> <!-- end .page-sidebar -->
            </div>

          <?php endwhile; endif; ?>

          <script>
            new Maplace({
              map_div: '#contact-page-map',
              controls_type: 'list',
              map_options: {
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                zoom: 14
              },
              locations: <?php echo json_encode($location); ?>
            }).Load();
          </script>
      </div> <!-- end .row -->
    </div> <!-- end .container -->
  </div> <!-- end #page-content -->

<?php get_footer(); ?>