<?php
/*
 * Template Name: Terms & Conditions Page
 * Description: Terms & Conditions Page Template for careers theme
 */
get_header(); ?>

	<div id="page-content">
		<div class="container">
			<div class="row">
				<?php if( have_posts() ) : ?><?php the_post(); ?>

					<div class="col-sm-8 page-content">
						<div class="white-container mb0">

							<?php the_content(); ?>
							
						</div>
					</div>

					<div class="col-sm-4 page-sidebar">
						<aside>
							<?php
								global $post;
								if ($post->post_parent) {
									$pages = get_pages(array("child_of"=>$post->ID));

									if ($pages) {
										array_unshift($pages, $post);
									}
								}

								$page_id = $post->post_parent;

								$parent =  get_page($page_id);
								$pages = get_pages(array("child_of"=>$page_id));
								if ($parent) {
									array_unshift($pages, $parent);	
								}
								
								
							?>
							<div class="widget sidebar-widget white-container links-widget">
								<ul>
									<?php foreach ($pages as $page) : ?>
										<li class="<?php echo ($post->ID === $page->ID) ? 'active' : '' ?>"><a href="<?php echo esc_url(get_the_permalink($page->ID)); ?>"><?php echo esc_attr($page->post_title); ?></a></li>
									<?php endforeach ?>
								</ul>
							</div>
						</aside>
					</div>

				<?php endif; ?>
			</div>
		</div> <!-- end .container -->
	</div> <!-- end #page-content -->
<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>