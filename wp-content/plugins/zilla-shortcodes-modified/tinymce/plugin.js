(function($) {
"use strict";   
 

              


 			//Shortcodes
            tinymce.PluginManager.add( 'zillaShortcodes', function( editor, url ) {

				editor.addCommand("zillaPopup", function ( a, params )
				{
					var popup = params.identifier;
					tb_show("Insert Shortcode", url + "/popup.php?popup=" + popup + "&width=" + 800);
				});
     
                editor.addButton( 'zilla_button', {
                    type: 'splitbutton',
                  //  image: "https://dl.dropboxusercontent.com/u/37351231/icon.png",
                    icon: false,
                    text:'Shortcode',
					title:  'Shortcodes',
					onclick : function(e) {},
					menu: [

							{text: 'Accordion',onclick:function(){
								editor.execCommand("zillaPopup", false, {title: 'Accordion',identifier: 'accordion'})
								 
							}},
							{text: 'Alert',onclick:function(){
								editor.execCommand("zillaPopup", false, {title: 'Alert',identifier: 'alert'})
								 
							}},

							{text: 'Buttons',onclick:function(){
								editor.execCommand("zillaPopup", false, {title: 'Buttons',identifier: 'button'})
								 //editor.insertContent('Title: ');
							}},
							{

         


								            text: 'Columns',
								            menu: [
								            	{text: 'row', onclick: function() {editor.insertContent('[row] [/row]');}},
								                {text: 'One Half', onclick: function() {editor.insertContent('[one_half] your content here [/one_half]');}},
								                {text: 'One Half Last', onclick: function() {editor.insertContent('[one_half_last]your content here[/one_half_last]');}},
								                {text: 'One Third', onclick: function() {editor.insertContent('[one_third] your content here [/one_third]');}},
								                {text: 'One Third Last', onclick: function() {editor.insertContent('[one_third_last] your content here [/one_third_last]');}},
								                {text: 'One Fourth', onclick: function() {editor.insertContent('[one_fourth] your content here [/one_fourth]');}},
								                {text: 'One Fourth Last', onclick: function() {editor.insertContent('[one_fourth_last] your content here [/one_fourth_last]');}},
								                {text: 'Three Fourth', onclick: function() {editor.insertContent('[three_fourth] your content here [/three_fourth]');}},
								                {text: 'Three Fourth Last', onclick: function() {editor.insertContent('[three_fourth_last] your content here [/three_fourth_last]');}},
								                {text: 'One Fourth Second', onclick: function() {editor.insertContent('[one_fourth_second] your content here [/one_fourth_second]');}},
								                {text: 'One Fourth Third', onclick: function() {editor.insertContent('[one_fourth_third] your content here [/one_fourth_third]');}},
								                {text: 'One Half Second', onclick: function() {editor.insertContent('[one_half_second] your content here [/one_half_second]');}},
								                {text: 'One Third Second', onclick: function() {editor.insertContent('[one_third_second] your content here [/one_third_second]');}},
								                {text: 'One Column', onclick: function() {editor.insertContent('[one_column] your content here [/one_column]');}},
								               
								            ]
							},

							{text: 'List',onclick:function(){
								editor.execCommand("zillaPopup", false, {title: 'List',identifier: 'list'})
								 //editor.insertContent('Title: ');
							}},
							{text: 'Tabs',onclick:function(){
								editor.execCommand("zillaPopup", false, {title: 'Tabs',identifier: 'tabs'})
								 //editor.insertContent('Title: ');
							}},

							{text: 'Video',onclick:function(){
								editor.execCommand("zillaPopup", false, {title: 'Video',identifier: 'video'})
								 //editor.insertContent('Title: ');
							}},




					]

                
        	  });
         
          });
         

 
})(jQuery);



