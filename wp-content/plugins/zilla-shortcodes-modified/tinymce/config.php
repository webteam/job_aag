<?php

/*-----------------------------------------------------------------------------------*/
/*	Button Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['button'] = array(
	'no_preview' => true,
	'params' => array(
		'url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button URL', 'textdomain'),
			'desc' => __('Add the button\'s url eg http://example.com', 'textdomain')
		),
		'color' => array(
			'type' => 'select',
			'label' => __('Button Style', 'textdomain'),
			'desc' => __('Select the button\'s style, ie the button\'s colour', 'textdomain'),
			'options' => array(
				'default' => 'Default',
				'red' => 'Red',
				'gray' => 'Gray',
				'green' => 'Green',

			)
		),
		'target' => array(
			'type' => 'select',
			'label' => __('Button Target', 'textdomain'),
			'desc' => __('_self = open in same window. _blank = open in new window', 'textdomain'),
			'options' => array(
				'_self' => '_self',
				'_blank' => '_blank'
			)
		),
		'icon' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Icon', 'textdomain'),
			'desc' => __('http://fortawesome.github.io/Font-Awesome/icons/', 'textdomain'),
		),
		'size' => array(
			'type' => 'select',
			'label' => __('Button Size', 'textdomain'),
			'desc' => __('Select button size', 'textdomain'),
			'options' => array(
				'' => '',
				'large' => 'Large'
			)
		),

		'content' => array(
			'std' => 'Button Text',
			'type' => 'text',
			'label' => __('Button\'s Text', 'textdomain'),
			'desc' => __('Add the button\'s text', 'textdomain'),
		)
	),
	'shortcode' => '[button url="{{url}}" icon="{{icon}}" color="{{color}}" target="{{target}}" size="{{size}}"] {{content}} [/button]',
	'popup_title' => __('Insert Button Shortcode', 'textdomain')
);

// [alert title="This is a default message box" type="error"]
// Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, soluta!
// [/alert]
/*-----------------------------------------------------------------------------------*/
/*	Alert Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['alert'] = array(
	'no_preview' => true,
	'params' => array(
		'type' => array(
			'type' => 'select',
			'label' => __('Alert Type', 'textdomain'),
			'desc' => __('Select the alert\'s style, ie the alert\'s success', 'textdomain'),
			'options' => array(
				'default' => 'Default',
				'info' => 'Info',
				'success' => 'Success',
				'warning' => 'Warning',
				'error' => 'Error',

			)
		),
		'title' => array(
			'std' => 'Title',
			'type' => 'text',
			'label' => __('ALert Title', 'textdomain'),
			'desc' => __('Your alert title goes here', 'textdomain'),
		),

		'content' => array(
			'std' => 'Subtitle',
			'type' => 'textarea',
			'label' => __('Subtitle Text', 'textdomain'),
			'desc' => __('Add alert subtitle text', 'textdomain'),
		)
	),
	'shortcode' => '[alert title="{{title}}" type="{{type}}"] {{content}} [/alert]',
	'popup_title' => __('Insert Alert Shortcode', 'textdomain')
);

/*-----------------------------------------------------------------------------------*/
/*	video Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['video'] = array(
	'no_preview' => true,
	'params' => array(

		'host' => array(

			'type' => 'select',
			'label' => __('Host Name', 'textdomain'),
			'desc' => __('Select the host name', 'textdomain'),
			'options' => array(
				'youtube'     => 'YouTube',
				'vimeo'       => 'Vimeo',
				'dailymotion' => 'Dailymotion',
			)
		),
		'video_id' => array(
			'std' => 'Video ID',
			'type' => 'text',
			'label' => __('Video id', 'textdomain'),
			'desc' => __('add the video id', 'textdomain'),
		)
	),
	'shortcode' => '[uou_video type="{{host}}" video_id="{{video_id}}" ][/uou_video]',
	'popup_title' => __('Insert Video Shortcode', 'textdomain')
);



/*-----------------------------------------------------------------------------------*/
/*	Tabs Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['tabs'] = array(
    'params' => array(
		'type' => array(
			'type' => 'select',
			'label' => __('Tab Direction', 'textdomain'),
			'desc' => __('Select the direction of Tab on page load', 'textdomain'),
			'options' => array(
				'top' => 'Top',
				'vertical' => 'Vertical',
				
			)
		),

   	),
    'no_preview' => true,
    'shortcode' => '[tabs type="{{type}}"] {{child_shortcode}}  [/tabs]',
    'popup_title' => __('Insert Tab Shortcode', 'textdomain'),
    
    'child_shortcode' => array(
        'params' => array(
        	'type' => array(
				'type' => 'select',
				'label' => __('Tab Active', 'textdomain'),
				'desc' => __('please select this just for one', 'textdomain'),
				'options' => array(
					'' => '',
					'true' => 'true',
					
				),
			),
            'title' => array(
                'std' => 'Title',
                'type' => 'text',
                'label' => __('Tab Title', 'textdomain'),
                'desc' => __('Title of the tab', 'textdomain'),
            ),
            'content' => array(
                'std' => 'Tab Content',
                'type' => 'textarea',
                'label' => __('Tab Content', 'textdomain'),
                'desc' => __('Add the tabs content', 'textdomain')
            )
        ),
        'shortcode' => '[tab active="{{type}}" title="{{title}}"] {{content}} [/tab]',
        'clone_button' => __('Add Tab', 'textdomain')
    )
);



// [accordion ]
// [aitem title="Web Developer at Envato" subtitle="September 2007 - June 2013"]





/*-----------------------------------------------------------------------------------*/
/*	accordion Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['accordion'] = array(
    'params' => array(),
    'no_preview' => true,
    'shortcode' => '[accordion] {{child_shortcode}}  [/accordion]',
    'popup_title' => __('Insert Accordion Shortcode', 'textdomain'),
    
    'child_shortcode' => array(
        'params' => array(
        	'type' => array(
				'type' => 'select',
				'label' => __('Accordion Active', 'textdomain'),
				'desc' => __('please select this just for one', 'textdomain'),
				'options' => array(
					'' => '',
					'true' => 'true',
					
				),
			),
            'title' => array(
                'std' => 'Title',
                'type' => 'text',
                'label' => __('Title', 'textdomain'),
                'desc' => __('', 'textdomain'),
            ),
            'content' => array(
                'std' => 'Content',
                'type' => 'textarea',
                'label' => __('Content', 'textdomain'),
                'desc' => __('Add content', 'textdomain')
            )

        ),
        'shortcode' => '[aitem title="{{title}}" active="{{type}}"] {{content}} [/aitem]',
        'clone_button' => __('Add Accordion', 'textdomain')
    )
);

// [list type="bullet"]
// [item]asdfsdlfja[/item]
// [item]asdfsdlfja[/item]
// [item]asdfsdlfja[/item]

// [/list]



/*-----------------------------------------------------------------------------------*/
/*	list Config
/*-----------------------------------------------------------------------------------*/


$zilla_shortcodes['list'] = array(
    'params' => array(
		'type' => array(
			'type' => 'select',
			'label' => __('List Style', 'textdomain'),
			'desc' => __('Select list style', 'textdomain'),
			'options' => array(
				'bullet' => 'Bullet',
				'number' => 'Number',
				
			)
		),
    ),
    'no_preview' => true,
    'shortcode' => '[list type="{{type}}"] {{child_shortcode}}  [/list]',
    'popup_title' => __('Insert Timeline Shortcode', 'textdomain'),
    
    'child_shortcode' => array(
        'params' => array(

            'content' => array(
                'std' => 'Content',
                'type' => 'text',
                'label' => __('Content', 'textdomain'),
                'desc' => __('', 'textdomain'),
            ),



        ),
        'shortcode' => '[item] {{content}} [/item]',
        'clone_button' => __('Add list item', 'textdomain')
    )
);







/*-----------------------------------------------------------------------------------*/
/*	Columns Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['columns'] = array(
	'params' => array(),
	'shortcode' => ' {{child_shortcode}} ', // as there is no wrapper shortcode
	'popup_title' => __('Insert Columns Shortcode', 'textdomain'),
	'no_preview' => true,
	
	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'column' => array(
				'type' => 'select',
				'label' => __('Column Type', 'textdomain'),
				'desc' => __('Select the type, ie width of the column.', 'textdomain'),
				'options' => array(
					'zilla_one_third' => 'One Third',
					'zilla_one_third_last' => 'One Third Last',
					'zilla_two_third' => 'Two Thirds',
					'zilla_two_third_last' => 'Two Thirds Last',
					'zilla_one_half' => 'One Half',
					'zilla_one_half_last' => 'One Half Last',
					'zilla_one_fourth' => 'One Fourth',
					'zilla_one_fourth_last' => 'One Fourth Last',
					'zilla_three_fourth' => 'Three Fourth',
					'zilla_three_fourth_last' => 'Three Fourth Last',
					'zilla_one_fifth' => 'One Fifth',
					'zilla_one_fifth_last' => 'One Fifth Last',
					'zilla_two_fifth' => 'Two Fifth',
					'zilla_two_fifth_last' => 'Two Fifth Last',
					'zilla_three_fifth' => 'Three Fifth',
					'zilla_three_fifth_last' => 'Three Fifth Last',
					'zilla_four_fifth' => 'Four Fifth',
					'zilla_four_fifth_last' => 'Four Fifth Last',
					'zilla_one_sixth' => 'One Sixth',
					'zilla_one_sixth_last' => 'One Sixth Last',
					'zilla_five_sixth' => 'Five Sixth',
					'zilla_five_sixth_last' => 'Five Sixth Last'
				)
			),
			'content' => array(
				'std' => '',
				'type' => 'textarea',
				'label' => __('Column Content', 'textdomain'),
				'desc' => __('Add the column content.', 'textdomain'),
			)
		),
		'shortcode' => '[{{column}}] {{content}} [/{{column}}] ',
		'clone_button' => __('Add Column', 'textdomain')
	)
);

?>