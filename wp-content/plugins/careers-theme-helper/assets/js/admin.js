(function($){

    $( "#meta_box_contact" ).hide();
    $( "#meta_box_contact_form" ).hide();

    if ($('#page_template').find(':selected').val() == "page-contact-us.php") {
        $( "#meta_box_contact" ).show();
        $( "#meta_box_contact_form" ).show();
        $( "#postdivrich" ).hide();
    } else {
        $( "#meta_box_contact" ).hide();
        $( "#meta_box_contact_form" ).hide();
        $( "#postdivrich" ).show();
    }

    $('#page_template').change(function () {
        if ($(this).find(':selected').val() == "page-contact-us.php") {
            $( "#meta_box_contact" ).show();
            $( "#meta_box_contact_form" ).show();
            $( "#postdivrich" ).hide();
        } else {
            $( "#meta_box_contact" ).hide();
            $( "#meta_box_contact_form" ).hide();
            $( "#postdivrich" ).show();
        }
    });

})(jQuery);