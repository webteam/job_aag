<?php
/**
 * Plugin Name: Careers theme helper
 * Plugin URI:  http://uou.ch
 * Description: Complete job management app on top of WordPress
 * Author:      Faysal Haque
 * Author URI:  http://faysalhaque.github.io
 * Version:     0.1
 * Text Domain: uou-careers
 * Domain Path: /languages/
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
    exit;


if ( ! class_exists( 'CareersThemeHelper' ) ) :


Class CareersThemeHelper {
	public $version = '0.0.1';

    protected static $_instance = null;

    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    public function __construct(){
    	define( 'CTH_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
		define( 'CTH_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
		define( 'CTH_FILE', __FILE__ );

        define( 'CTH_JS_PATH', CTH_URL.'/assets/js/' );

        require_once( CTH_DIR . '/includes/class-cth-post-type.php' );
        require_once( CTH_DIR . '/includes/shortcodes.php' );
		require_once( CTH_DIR . '/includes/widgets.php' );
        require( CTH_DIR . '/includes/functions.php' );

        add_action('init', array($this, 'cth_admin_load_scripts'));


        add_action('admin_enqueue_scripts', array($this, 'cth_admin_load_scripts') );
    }

    public function cth_admin_load_scripts() {

        wp_register_script( 'uou-admin', CTH_JS_PATH . 'admin.js', array('jquery'), $ver = false, true );
        wp_enqueue_script( 'uou-admin' );
    }

    /**
     * Localization
     * @return  void
     */
    public function cth_load_textdomain() {
        load_plugin_textdomain( 'careers', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }
}

endif;



function CTH() {
    return CareersThemeHelper::instance();
}

// Global for backwards compatibility.
$GLOBALS['careersthemehelper'] = CTH();