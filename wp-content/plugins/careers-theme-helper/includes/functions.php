<?php
// Create Slider
 
function careers_slider_template() {

    // Query Arguments
    $args = array(
        'post_type' => 'slide',
        'posts_per_page' => 5
    );  

    // The Query
    $the_query = new WP_Query( $args );

    // Check if the Query returns any posts
    if ( $the_query->have_posts() ) {

        // Start the Slider ?>
        <div class="flexslider header-slider">
            <ul class="slides">

                <?php
                // The Loop
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <?php $slide_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) ); ?>
                    <li>
                        <img src="<?php print IMAGES; ?>transparent.png" alt="">
                        <div data-image="<?php echo esc_url($slide_image) ?>"></div>
                    </li>
                <?php endwhile; ?>

            </ul><!-- .slides -->
        </div><!-- .flexslider -->

    <?php }
 
    // Reset Post Data
    wp_reset_postdata();
}