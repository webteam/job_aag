<?php
// Creating the widget 
class careers_popular_jobs_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'careers_popular_jobs_widget', 

			// Widget name will appear in UI
			__('Careers : Popular Jobs', 'careers'), 

			// Widget description
			array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'careers' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$no_of_jobs = $instance[ 'no_of_jobs' ];
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title']; ?>
			<ul class="footer-links">
							
			<?php
			// This is where you run the code and display the output
			$popularpost = new WP_Query( array( 'post_type' => 'job', 'posts_per_page' => $no_of_jobs, 'meta_key' => 'careers_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
			while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>

				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

			<?php endwhile; ?>
			</ul> 
		<?php echo $args['after_widget'];
	}
		
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) && isset( $instance[ 'no_of_jobs' ] ) ) {
			$title = $instance[ 'title' ];
			$no_of_jobs = $instance[ 'no_of_jobs' ];
		}
		else {
			$title = __( 'Popular Jobs', 'careers' );
			$no_of_jobs = 4;
		}
	// Widget admin form
	?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'no_of_jobs' ); ?>"><?php _e( 'Number of jobs:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'no_of_jobs' ); ?>" name="<?php echo $this->get_field_name( 'no_of_jobs' ); ?>" type="number" value="<?php echo esc_attr( $no_of_jobs ); ?>" />
	</p>
	<?php
	}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
	$instance = array();
	$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	$instance['no_of_jobs'] = ( ! empty( $new_instance['no_of_jobs'] ) ) ? strip_tags( $new_instance['no_of_jobs'] ) : '';
	return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function careers_popular_jobs_load_widget() {
	register_widget( 'careers_popular_jobs_widget' );
}
add_action( 'widgets_init', 'careers_popular_jobs_load_widget' );