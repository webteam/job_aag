<?php



function uou_fixing($content){

    $block = join("|",array('list','item','tabs','tab','accordion','aitem','titem','button','uou_video','header','row','column','one_half','one_half_last','one_third','one_third_last','two_third','two_third_last','one_fourth','one_fourth_last','three_fourth','three_fourth_last','one_fourth_second','one_fourth_third','one_half_second','one_third_second','one_column') );

    $result = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);
    $result = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$result);

    $Old     = array( '<br />', '<br>' );
    $New     = array( '','' );
    $result = str_replace( $Old, $New, $result );


    return $result;

}

add_filter('the_content', 'uou_fixing');
add_filter('the_content', 'do_shortcode', 7);



// function wrr_column_shortcode( $atts , $content = null ) {


//     extract( shortcode_atts( array(
//       "lg"          => false,
//       "md"          => false,
//       "sm"          => false,
//       "xs"          => false,
//       "offset_lg"   => false,
//       "offset_md"   => false,
//       "offset_sm"   => false,
//       "offset_xs"   => false,
//       "pull_lg"     => false,
//       "pull_md"     => false,
//       "pull_sm"     => false,
//       "pull_xs"     => false,
//       "push_lg"     => false,
//       "push_md"     => false,
//       "push_sm"     => false,
//       "push_xs"     => false,
          
//     ), $atts ) );

//     $class  = '';
//     $class .= ( $lg )             ? ' col-lg-' . $lg : '';
//     $class .= ( $md )             ? ' col-md-' . $md : '';
//     $class .= ( $sm )             ? ' col-sm-' . $sm : '';
//     $class .= ( $xs )             ? ' col-xs-' . $xs : '';
//     $class .= ( $offset_lg )      ? ' col-lg-offset-' . $offset_lg : '';
//     $class .= ( $offset_md )      ? ' col-md-offset-' . $offset_md : '';
//     $class .= ( $offset_sm )      ? ' col-sm-offset-' . $offset_sm : '';
//     $class .= ( $offset_xs )      ? ' col-xs-offset-' . $offset_xs : '';
//     $class .= ( $pull_lg )        ? ' col-lg-pull-' . $pull_lg : '';
//     $class .= ( $pull_md )        ? ' col-md-pull-' . $pull_md : '';
//     $class .= ( $pull_sm )        ? ' col-sm-pull-' . $pull_sm : '';
//     $class .= ( $pull_xs )        ? ' col-xs-pull-' . $pull_xs : '';
//     $class .= ( $push_lg )        ? ' col-lg-push-' . $push_lg : '';
//     $class .= ( $push_md )        ? ' col-md-push-' . $push_md : '';
//     $class .= ( $push_sm )        ? ' col-sm-push-' . $push_sm : '';
//     $class .= ( $push_xs )        ? ' col-xs-push-' . $push_xs : '';


//     return '<div class="'.$class.'">' . do_shortcode($content) . '</div>';


// }





/**
*######################################################################
*#  list item 
*######################################################################
*/


// Add Shortcode
function wrr_list_shortcode( $atts , $content = null ) {

	// Attributes
	$atts = extract( shortcode_atts(
		array(
			'type' => 'check',
		), $atts )
	);

	if( $type == 'check'){
		$class = "b-check-list";
	}
	else{
		$class = " ";
	}


	if ($type == "number"){
		return '<ol>'. do_shortcode($content) . '</ol>';
	}


	return '<ul class="'.$class.'">'. do_shortcode($content) . '</ul>';

}
add_shortcode( 'list', 'wrr_list_shortcode' );





function wrr_li_item_shortcode( $atts , $content = null ) {
	$atts = extract( shortcode_atts( array( 'default'=>'values' ),$atts ) );

	return '<li>'.do_shortcode($content).'</li>';
}
add_shortcode( 'item','wrr_li_item_shortcode' );



/**
*######################################################################
*#  Tabs
*######################################################################
*/


$tabs_divs = '';

function uou_tabs_group_shortcode($atts, $content = null ) {
    global $tabs_divs;

    extract(shortcode_atts(array(          
       'type' => 'top'
    ), $atts)); 

	$class= '';
	if($type == 'vertical'){
		$class= ' vertical';
	}

    $tabs_divs = '';

    $output = '<div class="responsive-tabs '.$class.'"><ul class="nav nav-tabs" ';
    $output.='>'.do_shortcode($content).'</ul>';
    $output.= '<div class="tab-content">'.$tabs_divs.'</div></div>';

    return $output;  
}  


function uou_tab_shortcode($atts, $content = null) {  
    global $tabs_divs;

    extract(shortcode_atts(array(  
        'id' => '',
        'title' => '', 
        'active' => false
    ), $atts));  

    $class= '';
    if($active == 'true'){
    	$class = ' active';
    }


    if(empty($id))
        $id = 'dummy-tab-'.rand(100,999);

    $output = '
        <li class="e-tab-title '.$class.'">
            <a href="#'.$id.'">'.$title.'</a>
        </li>
    ';

    $tabs_divs.= '<div class="tab-pane '.$class.'" id="'.$id.'">'.$content.'</div>';

    return $output;
}

add_shortcode('tabs', 'uou_tabs_group_shortcode');
add_shortcode('tab', 'uou_tab_shortcode');


/**
*######################################################################
*#  alert
*######################################################################
*/
function uou_alert_shortcode( $atts, $content = null )
{
  extract( shortcode_atts( array(
      'title' => '',
      'type' => '',
      ), $atts ) );
  
  if($type == 'default'){
    $class = " alert-default";
  }elseif($type == 'info'){
    $class = " alert-info";
  }elseif($type == 'success'){
    $class = " alert-success";
  }elseif($type == 'warning'){
    $class = " alert-warning";
  }elseif($type == 'error'){
    $class = " alert-error";
  }else{
    $class = "";
  }


  $output = '<div class="alert'. $class .'">';
  if($title){
    $output.=    '<h6>'. $title .'</h6>';
  }
  $output.=    '<p>'. $content .'</p>';
  $output.=    '<a href="#" class="close fa fa-times"></a>';
  $output.= '</div>';


  return $output;

}
add_shortcode('alert', 'uou_alert_shortcode');



function uou_button( $atts ,$content = null ) {
	$atts = extract( shortcode_atts( array('target'=>'_self', 'url'=> 'http://google.com', 'color'=>'' ,'icon' => '', 'size' => '' ),$atts ) );

	$add_icon ='';
	if(!empty($icon)){
		$add_icon='<i class="fa '.$icon.'"></i>';
	}

    if (!preg_match("~^(?:f|ht)tps?://~i", $url) ) {
        $url = "http://" . $url;
    }

	$newcolor = '';

	if($color == 'default'){	
		$newcolor = 'btn-default';
	}
	elseif($color == 'red'){
		$newcolor = 'btn-red';
	}
  elseif($color == 'gray'){
    $newcolor = 'btn-gray';
  } elseif($color == 'green'){
    $newcolor = 'btn-green';
  }
  
  $btnSize = '';
  if(!empty($size)) {
    $btnSize = 'btn-large';
  }

	$output = '<a  href="'.$url.'" target="'.$target.'" class="btn '.$newcolor.' ' . $btnSize . '">'.$add_icon.$content.'</a>';
	
	return $output;

}
add_shortcode( 'button','uou_button' );









/**
*######################################################################
*#  video  [video type="vimeo" video_id=""]
*######################################################################
*/



function uou_video($atts){
  if(isset($atts['type'])){
    switch($atts['type']){
      case 'youtube':
        return youtube($atts);
        break;
      case 'vimeo':
        return vimeo($atts);
        break;
      case 'dailymotion':
        return dailymotion($atts);
        break;
    }
  }
  return '';
}
add_shortcode('uou_video', 'uou_video');


function vimeo($atts) {
  extract(shortcode_atts(array(
    'video_id'  => '',
    'width' => false,
    'height' => false,
    'title' => 'false',
  ), $atts));

  if ($height && !$width) $width = intval($height * 16 / 9);
  if (!$height && $width) $height = intval($width * 9 / 16);
  if (!$height && !$width){
    $height = '400';
    $width = '650';
  }
  if($title!='false'){
    $title = 1;
  }else{
    $title = 0;
  }

  if (!empty($video_id) && is_numeric($video_id)){
    return "<div class='video_frame'><iframe src='http://player.vimeo.com/video/$video_id?title={$title}&amp;byline=0&amp;portrait=0' width='$width' height='$height' ></iframe></div>";
  }
}

function youtube($atts, $content=null) {
  extract(shortcode_atts(array(
    'video_id'  => '',
    'width'   => false,
    'height'  => false,
  ), $atts));
  
  if ($height && !$width) $width = intval($height * 16 / 9);
  if (!$height && $width) $height = intval($width * 9 / 16) + 25;
  if (!$height && !$width){
    $height = '400';
    $width = '650';
  }

  if (!empty($video_id)){
    return "<div class='video_frame'><iframe src='http://www.youtube.com/embed/$video_id' width='$width' height='$height'></iframe></div>";
  }
}

function dailymotion($atts, $content=null) {

  extract(shortcode_atts(array(
    'video_id'  => '',
    'width'   => false,
    'height'  => false,
  ), $atts));
  
  if ($height && !$width) $width = intval($height * 16 / 9);
  if (!$height && $width) $height = intval($width * 9 / 16);
  if (!$height && !$width){
    $height = '400';
    $width = '650';
  }

  if (!empty($video_id)){
    return "<div class='video_frame'><iframe src=http://www.dailymotion.com/embed/video/$video_id?width=$width&theme=none&foreground=%23F7FFFD&highlight=%23FFC300&background=%23171D1B&start=&animatedTitle=&iframe=1&additionalInfos=0&autoPlay=0&hideInfos=0' width='$width' height='$height'></iframe></div>";
  }
}



/**
*######################################################################
*#  accordion
*######################################################################
*/



function uou_accordion($atts, $content = null){
  extract(shortcode_atts(array(
    'id'=>''
    ), $atts));

  $content = preg_replace('/<br class="nc".\/>/', '', $content);
  $result = '<div class="accordion"><ul>';
  $result .= do_shortcode($content );
  $result .= '</ul></div>';

  return force_balance_tags( $result );
}
add_shortcode('accordion', 'uou_accordion');



function uou_accordion_item($atts, $content = null){
  extract(shortcode_atts(array(
    'active'=>false,
    'title'=>'',
    'subtitle'=>''

    ), $atts));

  $class = '';
  if($active == true) {
    $class = 'active';
  }

  $content = preg_replace('/<br class="nc".\/>/', '', $content);


  $result = '<li class="' . $class . '">';
  $result.='<a href="#">'.$title.'</a> ';
  $result.='<div>'.do_shortcode($content).'</div></li>';



  return force_balance_tags( $result );
}
add_shortcode('aitem', 'uou_accordion_item');







/**
 *######################################################################
 *#  row
 *######################################################################
 */

function xxl_theme_row($params, $content = null) {
    extract(shortcode_atts(array(
        'class' => ''
    ), $params));
    $result = '<div class="row ' . $class . '">';
    //echo '<textarea>'.$content.'</textarea>';
    $content = str_replace("]<br />", ']', $content);
    $content = str_replace("<br />\n[", '[', $content);
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('row', 'xxl_theme_row');


/**
 *######################################################################
 *#  column
 *######################################################################
 */

function xxl_theme_column($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'lg' => '',
        'mdoff' => '',
        'smoff' => '',
        'xsoff' => '',
        'lgoff' => '',
        'mdhide' => '',
        'smhide' => '',
        'xshide' => '',
        'lghide' => '',
        'mdclear' => '',
        'smclear' => '',
        'xsclear' => '',
        'lgclear' => '',
        'off'=>''
    ), $params));


    $arr = array('md', 'xs', 'sm');
    $classes = array();
    foreach ($arr as $k => $aa) {
        if (${$aa} == 12 || ${$aa} == '') {
            $classes[] = 'col-' . $aa . '-12';
        } else {
            $classes[] = 'col-' . $aa . '-' . ${$aa};
        }
    }
    $arr2 = array('mdoff', 'smoff', 'xsoff', 'lgoff');
    foreach ($arr2 as $k => $aa) {
        $nn = str_replace('off', '', $aa);
        if (${$aa} == 0 || ${$aa} == '') {
            //$classes[] = '';
        } else {
            $classes[] = 'col-' . $nn . '-offset-' . ${$aa};
        }
    }
    $arr2 = array('mdhide', 'smhide', 'xshide', 'lghide');
    foreach ($arr2 as $k => $aa) {
        $nn = str_replace('hide', '', $aa);
        if (${$aa} == 'yes') {
            $classes[] = 'hidden-' . $nn;
        }
    }
    $arr2 = array('mdclear', 'smclear', 'xsclear', 'lgclear');
    foreach ($arr2 as $k => $aa) {
        $nn = str_replace('clear', '', $aa);
        if (${$aa} == 'yes') {
            $classes[] = 'clear-' . $nn;
        }
    }
    if ($off != '') {
        $classes[] = 'col-lg-offset-'.$off;
    }

    if ($off != '') {
        $classes[] = 'col-lg-offset-'.$off;
    }
    $result = '<div class="col-lg-' . $lg . ' ' . implode(' ', $classes) . '">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('column', 'xxl_theme_column');


function xxl_theme_one_half($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-6 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . '  one-half">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_half', 'xxl_theme_one_half');

function xxl_theme_one_half_last($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-6 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' one-half-last">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_half_last', 'xxl_theme_one_half_last');

/* * *********************************************************
 * THIRD
 * ********************************************************* */

function xxl_theme_one_third($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="sc-column col-lg-4 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' ">'; //one-third
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_third', 'xxl_theme_one_third');

function xxl_theme_one_third_last($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-4 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . '  column-last">'; // one-third-last
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_third_last', 'xxl_theme_one_third_last');

function xxl_theme_two_third($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class=" col-lg-8 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' ">'; //two-third
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('two_third', 'xxl_theme_two_third');

function xxl_theme_two_third_last($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-8 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . '  column-last ">'; //two-third-last
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('two_third_last', 'xxl_theme_two_third_last');

/* * *********************************************************
 * FOURTH
 * ********************************************************* */

function xxl_theme_one_fourth($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-3 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' one-fourth">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_fourth', 'xxl_theme_one_fourth');

function xxl_theme_one_fourth_last($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-3 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' column-last one-fourth-last">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_fourth_last', 'xxl_theme_one_fourth_last');

function xxl_theme_three_fourth($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-3 ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . '  three-fourth">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('three_fourth', 'xxl_theme_three_fourth');

function xxl_theme_three_fourth_last($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-6  ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' column-last three-fourth-last">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('three_fourth_last', 'xxl_theme_three_fourth_last');

function xxl_theme_one_fourth_second($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-3  ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' one-fourth-second">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_fourth_second', 'xxl_theme_one_fourth_second');

function xxl_theme_one_fourth_third($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }

    $result = '<div class="col-lg-3  ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' one-fourth-third">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_fourth_third', 'xxl_theme_one_fourth_third');

function xxl_theme_one_half_second($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-6  ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' one-half-second">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_half_second', 'xxl_theme_one_half_second');

function xxl_theme_one_third_second($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-4  ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' one-third-second">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_third_second', 'xxl_theme_one_third_second');

function xxl_theme_one_column($params, $content = null) {
    extract(shortcode_atts(array(
        'md' => '',
        'sm' => '',
        'xs' => '',
        'off' => ''
    ), $params));
    if ($md == 12) {
        $mds = '';
    } else {
        $mds = 'col-md-' . $md;
    }
    if ($sm == 12) {
        $sms = '';
    } else {
        $sms = 'col-sm-' . $sm;
    }
    if ($xs == 12) {
        $xss = '';
    } else {
        $xss = 'col-xs-' . $xs;
    }
    $result = '<div class="col-lg-12  ' . $mds . ' ' . $sms . ' ' . $xss . ' col-lg-offset-' . $off . ' one-column">';
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}

add_shortcode('one_column', 'xxl_theme_one_column');

function careers_header($atts, $content = null) {
  extract(shortcode_atts(array(
    'type'  => '',
    'class' => '',
    'style'   => false,
  ), $atts));

  $before_title = ''; $after_title = '';

  if (empty($type)) {
    $type = 'h1';
  }

  if ($style == true) {
    $before_title = '<div class="title-lines">';
    $after_title = '</div>';
  }

  $classes = '';
  
  if (!empty($class)) {
    $classes = $class;
  }

  $result = $before_title;
  
  $result .= '<' . $type . ' class="' . $classes . '">';
  $result .= do_shortcode($content);
  $result .= '</' . $type . '>';

  $result .= $after_title;

  return $result;
}




add_shortcode('header', 'careers_header');






// function uou_theme_page_content($params, $content = null) {

//   // extract(shortcode_atts(array(

//   // ), $params);

//   $result = '<div class="col-sm-8 page-content">';
//   $result .= do_shortcode($content);
//   $result .= '</div>';

//   return $result;
// }

// add_shortcode('page_content', 'uou_theme_page_content');

// function uou_theme_page_sidebar($params, $content = null) {

//   // extract(shortcode_atts(array(

//   // ), $params));

//   $result = '<div class="col-sm-4 page-sidebar">';
//   $result .= do_shortcode($content);
//   $result .= '</div>';

//   return $result;
// }

// add_shortcode('page_sidebar', 'uou_theme_page_sidebar');
