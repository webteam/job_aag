<?php

/**
 * Careers Theme Helper
 *
 * Add Custom Post Type
 *
 * @class       Careers_Theme_Helper_Post_Types
 * @version     1.0.0
 * @package     Careers Theme Helper/includes
 * @category    Class
 * @author      UouApps
 */

class Careers_Theme_Helper_Post_Types {
    public function __construct(){

        include_once( 'vendor/cuztom/cuztom.php' );

        $this->create_post_type();
        // $this->create_metaboxes();

    }

    public function create_post_type() {
        $pages = new Cuztom_Post_Type( 'page' );

        $this->cth_add_contact_us_page_metabox( $pages );

        $team = new Cuztom_Post_Type( 'team', array(
            'supports' => array( 'title', 'thumbnail' ),
            'menu_icon' => 'dashicons-id-alt'
        ) );

        $this->cth_add_about_us_page_metabox( $team );

        $partners = new Cuztom_Post_Type( 'partner', array(
          'supports' => array( 'title', 'thumbnail' ),
          'menu_icon' => 'dashicons-groups',
          'has_archive' => false
        ) );

        $success = new Cuztom_Post_Type( 'success', array(
          'labels' => array(
            'name' => 'Success Stories',
            'singular_name' => 'Success Story',
            'add_new_item' => 'Add new success story'
          ),
          'supports' => array( 'title', 'thumbnail' ),
          'menu_icon' => 'dashicons-megaphone'
        ) );

        $success = new Cuztom_Post_Type( 'slide', array(
          'supports' => array( 'title', 'thumbnail' ),
          'menu_icon' => 'dashicons-images-alt'
        ) );
    }

    private function cth_add_contact_us_page_metabox( $pages ) {
        $pages->add_meta_box(
                'meta_box_contact',
            'Address',
            array(
                    'bundle',
                array(
                    array(
                        'name'          => 'title',
                        'label'         => __('Title', 'careers'),
                        'description'   => __('Your address title(Dhaka Office)', 'careers'),
                        'type'          => 'text'
                    ),
                    array(
                        'name'          => 'address',
                        'label'         => __('Address', 'careers'),
                        'description'   => __('Location', 'careers'),
                        'type'          => 'textarea'
                     ),
                    array(
                        'name'          => 'lat',
                        'label'         => __('Latitude', 'careers'),
                        'description'   => __('Store Latitude like : ( -37.83527632292268 )', 'careers'),
                        'type'          => 'text'
                    ),
                    array(
                        'name'          => 'lon',
                        'label'         => __('Longitude', 'careers'),
                        'description'   => __('Store Longitude like : ( 145.01455307006836 )', 'careers'),
                        'type'          => 'text'
                    ),
                    array(
                        'name'          => 'phone',
                        'label'         => __('Phone', 'careers'),
                        'description'   => __('Phone number', 'careers'),
                        'type'          => 'text'
                    ),
                    array(
                        'name'          => 'email',
                        'label'         => __('Email', 'careers'),
                        'description'   => __('Address Email', 'careers'),
                        'type'          => 'text'
                    )
                )
            )
        );


        $pages->add_meta_box(
                'meta_box_contact_form',
            'Contact Form',
            array(
                array(
                    'name'          => 'title',
                    'label'         => __('Title', 'careers'),
                    'description'   => __('Widget Title goes here', 'careers'),
                    'type'          => 'text'
                ),
                array(
                    'name'          => 'description',
                    'label'         => __('Widget Description', 'careers'),
                    'description'   => __('Contact form description', 'careers'),
                    'type'          => 'textarea'
                ),
                array(
                    'name'          => 'widget',
                    'label'         => __('Shortcode', 'careers'),
                    'description'   => __('Paste your contact form Shortcode here', 'careers'),
                    'type'          => 'textarea'
                ),
            )
        );
    }

    private function cth_add_about_us_page_metabox( $team ) {

        $team->add_meta_box(
           'meta_box_team',
           'Team Meta',
            array(
                array(
                    'name'          => 'designation',
                    'label'         => 'Designation',
                    'description'   => 'Like CEO  ...',
                    'type'          => 'text'
                ),
              )
           );
    }
}

new Careers_Theme_Helper_Post_Types();
