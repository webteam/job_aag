<?php

function get_user_role() {
    global $current_user;

    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);

    return $user_role;
}

function get_the_user($role = 'company'){
    
    $user_id = get_current_user_id();
    $args = array(
                 'post_type'   => $role,
                 'author' => $user_id
    );
    
    $query = new WP_Query( $args );

    return $query;
}
function isBookmarked($id = 0, $key=null){
    $user_id = get_current_user_id();
    $bookmarkedItemIDList = get_user_meta( $user_id, $key, true );
    if(isset($bookmarkedItemIDList) && is_array($bookmarkedItemIDList)){
        if(in_array($id, $bookmarkedItemIDList)){
            return true;
        }else{
           return false;
        }
       
    } else{
        return false;
    }
}
function get_user_info(){
     $user_id = get_current_user_id();
     $user_info = get_userdata($user_id);

     return $user_info;
 }


 function get_company_info( $company_id = null ){


     $query = get_post( $company_id );

     return $query;

 }



function get_companyList( ){
        global $wpdb;
        $sql =  "SELECT * FROM $wpdb->posts ".
        " WHERE $wpdb->posts.post_type = 'company' ".
        " AND ($wpdb->posts.post_status = 'publish') " ;

        $query = $wpdb->get_results( $sql );
       
        return $query;
}

function load_html_backend(){

    $template_loader = new Uou_Careers_Load_Template();
    ob_start();
    $template = $template_loader->locate_template( 'subscription-plan.php' );

    if(is_user_logged_in() ){
        include( $template );
    }
    echo ob_get_clean();

}
//add_action('admin_footer','load_html_backend'); 


function get_career_header(){
            
            require_once(UOU_CAREERS_DIR.'/includes/class-uou-careers-load-template.php');
            $template = new Uou_Careers_Load_Template();
            include($template->locate_template( 'header-dashboard.php' ));

}
function get_career_footer(){
            
            require_once(UOU_CAREERS_DIR.'/includes/class-uou-careers-load-template.php');
            $template = new Uou_Careers_Load_Template();
            include($template->locate_template( 'footer-dashboard.php' ));

}

function get_subscription_plans(){
                   
          $options =  get_option('subscription_plans');

          $subscription_options = json_decode($options);

          

          $membership_type =  get_user_meta(get_current_user_id(), 'membership_type', true);
    
          wp_localize_script('ng-controllers', 'subscription', array( 'show' => $subscription_options , 'membership_type' => $membership_type )  );
                   
}
function get_subscription_plan( $id = 0 ){
        $options =  get_option('subscription_plans');
        $subscription_options = json_decode($options);
            $return = array();
            if( isset($subscription_options) ){
                foreach ($subscription_options as $key => $value) {
                    if($value->id == $id){
                            $return = $value;
                            break;
                    }
                }
            }
           return $return;
                   
}
function billingPeriodArray(){
        return array(1=>"weekly", 2=>"monthly", 3=>"yearly");
}
function getPostList(){
    global $current_user, $wpdb;
    get_currentuserinfo();

    $args = array(
         'post_type'   => 'post',
         'author' => $current_user->ID,
         'post_status' => 'publish',
    );
    $query = new WP_Query( $args );
    
    $i = 0;

    $return = array();
    if(isset($query)){
        foreach ( $query->posts as $key => $value ) {
            $return[$i]['id'] = $value->ID;
            $return[$i]['post_title'] = $value->post_title;
            $return[$i]['link'] = $value->guid;
            $content = apply_filters('the_content', $value->post_content);
            $content = str_replace(']]>', ']]&gt;', $content);

            $return[$i]['post_content'] = $content;
            $return[$i]['type'] = 'post';
            $return[$i]['image']['attachment_id'] = get_post_thumbnail_id( $value->ID);
            $imageArray = wp_get_attachment_image_src( get_post_thumbnail_id( $value->ID ), 'medium');
            $return[$i]['image']['url'] = $imageArray[0];
            $metas = get_post_meta( $value->ID );
            if(isset($metas)){
                foreach ($metas as $key1 => $value1) {
                        if($key1 == "_edit_lock" || $key1 == "_edit_last" ) continue;
                        $return[$i]['post_meta'][$key1] = $value1[0];
                }
            }    
            $i++;
        }
    }  
    //echo "<pre>";print_r($return);echo "</pre>"; 
    return $return;
}

function getApplicationlist($comment_post_ID){
    global $wpdb;
    $args = array(
        'number' => '100',
        'post_id' => $comment_post_ID
    );
    $comments = get_comments($args);
    $results = array();
    foreach ($comments as $key => $value) {
        $sql = $wpdb->prepare( "SELECT ID, post_name, post_content FROM $wpdb->posts ".
            " WHERE $wpdb->posts.post_author = %d".
            " AND $wpdb->posts.post_type = 'candidate' ".
            " AND $wpdb->posts.post_status = 'publish' ", $value->user_id );
        $query = $wpdb->get_row( $sql );

        // Candidate age
        $date_of_birth = date("Y-m-d", strtotime('+1 day', strtotime(get_post_meta( $query->ID, 'date_of_birth', true ))));
        $age = date_diff(date_create($date_of_birth), date_create('today'))->y;

        // Candidate location
        $locations     = get_the_terms( $query->ID, 'location' );

        $arr           = array();

        if( !empty( $locations ) ) {
            foreach ($locations as $location) {
              if($location->parent != 0) {
                $arr[] = $location;
              }
            }

            if($arr[0]->term_id == $arr[1]->parent) {
              $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
            } else {
              $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
            }
        }

        $results[] = array(
            'comment_ID' => $value->comment_ID,
            'comment_post_ID'=>$value->comment_post_ID,
            'comment_author' => $value->comment_author,
            'comment_author_email'=> $value->comment_author_email,
            'comment_content' => $value->comment_content,
            'comment_date' => $value->comment_date,
            'candidate_id' => $query->ID,
            'candidate_url' => home_url( '/candidate/' .$query->post_name),
            'candidate_age' => $age,
            'candidate_loacation' => $state_country,
            'candidate_excerpt' => uou_careers_get_post_excerpt($query->post_content, 40),
        );
    }

    return $results;
}
function get_company_profile($post_id = 0, $post_title = null, $post_content= null){

            global $current_user, $wpdb;
            get_currentuserinfo();
            $attachment_id = get_post_meta( $post_id, 'avator', true );
            
            $imageArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'medium');
            $companySettings['profile'] = array(
                    'user_id' => $current_user->ID,
                    'user_nicename' => $current_user->user_nicename,
                    'user_email' => $current_user->user_email,
                    'user_url' => $current_user->user_url,
                    'post_id' => $post_id,
                    'post_title' => $post_title,
                    'post_content' => $post_content,
                    'image' => array(
                        'attachment_id' => get_post_thumbnail_id( $post_id), 
                        'url'=>$imageArray[0]
                    )
            );
           
            $taxonomylist = wp_get_object_terms($post_id, array('location'));
        
            foreach ($taxonomylist as $key => $value) {
                if($value->taxonomy == "location")
                    $companySettings['profile']['taxonomy'][$value->taxonomy][] = $value->term_id;
                else
                    $companySettings['profile']['taxonomy'][$value->taxonomy] = $value->term_id;
            }
            //echo "<pre>";print_r($attachment_id);echo "</pre>";
            $metas = get_post_meta( $post_id );
            foreach ($metas as $key => $value) {
                        if($key == "_edit_lock" || $key == "_edit_last" ) continue;
                        $companySettings['profile']['post_meta'][$key] = $value[0];
            }
            
            // retreiving recruiters
            $args = array( 
                'role'         => 'recruiter',
                'meta_key'     => 'company_id',
                'meta_value'   => $post_id,
            );

            
            $recruiters = get_users( $args );

            //echo "<pre>";echo $post_id;print_r($recruiters);echo "</pre>";
            $i = 0;
            $companySettings['recruiters'] = array();
            foreach ($recruiters as $key => $recruiter) {

                $companySettings['recruiters'][$i]['id'] = $recruiter->ID;
                $companySettings['recruiters'][$i]['user_nicename'] = $recruiter->user_nicename;
                $companySettings['recruiters'][$i]['user_email'] = $recruiter->user_email;
                $companySettings['recruiters'][$i]['user_login'] = $recruiter->user_login;
                $companySettings['recruiters'][$i]['profile_link'] = home_url().'/recruiter/'.$recruiter->user_nicename;
                $i++;
            }
            
            
            // retreive jobs
           
            $sql = $wpdb->prepare( "SELECT * FROM $wpdb->posts ".
            " INNER JOIN $wpdb->postmeta AS pm ON pm.post_id = $wpdb->posts.ID".
            " WHERE pm.meta_key = 'company_id'".
            " AND pm.meta_value = '%s' ".
            " AND $wpdb->posts.post_type = 'job' ".
            " AND ($wpdb->posts.post_status = 'publish') ", $post_id );

            $query = $wpdb->get_results( $sql );
            $i = 0;
            $companySettings['jobs'] = array();
            foreach ( $query as $key => $value ) {
                $companySettings['jobs'][$i]['id'] = $value->ID;
                $companySettings['jobs'][$i]['post_title'] = $value->post_title;
                $companySettings['jobs'][$i]['post_name'] = $value->post_name;
                
                $content = apply_filters('the_content', $value->post_content);
                $content = str_replace(']]>', ']]&gt;', $content);

                $companySettings['jobs'][$i]['post_content'] = $content;
                $metas = get_post_meta( $value->ID );
                $taxonomylist = wp_get_post_terms($value->ID, array('industry','role','job_type','nationality','gender','career_level', 'years_of_experience'));
                
                foreach ($taxonomylist as $key1 => $value1) {
                    $companySettings['jobs'][$i]['taxonomy'][$value1->taxonomy][] = $value1->term_id;
                }
                $companySettings['jobs'][$i]['taxonomy']['region'] =   getTaxonomyByID( $value->ID, 0);
            
            
                if($companySettings['jobs'][$i]['taxonomy']['region'] > 0)
                    $companySettings['jobs'][$i]['taxonomy']['country'] = getTaxonomyByID($value->ID, $companySettings['jobs'][$i]['taxonomy']['region']);
                
                
                if($companySettings['jobs'][$i]['taxonomy']['country'] > 0)
                    $companySettings['jobs'][$i]['taxonomy']['state'] = getTaxonomyByID($value->ID, $companySettings['jobs'][$i]['taxonomy']['country']);
                
                    //echo "<pre>";print_r($companySettings['jobs'][$i]['taxonomy']);echo "</pre>";
                foreach ($metas as $key1 => $value1) {
                        if($key1 == "_edit_lock" || $key1 == "_edit_last" ) continue;
                        if($key1 == "featured"){
                            $companySettings['jobs'][$i]['post_meta'][$key1] = (bool)$value1[0];
                        
                        }else{
                           $companySettings['jobs'][$i]['post_meta'][$key1] = (preg_match('/[^,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t]/',
   preg_replace('/"(\\.|[^"\\\\])*"/', '', $value1[0])))? $value1[0] : json_decode($value1[0]);

                        }
                }
                $recruiter_id = get_post_meta( $value->ID, 'recruiter_id', true);
                if( (int)$recruiter_id > 0 ){
                    $recruiter_info = get_userdata( (int)$recruiter_id );
                    $companySettings['jobs'][$i]['post_meta']['recruiter_name'] = isset($recruiter_info->data->user_login)?$recruiter_info->data->user_login:'';
                }else{
                    $companySettings['jobs'][$i]['post_meta']['recruiter_name'] = '';
                }
                $companySettings['jobs'][$i]['applicationlist'] = getApplicationlist($value->ID);
                $i++;
            }
            
            $companySettings['postlist'] = array();
            $companySettings['postlist'] = getPostList();
            

            wp_localize_script('ng-controllers', 'user_object', array( 'userData' => $companySettings )  );


            $GLOBALS['uou_company'] =$companySettings;

            return $companySettings;
        
}

function get_recruiter_profile($post_id = 0, $post_title = null, $post_content= null){
            global $current_user, $wpdb;
            get_currentuserinfo();
            $attachment_id = get_post_meta( $post_id, 'avator', true );
            $metas = get_post_meta( $post_id );
            $imageArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'medium');
            $recruiterSettings['profile'] = array(
                'user_id' => $current_user->ID,
                'user_nicename' => $current_user->user_nicename,
                'user_email' => $current_user->user_email,
                'user_url' => $current_user->user_url,
                'post_id' => $post_id,
                'post_title' => $post_title,
                'post_content' => $post_content,
                'profile_link' => home_url().'/recruiter/'.$current_user->user_nicename,
                'image' => array(
                        'attachment_id' => get_post_thumbnail_id( $post_id), 
                        'url'=>$imageArray[0]
                    )
            );
            foreach ($metas as $key => $value) {
                if($key == "_edit_lock" || $key == "_edit_last" ) continue;
                $recruiterSettings['profile']['post_meta'][$key] = $value[0];
            }
           
            // robi
            $company_id = get_user_meta( $current_user->ID, "company_admin_id", true );
            $sql =  "SELECT * FROM $wpdb->posts ".
            " INNER JOIN $wpdb->postmeta AS pm ON pm.post_id = $wpdb->posts.ID".
            " WHERE $wpdb->posts.post_author = '".$company_id."'".
            " AND pm.meta_key = 'recruiter_id'".
            " AND pm.meta_value = '".$current_user->ID."'".
            " AND $wpdb->posts.post_type = 'job' ".
            " AND ($wpdb->posts.post_status = 'publish') "  ;
            
            $query = $wpdb->get_results( $sql );
            $i = 0;
            $recruiterSettings['jobs'] = array();
            $temp_array = array();
            foreach ( $query as $key => $value ) {
                 if(in_array($value->ID, $temp_array))
                    continue;
                $recruiterSettings['jobs'][$i]['id'] = $value->ID;
                $recruiterSettings['jobs'][$i]['post_title'] = $value->post_title;
                $recruiterSettings['jobs'][$i]['post_name'] = $value->post_name;
                $content = apply_filters('the_content', $value->post_content);
                $content = str_replace(']]>', ']]&gt;', $content);

                $recruiterSettings['jobs'][$i]['post_content'] = $content;
                $taxonomylist = wp_get_post_terms($value->ID, array('industry','role','job_type','nationality','gender','career_level', 'years_of_experience'));
                
                foreach ($taxonomylist as $key1 => $value1) {
                    $recruiterSettings['jobs'][$i]['taxonomy'][$value1->taxonomy][] = $value1->term_id;
                }
                $recruiterSettings['jobs'][$i]['taxonomy']['region'] =   getTaxonomyByID( $value->ID, 0);
            
            
                if($recruiterSettings['jobs'][$i]['taxonomy']['region'] > 0)
                    $recruiterSettings['jobs'][$i]['taxonomy']['country'] = getTaxonomyByID($value->ID, $recruiterSettings['jobs'][$i]['taxonomy']['region']);
                
                
                if(isset($recruiterSettings['jobs'][$i]['taxonomy']['country']) && $recruiterSettings['jobs'][$i]['taxonomy']['country'] > 0)
                    $recruiterSettings['jobs'][$i]['taxonomy']['state'] = getTaxonomyByID($value->ID, $recruiterSettings['jobs'][$i]['taxonomy']['country']);
                
                $metas = get_post_meta( $value->ID );
                foreach ($metas as $key1 => $value1) {
                        if($key1 == "_edit_lock" || $key1 == "_edit_last" ) continue;
                        $recruiterSettings['jobs'][$i]['post_meta'][$key1] = $value1[0];
                }
                $recruiterSettings['jobs'][$i]['applicationlist'] = getApplicationlist($value->ID);
                $i++;
                $temp_array[] =  $value->ID;
            }
            $recruiterSettings['postlist'] = array();
            $recruiterSettings['postlist'] = getPostList();
            wp_localize_script('ng-controllers', 'user_object', array( 'userData' => $recruiterSettings )  );
            return $recruiterSettings;
}
function getTaxonomyByID($object_id = 0, $parent_id = 0, $taxonomy = "location"){
    global $wpdb;
    $sql = "SELECT $wpdb->term_taxonomy.term_id,$wpdb->term_taxonomy.term_id  FROM $wpdb->term_relationships ".
            " INNER JOIN $wpdb->term_taxonomy ON $wpdb->term_taxonomy.term_taxonomy_id = $wpdb->term_relationships.term_taxonomy_id ".
            " WHERE $wpdb->term_taxonomy.parent = $parent_id AND $wpdb->term_relationships.object_id = $object_id AND $wpdb->term_taxonomy.taxonomy = '".$taxonomy."'  ORDER BY $wpdb->term_taxonomy.taxonomy";
    $query = $wpdb->get_row( $sql );
    if(isset($query->term_id))
        return $query->term_id;
    else
        return 0;
}
function get_candidate_profile($post_id = 0, $post_title = null, $post_content= null){

            global $current_user, $wpdb;
            get_currentuserinfo();
            $attachment_id = get_post_meta( $post_id, 'avator', true );
            $imageArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'medium');
            $candidateSettings['profile'] = array(
                    'user_id' => $current_user->ID,
                    'user_login'=>$current_user->user_login,
                    'user_nicename' => $current_user->user_nicename,
                    'user_email' => $current_user->user_email,
                    'user_url' => $current_user->user_url,
                    'post_id' => $post_id,
                    'post_title' => $post_title,
                    'post_content' => $post_content,
                    'image' => array(
                        'attachment_id' => get_post_thumbnail_id( $post_id), 
                        'url'=>$imageArray[0]
                    )

            );
            
            $taxonomylist = wp_get_object_terms($post_id, array('nationality','gender','career_level','degree','years_of_experience'));
        
            foreach ($taxonomylist as $key => $value) {
                $candidateSettings['profile']['taxonomy'][$value->taxonomy] = $value->term_id;
            }

            $candidateSettings['profile']['taxonomy']['region'] =   getTaxonomyByID( $post_id, 0);
            
            
            if($candidateSettings['profile']['taxonomy']['region'] > 0)
                $candidateSettings['profile']['taxonomy']['country'] = getTaxonomyByID($post_id, $candidateSettings['profile']['taxonomy']['region']);
            
            
            if(isset($candidateSettings['profile']['taxonomy']['country']) && $candidateSettings['profile']['taxonomy']['country'] > 0)
                $candidateSettings['profile']['taxonomy']['state'] = getTaxonomyByID($post_id, $candidateSettings['profile']['taxonomy']['country']);
            
            
            $metas = get_post_meta( $post_id );
            foreach ($metas as $key => $value) {
                    
                    if($key == "_edit_lock" || $key == "_edit_last" ) continue;
                    $candidateSettings['profile']['post_meta'][$key] = (preg_match('/[^,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t]/',
   preg_replace('/"(\\.|[^"\\\\])*"/', '', $value[0])))?$value[0]:json_decode($value[0]);
                    
            }
            $candidateSettings['name'] = get_user_meta($current_user->ID, 'first_name', true) .' '. get_user_meta($current_user->ID, 'last_name', true); 
            $candidateSettings['post_id'] = $post_id; 
            
            $candidateSettings['postlist'] = array();
            $candidateSettings['postlist'] = getPostList();

            wp_localize_script('ng-controllers', 'user_object', array( 'userData' => $candidateSettings )  );
            return $candidateSettings;
        
}



/**
 * @param   Void  
 * @return  String [ permalink ]
 * @since   Version 1.0
 */



function get_users_public_profile(){

    $user_ID = get_current_user_id();

    $company = get_posts(array(
        'post_type'      => 'company',
        'posts_per_page' => 1,
        'author'         => $user_ID ,
        
    ));

    
    $candidate = get_posts(array(
        'post_type'      => 'candidate',
        'posts_per_page' => 1,
        'author'         => $user_ID ,
        
    ));

    $recruiter = get_posts(array(
        'post_type'      => 'recruiter',
        'posts_per_page' => 1,
        'author'         => $user_ID ,
        
    ));


    if( !empty($company) ){

        echo esc_url( home_url( '/company/' ) ).$company[0]->post_name;
        
      //  _log($company[0]);

      //  return get_permalink( $company[0]->ID );

    } elseif( !empty($candidate) ){

         echo esc_url( home_url( '/candidate/' ) ).$candidate[0]->post_name;
        
    }
    elseif( !empty($recruiter) ){

         echo esc_url( home_url( '/recruiter/' ) ).$recruiter[0]->post_name;
        
    }

    else{
        echo get_the_author_link();
    }

}







function careers_dashboard_link() {
  
    
    if( !get_query_var('dashboard') ){

        if( is_user_logged_in() ){

            echo '<a class="btn btn-danger  fixed-button" href="'.esc_url( home_url( '/dashboard' ) ).'"><span class="glyphicon glyphicon-stats"></span> Dashboard </a>';  

        }
    }else{
        if( is_user_logged_in() ){

            echo '<a class="btn btn-danger  fixed-button" href="'.esc_url( home_url( '/' ) ).'"><span class="glyphicon glyphicon-stats"></span> Back To Site </a>';  

        }
    }
    
}
add_action('wp_footer', 'careers_dashboard_link', 100);







function get_country_list(){

            return array(
                'AF' => __( 'Afghanistan', 'careers' ),
                'AX' => __( '&#197;land Islands', 'careers' ),
                'AL' => __( 'Albania', 'careers' ),
                'DZ' => __( 'Algeria', 'careers' ),
                'AD' => __( 'Andorra', 'careers' ),
                'AO' => __( 'Angola', 'careers' ),
                'AI' => __( 'Anguilla', 'careers' ),
                'AQ' => __( 'Antarctica', 'careers' ),
                'AG' => __( 'Antigua and Barbuda', 'careers' ),
                'AR' => __( 'Argentina', 'careers' ),
                'AM' => __( 'Armenia', 'careers' ),
                'AW' => __( 'Aruba', 'careers' ),
                'AU' => __( 'Australia', 'careers' ),
                'AT' => __( 'Austria', 'careers' ),
                'AZ' => __( 'Azerbaijan', 'careers' ),
                'BS' => __( 'Bahamas', 'careers' ),
                'BH' => __( 'Bahrain', 'careers' ),
                'BD' => __( 'Bangladesh', 'careers' ),
                'BB' => __( 'Barbados', 'careers' ),
                'BY' => __( 'Belarus', 'careers' ),
                'BE' => __( 'Belgium', 'careers' ),
                'PW' => __( 'Belau', 'careers' ),
                'BZ' => __( 'Belize', 'careers' ),
                'BJ' => __( 'Benin', 'careers' ),
                'BM' => __( 'Bermuda', 'careers' ),
                'BT' => __( 'Bhutan', 'careers' ),
                'BO' => __( 'Bolivia', 'careers' ),
                'BQ' => __( 'Bonaire, Saint Eustatius and Saba', 'careers' ),
                'BA' => __( 'Bosnia and Herzegovina', 'careers' ),
                'BW' => __( 'Botswana', 'careers' ),
                'BV' => __( 'Bouvet Island', 'careers' ),
                'BR' => __( 'Brazil', 'careers' ),
                'IO' => __( 'British Indian Ocean Territory', 'careers' ),
                'VG' => __( 'British Virgin Islands', 'careers' ),
                'BN' => __( 'Brunei', 'careers' ),
                'BG' => __( 'Bulgaria', 'careers' ),
                'BF' => __( 'Burkina Faso', 'careers' ),
                'BI' => __( 'Burundi', 'careers' ),
                'KH' => __( 'Cambodia', 'careers' ),
                'CM' => __( 'Cameroon', 'careers' ),
                'CA' => __( 'Canada', 'careers' ),
                'CV' => __( 'Cape Verde', 'careers' ),
                'KY' => __( 'Cayman Islands', 'careers' ),
                'CF' => __( 'Central African Republic', 'careers' ),
                'TD' => __( 'Chad', 'careers' ),
                'CL' => __( 'Chile', 'careers' ),
                'CN' => __( 'China', 'careers' ),
                'CX' => __( 'Christmas Island', 'careers' ),
                'CC' => __( 'Cocos (Keeling) Islands', 'careers' ),
                'CO' => __( 'Colombia', 'careers' ),
                'KM' => __( 'Comoros', 'careers' ),
                'CG' => __( 'Congo (Brazzaville)', 'careers' ),
                'CD' => __( 'Congo (Kinshasa)', 'careers' ),
                'CK' => __( 'Cook Islands', 'careers' ),
                'CR' => __( 'Costa Rica', 'careers' ),
                'HR' => __( 'Croatia', 'careers' ),
                'CU' => __( 'Cuba', 'careers' ),
                'CW' => __( 'Cura&Ccedil;ao', 'careers' ),
                'CY' => __( 'Cyprus', 'careers' ),
                'CZ' => __( 'Czech Republic', 'careers' ),
                'DK' => __( 'Denmark', 'careers' ),
                'DJ' => __( 'Djibouti', 'careers' ),
                'DM' => __( 'Dominica', 'careers' ),
                'DO' => __( 'Dominican Republic', 'careers' ),
                'EC' => __( 'Ecuador', 'careers' ),
                'EG' => __( 'Egypt', 'careers' ),
                'SV' => __( 'El Salvador', 'careers' ),
                'GQ' => __( 'Equatorial Guinea', 'careers' ),
                'ER' => __( 'Eritrea', 'careers' ),
                'EE' => __( 'Estonia', 'careers' ),
                'ET' => __( 'Ethiopia', 'careers' ),
                'FK' => __( 'Falkland Islands', 'careers' ),
                'FO' => __( 'Faroe Islands', 'careers' ),
                'FJ' => __( 'Fiji', 'careers' ),
                'FI' => __( 'Finland', 'careers' ),
                'FR' => __( 'France', 'careers' ),
                'GF' => __( 'French Guiana', 'careers' ),
                'PF' => __( 'French Polynesia', 'careers' ),
                'TF' => __( 'French Southern Territories', 'careers' ),
                'GA' => __( 'Gabon', 'careers' ),
                'GM' => __( 'Gambia', 'careers' ),
                'GE' => __( 'Georgia', 'careers' ),
                'DE' => __( 'Germany', 'careers' ),
                'GH' => __( 'Ghana', 'careers' ),
                'GI' => __( 'Gibraltar', 'careers' ),
                'GR' => __( 'Greece', 'careers' ),
                'GL' => __( 'Greenland', 'careers' ),
                'GD' => __( 'Grenada', 'careers' ),
                'GP' => __( 'Guadeloupe', 'careers' ),
                'GT' => __( 'Guatemala', 'careers' ),
                'GG' => __( 'Guernsey', 'careers' ),
                'GN' => __( 'Guinea', 'careers' ),
                'GW' => __( 'Guinea-Bissau', 'careers' ),
                'GY' => __( 'Guyana', 'careers' ),
                'HT' => __( 'Haiti', 'careers' ),
                'HM' => __( 'Heard Island and McDonald Islands', 'careers' ),
                'HN' => __( 'Honduras', 'careers' ),
                'HK' => __( 'Hong Kong', 'careers' ),
                'HU' => __( 'Hungary', 'careers' ),
                'IS' => __( 'Iceland', 'careers' ),
                'IN' => __( 'India', 'careers' ),
                'ID' => __( 'Indonesia', 'careers' ),
                'IR' => __( 'Iran', 'careers' ),
                'IQ' => __( 'Iraq', 'careers' ),
                'IE' => __( 'Republic of Ireland', 'careers' ),
                'IM' => __( 'Isle of Man', 'careers' ),
                'IL' => __( 'Israel', 'careers' ),
                'IT' => __( 'Italy', 'careers' ),
                'CI' => __( 'Ivory Coast', 'careers' ),
                'JM' => __( 'Jamaica', 'careers' ),
                'JP' => __( 'Japan', 'careers' ),
                'JE' => __( 'Jersey', 'careers' ),
                'JO' => __( 'Jordan', 'careers' ),
                'KZ' => __( 'Kazakhstan', 'careers' ),
                'KE' => __( 'Kenya', 'careers' ),
                'KI' => __( 'Kiribati', 'careers' ),
                'KW' => __( 'Kuwait', 'careers' ),
                'KG' => __( 'Kyrgyzstan', 'careers' ),
                'LA' => __( 'Laos', 'careers' ),
                'LV' => __( 'Latvia', 'careers' ),
                'LB' => __( 'Lebanon', 'careers' ),
                'LS' => __( 'Lesotho', 'careers' ),
                'LR' => __( 'Liberia', 'careers' ),
                'LY' => __( 'Libya', 'careers' ),
                'LI' => __( 'Liechtenstein', 'careers' ),
                'LT' => __( 'Lithuania', 'careers' ),
                'LU' => __( 'Luxembourg', 'careers' ),
                'MO' => __( 'Macao S.A.R., China', 'careers' ),
                'MK' => __( 'Macedonia', 'careers' ),
                'MG' => __( 'Madagascar', 'careers' ),
                'MW' => __( 'Malawi', 'careers' ),
                'MY' => __( 'Malaysia', 'careers' ),
                'MV' => __( 'Maldives', 'careers' ),
                'ML' => __( 'Mali', 'careers' ),
                'MT' => __( 'Malta', 'careers' ),
                'MH' => __( 'Marshall Islands', 'careers' ),
                'MQ' => __( 'Martinique', 'careers' ),
                'MR' => __( 'Mauritania', 'careers' ),
                'MU' => __( 'Mauritius', 'careers' ),
                'YT' => __( 'Mayotte', 'careers' ),
                'MX' => __( 'Mexico', 'careers' ),
                'FM' => __( 'Micronesia', 'careers' ),
                'MD' => __( 'Moldova', 'careers' ),
                'MC' => __( 'Monaco', 'careers' ),
                'MN' => __( 'Mongolia', 'careers' ),
                'ME' => __( 'Montenegro', 'careers' ),
                'MS' => __( 'Montserrat', 'careers' ),
                'MA' => __( 'Morocco', 'careers' ),
                'MZ' => __( 'Mozambique', 'careers' ),
                'MM' => __( 'Myanmar', 'careers' ),
                'NA' => __( 'Namibia', 'careers' ),
                'NR' => __( 'Nauru', 'careers' ),
                'NP' => __( 'Nepal', 'careers' ),
                'NL' => __( 'Netherlands', 'careers' ),
                'AN' => __( 'Netherlands Antilles', 'careers' ),
                'NC' => __( 'New Caledonia', 'careers' ),
                'NZ' => __( 'New Zealand', 'careers' ),
                'NI' => __( 'Nicaragua', 'careers' ),
                'NE' => __( 'Niger', 'careers' ),
                'NG' => __( 'Nigeria', 'careers' ),
                'NU' => __( 'Niue', 'careers' ),
                'NF' => __( 'Norfolk Island', 'careers' ),
                'KP' => __( 'North Korea', 'careers' ),
                'NO' => __( 'Norway', 'careers' ),
                'OM' => __( 'Oman', 'careers' ),
                'PK' => __( 'Pakistan', 'careers' ),
                'PS' => __( 'Palestinian Territory', 'careers' ),
                'PA' => __( 'Panama', 'careers' ),
                'PG' => __( 'Papua New Guinea', 'careers' ),
                'PY' => __( 'Paraguay', 'careers' ),
                'PE' => __( 'Peru', 'careers' ),
                'PH' => __( 'Philippines', 'careers' ),
                'PN' => __( 'Pitcairn', 'careers' ),
                'PL' => __( 'Poland', 'careers' ),
                'PT' => __( 'Portugal', 'careers' ),
                'QA' => __( 'Qatar', 'careers' ),
                'RE' => __( 'Reunion', 'careers' ),
                'RO' => __( 'Romania', 'careers' ),
                'RU' => __( 'Russia', 'careers' ),
                'RW' => __( 'Rwanda', 'careers' ),
                'BL' => __( 'Saint Barth&eacute;lemy', 'careers' ),
                'SH' => __( 'Saint Helena', 'careers' ),
                'KN' => __( 'Saint Kitts and Nevis', 'careers' ),
                'LC' => __( 'Saint Lucia', 'careers' ),
                'MF' => __( 'Saint Martin (French part)', 'careers' ),
                'SX' => __( 'Saint Martin (Dutch part)', 'careers' ),
                'PM' => __( 'Saint Pierre and Miquelon', 'careers' ),
                'VC' => __( 'Saint Vincent and the Grenadines', 'careers' ),
                'SM' => __( 'San Marino', 'careers' ),
                'ST' => __( 'S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'careers' ),
                'SA' => __( 'Saudi Arabia', 'careers' ),
                'SN' => __( 'Senegal', 'careers' ),
                'RS' => __( 'Serbia', 'careers' ),
                'SC' => __( 'Seychelles', 'careers' ),
                'SL' => __( 'Sierra Leone', 'careers' ),
                'SG' => __( 'Singapore', 'careers' ),
                'SK' => __( 'Slovakia', 'careers' ),
                'SI' => __( 'Slovenia', 'careers' ),
                'SB' => __( 'Solomon Islands', 'careers' ),
                'SO' => __( 'Somalia', 'careers' ),
                'ZA' => __( 'South Africa', 'careers' ),
                'GS' => __( 'South Georgia/Sandwich Islands', 'careers' ),
                'KR' => __( 'South Korea', 'careers' ),
                'SS' => __( 'South Sudan', 'careers' ),
                'ES' => __( 'Spain', 'careers' ),
                'LK' => __( 'Sri Lanka', 'careers' ),
                'SD' => __( 'Sudan', 'careers' ),
                'SR' => __( 'Suriname', 'careers' ),
                'SJ' => __( 'Svalbard and Jan Mayen', 'careers' ),
                'SZ' => __( 'Swaziland', 'careers' ),
                'SE' => __( 'Sweden', 'careers' ),
                'CH' => __( 'Switzerland', 'careers' ),
                'SY' => __( 'Syria', 'careers' ),
                'TW' => __( 'Taiwan', 'careers' ),
                'TJ' => __( 'Tajikistan', 'careers' ),
                'TZ' => __( 'Tanzania', 'careers' ),
                'TH' => __( 'Thailand', 'careers' ),
                'TL' => __( 'Timor-Leste', 'careers' ),
                'TG' => __( 'Togo', 'careers' ),
                'TK' => __( 'Tokelau', 'careers' ),
                'TO' => __( 'Tonga', 'careers' ),
                'TT' => __( 'Trinidad and Tobago', 'careers' ),
                'TN' => __( 'Tunisia', 'careers' ),
                'TR' => __( 'Turkey', 'careers' ),
                'TM' => __( 'Turkmenistan', 'careers' ),
                'TC' => __( 'Turks and Caicos Islands', 'careers' ),
                'TV' => __( 'Tuvalu', 'careers' ),
                'UG' => __( 'Uganda', 'careers' ),
                'UA' => __( 'Ukraine', 'careers' ),
                'AE' => __( 'United Arab Emirates', 'careers' ),
                'GB' => __( 'United Kingdom (UK)', 'careers' ),
                'US' => __( 'United States (US)', 'careers' ),
                'UY' => __( 'Uruguay', 'careers' ),
                'UZ' => __( 'Uzbekistan', 'careers' ),
                'VU' => __( 'Vanuatu', 'careers' ),
                'VA' => __( 'Vatican', 'careers' ),
                'VE' => __( 'Venezuela', 'careers' ),
                'VN' => __( 'Vietnam', 'careers' ),
                'WF' => __( 'Wallis and Futuna', 'careers' ),
                'EH' => __( 'Western Sahara', 'careers' ),
                'WS' => __( 'Western Samoa', 'careers' ),
                'YE' => __( 'Yemen', 'careers' ),
                'ZM' => __( 'Zambia', 'careers' ),
                'ZW' => __( 'Zimbabwe', 'careers' )
            );



}








 function get_currency(){

          return array(
                'AED' => __( 'United Arab Emirates Dirham', 'careers' ),
                'AUD' => __( 'Australian Dollars', 'careers' ),
                'BDT' => __( 'Bangladeshi Taka', 'careers' ),
                'BRL' => __( 'Brazilian Real', 'careers' ),
                'BGN' => __( 'Bulgarian Lev', 'careers' ),
                'CAD' => __( 'Canadian Dollars', 'careers' ),
                'CLP' => __( 'Chilean Peso', 'careers' ),
                'CNY' => __( 'Chinese Yuan', 'careers' ),
                'COP' => __( 'Colombian Peso', 'careers' ),
                'CZK' => __( 'Czech Koruna', 'careers' ),
                'DKK' => __( 'Danish Krone', 'careers' ),
                'DOP' => __( 'Dominican Peso', 'careers' ),
                'EUR' => __( 'Euros', 'careers' ),
                'HKD' => __( 'Hong Kong Dollar', 'careers' ),
                'HRK' => __( 'Croatia kuna', 'careers' ),
                'HUF' => __( 'Hungarian Forint', 'careers' ),
                'ISK' => __( 'Icelandic krona', 'careers' ),
                'IDR' => __( 'Indonesia Rupiah', 'careers' ),
                'INR' => __( 'Indian Rupee', 'careers' ),
                'NPR' => __( 'Nepali Rupee', 'careers' ),
                'ILS' => __( 'Israeli Shekel', 'careers' ),
                'JPY' => __( 'Japanese Yen', 'careers' ),
                'KIP' => __( 'Lao Kip', 'careers' ),
                'KRW' => __( 'South Korean Won', 'careers' ),
                'MYR' => __( 'Malaysian Ringgits', 'careers' ),
                'MXN' => __( 'Mexican Peso', 'careers' ),
                'NGN' => __( 'Nigerian Naira', 'careers' ),
                'NOK' => __( 'Norwegian Krone', 'careers' ),
                'NZD' => __( 'New Zealand Dollar', 'careers' ),
                'PYG' => __( 'Paraguayan Guaraní', 'careers' ),
                'PHP' => __( 'Philippine Pesos', 'careers' ),
                'PLN' => __( 'Polish Zloty', 'careers' ),
                'GBP' => __( 'Pounds Sterling', 'careers' ),
                'RON' => __( 'Romanian Leu', 'careers' ),
                'RUB' => __( 'Russian Ruble', 'careers' ),
                'SGD' => __( 'Singapore Dollar', 'careers' ),
                'ZAR' => __( 'South African rand', 'careers' ),
                'SEK' => __( 'Swedish Krona', 'careers' ),
                'CHF' => __( 'Swiss Franc', 'careers' ),
                'TWD' => __( 'Taiwan New Dollars', 'careers' ),
                'THB' => __( 'Thai Baht', 'careers' ),
                'TRY' => __( 'Turkish Lira', 'careers' ),
                'USD' => __( 'US Dollars', 'careers' ),
                'VND' => __( 'Vietnamese Dong', 'careers' ),
                'EGP' => __( 'Egyptian Pound', 'careers' ),
            );

 }

 function get_currency_position(){
                return array(
                    'left'        => __( 'Left', 'careers' ) . ' ($99.99)',
                    'right'       => __( 'Right', 'careers' ) . ' (99.99$)',
                    'left_space'  => __( 'Left with space', 'careers' ) . ' ($ 99.99)',
                    'right_space' => __( 'Right with space', 'careers' ) . ' (99.99 $)'
                );

 }

function coverletters($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    $show = false;
    $user_info = get_userdata(get_current_user_id());
    $role = implode(', ', $user_info->roles);
    switch ($role) {
        case 'company_admin':
            $post_id = get_post_meta( $comment->comment_post_ID, 'company_id',true );
            $post = get_post( $post_id );
            $user_id = $post->post_author;

            break;
        case 'recruiter':
            $user_id = get_post_meta( $comment->comment_post_ID, 'company_id',true );
            break;
        default:
            $user_id = 0;
            $show = false;
            break;
    }

    if(get_current_user_id() == $user_id)
        $show = true;
    
    if(get_current_user_id() == $comment->user_id)
        echo "You have already applied this job.";
    ?>
    <?php if($show):?>
        <div>
            <div class="applicants">
                <?php echo get_comment_author_link( $comment->comment_ID ) ?>
                <p><strong>Coverletter:</strong
                <?php comment_text(); ?></p>
            </div>
        </div>    

    <?php endif;?>    
    <?php
}

function filter_handler( $approved , $commentdata )
{
  // inspect $commentdata to determine approval, disapproval, or spam status
    global $post;
    if($post->post_type == 'job'){
        return 1;
    }else{
        return 0;
    }     
}

add_filter( 'pre_comment_approved' , 'filter_handler' , '99', 2 );
// ai funciton tau chalaben 

function uou_careers_authored_content($query) {

    //get current user info to see if they are allowed to access ANY posts and pages
    $current_user = wp_get_current_user();
    // set current user to $is_user
    $is_user = $current_user->user_login;

    //if is admin or 'is_user' does not equal #username
    if (!current_user_can('manage_options')){
        //if in the admin panel
        if($query->is_admin) {

            global $user_ID;
            $query->set('author',  $user_ID);

        }
        return $query;
    }
    return $query;
}
add_filter('pre_get_posts', 'uou_careers_authored_content');

function uou_careers_get_post_excerpt( $text, $length = 60 ){
      $text = strip_shortcodes( $text );
      $text = apply_filters( 'the_content', $text );
      $text = str_replace( ']]>', ']]>', $text );

      $excerpt_length = apply_filters( 'excerpt_length', $length );
      $excerpt_more   = apply_filters( 'excerpt_more', ' ' . '[...]' );
      $text           = wp_trim_words( $text, $excerpt_length, '' );
      return $text;
}
function add_comment_extra_values($comment_id) {
    if(isset($_POST['cv'])) {
        $cv = wp_filter_nohtml_kses($_POST['cv']);
        add_comment_meta($comment_id, 'cv', $cv, false);
    }
 
}
add_action ('comment_post', 'add_comment_extra_values', 1);
