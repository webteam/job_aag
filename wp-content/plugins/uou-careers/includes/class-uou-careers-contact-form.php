<?php
global $error;
class Uou_careers_Contactform{
	public $error = null;
	public function __construct(){

		add_action('init', array($this, 'init'));
    }
 	
	function custom_wp_mail_from( $original_email_address )
	{
		extract($_POST);
		return $from;
	}
    public function init()
    {
    	
    	extract($_POST);
    	if (!empty($_POST['nonce_contact_form']))
        {
            if (!wp_verify_nonce($_POST['nonce_contact_form'], 'handle_contact_form'))
            {
                if (empty($_POST['to']))
                {
                    $error = __('Please enter valid email.', 'uou');
                }
                if (empty($_POST['from']))
                {
                   $error = __('Please enter valid email.', 'uou');
                }
                if (empty($_POST['subject']))
                {
                    $error =  __('Please enter the subject.', 'uou');
                }
                if (empty($_POST['message']))
                {
                    $error = __('Please enter the message.', 'uou');
                }
                
            }else{
	        	extract($_POST);
	        	add_filter( 'wp_mail_from', array( $this, 'custom_wp_mail_from' ));
	        	wp_mail( $to, $subject, $message);
	        	remove_filter( 'wp_mail_from', array( $this, 'custom_wp_mail_from' ) );
	        }
        }
    }
}
new Uou_careers_Contactform();