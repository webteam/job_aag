<?php
/*
Description: Megadi Support validation functions
Author: UOU Apps
Author URI: http://themeforest.net/user/uouapps
Version: 1.0.0
License: GNU General Public License v3.0
License URI: http://www.opensource.org/licenses/gpl-license.php

@package WordPress
@subpackage Megadi
@reference 	 http://hungred.com/useful-information/php-form-validation-snippets/
*/

/*  Copyright 2014  Ata Alqadi  (email : ata.alqadi@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 3, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
?>
<?php
class Form_Validation{
            public function uc_taken_username($username) {
                         if (username_exists( $username )) return true;
                         else return false;
                        
            }
            public function uc_taken_email($email) {
                         if ( email_exists( $email))return true;
                         else return false;
            }
    public function isValidEmail($email){
        return preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $email);
    }

    public function sanitizeEmaill($email)
    {
        return  preg_replace( '((?:\n|\r|\t|%0A|%0D|%08|%09)+)i' , '', $email );
    }

    public function check_email($email)
    {
        $email_error = false;
        $Email = htmlspecialchars(stripslashes(strip_tags(trim($email))));
        /*parse unnecessary characters to prevent exploits*/
        if ($Email == '') { $email_error = true; }
        elseif (!eregi('^([a-zA-Z0-9._-])+@([a-zA-Z0-9._-])+\.([a-zA-Z0-9._-])([a-zA-Z0-9._-])+', $Email)) { $email_error = true; }
        else {
            list($Email, $domain) = split('@', $Email, 2);
            if (! checkdnsrr($domain, 'MX')) { $email_error = true; }
            else {
                $array = array($Email, $domain);
                $Email = implode('@', $array);
            }
        }

        if ($email_error) { return false; } else{return true;}
    }

    public function isValidNumber($value)
    {
        #is_ double($value);
        #is_ float($value);
        #is_ int($value);
        #is_ integer($value);
        return is_numeric($value);
    }

    public function sanitizeNumber($str)
    {
        #letters and space only
        return preg_match('/[^0-9]/', '', $str);
    }

    public function isValidText($str, $minlen = 0)
    {
        #letters and space only
        return (strlen($str) > $minlen);
    }

    public function isValidString($str, $extra = '')
    {
        #letters and space only
        return preg_match('/^[A-Za-z\s '.$extra.']+$/', $str);
    }

    public function isValidAlphanumeric($string)
    {
        return ctype_alnum ($string);
    }

    public function sanitizeAlphanumeric($string)
    {
        return preg_replace('/[^a-zA-Z0-9]/', '', $string);
    }

    public function isValidUrl($url){
        /*
        $url_regex = '/^(http\:\/\/[a-zA-Z0-9_\-]+(?:\.[a-zA-Z0-9_\-]+)*\.[a-zA-Z]{2,4}(?:\/[a-zA-Z0-9_]+)*(?:\/[a-zA-Z0-9_]+\.[a-zA-Z]{2,4}(?:\?[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)?)?(?:\&[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)*)$/';
        */

        /* Make sure it's a properly formatted URL. */
        // From: http://www.daniweb.com/web-development/php/threads/290866
        // Scheme
        $url_regex = '^(https?|s?ftp\:\/\/)|(mailto\:)';
        // User and password (optional)
        $url_regex .= '([a-z0-9\+!\*\(\)\,\;\?&=\$_\.\-]+(\:[a-z0-9\+!\*\(\)\,\;\?&=\$_\.\-]+)?@)?';
        // Hostname or IP
        // http://x = allowed (ex. http://localhost, http://routerlogin)
        $url_regex .= '[a-z0-9\+\$_\-]+(\.[a-z0-9\+\$_\-]+)*';
        // http://x.x = minimum
        // $url_regex .= "[a-z0-9\+\$_\-]+(\.[a-z0-9+\$_\-]+)+";
        // http://x.xx(x) = minimum
        // $url_regex .= "([a-z0-9\+\$_\-]+\.)*[a-z0-9\+\$_\-]{2,3}";
        // use only one of the above
        // Port (optional)
        $url_regex .= '(\:[0-9]{2,5})?';
        // Path (optional)
        // $urlregex .= '(\/([a-z0-9\+\$_\-]\.\?)+)*\/?';
        // GET Query (optional)
        $url_regex .= '(\?[a-z\+&\$_\.\-][a-z0-9\;\:@\/&%=\+\$_\.\-]*)?';
        // Anchor (optional)
        // $urlregex .= '(\#[a-z_\.\-][a-z0-9\+\$_\.\-]*)?$';


        if ($url == 'http://' || $url == 'https://') {
            return false;
        }

        return preg_match('/'.$url_regex.'/i', $url);
    }

    public function isValidIPAddress($IP){
        return preg_match('/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/',$IP);
    }

    public function isValidUsername($username){
        #alphabet, digit, @, _ and . are allow. Minimum 6 character. Maximum 50 characters (email address may be more)
        return preg_match('/^[a-zA-Z\d_@.]{6,50}$/i', $username);
    }

    public function isValidPassword($password){
        #must contain 8 characters, 1 uppercase, 1 lowercase and 1 number
        return preg_match('/^(?=^.{4,}$)((?=.*[A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z]))^.*$/', $password);
    }

    public function isValidCreditCard($cc){
        #eg. 718486746312031
        return preg_match('/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6011[0-9]{12}|3(?:0[0-5]|[68][0-9])[0-9]{11}|3[47][0-9]{13})$/', $cc);
    }

    public function isValidDate($date){
        #05/12/2109
        #05-12-0009
        #05.12.9909
        #05.12.99
        return preg_match('/^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.][0-9]?[0-9]?[0-9]{2})*$/', $date);
    }

    public function sanitizePhone($phone){
        $phone = preg_replace('#[\- ]+#is', '', $phone);
        $phone = preg_replace('#^\+#is', '00', $phone);

        return $phone;
    }

    public function isValidPhone($phone){
        return preg_match('/^(\+?\d{7,15})(( x| ext)\d{1,5})?$/', $phone);
    }

    public function isValidColor($color){
        #CCC
        #CCCCC
        #FFFFF
        return preg_match('/^#(?:(?:[a-f0-9]{3}){1,2})$/i', $color);
    }

    public function _clean($str){
        return is_array($str) ? array_map(array($this, '_clean'), $str) : str_replace('\\', '\\\\', strip_tags(trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES))));
    }
    /*usage call it somewhere in beginning of your script
    _clean($_POST);
    _clean($_GET);
    _clean($_REQUEST);// and so on..*/
}

?>