<?php
include('vendor/validation.php');
class Uou_Careers_ajax_post extends Form_Validation{


            private $_name = null;
            private $_fname = null;
            private $_lname = null;
            private $_username = null;
            private $_password = null;
            private $_email = null;
            private $_role = null;
            private $_user_nicename = null;

            private $_registration_type = null;
            private $_response = array();

            // create a response object
            private $response = null;
            
            // create an errors object
            private $errors = null;

            public function __construct(){
                $this->response = new stdClass;
                $this->errors = new stdClass;
                // registration
                add_action( "wp_ajax_nopriv_uc_register", array ( $this, 'uc_register_validation' ) );
                add_action( "wp_ajax_uc_register",   array ( $this, 'uc_register_validation' ) );

                add_action( "wp_ajax_nopriv_insert_attachment", array ( $this, 'insert_attachment' ) );
                add_action( "wp_ajax_insert_attachment",   array ( $this, 'insert_attachment' ) );

                add_action( "wp_ajax_nopriv_uou_save_post", array ( $this, 'uou_save_post' ) );
                add_action( "wp_ajax_uou_save_post",   array ( $this, 'uou_save_post' ) );

                add_action( "wp_ajax_nopriv_uou_edit_post", array ( $this, 'uou_edit_post' ) );
                add_action( "wp_ajax_uou_edit_post",   array ( $this, 'uou_edit_post' ) );

                add_action( "wp_ajax_nopriv_uou_delete_post", array ( $this, 'uou_delete_post' ) );
                add_action( "wp_ajax_uou_delete_post",   array ( $this, 'uou_delete_post' ) );

                add_action( "wp_ajax_nopriv_upload_editor_image", array ( $this, 'upload_editor_image' ) );
                add_action( "wp_ajax_upload_editor_image",   array ( $this, 'upload_editor_image' ) );

                // company edit action
                add_action( "wp_ajax_nopriv_uc_edit_worker", array ( $this, 'uc_edit_worker' ) );
                add_action( "wp_ajax_uc_edit_worker",   array ( $this, 'uc_edit_worker' ) );

                // user resume save action
                add_action( "wp_ajax_nopriv_uc_user_save_resume", array ( $this, 'uc_user_save_resume' ) );
                add_action( "wp_ajax_uc_user_save_resume",   array ( $this, 'uc_user_save_resume' ) );

                // create recruiters
                add_action( "wp_ajax_nopriv_uc_delete_user", array ( $this, 'uc_delete_user' ) );
                add_action( "wp_ajax_uc_delete_user",   array ( $this, 'uc_delete_user' ) );
                // delete recruiters
                add_action( "wp_ajax_nopriv_uc_create_user", array ( $this, 'uc_create_user' ) );
                add_action( "wp_ajax_uc_create_user",   array ( $this, 'uc_create_user' ) );

                 add_action( "wp_ajax_nopriv_uc_edit_user", array ( $this, 'uc_edit_user' ) );
                add_action( "wp_ajax_uc_edit_user",   array ( $this, 'uc_edit_user' ) );

                // create job
                add_action( "wp_ajax_nopriv_uc_create_job", array ( $this, 'uc_create_job' ) );
                add_action( "wp_ajax_uc_create_job",   array ( $this, 'uc_create_job' ) );

                // edit job
                add_action( "wp_ajax_nopriv_uc_edit_job", array ( $this, 'uc_edit_job' ) );
                add_action( "wp_ajax_uc_edit_job",   array ( $this, 'uc_edit_job' ) );

                // delete job
                add_action( "wp_ajax_nopriv_uc_delete_job", array ( $this, 'uc_delete_job' ) );
                add_action( "wp_ajax_uc_delete_job",   array ( $this, 'uc_delete_job' ) );

                //login action
                add_action( "wp_ajax_nopriv_uc_login", array ( $this, 'uc_login' ) );
                add_action( "wp_ajax_uc_login",   array ( $this, 'uc_login' ) );

                
                // get not assigned job
                add_action( "wp_ajax_nopriv_uc_not_assigned_job", array ( $this, 'uc_not_assigned_job' ) );
                add_action( "wp_ajax_uc_not_assigned_job",   array ( $this, 'uc_not_assigned_job' ) );

                // get not assigned recruiter
                add_action( "wp_ajax_nopriv_uc_recruiters", array ( $this, 'uc_recruiters' ) );
                add_action( "wp_ajax_uc_recruiters",   array ( $this, 'uc_recruiters' ) );

                
                // assign recruiter to job
                add_action( "wp_ajax_nopriv_uc_assign_recruiter_to_job", array ( $this, 'uc_assign_recruiter_to_job' ) );
                add_action( "wp_ajax_uc_assign_recruiter_to_job",   array ( $this, 'uc_assign_recruiter_to_job' ) );

                // assign recruiter to company
                add_action( "wp_ajax_nopriv_uc_unique_username", array ( $this, 'uc_unique_username' ) );
                add_action( "wp_ajax_uc_unique_username",   array ( $this, 'uc_unique_username' ) );

                 // assign recruiter to company
                add_action( "wp_ajax_nopriv_uc_unique_email", array ( $this, 'uc_unique_email' ) );
                add_action( "wp_ajax_uc_unique_email",   array ( $this, 'uc_unique_email' ) );


                add_action( "wp_ajax_nopriv_uc_store_subscription_data", array ( $this, 'uc_store_subscription_data' ) );
                add_action( "wp_ajax_uc_store_subscription_data",   array ( $this, 'uc_store_subscription_data' ) );

                add_action( "wp_ajax_nopriv_uc_get_plans", array ( $this, 'uc_get_plans' ) );
                add_action( "wp_ajax_uc_get_plans",   array ( $this, 'uc_get_plans' ) );


                 add_action( "wp_ajax_nopriv_uc_remove_assigned_recruiter_from_job", array ( $this, 'uc_remove_assigned_recruiter_from_job' ) );
                add_action( "wp_ajax_uc_remove_assigned_recruiter_from_job",   array ( $this, 'uc_remove_assigned_recruiter_from_job' ) );

                add_action( "wp_ajax_nopriv_uou_get_country", array ( $this, 'uou_get_country' ) );
                add_action( "wp_ajax_uou_get_country",   array ( $this, 'uou_get_country' ) ); 

                add_action( "wp_ajax_nopriv_uou_get_state", array ( $this, 'uou_get_state' ) );
                add_action( "wp_ajax_uou_get_state",   array ( $this, 'uou_get_state' ) ); 

                add_action( "wp_ajax_nopriv_uou_career_save_as_bookmark", array ( $this, 'uou_career_save_as_bookmark' ) );
                add_action( "wp_ajax_uou_career_save_as_bookmark",   array ( $this, 'uou_career_save_as_bookmark' ) ); 

            }
            public function uou_career_save_as_bookmark(){
                extract($_POST);
                $user_id = get_current_user_id();
                $bookmarkedItemIDList = get_user_meta( $user_id, $key, true );
                if ( ! is_user_logged_in() ) {
                    $this->errors->already = "You need to login first.";
                    $this->response->errors = $this->errors;
                }else{
                    if(isset($bookmarkedItemIDList) && is_array($bookmarkedItemIDList)){
                        if(in_array($id, $bookmarkedItemIDList)){
                            $this->errors->already = "This item already bookmarked.";
                            $this->response->errors = $this->errors;
                        }else{
                            $prev = $bookmarkedItemIDList;
                            $bookmarkedItemIDList[] = $id;
                            update_user_meta( $user_id, $key, $bookmarkedItemIDList, $prev );
                            $this->response->saved = true;
                        }
                       
                    } else{
                        add_user_meta( $user_id, $key, array($id) );
                        $this->response->saved = true;
                    }
                }    
                echo json_encode( $this->response);
                wp_die();
                
            }
            public function uou_get_country(){
                global $wpdb;
                $term_id = $_POST['term_id'];
                $sql = "SELECT $wpdb->terms.name, $wpdb->term_taxonomy.* FROM $wpdb->term_taxonomy ".
                        " INNER JOIN $wpdb->terms ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id WHERE $wpdb->term_taxonomy.parent = $term_id    ORDER BY $wpdb->term_taxonomy.taxonomy";
                //echo $sql;
                $query = $wpdb->get_results( $sql );
                $terms = array();
                $i = 0;
                foreach ($query as $value) {
                    $terms[$value->taxonomy][$i]['term_id'] = $value->term_id;
                    $terms[$value->taxonomy][$i]['term_name'] = $value->name;
                    $i++;
                }
                echo json_encode( $terms);
                wp_die();
            }
            public function uou_get_state(){
                global $wpdb;
                $term_id = $_POST['term_id'];
                $sql = "SELECT $wpdb->terms.name, $wpdb->term_taxonomy.* FROM $wpdb->term_taxonomy ".
                        " INNER JOIN $wpdb->terms ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id WHERE $wpdb->term_taxonomy.parent = $term_id    ORDER BY $wpdb->term_taxonomy.taxonomy";
                //echo $sql;
                $query = $wpdb->get_results( $sql );
                $terms = array();
                $i = 0;
                foreach ($query as $value) {
                    $terms[$value->taxonomy][$i]['term_id'] = $value->term_id;
                    $terms[$value->taxonomy][$i]['term_name'] = $value->name;
                    $i++;
                }
                echo json_encode( $terms);
                wp_die();
            }

            public function uc_get_plans(){

                $options =  json_decode( get_option('subscription_plans') );
                if(version_compare(phpversion(), '5.3.3', '>='))
                    echo json_encode( $options , JSON_NUMERIC_CHECK );
                else
                     echo json_encode( $options);
               
                wp_die();
            }








            // store subscription plans and push the plans into the stripe/ different payemnt gateway . 


            public function uc_store_subscription_data(){


                global $careers;
                if(version_compare(phpversion(), '5.3.3', '>='))
                    $data = json_encode( $_POST['data'] , JSON_NUMERIC_CHECK );
                else
                    $data = json_encode( $_POST['data'] );

                if( $data !=  get_option('subscription_plans') )
                {
                 
                    delete_option( 'subscription_plans' );
                    $store = update_option( 'subscription_plans' , $data );

                    if( $store ){   
                           do_action( 'careers_gateway_execute_'.$careers->payment_option );
                           echo json_encode(array('msg'=>"Successfully added", "error"=> 0 ) );                       
                    }
                }else{
                     echo json_encode(array('msg'=>"Nothing Changed !", "error"=> 0 ) );  
                }

                wp_die();
            }


            function uc_unique_username(){

                if (username_exists( $_POST['username'] )) echo json_encode(array('isValid'=>false));
                else echo json_encode(array('isValid'=>true));
                exit;
            }
            
            function uc_unique_email(){
                //echo json_encode(array('isValid'=>$_POST['email']));exit;
                if ( email_exists( $_POST['email']))echo json_encode(array('isValid'=>false));
                else echo json_encode(array('isValid'=>true));
                exit;
            }
            function uc_assign_recruiter_to_job(){
                extract($_POST);
                if ( !update_post_meta ($job_id, 'recruiter_id', $recruiter_id) ) { 
                    add_post_meta($job_id, 'recruiter_id', $recruiter_id, true ); 
                }; 
                do_action( 'uou_job_assignment', $job_id, $recruiter_id);

                $this->response->msg = "Successfully assigned recruiter to this job";

                echo json_encode($this->response);
                wp_die();
            }
            function uc_remove_assigned_recruiter_from_job(){
                extract($_POST);
                if ( !update_post_meta ($job_id, 'recruiter_id', 0) ) { 
                    add_post_meta($job_id, 'recruiter_id', 0, true ); 
                }; 
                //do_action( 'uou_job_assign', $job_id, $recruiter_id, true );
                $this->response->msg = "Successfully remove recruiter from job";

                echo json_encode($this->response);
                wp_die();
            }
            
            public function uc_recruiters(){
                extract($_POST);
                global $wpdb;
                $args = array(
                    'meta_key' => 'company_id',
                    'meta_value'=> $company_id,
                    'role' => 'recruiter'
                );
                $users = get_users( $args );
                $key = 0;
                foreach ($users as $value) {
                    $query[$key]['id'] = $value->ID;
                    $query[$key]['recruiter_name'] = $value->user_login;
                    $key++;
                }
                
                $this->response->recruiterlist = $query;
                echo json_encode($this->response);
                wp_die();
            
            }
             public function uc_not_assigned_job(){

                extract($_POST);


                global $wpdb;
                $query = array();
                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => 'company_id',
                            'value' => $company_id
                        ),
                        array(
                            'key' => 'recruiter_id',
                            'value' => $recruiter_id
                        )

                    ),
                    'post_type' => 'job',
                    'post_status'=> 'publish',
                    'posts_per_page' => 999999,
                );
                $items = get_posts($args);
                $key = 0;
                foreach ($items as $value) {
                    $query[$key]['ID'] = $value->ID;
                    $query[$key]['post_title'] = $value->post_title;
                    $query[$key]['post_content'] = strip_tags($value->post_content);
                    $query[$key]['assigned'] = 1;
                    $key++;
                }
                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => 'company_id',
                            'value' => $company_id
                        ),
                        array(
                            'key' => 'recruiter_id',
                            'value' => 0
                        )

                    ),
                    'post_type' => 'job',
                    'post_status'=> 'publish',
                    'posts_per_page' => 999999,
                );
                
                $items = get_posts($args);
                foreach ($items as $value) {
                    $query[$key]['ID'] = $value->ID;
                    $query[$key]['post_title'] = $value->post_title;
                    $query[$key]['post_content'] = strip_tags($value->post_content);
                    $query[$key]['assigned'] = 0;
                    $key++;
                }
                $this->response->companylist = $query;
                echo json_encode($this->response);
                wp_die();
            }
            

            
            //starting of job section
            public function updateJobMeta( $id=0, $key = null, $value = null ){
                if ( !update_post_meta ($id, $key, $value) ) { 
                    add_post_meta($id, $key, $value, true ); 
                }; 
            }

            public function uc_create_job(){

                extract($_POST);
               // print_r($data);exit;
                $result = array();

                // Create Job

                if(isset($data['post_meta']['featured']) && $data['post_meta']['featured'] ){
                    $data['post_meta']['featured'] = 1;
                    $quota = "feature_job_quota";
                    if( !Uou_Careers_user_permission::checkQuotaAvailablity( "feature_job_quota", 'job', 1 )){
                        $this->errors->jobs_quota = esc_attr('Your don\'t have enough quota to post a featured job.','uou-careers');
                    }
                }else{
                    $quota = "job_quota";
                    if( !Uou_Careers_user_permission::checkQuotaAvailablity( "job_quota", 'job', 0 )){
                        $this->errors->jobs_quota = esc_attr('Your don\'t have enough quota to post a job.','uou-careers');
                    }
                }    

                // validate fields
                if ( !isset($data['post_title']) || empty($data['post_title']) ) {
                    $this->errors->post_title = "Please enter job title";
                }
                if ( !isset($data['post_content']) || empty($data['post_content']) ) {
                    $this->errors->post_content = "Please enter job description";
                }
                
                if (get_object_vars($this->errors)) {
                        $this->response->errors = $this->errors;
                } else {
                    $user_id = get_current_user_id();
                    $user_info = get_userdata($user_id);
                    $role = @implode(', ', $user_info->roles);

                    $job_data = array(
                        'post_title'    => wp_strip_all_tags( $data['post_title'] ),
                        'post_content' => $data['post_content'],
                        'post_status'   => 'publish',
                        'post_type'     => 'job',
                        'post_author'   => $user_id
                    );

                    // Insert the company post into the database
                    $job_id = wp_insert_post( $job_data );

                    if($job_id){
                        if(isset($data['taxonomy'])){
                            $locationArray =array();
                            foreach ($data['taxonomy'] as $key => $value) {
                                if($key == "country" || $key == "state" || $key == "region" || $key == "location"){
                                    $locationArray[] = $value;
                                    continue;
                                }

                                if(is_array($value))
                                    $cat_ids = array_map( 'intval', $value );
                                else   
                                    $cat_ids = intval($value);
                                wp_set_object_terms( $job_id, $cat_ids, $key);
                            }
                            
                            if(!empty($locationArray)){
                                $terms = array_map( 'intval', $locationArray );//print_r($terms);
                                $error = wp_set_object_terms( $job_id, $terms, 'location');//print_r($error);
                            } 
                        }
                        foreach ($data['post_meta'] as $key => $value) {
                            if(is_array($value)){
                                $value = json_encode($value);
                            }
                            // Add or Update the meta field in the database.
                            if ( ! update_post_meta ($job_id, $key, $value) ) { 
                                        add_post_meta($job_id, $key, $value, true ); 
                            };     
                        }
                        
                    $this->response->recruiter_id = 0;    
                    switch ($role) {
                        case 'company_admin':
                            $this->updateJobMeta($job_id, "company_id", $data['post_id']);
                            $this->updateJobMeta($job_id, "recruiter_id", 0);
                            $admin_id = $user_id;
                            break;

                        case 'recruiter':
                            $admin_id = get_user_meta($user_id, "company_admin_id", true);
                            $company_id = get_user_meta($user_id, "company_id", true);
                            $this->updateJobMeta($job_id, "company_id", $company_id);
                            $this->updateJobMeta($job_id, "recruiter_id", $user_id);
                            $this->response->recruiter_id = $user_id;
                            break;
                   
                        
                        default:
                            break;
                    }
                        
                        //echo $limit;
                        $this->response->saved = true;
                        $this->response->ID = $job_id;
                        
                    }else{
                       $response->error_message = "Sorry, there was a problem saving your information.";
                    }
                }  
                    
                echo json_encode($this->response);
                wp_die();
            }



            function uc_delete_job(){
                if ( wp_delete_post( $_POST['id'] )){
                    delete_post_meta($_POST['id'],'company_id' );

                    $this->response->saved = true;
                }else{
                    $this->response->errors->id = 'You provide wrong user id';
                }
                echo json_encode($this->response);
                

                wp_die();
            }



            public function uc_edit_job(){
                extract($_POST);
                $result = array();

                // validate fields
                if ( !isset($data['post_title']) || empty($data['post_title']) ) {
                    $this->errors->post_title = "Please enter job title";
                }
                if ( !isset($data['post_content']) || empty($data['post_content']) ) {
                    $this->errors->post_content = "Please enter job description";
                }
                
                if (get_object_vars($this->errors)) {
                        $this->response->errors = $this->errors;
                } else {
                    $user_id = get_current_user_id();
                    $user_info = get_userdata($user_id);
                    $role = @implode(', ', $user_info->roles);

                    $job_data = array(
                        'ID'           => $data['id'],
                        'post_title'    => wp_strip_all_tags( $data['post_title'] ),
                        'post_content' => $data['post_content'],
                        'post_status'   => 'publish',
                        'post_type'     => 'job',
                    );

                    // Insert the company post into the database
                    $job_id = wp_update_post( $job_data );

                    if($job_id){
                        if(isset($data['taxonomy'])){
                            $locationArray =array();
                            foreach ($data['taxonomy'] as $key => $value) {
                                if($key == "country" || $key == "state" || $key == "region" || $key == "location"){
                                    $locationArray[] = $value;
                                    continue;
                                }

                                if(is_array($value))
                                    $cat_ids = array_map( 'intval', $value );
                                else   
                                    $cat_ids = intval($value);
                                wp_set_object_terms( $job_id, $cat_ids, $key);
                            }
                            
                            if(!empty($locationArray)){
                                $terms = array_map( 'intval', $locationArray );//print_r($terms);
                                $error = wp_set_object_terms( $job_id, $terms, 'location');//print_r($error);
                            } 
                        }
                        foreach ($data['post_meta'] as $key => $value) {
                            // Add or Update the meta field in the database.
                            if(is_array($value)){
                                $value = json_encode($value);
                            }
                            if ( ! update_post_meta ($job_id, $key, $value) ) { 
                                        add_post_meta($job_id, $key, $value, true ); 
                            };     
                        }
                        //echo $limit;
                        $this->response->saved = true;
                        $this->response->ID = $job_id;
                        
                    }else{
                       $response->error_message = "Sorry, there was a problem saving your information.";
                    }
                }   
                    
                echo json_encode($this->response);
                wp_die();
            }
            //ending of job section

            // user manipulation seciton
            public function uc_create_user(){
                extract($_POST);
                $result = array();

                if( !Uou_Careers_user_permission::checkQuotaAvailablity( "member_quota", 'recruiter' )){
                    $this->errors->recruiter_quota = esc_attr('Your dont have enough quota to add a recruiter.','uou-careers');
                }
                // validate fields
                if ( !isset($data['user_login']) || empty($data['user_login']) ) {
                    $this->errors->user_login = "Please enter your username";
                }else{
                    $this->_user_nicename = $data['user_login'];
                    $this->_username = $data['user_login'];
                }

                if( $this->uc_taken_username($data['user_login'])){
                    $this->errors->unique_username = 'Username must be unique.';
                }
                if( !$this->isValidUsername($data['user_login'], 1)){
                     $this->errors->user_unique = "Username must be valid.";
                }
                
                if ( !isset($data['user_pass']) || empty($data['user_pass']) ) {
                    $this->errors->user_pass = "Please enter your password";
                }else{
                    $this->_password = $data['user_pass'];
                }
                if (!isset($data['user_email']) || !filter_var($data['user_email'],FILTER_VALIDATE_EMAIL)) {
                    $this->errors->user_email = "Please enter a valid email address";
                }else{
                    $this->_email = $data['user_email'];
                }

                
                if( $this->uc_taken_email($data['user_email'])){
                    $this->errors->unique_email = 'Email already taken.';
                }else{
                    $this->_email = $data['user_email'];
                }
                
                // check to see if errors exist
                if (get_object_vars($this->errors)) {
                    $this->response->errors = $this->errors;
                } else {
                    if($data['role'] == "recruiter")
                        $this->_role = 'recruiter';
                    $user = $this->uou_create_user_safe( );
                    $user_id = $user['user_id'];
                    // Create company
                    $post_data = array(
                        'post_title'    => wp_strip_all_tags( $this->_username ),
                        'post_status'   => 'publish',
                        'post_type'     => $this->_role,
                        'post_author'   => $user_id
                    );

                    // Insert the company post into the database
                    $post_id = wp_insert_post( $post_data );
                    if($user_id){

                        add_user_meta( $user_id, 'company_id', $data['company_id']);
                        add_user_meta( $user_id, 'company_admin_id', $data['company_admin_id']);

                        if ( ! update_post_meta ($post_id, 'company_id', $data['company_id']) ) { 
                            add_post_meta($post_id, 'company_id', $data['company_id'], true ); 
                        };
                        if ( ! update_post_meta ($post_id, 'company_admin_id', $data['company_admin_id']) ) { 
                            add_post_meta($post_id, 'company_admin_id', $data['company_admin_id'], true ); 
                        };
                        
                        $this->response->company_id = get_user_meta($user_id,'company_id',true);
                        $this->response->saved = true;
                        $this->response->ID = $user_id;
                    }else{
                        $response->error_message = "Sorry, there was a problem saving your information.";
                    }
                }
                echo json_encode($this->response);
                wp_die();
            }
            public function uc_edit_worker(){
                extract($_POST);
                //print_r($data);exit;

                $result = array();

                // validate fields
                if ( !isset($data['user_login']) || empty($data['user_login']) ) {
                    $this->errors->user_login = "Please enter your username";
                }
                
                if (!isset($data['user_email']) || !filter_var($data['user_email'],FILTER_VALIDATE_EMAIL)) {
                    $this->errors->user_email = "Please enter a valid email address";
                }

                if( !$this->isValidUsername($data['user_login'], 1)){
                     $this->errors->user_valid = "Username must be valid.";
                }
                $user_old = get_userdata($data['id']);


                if( $this->uc_taken_email($data['user_email']) && $user_old->user_email != $data['user_email']){
                    $this->errors->unique_email = 'Email already taken.';
                }


                if( $this->uc_taken_username($data['user_login']) && $user_old->user_login != $data['user_login']){
                    $this->errors->unique_username = 'Username already taken.';
                }
                
                // check to see if errors exist
                if (get_object_vars($this->errors)) {
                    $this->response->errors = $this->errors;
                } else {
                    $user_id = wp_update_user( array( 
                        'ID' => $data['id'],
                        'user_login'=>$data['user_login'],
                        'user_email' => $data['user_email']
                        ) 
                    );

                    if ( is_wp_error( $user_id ) ) {
                        // There was an error, probably that user doesn't exist.
                        $response->error_message = "Sorry, there was a problem saving your information.";
                    } else {
                        $this->response->saved = true;
                        $this->response->ID = $user_id;
                    }
                   

                }
                echo json_encode($this->response);
                wp_die();
            }
            public function uc_edit_user( ){
                        
                $user_info = get_userdata(get_current_user_id());
                $role = implode(', ', $user_info->roles);
                $result = array( );

                //print_r($_POST);exit;
                if ( isset($_POST[ 'data' ][ 'user_nicename']) && !empty( $_POST[ 'data' ][ 'user_nicename'] ) )
                    wp_update_user( array ('ID' => get_current_user_id(), 'user_nicename'=> esc_attr( $_POST['data']['user_nicename'] ) ) );
                else
                    $this->errors->name = 'Please enter a name.';       

                if ( isset($_POST[ 'data' ][ 'user_url']) && !empty( $_POST['data']['user_url']  ) )
                    wp_update_user( array ('ID' => get_current_user_id(), 'user_url'=> esc_url( $_POST['data']['user_url'] ) ) );
                // else
                //     $this->errors->user_url = 'Please enter a valid url.';

                if ( isset($_POST[ 'data' ][ 'user_email']) && !empty( $_POST['data']['user_email'] ) ){
                            
                    if (!is_email(esc_attr( $_POST['data']['user_email'] ))){
                        $this->errors->email = __('The Email you entered is not valid.  please try again.', 'profile');
                   
                    }elseif(email_exists(esc_attr( $_POST['data']['user_email'] )) != get_current_user_id() ){
                        $this->errors->email = __('The Email you entered is not valid.  please try again.', 'profile');
                    }
                    wp_update_user( array ('ID' => get_current_user_id(), 'user_email' => esc_attr( $_POST['data']['user_email'] )));
                
                }else{
                    $this->errors->email = 'Please enter a valid email address.';     
                }
                if (get_object_vars($this->errors)) {
                    $this->response->errors = $this->errors;
                } else {
                    
                    
                    $user_update_post = array(
                            'ID'           => $_POST['data']['post_id'],
                            'post_title'   => $_POST['data']['post_title'],
                            'post_content' => $_POST['data']['post_content'],
                    );

                    $do_update = wp_update_post( $user_update_post );
                   
                    if($do_update){
                        if(isset($_POST['data']['image']['attachment_id'])){
                            set_post_thumbnail( $_POST['data']['post_id'], $_POST['data']['image']['attachment_id'] );
                        }
                        if(isset($_POST['data']['taxonomy'])){
                            $locationArray =array();
                            foreach ($_POST['data']['taxonomy'] as $key => $value) {
                                if($key == "country" || $key == "state" || $key == "region"){
                                    $locationArray[] = $value;
                                    continue;
                                }

                                $terms = intval($value);
                                wp_set_object_terms( $_POST['data']['post_id'], $terms, $key);
                            }
                            
                            if(!empty($locationArray)){

                                $terms = array_map( 'intval', $locationArray );//print_r($terms);
                                $error = wp_set_object_terms( $_POST['data']['post_id'], $terms, 'location');//print_r($error);
                            }    
                        }
                        if(isset($_POST['data']['post_meta']) && is_array($_POST['data']['post_meta'])){
                            foreach ($_POST['data']['post_meta'] as $key => $value) {
                                        if(is_array($value)){
                                            $value = json_encode($value);
                                        }
                                        if ( ! update_post_meta ($_POST['data']['post_id'], $key, $value) ) { 
                                                    add_post_meta($_POST['data']['post_id'], $key, $value, true ); 
                                        };     
                            }
                        }
                        $this->response->saved = true;

                    }else{
                        $response->error_message = "Sorry, there was a problem saving your information.";
                    }
                    
                }        


                echo json_encode($this->response);
                wp_die();
            }


            function uc_delete_user(){

                if ( wp_delete_user( $_POST['id'] )){
                    global $wpdb;
                    $pids = $wpdb->get_col($wpdb->prepare("SELECT ID FROM {$wpdb->posts} WHERE post_author = %d AND post_status = 'publish' AND post_type=%d", $_POST['id'], $_POST['role']));
                    foreach($pids as $post_id)
                    {
                      wp_delete_post( $post_id );
                      delete_post_meta($_POST['id'],'post_id' );
                    }
                     
                    $this->response->saved = true;
                }else{
                    $this->response->errors->id = 'You provide wrong user id';
                }
                echo json_encode($this->response);
                exit;
            }
            //ending of user manipulation section

            //authentication hook 
            public function uc_login(){
                    global $wp_query;
                    // First check the nonce, if it fails the function will break
                   // check_ajax_referer( 'ajax-login-nonce', 'security' );

                    // Nonce is checked, get the POST data and sign user on
                    $info = array();
                    $info['user_login'] = $_POST['data']['username'];
                    $info['user_password'] = $_POST['data']['password'];
                    $info['remember'] = true;
                    $user_signon = wp_signon( $info, false );
                    if ( is_wp_error($user_signon) ){

                        echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
                    } else {

                        $role= @$user_signon->user_signon->roles[0];

                        global $wpdb;
                        $post_name = $wpdb->get_var("SELECT post_name FROM ".$wpdb->prefix."posts WHERE post_author = '" .  $user_signon->ID . "' AND post_type = 'company' AND post_status = 'publish'");

                        echo json_encode(array(
                            'loggedin'=>true,
                            'message'=>__('Login successful, redirecting...'),
                            'url'  =>  get_site_url()."/dashboard"
                        ));


                    }

                    wp_die();
                }
                // end of authentication hook

                // user creating function 
                public function uou_create_user_safe(){
                    $userdata = array(
                        'user_login'    =>  $this->_username,
                        'user_pass'  => $this->_password,
                        'user_email'  => $this->_email,
                        'user_nicename' => $this->_user_nicename,
                        'role'  => $this->_role
                    );
                    $user_id = wp_insert_user( $userdata ) ;

                    //On success
                    if( !is_wp_error($user_id) ) {
                        return array('user_id'=>$user_id, 'error'=>"");
                    }else{
                        return array('error'=>true);
                    }
                }
                // registreation hook
                public function uc_register_validation(){

                    global $uc_signup_errors;
                    extract($_POST);
                    if( $this->isValidText($data['name'], 1) && $this->isValidString($data['name'], 1) ){
                        $this->_name = trim($data['name']);
                    }else{
                        $this->errors->name = 'Please enter a valid name';
                    }

                    if( $this->isValidUsername($data['username'], 1) ){
                        $this->_username = trim($data['username']);
                    }else{
                        $this->errors->username = ' Alphabet, digit, @, _ and . are allow. Minimum 6 character. Maximum 50 characters';
                    }
                    if( $this->uc_taken_username($data['username'])){
                        $this->errors->user = ' Username already taken.';
                    }

                    if( $this->isValidEmail($data['email'], 1) ){
                        $this->_email = $this->sanitizeEmaill($data['email']);
                    }else{
                        $this->_response['error']['email'] = 'Please enter a valid email';
                         $this->errors->user = ' Username already taken.';
                    }
                    if( $this->uc_taken_email($data['email'])){
                        $this->errors->user = ' Email already taken.';
                    }

                    if( isset($data['password']) ){
                        $this->_password = trim($data['password']);
                    }else{
                         $this->errors->password = 'Password field is empty.';
                    }
                    if($this->isValidText($data['registration_type']['value'], 2)){
                        $this->_registration_type = trim($data['registration_type']['value']);
                    }else{
                        $this->errors->registration_type = "Please Select a Registration 'type";
                    }
                    if (get_object_vars($this->errors)) {
                        $this->response->errors = $this->errors;
                    } else {
                        $this->uc_register_handle();
                        $info = array();
                        $info['user_login'] = $this->_username;
                        $info['user_password'] = $this->_password;
                        $info['remember'] = true;

                        $user_signon = wp_signon( $info, false );
                        $this->response->saved = true;
                        $this->response->url =  get_site_url()."/dashboard";
                        
                    }
                    //print_r($data);
                    echo json_encode($this->response);
                    wp_die();
                }
                
                public function uc_register_handle(){


                    global $careers;

                    if($this->_registration_type == 'company'){
                        // Create company Admin
                        $this->_role = 'company_admin';
                        $company_admin = $this->uou_create_user_safe( );


                        if( strlen($company_admin['error']) )
                            return;

                        $user_id = $company_admin['user_id'];


                        // user_meta te first plan id ta dhukaite hobe . 

                        update_user_meta( $user_id , 'membership_type' , 1 );


                        // Create company
                        $company_data = array(
                            'post_title'    => wp_strip_all_tags( $this->_name ),
                            'post_status'   => 'publish',
                            'post_type'     => 'company',
                            'post_author'   => $user_id
                        );

                        // Insert the company post into the database
                        $company = wp_insert_post( $company_data );

                       
                        // Insert the company post into the database
                        add_user_meta( $user_id, "membership_type", 1 );
                    }else{

                        // Create company Admin
                        $this->_role = 'candidate';
                        $candidate = $this->uou_create_user_safe( );
                        if(strlen($candidate['error']))
                            return;
                        $user_id = $candidate['user_id'];
                        // Create Company
                        $candidate_data = array(
                            'post_title'    => wp_strip_all_tags(  $this->_name ),
                            'post_status'   => 'publish',
                            'post_type'     => 'candidate',
                            'post_author'   => $user_id
                        );

                        $candidate_post_id = wp_insert_post( $candidate_data );
                        
                    }
                    $user_data = array(
                        'user_login'    =>  $this->_username,
                        'user_pass'  => $this->_password,
                        'user_email'  => $this->_email,
                        'user_nicename' => $this->_user_nicename,
                        'role'  => $this->_role
                    );
                    do_action( 'uou_new_user', $user_id, $user_data, true );
                    return true;
                }
                
       
    //$file_handler, $post_id, $settpdf='false'
    function insert_attachment( ) {
        global $wpdb;
        $user_ID = get_current_user_id();
        $user_info = get_userdata($user_ID);
        $role = @implode(', ', $user_info->roles);
        if($role == "company_admin"){
            $post_type = '会社';
        }elseif($role == "candidate"){
            $post_type = '候補者';
        }elseif ($role == "recruiter") {
            $post_type = 'リクルーター';
        }
        $sql = $wpdb->prepare( "SELECT ID FROM $wpdb->posts ".
            " WHERE $wpdb->posts.post_author = '%s'".
            " AND ($wpdb->posts.post_status = 'publish') AND ($wpdb->posts.post_type = '".$post_type."') ", $user_ID );

        $post = $wpdb->get_row( $sql );
        // check to make sure its a successful upload
        if( !empty($_SERVER['HTTP_ORIGIN']) ){
            // Enable CORS
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
            header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');
        }
        if( $_SERVER['REQUEST_METHOD'] == 'OPTIONS' ){
            exit;
        }
        if( strtoupper($_SERVER['REQUEST_METHOD']) == 'POST' ){
            //file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/uploads/'.$user_ID.'.'.$ext[1], $data);
            
            require_once( ABSPATH . 'wp-admin' . '/includes/image.php' );
            require_once( ABSPATH . 'wp-admin' . '/includes/file.php' );
            require_once( ABSPATH . 'wp-admin' . '/includes/media.php' );

            $attach_id = media_handle_upload( 'filedata', $post->ID );
            update_post_meta( $post->ID,'avator', $attach_id );
            set_post_thumbnail( $post->ID, $attach_id );

            $image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium');
            $array = array(
                'filelink' => $image_attributes
            );
            echo stripslashes(json_encode($array));
            exit;
        }
    }
    function upload_editor_image(){
        // check to make sure its a successful upload
        if( !empty($_SERVER['HTTP_ORIGIN']) ){
            // Enable CORS
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
            header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type');
        }
        if( $_SERVER['REQUEST_METHOD'] == 'OPTIONS' ){
            exit;
        }
        if( strtoupper($_SERVER['REQUEST_METHOD']) == 'POST' ){
            //file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/uploads/'.$user_ID.'.'.$ext[1], $data);
            if(isset($_FILES)){
                $uploads_dir = dirname(dirname(dirname(dirname(__FILE__))))."/uploads/"."editor/";
                if(!is_dir($uploads_dir)){
                    mkdir($uploads_dir, 0777);
                }
                $tmp_name = $_FILES["file"]["tmp_name"];
                $name = $_FILES["file"]["name"];
                move_uploaded_file($tmp_name, $uploads_dir.$name);
                
                
            }

            $array = array(
                'filelink' => get_site_url().'/wp-content/uploads/editor/'.$_FILES["file"]["name"]
            );
            echo stripslashes(json_encode($array));
            exit;
        }
    }
    
    public function uc_user_save_resume( ){
        extract($_POST);               
        $user_info = get_userdata(get_current_user_id());
        $role = implode(', ', $user_info->roles);
        $result = array( );
        
        $explode = explode(' ', $data[ 'name']);
        if ( isset($data[ 'name' ]) && !empty( $data[ 'name'] ) ){
            update_user_meta( get_current_user_id(), 'first_name', $explode[0] );
            update_user_meta( get_current_user_id(), 'last_name', isset($explode[1])? $explode[1]: $explode[0]);
        }else{
            $this->errors->name = 'Please enter a name.';
        }           

       

        if(isset($data['taxonomy'])){
            $locationArray =array();
            foreach ($data['taxonomy'] as $key => $value) {
                if($key == "country" || $key == "state" || $key == "region" || $key == "location"){
                    $locationArray[] = $value;
                    continue;
                }

                $terms = intval($value);
                wp_set_object_terms( $data['post_id'], $terms, $key);
            }
            
            if(!empty($locationArray)){

                $terms = array_map( 'intval', $locationArray );//print_r($terms);
                $error = wp_set_object_terms( $data['post_id'], $terms, 'location');//print_r($error);
            }    
        }
        if(isset($data['resume_meta']) && is_array($data['resume_meta'])){
            foreach ($data['resume_meta'] as $key => $value) {
                if(is_array($value)){
                    $value = json_encode($value);
                }
                if ( ! update_post_meta ($data['post_id'], $key, $value) ) { 
                    add_post_meta($data['post_id'], $key, $value, true ); 
                };     
            }
        }
        
        if (get_object_vars($this->errors)) {
            $this->response->errors = $this->errors;
        } else {
            $this->response->saved = true;
        }
                


        echo json_encode($this->response);
        wp_die();
    }
    public function uou_save_post(){
        extract($_POST);
        $result = array();
        // validate fields
        if( !Uou_Careers_user_permission::checkQuotaAvailablity( "blog_quota" )){
                $this->errors->blog_quota = esc_attr('Your dont have enough quota to add a post.','uou-careers');
            }
        if ( !isset($data['post_title']) || empty($data['post_title']) ) {
            $this->errors->post_title = "Please enter title";
        }
        if ( !isset($data['post_content']) || empty($data['post_content']) ) {
            $this->errors->post_content = "Please enter description";
        }
        //print_r(wp_strip_all_tags( $data['post_content']));exit;
        if (get_object_vars($this->errors)) {
                $this->response->errors = $this->errors;
        } else {
            $post_data = array(
                'post_title'    => wp_strip_all_tags( $data['post_title']),
                'post_name'    =>  implode('-',explode(' ',wp_strip_all_tags( $data['post_title']))),
                'post_content'  => $data['post_content'],
                'post_status'   => 'publish',
                'post_type'     => $data['type'],
                'post_author'   => get_current_user_id()
            );

            // Insert the company post into the database
            $post_id = wp_insert_post( $post_data );

            if($post_id){
                //echo $limit;
                if(isset($data['image']['attachment_id'])){
                    set_post_thumbnail( $post_id, $data['image']['attachment_id'] );
                }
                $this->response->saved = true;
                $this->response->ID = $post_id;

            }else{
                $this->errors->unknown = "Something wrong would be here.";
                $this->response->errors = $this->errors;
            }
        }
        echo json_encode($this->response);
        wp_die();    
    }
    public function uou_edit_post(){
        extract($_POST);
        $result = array();

         // validate fields
        if ( !isset($data['post_title']) || empty($data['post_title']) ) {
            $this->errors->post_title = "Please enter title";
        }
        if ( !isset($data['post_content']) || empty($data['post_content']) ) {
            $this->errors->post_content = "Please enter description";
        }
        
        if (get_object_vars($this->errors)) {
                $this->response->errors = $this->errors;
        } else {
            $post_data = array(
                'ID'           => $data['id'],
                'post_title'    => wp_strip_all_tags( $data['post_title']),
                'post_name'    =>  implode('-',explode(' ',wp_strip_all_tags( $data['post_title']))),
                'post_content'  => $data['post_content'],
                'post_status'   => 'publish',
                'post_type'     => $data['type'],
                'post_author'   => get_current_user_id()
            );
            // Insert the company post into the database
            $post_id = wp_update_post( $post_data );

            if($post_id){
                if(isset($data['image']['attachment_id'])){
                    set_post_thumbnail( $post_id, $data['image']['attachment_id'] );
                }
                $this->response->saved = true;
                $this->response->ID = $post_id;

            }else{
                $this->errors->unknown = "Something wrong would be here.";
                $this->response->errors = $this->errors;
            }
        }
        echo json_encode($this->response);
        wp_die();    
    }

    function uou_delete_post(){
        if ( wp_delete_post( $_POST['id'] )){
            $this->response->saved = true;
        }else{
            $this->response->errors->id = 'You provide wrong user id';
        }
        echo json_encode($this->response);
        exit;
    }
    
    
    
    
   
    
}    

new Uou_Careers_ajax_post();