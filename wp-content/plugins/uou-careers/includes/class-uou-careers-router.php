<?php
class Uou_Careers_Router {



            public function __construct(){
                add_action( 'init', array( $this , 'add_careers_rule' ) );
                add_filter( 'query_vars',array( $this, 'add_careers_query_var' ) );
                add_filter( 'template_include', array( $this, 'load_careers_template' ) );
            }

            public function add_careers_rule(){
                global $wp_rewrite;


                add_rewrite_rule(  'candidate/([^/]+)', 'index.php?candidate=$matches[1]', 'top' );
                // add_rewrite_rule(  'candidate', 'index.php?candidate=yes', 'top' );
                add_rewrite_rule(  'company/([^/]+)', 'index.php?company=$matches[1]', 'top' );

                add_rewrite_rule(  'bookmarks', 'index.php?bookmarks=yes', 'top' );
                add_rewrite_rule(  'recruiter/([^/]+)', 'index.php?recruiter=$matches[1]', 'top' );
                add_rewrite_rule(  'recruiters', 'index.php?recruiters=yes', 'top' );
               
                add_rewrite_rule( 'dashboard/subscription/plan', 'index.php?dashboard=yes&plan=yes', 'top' );
                add_rewrite_rule( 'dashboard/subscription/add', 'index.php?dashboard=yes&add_subscription=yes', 'top' );
                add_rewrite_rule( 'dashboard/subscription/invoices', 'index.php?dashboard=yes&invoices=yes', 'top' );
                add_rewrite_rule( 'dashboard/subscription/update', 'index.php?dashboard=yes&update=yes', 'top' );
                add_rewrite_rule( 'dashboard/subscription', 'index.php?dashboard=yes&subscription=yes', 'top' );
                add_rewrite_rule( 'dashboard/my-profile', 'index.php?dashboard=yes&my-profile=yes', 'top' );
                add_rewrite_rule( 'dashboard', 'index.php?dashboard=yes', 'top' );

               
                $wp_rewrite->flush_rules();
            }


            

            public function add_careers_query_var( $vars ){

                        $vars[] = 'edit';
                        $vars[] = 'dashboard';
                        $vars[] = 'recruiter';
                        $vars[] = 'recruiters';
                        $vars[] = 'job';
                        $vars[] = 'company';
                        $vars[] = 'candidate';
                        $vars[] = 'single';
                        $vars[] = 'page';
                        $vars[] = 'plan';
                        $vars[] = 'invoices';
                        $vars[] = 'update';
                        $vars[] = 'add_subscription';
                        $vars[] = 'subscription';
                        $vars[] = 'my-profile';
                        $vars[] = 'bookmarks';


                       return $vars;
            }


             public function load_careers_template($template){
                        global $post;
                        
                        $template_loader = new Uou_Careers_Load_Template();
                        $role = Uou_Careers_basic_process::get_role();


                        if( get_query_var('dashboard') && get_query_var('plan') == 'yes' ){
                                          
                                // to update or change plans
                                return $template_loader->locate_template( 'dashboard/plans/plans.php' );
        
                        } 
                        elseif( get_query_var('dashboard') && get_query_var('invoices') == 'yes' ){

                                // all the invoices           
                                return $template_loader->locate_template( 'dashboard/plans/invoices.php' );
                        } 
                        elseif( get_query_var('dashboard') && get_query_var('update') == 'yes' ){

                                // updates                     
                                return $template_loader->locate_template( 'dashboard/plans/update.php' );
 
                        } 

                        elseif( get_query_var('dashboard') && get_query_var('subscription') == 'yes' ){

                                // updates                     
                                return $template_loader->locate_template( 'dashboard/plans/subscription.php' );
 
                        }  


                        elseif( get_query_var('dashboard') && get_query_var('add_subscription') == 'yes' ){

                                // updates                     
                                return $template_loader->locate_template( 'dashboard/plans/add.php' );
 
                        }
                        elseif( get_query_var('bookmarks') && get_query_var('bookmarks') == 'yes' ){
                              return $template_loader->locate_template( 'bookmarks.php' );
                        }                          




                         
                        elseif( get_query_var('dashboard'))  {
                                    switch ( $role ) {

                                                case 'company_admin':

                                                    if( get_query_var('my-profile') == 'yes' ){
                                                        return $template_loader->locate_template( 'dashboard/profile/company-profile.php' );

                                                    }else{

                                                        return $template_loader->locate_template( 'dashboard/company-dash.php' );
                                                    }
                                                break;    
                                                case 'recruiter':
                                                    if( get_query_var('my-profile') == 'yes' ){
                                                        return $template_loader->locate_template( 'dashboard/profile/recruiter-profile.php' );
                                                    }else{
                                                        return $template_loader->locate_template( 'dashboard/recruiter-dash.php' );
                                                    }    
                                                break;
                                               case 'candidate':
                                                    if( get_query_var('my-profile') == 'yes' ){
                                                        return $template_loader->locate_template( 'dashboard/profile/candidate-profile.php' );
                                                    }else{
                                                        return $template_loader->locate_template( 'dashboard/profile/candidate-profile.php' );
                                                    }        
                                                
                                                default:
                                                           // return get_template_directory().'/index.php';
                                                            return $template;
                                    }
                        }

                       // company
                        elseif( get_query_var( 'company' ) && get_query_var( 'company' ) != 'yes' ){

                                    return $template_loader->locate_template( 'company-single.php' );
                        
                        }elseif( get_query_var( 'company' )  ){

                            return $template_loader->locate_template( 'company.php' );

                        }
                        // recruiters 
                        elseif( get_query_var( 'recruiters' ) ){

                            return $template_loader->locate_template( 'recruiters.php' );

                        }elseif( get_query_var( 'recruiter' ) && get_query_var( 'recruiter' ) != 'yes' ){

                            return $template_loader->locate_template( 'recruiter-single.php' );

                        }   

                        elseif( get_query_var( 'candidate') && get_query_var( 'candidate' ) != 'yes' ){

                            return $template_loader->locate_template( 'candidate-single.php' );

                        }
                        elseif( get_query_var( 'candidate'  ) ){

                            return $template_loader->locate_template( 'candidates.php' );

                        }

                        else{
                            return $template;
                        }
            }
}

new Uou_Careers_Router();


