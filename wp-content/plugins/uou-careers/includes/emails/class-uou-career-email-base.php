<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


abstract class Uou_Career_Email{

	var $id;
	var $title;
	
	var $enabled;
	var $description;
	var $template_plain;
	var $template_html;
	var $template_base;
	var $recipient;
	var $heading;
	var $subject;
	var $object;
	var $find;
	var $replace;
	var $mime_boundary;
	var $mime_boundary_header;
	var $sending;

    var $plain_search = array(
        "/\r/",                                  // Non-legal carriage return
        '/&(nbsp|#160);/i',                      // Non-breaking space
        '/&(quot|rdquo|ldquo|#8220|#8221|#147|#148);/i',
		                                         // Double quotes
        '/&(apos|rsquo|lsquo|#8216|#8217);/i',   // Single quotes
        '/&gt;/i',                               // Greater-than
        '/&lt;/i',                               // Less-than
        '/&#38;/i',                              // Ampersand
        '/&#038;/i',                             // Ampersand
        '/&amp;/i',                              // Ampersand
        '/&(copy|#169);/i',                      // Copyright
        '/&(trade|#8482|#153);/i',               // Trademark
        '/&(reg|#174);/i',                       // Registered
        '/&(mdash|#151|#8212);/i',               // mdash
        '/&(ndash|minus|#8211|#8722);/i',        // ndash
        '/&(bull|#149|#8226);/i',                // Bullet
        '/&(pound|#163);/i',                     // Pound sign
        '/&(euro|#8364);/i',                     // Euro sign
        '/&#36;/',                               // Dollar sign
        '/&[^&;]+;/i',                           // Unknown/unhandled entities
        '/[ ]{2,}/'                              // Runs of spaces, post-handling
    );

    var $plain_replace = array(
        '',                                     // Non-legal carriage return
        ' ',                                    // Non-breaking space
        '"',                                    // Double quotes
        "'",                                    // Single quotes
        '>',
        '<',
        '&',
        '&',
        '&',
        '(c)',
        '(tm)',
        '(R)',
        '--',
        '-',
        '*',
        '£',
        'EUR',                                  // Euro sign. € ?
        '$',                                    // Dollar sign
        '',                                     // Unknown/unhandled entities
        ' '                                     // Runs of spaces, post-handling
    );

	function __construct() {

		if ( is_null( $this->template_base ) ) {
			$this->template_base = UOU_CAREERS_URL . '/templates/';
		}

		// Settings
		$this->heading 			= get_option( 'heading', $this->heading );
		$this->subject      	= get_option( 'subject', $this->subject );
		$this->email_type     	= get_option( 'email_type' );
		$this->enabled   		= get_option( 'enabled' );

		// Find/replace
		$this->find = array( '{blogname}', '{site_title}', '{job_name}', '{company_name}' );
		$this->replace = array( $this->get_blogname(), $this->get_blogname() );

		// For multipart messages
		add_filter( 'phpmailer_init', array( $this, 'handle_multipart' ) );

		
	}

	function handle_multipart( $mailer )  {

		if ( $this->sending && $this->get_email_type() == 'multipart' ) {

			$mailer->AltBody = wordwrap( preg_replace( $this->plain_search, $this->plain_replace, strip_tags( $this->get_content_plain() ) ) );
			//$mailer->AltBody = wordwrap( html_entity_decode( strip_tags( $this->get_content_plain() ) ), 70 );
			$this->sending = false;
		}

		return $mailer;
	}

	
	function format_string( $string ) {
		return str_replace( $this->find, $this->replace, $string );
	}
	
	function get_subject() {
		return apply_filters( 'uou_email_subject_' . $this->id, $this->format_string( $this->subject ), $this->object );
	}

	
	function get_heading() {
		return apply_filters( 'uou_email_heading_' . $this->id, $this->format_string( $this->heading ), $this->object );
	}

	
	function get_recipient() {
		return apply_filters( 'uou_email_recipient_' . $this->id, $this->recipient, $this->object );
	}

	
	function get_headers() {
		return apply_filters( 'uou_email_headers', "Content-Type: " . $this->get_content_type() . "\r\n", $this->id, $this->object );
	}

	
	
	
	function get_email_type() {
		$this->email_type = get_option( 'uou_email_type_' . $this->id );
		return $this->email_type ? $this->email_type : 'plain' ;
	}

	
	function get_content_type() {
		switch ( $this->get_email_type() ) {
			case "html" :
				return 'text/html';
			case "multipart" :
				return 'multipart/alternative';
			default :
				return 'text/plain';
		}

	}

	function get_attachments() {
		return apply_filters( 'uou_email_attachments', array(), $this->id, $this->object );
	}
	
	function is_enabled() {
		$enabled = $this->enabled == "yes" ? true : false;

		return apply_filters( 'uou_email_enable_' . $this->id, $enabled, $this->object );
	}

	
	function get_blogname() {
		return wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
	}

	
	function get_content() {

		$this->sending = true;

		if ( $this->get_email_type() == 'plain' ) {
			$email_content = preg_replace( $this->plain_search, $this->plain_replace, strip_tags( $this->get_content_plain() ) );
		} else {
			$email_content = $this->get_content_html();
		}

		return wordwrap( $email_content, 70 );
	}

	function get_content_plain() {}


	function get_content_html() {}

	function get_from_name() {
		return wp_specialchars_decode( esc_html( get_option( 'uou_email_from_name' ) ), ENT_QUOTES );
	}


	function get_from_address() {

		return sanitize_email( get_option( 'uou_email_from_address' ) );
	}


	function send( $to, $subject, $message, $headers, $attachments = array() ) {
		add_filter( 'wp_mail_from', array( $this, 'get_from_address' ) );
		add_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );
		add_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );
		//echo $to."<br>".$subject."<br>". $message."<br>". $headers."<br>";
		$return = wp_mail( $to, $subject, $message, $headers );
		
		remove_filter( 'wp_mail_from', array( $this, 'get_from_address' ) );
		remove_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );
		remove_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );
		
		return $return;
	}


}
