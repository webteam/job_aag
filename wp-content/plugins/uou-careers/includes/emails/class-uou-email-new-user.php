<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'UOU_Email_New_User' ) ) :


class UOU_Email_New_User extends Uou_Career_Email {

	var $user_login;
	var $user_email;
	var $user_pass;

	function __construct() {

		$this->id 				= 'new_user';
		$this->title 			= __( 'New account', 'uou' );
		$this->description		= __( 'Customer new account emails are sent when a customer signs up via the checkout or My Account page.', 'uou' );

		$this->template_html 	= 'emails/new-user.php';
		$this->template_plain 	= 'emails/plain/new-user.php';

		$this->subject 			= __( 'Your account on {site_title}', 'uou');
		$this->heading      	= __( 'Welcome to {site_title}', 'uou');

		// Call parent constuctor
		parent::__construct();
	}

	function trigger( $user_id, $user_data = '', $password_generated = false ) {

		if ( $user_id ) {
			$this->object 		= new WP_User( $user_id );

			$this->user_pass          = ! empty( $user_data['user_pass'] ) ? $user_data['user_pass'] : '';
			$this->user_login         = stripslashes( $this->object->user_login );
			$this->user_email         = stripslashes( $this->object->user_email );
			$this->recipient          = $this->user_email;
			$this->password_generated = $password_generated;
		}
		if ( ! $this->is_enabled() || ! $this->get_recipient() )
			return;

		$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers());
	}
	function is_enabled() {
		$enabled = get_option( 'uou_email_enable_new_user');
		return  $enabled;
	}

	// function get_heading() {
	// 	return  get_option( 'uou_email_heading_new_user' );
	// }
		
	
	function get_content_html() {
		ob_start();
		
		$email_heading   = $this->get_heading();
		$user_login  = $this->user_login;
		$user_pass = $this->user_pass;
		$blogname  = $this->get_blogname();
		$password_generated = $this->password_generated;
		$sent_to_admin = false;
		$plain_text    = false;
		$link    = home_url().'/career/dashboard';
		include(UOU_CAREERS_DIR."/templates/".$this->template_html);
		return ob_get_clean();
	}

	
	function get_content_plain() {
		ob_start();
		$email_heading   = $this->get_heading();
		$user_login  = $this->user_login;
		$user_pass = $this->user_pass;
		$blogname  = $this->get_blogname();
		$password_generated = $this->password_generated;
		$sent_to_admin = false;
		$plain_text    = true;
		$link    = home_url().'/career/dashboard';
		include(UOU_CAREERS_DIR."/templates/".$this->template_plain);
		return ob_get_clean();
	}
}

endif;

return new UOU_Email_New_User();