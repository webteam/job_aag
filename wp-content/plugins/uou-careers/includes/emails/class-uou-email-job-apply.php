<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'UOU_Email_Job_Apply' ) ) :


class UOU_Email_Job_Apply extends Uou_Career_Email {

	

	function __construct() {

		$this->id 				= 'job_apply';
		$this->title 			= __( 'New Job Apply', 'uou' );
		$this->description		= __( 'Job Apply emails will sent when any candidate apply a job to join.', 'uou' );

		$this->template_html 	= 'emails/job-apply.php';
		$this->template_plain 	= 'emails/plain/job-apply.php';

		$this->subject 			= __( 'Applied on {job_name}', 'uou');
		$this->heading      	= __( 'Welcome to {site_title}', 'uou');

		// Call parent constuctor
		parent::__construct();
	}

	function trigger( $job_id, $user_id) {
		$this->recipient = array();
		if ( $user_id ) {
			$this->object 		= new WP_User( $user_id );

			$this->user_login         = stripslashes( $this->object->user_login );
			$this->user_email         = stripslashes( $this->object->user_email );
			
		}
		
		if($company_post_id = get_post_meta( $job_id, "company_id", true )){
			$postdata = get_post($company_post_id, ARRAY_A);
			$authorID = $postdata['post_author'];
			$user_info = get_userdata($authorID);
			$this->recipient[] = $user_info->user_email;
		}

		
		if($recruiter_id = get_post_meta( $job_id, "recruiter_id", true )){
			$this->recipient[] = $user_info->user_email;
		}


		
		if ( ! $this->is_enabled() || ! $this->get_recipient() )
			return;
		$rec = $this->get_recipient();
		foreach ($rec as $key => $value) {
			$this->send( $value, $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		}
		
	}
	function is_enabled() {
		$enabled = get_option( 'uou_email_enable_job_apply');
		return  $enabled;
	}

	function get_heading() {
		return  get_option( 'uou_email_heading_job_apply' );
	}
		
	
	function get_content_html() {
		ob_start();
		
		$email_heading   = $this->get_heading();
		$user_login  = $this->user_login;
		$blogname  = $this->get_blogname();
		$sent_to_admin = true;
		$plain_text    = false;
		
		include(UOU_CAREERS_DIR."/templates/".$this->template_html);
		return ob_get_clean();
	}

	
	function get_content_plain() {
		ob_start();
		$email_heading   = $this->get_heading();
		$user_login  = $this->user_login;
		$blogname  = $this->get_blogname();
		$sent_to_admin = true;
		$plain_text    = true;
		include(UOU_CAREERS_DIR."/templates/".$this->template_plain);
		return ob_get_clean();
	}
}

endif;

return new UOU_Email_Job_Apply();