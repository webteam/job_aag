<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'UOU_Email_Job_Assignment' ) ) :


class UOU_Email_Job_Assignment extends Uou_Career_Email {

	var $job;

	function __construct() {

		$this->id 				= 'job_assignment';
		$this->title 			= __( 'New Job Assignment', 'uou' );
		$this->description		= __( 'Job Assignment emails will sent when any job has been assigned to agent.', 'uou' );

		$this->template_html 	= 'emails/job-assignment.php';
		$this->template_plain 	= 'emails/plain/job-assignment.php';

		$this->subject 			= __( 'Assignment on {job_name}', 'uou');
		$this->heading      	= __( 'Welcome to {site_title}', 'uou');
		$this->job_id             = 0;

		// Call parent constuctor
		parent::__construct();
	}

	function trigger( $job_id, $user_id) {

		if ( $user_id ) {
			$this->object 		= new WP_User( $user_id );

			$this->user_login         = stripslashes( $this->object->user_login );
			$this->user_email         = stripslashes( $this->object->user_email );
			$this->recipient          = $this->user_email;
		}
		
		$this->job_id = $job_id;
		$this->recipient = $this->user_email;
		//echo $this->get_recipient();
		if ( ! $this->is_enabled() || ! $this->get_recipient() )
			return;
		$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers());
		
		
	}
	function is_enabled() {
		$enabled = get_option( 'uou_email_enable_job_assignment');
		return  $enabled;
	}

	// function get_heading() {
	// 	return  get_option( 'uou_email_heading_job__assignment' );
	// }
		
	
	function get_content_html() {
		ob_start();
		
		$email_heading   = $this->get_heading();
		$user_login  = $this->user_login;
		$blogname  = $this->get_blogname();
		$job = get_post($this->job_id);

	    $sent_to_admin = true;
		$plain_text    = false;
		$link    = home_url().'/career/dashboard';
		include(UOU_CAREERS_DIR."/templates/".$this->template_html);
		return ob_get_clean();
	}

	
	function get_content_plain() {
		ob_start();
		$email_heading   = $this->get_heading();
		$user_login  = $this->user_login;
		$blogname  = $this->get_blogname();
		$job = get_post($this->job_id);
		$sent_to_admin = true;
		$plain_text    = true;
		$link    = home_url().'/career/dashboard';
		include(UOU_CAREERS_DIR."/templates/".$this->template_plain);
		return ob_get_clean();
	}
}

endif;

return new UOU_Email_Job_Assignment();