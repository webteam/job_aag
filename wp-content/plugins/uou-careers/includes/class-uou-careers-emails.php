<?php

class Uou_Careers_Emails {

	public $emails;
	private $_from_address;
	private $_from_name;
	private $_content_type;
	protected static $_instance = null;

	public static function instance() {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
	}

	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'uou' ), '2.1' );
	}

	
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'uou' ), '2.1' );
	}

	
	function __construct() {

		$this->init();

		add_action( 'uou_email_header', array( $this, 'email_header' ) );
		add_action( 'uou_email_footer', array( $this, 'email_footer' ) );
		
		add_action( 'uou_new_user_notification', array( $this, 'new_user' ), 10, 4 );
		add_action( 'uou_job_apply_notification', array( $this, 'job_apply' ), 10, 3 );
		add_action( 'uou_job_assignment_notification', array( $this, 'job_assignment' ), 10, 3 );


		do_action( 'uou_email', $this );
	}

	
	function init() {
		include_once( 'emails/class-uou-career-email-base.php' );

		$this->emails['UOU_Email_New_User']      = include( 'emails/class-uou-email-new-user.php' );
		$this->emails['UOU_Email_Job_Apply']    = include( 'emails/class-uou-email-job-apply.php' );
		$this->emails['UOU_Email_Job_Assignment']                 = include( 'emails/class-uou-email-job-assignment.php' );

		$this->emails = apply_filters( 'uou_email_classes', $this->emails );
	}

	
	function get_emails() {
		return $this->emails;
	}
	function get_option_by_key($key){
		$email_template_options = get_option( 'uc_email_template_options' );
		return $email_template_options[$key];
	}
	
	function get_from_name() {
		if ( ! $this->_from_name )
			$this->_from_name = $this->get_option_by_key( 'uou_email_from_name' );

		return wp_specialchars_decode( $this->_from_name );
	}

	
	function get_from_address() {
		if ( ! $this->_from_address )
			$this->_from_address = $this->get_option_by_key( 'uou_email_from_address' );

		return $this->_from_address;
	}

	
	function get_content_type() {
		return $this->_content_type;
	}

	
	function email_header( $email_heading ) {
		include(UOU_CAREERS_DIR."/templates/"."emails/email-header.php" );
	}

	
	function email_footer() {
		include(UOU_CAREERS_DIR."/templates/"."emails/email-footer.php");
	}

	
	function wrap_message( $email_heading, $message, $plain_text = false ) {
		// Buffer
		ob_start();

		do_action( 'uou_email_header', $email_heading );

		echo wpautop( wptexturize( $message ) );

		do_action( 'uou_email_footer' );

		// Get contents
		$message = ob_get_clean();

		return $message;
	}

	
	function send( $to, $subject, $message, $headers = "Content-Type: text/html\r\n", $attachments = "", $content_type = 'text/html' ) {

		// Set content type
		$this->_content_type = $content_type;

		// Filters for the email
		add_filter( 'wp_mail_from', array( $this, 'get_from_address' ) );
		add_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );
		add_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );

		// Send
		wp_mail( $to, $subject, $message, $headers, $attachments );
	

		// Unhook filters
		remove_filter( 'wp_mail_from', array( $this, 'get_from_address' ) );
		remove_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );
		remove_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );
	}

	
	
	function new_user( $user_id, $new_user_data = array(), $password_generated = false ) {

		if ( ! $user_id )
			return;
		
		$user_pass = ! empty( $new_user_data['user_pass'] ) ? $new_user_data['user_pass'] : '';

		$email = $this->emails['UOU_Email_New_User'];
		
		$email->trigger( $user_id, $new_user_data, $password_generated );
	}
	function job_apply( $job_id, $user_id ) {
		if ( ! $job_id )
			return;
		$email = $this->emails['UOU_Email_Job_Apply'];
		$email->trigger( $job_id, $user_id);
	}
	function job_assignment( $job_id, $user_id ) {

		if ( ! $job_id )
			return;
		$email = $this->emails['UOU_Email_Job_Assignment'];
		
		$email->trigger( $job_id, $user_id);
	}

}

new Uou_Careers_Emails();
