<?php
class Uou_Careers_user_permission{
	public function __construct(){
		
	}
	// add a  job permission
	public function uc_career_user_can_post_company() {
		$can_post = true;

		if ( ! is_user_logged_in() ) {
			if ( !uc_career_user_is_agency()) {
				$can_post = false;
			}
		}

		return apply_filters( 'uc_career_user_can_post_company', $can_post );
	}

	public function uc_career_user_can_edit_company( $company_id = 0 ) {
		$can_edit = true;
		$company   =  get_post( $company_id );

		if ( ! is_user_logged_in() ) {
			$can_edit = false;
		} elseif( !self::uc_career_user_is_agency() && !self::uc_career_user_is_company()){
			$can_edit = false;
		}elseif( $company->post_author != get_current_user_id() ){
			$can_edit = false;
		}

		return apply_filters( 'uc_career_user_can_edit_company', $can_edit, $company_id );
	}

	

    // edit agent permission

    static public function uc_career_user_can_edit_recruiter(  $agent_login = 0 ) {
        $can_edit =  true;
        $agent   =  get_user_by('login', $agent_login );


        if ( ! is_user_logged_in() ) {
            $can_edit = false;
        } elseif( $agent->ID != get_current_user_id() ){
            $can_edit = false;
        }

        return apply_filters( 'uc_career_user_can_edit_agent', $can_edit, $agent_login  );
    }


	
	// add a  job permission
	public function uc_career_user_can_post_job() {
		$can_post = true;

		if ( ! is_user_logged_in() ) {
			if (self::uc_career_user_is_candidate()) {
				$can_post = false;
			}
		}

		return apply_filters( 'uc_career_user_can_post_job', $can_post );
	}


	// edit a  job permission
	public function uc_career_user_can_edit_job( $job_id ) {
		$can_edit = true;
		$job      = get_post( $job_id );

		if ( ! is_user_logged_in() ) {
			$can_edit = false;
		} elseif ( $job->post_author != get_current_user_id() && !is_job_assigned($job_id)) {
			$can_edit = false;
		}

		return apply_filters( 'uc_career_user_can_edit_job', $can_edit, $job_id );
	}

	function is_job_assigned($job_id){
		$is_job_assigned = true;
		$id = get_current_user_id();
		$user_info = get_userdata($id);
		$role = @implode(', ', $user_info->roles);

		if($role== "company_admin"){
			$key = "company_id";
		}elseif ($role == "recruiter") {
			$key = "recruiter_id";
		}
		$assigned_id = get_post_meta( $job_id, $key, true );

		if( ! empty( $assigned_id ) ) {
		  	if($assigned_id != $id){
		  		$is_job_assigned = false;
		  	}
		} else{
			$is_job_assigned = false;
		}
		return apply_filters(  'is_job_assigned', $is_job_assigned );
	}

	

	// check user is company
	public function uc_career_user_is_company(){
		$company = true;
		$id = get_current_user_id();
		$user_info = get_userdata($id);
		$role = @implode(', ', $user_info->roles);
		if($role != "company_admin"){
			$company = false;
		}
		return apply_filters( 'uc_career_user_is_company', $company);
	}
	// check user is agent
	public function uc_career_user_is_recruiter(){
		$agent = true;
		$id = get_current_user_id();
		$user_info = get_userdata($id);
		$role = @implode(', ', $user_info->roles);
		if($role != "agent"){
			$agent = false;
		}
		return apply_filters( 'uc_career_user_is_recruiter', $agent);
	}

	// check user is canditate job permission
	public function uc_career_user_is_candidate(){
		$canditate = true;
		$id = get_current_user_id();
		$user_info = get_userdata($id);
		$role = @implode(', ', $user_info->roles);
		if($role != "canditate"){
			$canditate = false;
		}
		return apply_filters( 'uc_career_user_is_candidate', $canditate);
	}

	
	static function uou_careers_count_posts_from_month($user_id ,$year, $month, $post_type="job", $featured = 0) {
	    global $wpdb;
	    if($post_type == "recruiter"){
			$query = "SELECT count(ID) FROM {$wpdb->posts} INNER JOIN $wpdb->postmeta ON $wpdb->postmeta.post_id = $wpdb->posts.ID WHERE post_type='".$post_type."' AND post_status='publish'  AND $wpdb->postmeta.meta_key = 'company_admin_id' AND $wpdb->postmeta.meta_value = %s AND  YEAR(post_date)=%d AND MONTH(post_date)=%d";    
			return $wpdb->get_var( $wpdb->prepare( $query, $user_id, $year, $month ) );
		}else{
			$query = "SELECT count(ID) FROM {$wpdb->posts} INNER JOIN $wpdb->postmeta ON $wpdb->postmeta.post_id = $wpdb->posts.ID WHERE post_type='".$post_type."' AND post_status='publish'  AND post_author = %s AND  YEAR(post_date)=%d AND MONTH(post_date)=%d AND $wpdb->postmeta.meta_key = 'featured' AND $wpdb->postmeta.meta_value = %s";    
			return $wpdb->get_var( $wpdb->prepare( $query, $user_id, $year, $month, $featured ) );
			
		}
	}
	
	static function uou_careers_count_user_posts_by_type_and_date($user_id , $post_type, $startdate , $enddate, $featured=0 )
	{


		global $wpdb;
		if($post_type == "recruiter"){
			$count = $wpdb->get_var( 
				$wpdb->prepare( 
					"SELECT COUNT(*) FROM $wpdb->posts INNER JOIN $wpdb->postmeta ON $wpdb->postmeta.post_id = $wpdb->posts.ID WHERE post_type = '".$post_type."' AND post_status = 'publish' AND $wpdb->postmeta.meta_key = 'company_admin_id' AND $wpdb->postmeta.meta_value = %s AND post_date >= %s  AND post_date < %s" , $user_id , $startdate , $enddate 
				) 
			);
		}else{
			$count = $wpdb->get_var( 
				$wpdb->prepare( 
					"SELECT COUNT(*) FROM $wpdb->posts INNER JOIN $wpdb->postmeta ON $wpdb->postmeta.post_id = $wpdb->posts.ID WHERE post_type = '".$post_type."' AND post_status = 'publish' AND post_author = %s AND post_date >= %s  AND post_date < %s AND $wpdb->postmeta.meta_key = 'featured' AND $wpdb->postmeta.meta_value = %s" , $user_id , $startdate , $enddate, $featured 
				) 
			);
			
		}
		
		return apply_filters( 'get_usernumposts', $count, $user_id );

	}


	// check user is hr job permission
	static public function checkQuotaAvailablity($quota = null, $post_type = "job", $featured = 0){
		$is_available = false;
		global $wpdb;
		
		$user_id = get_current_user_id();
		$user_info = get_userdata($user_id);
		$role = @implode(', ', $user_info->roles);

		switch ($role) {
			
			case 'recruiter':
				$admin_user_id = get_user_meta($user_id, "company_admin_id", true);
				break;

			default:
				$admin_user_id = $user_id;
				break;
		}

		$membership_type = get_user_meta($admin_user_id, "membership_type", true);
		$numOfPost = 0;
		if($membership_type == 1){
			$currentYear = date("Y");
  	 		$currentMonth = date("n");
			
  	 		$numOfPost = Uou_Careers_user_permission::uou_careers_count_posts_from_month($user_id, $currentYear, $currentMonth, $post_type, $featured);
			//echo $numOfPost.' '.$featured;exit;
		}else{
			$customer_id = get_user_meta( $user_id, 'customer_id', true );
			$customerinfo = Uou_careers_stripe::stripe_customer_info( $customer_id );
		
			update_user_meta( $user_id, 'subscription' , $customerinfo->subscriptions );
			$data = $customerinfo->subscriptions;
			$startdate = date("Y-m-d H:i:s", $data->data[0]->current_period_start);
  	 		$enddate = date("Y-m-d H:i:s", $data->data[0]->current_period_end);
  	 		
  	 		$numOfPost = Uou_Careers_user_permission::uou_careers_count_user_posts_by_type_and_date($user_id, $post_type, $startdate, $enddate, $featured);
		}
		$subscriptionPlan = get_subscription_plan($membership_type);
		if(isset($subscriptionPlan->$quota)){
			if( $numOfPost < $subscriptionPlan->$quota){
				$is_available = true;
			}else{
				$is_available = false;
			}
		}else{
			$is_available = false;
		}
		
		
		
		return apply_filters( 'checkQuotaAvailablity', $is_available, $quota);
	}

	

}
