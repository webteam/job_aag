<?php


class Uou_Careers_Shortcodes {

    private $template_loader;


    public function __construct(){


      add_shortcode( 'uc-register', array( $this, 'uou_register_shortcode' ) );
      add_shortcode( 'uc-login', array( $this, 'uou_login_shortcode' ) );
      add_shortcode( 'uc-candidate-register', array( $this, 'uou_candidate_register_shortcode' ) );
      add_shortcode( 'uc-jobs', array( $this, 'uou_jobs_listing_shortcode' ) );
      add_shortcode( 'uc-company', array( $this, 'uou_company_listing_shortcode' ) );
      add_shortcode( 'uc-recruiter', array( $this, 'uou_recruiter_listing_shortcode' ) );
      add_shortcode( 'uc-candidate', array( $this, 'uou_candidate_listing_shortcode' ) );
      add_shortcode( 'uc-partners', array( $this, 'uou_partners_shortcode' ) );
      $this->template_loader = new Uou_Careers_Load_Template();
    }


    public function uou_register_shortcode(){
      ob_start();
      $template = $this->template_loader->locate_template( 'register.php' );
      include( $template );
      return ob_get_clean();
    }

    public function uou_login_shortcode(){
      ob_start();
      $template = $this->template_loader->locate_template( 'login.php' );
      include( $template );
      return ob_get_clean();
    }

    public function uou_candidate_register_shortcode(){
      ob_start();
      $template = $this->template_loader->locate_template( 'cregister.php' );
      include( $template );
      return ob_get_clean();
    }

    public function uou_jobs_listing_shortcode() {
      ob_start();
      $template = $this->template_loader->locate_template( 'shortcodes/joblisting.php' );
      include( $template );
      return ob_get_clean();
    }

    public function uou_company_listing_shortcode() {
      ob_start();
      $template = $this->template_loader->locate_template( 'shortcodes/companylisting.php' );
      include( $template );
      return ob_get_clean();
    }

    public function uou_recruiter_listing_shortcode() {
      ob_start();
      $template = $this->template_loader->locate_template( 'shortcodes/recruiterlisting.php' );
      include( $template );
      return ob_get_clean();
    }

    public function uou_candidate_listing_shortcode() {
      ob_start();
      $template = $this->template_loader->locate_template( 'shortcodes/candidatelisting.php' );
      include( $template );
      return ob_get_clean();
    }


    public function uou_partners_shortcode() {
      ob_start();
      $template = $this->template_loader->locate_template( 'shortcodes/partners.php' );
      include( $template );
      return ob_get_clean();
    }
    




}

new Uou_Careers_Shortcodes();