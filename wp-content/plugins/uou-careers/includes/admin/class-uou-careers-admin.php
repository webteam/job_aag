<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Uou_Careers_Admin {

    
            public function __construct(){
                    include_once( UOU_CAREERS_DIR . '/includes/vendor/wrapper/class.settingsapi.php' );
                    $this->load_settings();
                    // registration
                    add_action( "wp_ajax_nopriv_uc_add_subscription_plan", array ( $this, 'uc_add_subscription_plan' ) );
                    add_action( "wp_ajax_uc_add_subscription_plan",   array ( $this, 'uc_add_subscription_plan' ) );   
                    add_action( "wp_ajax_nopriv_uc_get_subscription_plan", array ( $this, 'uc_get_subscription_plan' ) );
                    add_action( "wp_ajax_uc_get_subscription_plan",   array ( $this, 'uc_get_subscription_plan' ) );   

                    add_action( "wp_ajax_nopriv_uc_add_email_options", array ( $this, 'uc_add_email_options' ) );
                    add_action( "wp_ajax_uc_add_email_options",   array ( $this, 'uc_add_email_options' ) ); 
                    add_action( "wp_ajax_nopriv_uc_get_email_options", array ( $this, 'uc_get_email_options' ) );
                    add_action( "wp_ajax_uc_get_email_options",   array ( $this, 'uc_get_email_options' ) );   
                    

            }
           
           
            public function load_settings(){
                        $page = new Page('Careers', array('type' => 'menu'));

                        $settings = array();


                        $currency_options = array();
                        $currency_options['dollar'] = 'Dollar';
                        $currency_options['pound'] = 'Pound';                        
                        $currency_options = apply_filters('uou_currency_options',$currency_options);

                        $settings['General']['fields'] = array(
                            array(
                                'type'    => 'select',
                                'name'    => 'uou_currency_option',
                                'label'   => 'Currency',
                                'options' => $currency_options
                            ),
                            
                            array(
                                'type'  => 'text',
                                'name'  => 'uou_currency_sign',
                                'label' => 'Currency Sign'
                              ),

                        );

                      

                       // $settings['General'] = array();

                        
                        
                        $payment_options = array();
                        $payment_options['stripe'] = 'Stripe';                        
                        $payment_options = apply_filters('uou_payment_method_options',$payment_options);

                      

                        $settings['Payments']['fields'] = array(

                            array(
                                'type'    => 'select',
                                'name'    => 'uc_payment_method',
                                'label'   => 'Payment method',
                                'options' => $payment_options
                            )                            
                        );


                        $tpath = UOU_CAREERS_URL.'/assets/tpl/subscription/create.html';

                        $tpath = "'".$tpath."'";




                        $payments_data = get_option('payments');
                        $payment_option = $payments_data['uc_payment_method'] ? $payments_data['uc_payment_method'] : ' ';


                        $notice = '';


                        $notice = apply_filters('uou_subscription_create_page_notice_'.$payment_option , $notice);

                        if($notice){
                            $datashow = '<div class="alert alert-info">'.$notice.'</div>';
                        }else{
                            $datashow = '';
                        }

                        $desc = $datashow.'<div ng-app="CreatePlans"> <div ng-controller="MakeSubscription"> <ng-include src="'.$tpath.'" ></ng-include> </div></div> ';


                        $settings['Subscription Plan']['fields'] = array(
                            array(
                                'type' => 'hidden_text',
                                'name' => 'uc_subscription_plans',
                                'label' => ' ',
                                'desc' => '<div id="uc_create_subscription"> '.$desc.' </div>'
                            ),

                        );

                        $settings['Stripe']['fields']= array(

                            array(
                                'type'  => 'select',
                                'name'  => 'uc_stripe_environment',
                                'label' => 'Environment',
                                'options' => array(
                                    'test'   => 'Test',
                                    'live'   => 'Live'
                                )
                            ),

                            array(
                                'type'  => 'text',
                                'name'  => 'uc_stripe_test_secret_key',
                                'label' => 'Test Secret Key'
                              ),

                            array(
                                'type'  => 'text',
                                'name'  => 'uc_stripe_test_public_key',
                                'label' => 'Test Public Key'
                              ),


                            array(
                                'type'  => 'text',
                                'name'  => 'uc_stripe_live_secret_key',
                                'label' => 'Live Secret Key'
                              ),                            


                            array(
                                'type'  => 'text',
                                'name'  => 'uc_stripe_live_public_key',
                                'label' => 'Live public Key'
                              )


                        );


                        // email settings section 

                        $tpath = UOU_CAREERS_URL.'/assets/tpl/email/emailsettings.html';
                        $tpath = "'".$tpath."'";    
                        $notice = '';


                        $notice = apply_filters('uou_email_settings_page_notice_', $notice);

                        if($notice){
                            $datashow = '<div class="alert alert-info">'.$notice.'</div>';
                        }else{
                            $datashow = '';
                        }

                        $desc = $datashow.'<div ng-app="EmailSettings" id="emailsettings"> <div ng-controller="emailController"> <ng-include src="'.$tpath.'" ></ng-include> </div></div> ';


                        $settings['Email Settings']['fields'] = array(
                            array(
                                'type' => 'hidden_text',
                                'name' => 'uc_email_propertry',
                                'label' => ' ',
                                'desc' => '<div id="uc_email_propertry"> '.$desc.' </div>'
                            ),

                        );

                        // end of email settings section


                        $settings = apply_filters('uou_careers_tab', $settings );
                       // echo "<pre>";print_r($this);echo "</pre>";
                        $call_career = new TSettingsApi( $page , $settings );

                        // add subpage or something
                        do_action('uou_settings_page');


            }
            function uc_add_subscription_plan(){
                        delete_option('subscription_plan');
                        update_option('subscription_plan',$_POST['plan'] );




                        $payments_data = get_option('payments');
                        $payment_option = $payments_data['uc_payment_method'] ? $payments_data['uc_payment_method'] : ' ';

                        do_action('push_plans_'.$payment_option);

                        echo json_encode(array('code'=>1, 'msg'=>"Subscription plan changes successfully."));
                        wp_die();
            }
            function uc_get_subscription_plan(){
                   
                    $data = get_option('subscription_plan', 0);
                    echo json_encode($data);
                    wp_die();
            }
            function uc_add_email_options(){
                extract($_POST);
                foreach ($data as $key => $value) {
                   update_option( $key,$value );
                }
                echo json_encode(array('code'=>1, 'msg'=>"Email Option changes saved successfully."));
                wp_die();
            }
            function uc_get_email_options(){
                    global $wpdb;
                    $data = array();

                    $sql = "SELECT option_name, option_value FROM $wpdb->options ".
                    " WHERE $wpdb->options.option_name like '%uou_email_%'";

                    $query = $wpdb->get_results( $sql );

                    foreach ($query as $key => $value) {
                        $data[$value->option_name] = $value->option_value; 
                    }
                    if(!isset($data['uou_email_from_address']))
                        $data['uou_email_from_address'] = get_option('admin_email'); 
                    if(!isset($data['uou_email_from_name']))
                        $data['uou_email_from_name'] = esc_attr(get_bloginfo('title'));

                    echo json_encode($data);
                    wp_die();
            }
           

            

}



new Uou_Careers_Admin();