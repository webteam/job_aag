<?php

class Uou_Careers_basic_process {
            function __construct(){
                        add_action( 'after_setup_theme', array( $this, 'uc_theme_disable_admin_bar' ) );
                        add_action( 'admin_init', array( $this, 'uc_redirect_admin' ) );
            }

            /**
             * Disable admin bar on the frontend of your website
             * for subscribers, agency_admin, company_admin, agents, candidates.
             */
            function uc_theme_disable_admin_bar() { 
                        if( ! current_user_can('edit_posts') )
                            add_filter('show_admin_bar', '__return_false'); 
            }
            
             
            /**
             * Redirect back to homepage and not allow access to 
             * WP admin for subscribers, agency_admin, company_admin, agents, candidates.
             */
            function uc_redirect_admin(){
                        
                        if ( ! current_user_can( 'administrator' ) ){
                                    if (defined( 'DOING_AJAX') && DOING_AJAX) {
                                                // do nothing
                                    } else {
                                                wp_redirect( site_url() );
                                                exit;
                                    }
                        }
            }
            static function get_role(){
                    $user_info = get_userdata( get_current_user_id() );
                    $role = @implode(', ', $user_info->roles);
                    return $role;
            }


}
new Uou_Careers_basic_process();