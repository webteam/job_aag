<?php

class Uou_Careers_Post_Types {

      public function __construct( ){

              include_once( UOU_CAREERS_DIR . '/includes/vendor/cuztom/cuztom.php' );

              $this->create_post_type( );
              // add_filter('comment_form_default_fields', array($this,'custom_fields') );
              // add_action( 'comment_form_logged_in_after', array($this,'additional_fields') );
              // add_action( 'comment_form_after_fields', array($this,'additional_fields' ) );
              add_filter( "comments_template", array($this,"custom_comment_template") );
              //add_action('comment_post', 'my_func');
      }


      public function create_post_type( ){

                  $company  = new Cuztom_Post_Type( 'company', array(
                              // "menu_position" => 25,
                              'has_archive' => true,
                              'supports' => array('title', 'editor','thumbnail' ,'excerpt', 'custom-fields'),
                              'rewrite' => false,
                              'menu_icon' => 'dashicons-location-alt'
                  ) );

                  
                   $jobs  = new Cuztom_Post_Type( 'job', array(
                              // "menu_position" => 25,
                              'has_archive' => true,
                              'supports' => array('title', 'editor','thumbnail' ,'excerpt', 'custom-fields'),
                              'menu_icon' => 'dashicons-admin-site'
                  ) );
                  $candidate  = new Cuztom_Post_Type( 'candidate', array(
                              // "menu_position" => 25,
                              'has_archive' => true,
                              'supports' => array('title', 'editor','thumbnail' ,'excerpt', 'custom-fields'),
                              'menu_icon' => 'dashicons-welcome-learn-more'
                  ) );
                  $recruiter  = new Cuztom_Post_Type( 'recruiter', array(
                              // "menu_position" => 25,
                              'has_archive' => true,
                              'supports' => array('title', 'editor','thumbnail' ,'excerpt', 'custom-fields'),
                              'menu_icon' => 'dashicons-universal-access'
                  ) );
                 
                  // $plan  = new Cuztom_Post_Type( 'plan', array(
                  //             // "menu_position" => 25,
                  //             'has_archive' => true,
                  //             'supports' => array('title', 'editor','thumbnail' ,'excerpt'),
                  //             'menu_icon' => 'dashicons-chart-bar'
                  // ) );
                  
                  $args = array(
                      'hierarchical' => true
                  );
                  $taxonomy = register_cuztom_taxonomy( 'Job Type', 'job' , $args );
                  $taxonomy = register_cuztom_taxonomy( 'Role', 'job' , $args );
                  $taxonomy = register_cuztom_taxonomy( 'Industry', array('job','company') , $args );
                  $location = register_cuztom_taxonomy( 'Location', array('job', 'candidate','company'), $args );
                  $taxonomy = register_cuztom_taxonomy( 'Career Level', array('job', 'candidate') , $args );
                  $taxonomy = register_cuztom_taxonomy( 'Gender', array('job', 'candidate','company') , $args );
                  $taxonomy = register_cuztom_taxonomy( 'Nationality', array('job', 'candidate') , $args );
                  $taxonomy = register_cuztom_taxonomy( 'Degree', array('job', 'candidate') , $args );
                  $taxonomy = register_cuztom_taxonomy( 'Years Of Experience', array('job', 'candidate') , $args );
                  
                  
                  $company->add_meta_box(
                              'user_contact',
                              'Contact Details', 
                              array(
                                          array(
                                                      'name'          => 'foundation_year',
                                                      'label'         => 'Founded',
                                                      'description'   => 'Foundation year',
                                                      'type'          => 'date',
                                                      'args'       => array(
                                                          'date_format' => 'yy'
                                                      )
                                           ),
                                           array(
                                                      'name'          => 'legal_entity',
                                                      'label'         => 'Legal Entity',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           ),
                                           array(
                                                      'name'          => 'turnover',
                                                      'label'         => 'Turnover',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           ),
                                           array(
                                                      'name'          => 'employee_total',
                                                      'label'         => 'Number of employees',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           ),
                                           array(
                                                      'name'          => 'address',
                                                      'label'         => 'Address',
                                                      'description'   => '',
                                                      'type'          => 'textarea'
                                           ),
                                           array(
                                                      'name'          => 'phone',
                                                      'label'         => 'Phone',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           ),
                                           array(
                                                      'name'          => 'fax',
                                                      'label'         => 'Fax',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           )
                                           ,
                                           array(
                                                      'name'          => 'post_code',
                                                      'label'         => 'Post Code',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           )
                                           ,
                                           array(
                                                      'name'          => 'latitude',
                                                      'label'         => 'Latitude',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           )
                                           ,
                                           array(
                                                      'name'          => 'longitude',
                                                      'label'         => 'Longitude',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           )
                              )
                  );
                  
                  $company->add_meta_box(
                              'user_opening_hours',
                              'Opening Hours', 
                              array(
                                          array(
                                                      'name'          => 'monday_friday',
                                                      'label'         => 'Monday-Friday',
                                                      'description'   => 'Foundation year',
                                                      'type'          => 'text'
                                           ),
                                           array(
                                                      'name'          => 'saturday',
                                                      'label'         => 'Saturday',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           ),
                                           array(
                                                      'name'          => 'sunday',
                                                      'label'         => 'Sunday',
                                                      'description'   => '',
                                                      'type'          => 'text'
                                           )
                              )
                  );


                  
                  $candidate->add_meta_box(
                              'meta_box_id',
                              'Candiate Resume',
                              array(
                                  array(
                                      'name'          => 'candidate_resume',
                                      'label'         => 'Candiate Resume',
                                      'description'   => 'Add a large Concise Heading',
                                      'type'          => 'file'
                                  )
                              )
                   );


                  

                  // $plan->add_meta_box(
                  //             'meta_box_id',
                  //             'Page Head',
                  //             array(
                  //                         array(
                  //                             'name'          => 'bighead',
                  //                             'label'         => 'Page Heading',
                  //                             'description'   => 'Add a large Concise Heading',
                  //                             'type'          => 'text'
                  //                         ),
                  //                         array(
                  //                             'name'          => 'bigdescription',
                  //                             'label'         => 'Page Description',
                  //                             'description'   => 'Add a large Concise sub Heading',
                  //                             'type'          => 'textarea'
                  //                         )
                  //             )
                  // );

      }
    

     function custom_comment_template( $comment_template ) {
           global $post;
           $template_loader = new Uou_Careers_Load_Template();
           if ( !( is_singular() && ( have_comments() || 'open' == $post->comment_status ) ) ) {
              return;
           }
           if($post->post_type == 'job'){ // assuming there is a post type called business
               //dirname(__FILE__) . '/reviews.php';
               return $template_loader->locate_template( 'coverletter.php' );
           }
      }


            
}

new Uou_Careers_Post_Types();


