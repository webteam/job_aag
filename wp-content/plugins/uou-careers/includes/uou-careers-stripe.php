<?php


// /**

// 	1. Need to do class based 
// 	2. Mail handle both side 
// 	3. Invoice PDF format 
// 	4. Cancel

//  */


	class Uou_careers_stripe{


		public function __construct(){


			include( 'vendor/stripe/lib/Stripe.php');



			//add_action('careers_create_plans_info_stripe', array( $this, 'stripe_admin_info') );

			// create subscription plans into stripe 

			add_action('careers_gateway_execute_stripe', array( $this, 'create_stripe_subscription') );



			// client side token generate 
			add_action('uc_subscription_token_stripe', array( $this , 'create_stripe_token') );

			
			// payment form 
			add_action('uc_payment_form_stripe', array( $this , 'generate_stripe_add_form') );

			// payment form update
			add_action('uc_payment_update_form_stripe', array( $this , 'generate_stripe_update_form') );

			add_action( "wp_ajax_nopriv_uc_stripe_handle", array( $this , 'uc_stripe_handle') );
	    	add_action( "wp_ajax_uc_stripe_handle", array( $this ,'uc_stripe_handle') );

		    add_action( "wp_ajax_nopriv_uc_stripe_handle_repay", array( $this , 'uc_stripe_handle_repay') );
		    add_action( "wp_ajax_uc_stripe_handle_repay", array( $this , 'uc_stripe_handle_repay') );

		    add_action( "wp_ajax_nopriv_uc_stripe_update_card", array( $this , 'uc_stripe_update_card') );
		    add_action( "wp_ajax_uc_stripe_update_card",  array( $this , 'uc_stripe_update_card') );




		   	// invoice 

			add_action('invoice_list_stripe', array( $this,'show_all_invoices') );


		}
		// recieve customer information
		static function stripe_customer_info( $customer_key ){

			$stripe = get_option('stripe');
			if($stripe['uc_stripe_environment'] == "test"){
				Stripe::setApiKey( $stripe['uc_stripe_test_secret_key'] );
			}else{
				Stripe::setApiKey( $stripe['uc_stripe_live_secret_key'] );
			}
			
		    $customer = Stripe_Customer::retrieve($customer_key);

		    return $customer;
		}




		/*
		|--------------------------------------------------------------------------
		| Show recent Invoices
		|--------------------------------------------------------------------------
		| - need to implement upcoming Invoices	
		| - need to show the dates [ starting and ending]
		| - need to show invoice in pdf 
		| - Email	 
		*/


		public function show_all_invoices(){

			 $current_user = wp_get_current_user();
			 $customer_id = get_user_meta($current_user->ID, 'customer_id', true);


			 	$stripe = get_option('stripe');
				if($stripe['uc_stripe_environment'] == "test"){
					Stripe::setApiKey( $stripe['uc_stripe_test_secret_key'] );
				}else{
					Stripe::setApiKey( $stripe['uc_stripe_live_secret_key'] );
				}


			if( isset($customer_id) && !empty($customer_id) ){

			 	try {

			 		$invoice = Stripe_Invoice::all(array(
					  "customer" => $customer_id
					));


						if($invoice):
				?>
			     			<table>
								<tr>
									<td> Date </td>
									<td> Payment </td>
									<td> Status </td>
								<!-- 	<td> Invoice </td> -->
								</tr>
	     							<?php   foreach ($invoice->data as $invoice_result ) : ?>


														<tr>
															<td> <?php echo gmdate("F j, Y, g:i a", $invoice_result['date']);?> </td>
															<td> <?php echo (float)$invoice_result['total']/100; echo ' '.$invoice_result['currency']; ?> </td>
															<td> <?php echo $invoice_result['paid'] ? 'Paid':'Not Paid';  ?> </td>
															<!-- <td>  </td> -->
														</tr>
															
									<?php	endforeach;  ?>
							</table>			
				<?php   else: 
							echo 'Nothing to show';
				        endif;

			 	}catch (Stripe_InvalidRequestError $e) {
				
						  $body = $e->getJsonBody();
			  			  $err  = $body['error'];
			  			  echo $err.'<br/>';

				}
				catch (Stripe_ApiConnectionError $e) {

			  			  $body = $e->getJsonBody();
			  			  $err  = $body['error'];
			  			  echo $err.'<br/>';

				}
				 catch (Stripe_Error $e) {
					
							  			  $body = $e->getJsonBody();
			  			  $err  = $body['error'];
			  			  echo $err.'<br/>';
				}

			 	catch (Exception $e) {
			 			$error = $e->getMessage();
			 			echo $error;                       
			 	}



			 } // if customer id 

		}







		/*
		|--------------------------------------------------------------------------
		| Create Stipe Subscription plan
		|--------------------------------------------------------------------------
		|
		| 
		|
		*/

		public function create_stripe_subscription(){

			$stripe = get_option('stripe');
			if($stripe['uc_stripe_environment'] == "test"){
				Stripe::setApiKey( $stripe['uc_stripe_test_secret_key'] );
			}else{
				Stripe::setApiKey( $stripe['uc_stripe_live_secret_key'] );
			}


			$stripe_plans = Stripe_Plan::all();

		    foreach ($stripe_plans->data as $stripe_plan ) {
					
					$plan = Stripe_Plan::retrieve($stripe_plan->id);
					$plan->delete();
			}

			$subscription_plans = get_option('subscription_plans');
			$subscription_plans = json_decode($subscription_plans);
			$subscription_array = array();

		    foreach( $subscription_plans as $plans ){

				$build_subscription = array();

				$build_subscription['id'] = sanitize_title($plans->title); 
				$build_subscription['name'] = $plans->title; 
				$build_subscription['amount'] = (float)$plans->membership_price * 100; 
				$build_subscription['interval'] = $plans->membership_type; 
				$build_subscription['currency'] = 'usd'; 

				try{

					$response = Stripe_Plan::create($build_subscription);

				}catch(Stripe_CardError $e) {
				  
		     	    $this->show_error_message($e);

				} catch (Stripe_InvalidRequestError $e) {
					
					$this->show_error_message($e);

				} catch (Stripe_AuthenticationError $e) {

					$this->show_error_message($e);

				} catch (Stripe_ApiConnectionError $e) {

				  	$this->show_error_message($e);

				} catch (Stripe_Error $e) {
						
					$this->show_error_message($e);

				} catch (Exception $e) {
					echo json_encode(array('msg'=> 'Having problem' , "error"=> 1 ) );                       
		
				}
				
			}

				
		}
		/*
		|--------------------------------------------------------------------------
		| Helper functions
		|--------------------------------------------------------------------------
		|
		| 
		|
		*/


		public function show_error_message( $e ){

			  $body = $e->getJsonBody();
			  $err  = $body['error'];
                
    		  echo json_encode(array('msg'=> 'Having problem' , "error"=> 1 ) ); 
		}

		/*
		|--------------------------------------------------------------------------
		| Create Stripe Token
		|--------------------------------------------------------------------------
		|
		| 
		|
		*/

		public function create_stripe_token(){ 


			$stripe_option = array();

		 	$stripe_option = get_option('stripe');




		 	if( $stripe_option['uc_stripe_environment'] == 'test' ){

			 	$stripe = array(
				  "secret_key"      => $stripe_option['uc_stripe_test_secret_key'],
				  "publishable_key" => $stripe_option['uc_stripe_test_public_key']
				);

		 	}else{

			 	$stripe = array(
				  "secret_key"      => $stripe_option['uc_stripe_live_secret_key'],
				  "publishable_key" => $stripe_option['uc_stripe_live_public_key']
				);
		 	}

				wp_localize_script( 'ajaxHandle', 'stripe_token', $stripe['publishable_key'] );
		 }
		/*
		|--------------------------------------------------------------------------
		|   Show Payment form
		|--------------------------------------------------------------------------
		|
		| 
		|
		*/

		public function generate_stripe_add_form(){

		 	   $template_loader = new Uou_Careers_Load_Template();		   
		       $template = $template_loader->locate_template( 'stripe/add-card-stripe.php' );
		       include( $template );
		       
		 }
		/*
		|--------------------------------------------------------------------------
		| Update Payment form
		|--------------------------------------------------------------------------
		|
		| 
		|
		*/	 

		 function generate_stripe_update_form(){

		 	   $template_loader = new Uou_Careers_Load_Template();		   
		       $template = $template_loader->locate_template( 'stripe/update-card-stripe.php' );
		       include( $template );
		       
		 }














	 	/*
		|--------------------------------------------------------------------------
		| Add Card first time with Stripe  
		|--------------------------------------------------------------------------
		|
		| 
		|
		*/





	    function uc_stripe_handle(){



	    	 $current_user = wp_get_current_user();





	    	$stripe = get_option('stripe');
	    	
	    	$response = $_POST['response'];
	    	$package = $_POST['package'];

	    	$subscription_plans = json_decode( get_option('subscription_plans') );

	    	foreach( $subscription_plans as $plan ) {
	    		if($plan->id == $package){
	    			$package_title = sanitize_title($plan->title);
	    		}
	    	}


	    	$error = '';
	  		$success = '';

	    	try{

		    	if($stripe['uc_stripe_environment'] == "test"){
					Stripe::setApiKey( $stripe['uc_stripe_test_secret_key'] );
				}else{
					Stripe::setApiKey( $stripe['uc_stripe_live_secret_key'] );
				}

				$customer =  Stripe_Customer::create(array(
				  "card" => $response['id'], // obtained from Stripe.js
				  "plan" => $package_title,
				  "email" => $current_user->user_email
				));






				if( isset($customer->id) ){
					update_user_meta( $current_user->ID , 'customer_id' , $customer->id );
					update_user_meta( $current_user->ID , 'subscription' , $customer->subscriptions );
					update_user_meta( $current_user->ID , 'card' , $response['card'] );
					update_user_meta( $current_user->ID , 'membership_type' , $package );

				

				}
				
		    	

	    	} catch (Stripe_CardError $e) {
			    $error = $e->getMessage();
			}



	    	wp_die();
	    }



		/*
		|--------------------------------------------------------------------------
		| If card is already saved and using existing card
		|--------------------------------------------------------------------------
		|
		| 
		|
		*/




	    public function uc_stripe_handle_repay(){

	    	extract($_POST);

	    	$stripe = get_option('stripe');

	    	$user_id = get_current_user_id();


	    	$subscription = get_user_meta( $user_id ,'subscription', true ); 
	    	$customer_id = get_user_meta( $user_id ,'customer_id', true);
	    	$membership_type = get_user_meta( $user_id ,'membership_type', true);

	    	

			$subscription_plans = json_decode( get_option('subscription_plans') );

	    	foreach( $subscription_plans as $plan ) {
	    		if($plan->id == $package_id){
	    			$package_title = sanitize_title($plan->title);
	    		}
	    	}

	    	try{

		    	if($stripe['uc_stripe_environment'] == "test"){
					Stripe::setApiKey( $stripe['uc_stripe_test_secret_key'] );
				}else{
					Stripe::setApiKey( $stripe['uc_stripe_live_secret_key'] );
				}

				$cu = Stripe_Customer::retrieve($customer_id);

				if( $cu):

					
					if( $membership_type != $package_id){
						$cu->subscriptions->create(array("plan" => $package_title ));
						update_user_meta($user_id,'membership_type',$package_id);
						echo json_encode(array('msg'=> 'Updated  Successfully' , "error"=> 0 ) ); 
					}else{
						echo json_encode(array('msg'=> 'Already Subscribed' , "error"=> 1 ) );  
					}			
					

				else:

					echo json_encode(array('msg'=> 'Having Problem' , "error"=> 1 ) );  

				endif;

					    	

	    	}catch (Stripe_InvalidRequestError $e) {
				
						  $body = $e->getJsonBody();
			  			  $err  = $body['error'];
			  			  echo json_encode(array('msg'=> $err , "error"=> 1 ) );  

			}


	    	 catch (Exception $e) {
			    $error = $e->getMessage();

			    echo json_encode(array('msg'=> $error , "error"=> 1 ) );                       
			}





	    	wp_die();
	    }


	    
		public function uc_stripe_update_card(){

	     	$response = $_POST['response'];
	    	$stripe = get_option('stripe');


	    	try {

	    		if($stripe['uc_stripe_environment'] == "test"){
					Stripe::setApiKey( $stripe['uc_stripe_test_secret_key'] );
				}else{
					Stripe::setApiKey( $stripe['uc_stripe_live_secret_key'] );
				}
	    		
	    		$card = get_user_meta( get_current_user_id() ,'card', true ); 
	    		$customer_id = get_user_meta( get_current_user_id() ,'customer_id', true ); 



				$cu = Stripe_Customer::retrieve($customer_id);  


				$re = $cu->cards->all(array('limit'=>1));		

				
         		// delete existing one
	    		$cu->cards->retrieve($re->data[0]->id)->delete();

	    		$cu->cards->create(array("card" => $response['id']));

	    		update_user_meta( get_current_user_id() , 'card' , $response['card'] ); 

	    		echo json_encode(array('msg'=> 'Successfully Added' , "error"=> 0 ) );  



	    	}
	    	catch (Stripe_InvalidRequestError $e) {
				
						  $body = $e->getJsonBody();
			  			  $err  = $body['error'];
			  			  echo json_encode(array('msg'=> $err , "error"=> 1 ) );  

			}
	     	catch (Exception $e) {

	    	    $error = $e->getMessage();
			    echo json_encode(array('msg'=> $error , "error"=> 1 ) );   
	    	}

	    	wp_die();

	    }


	}


	new Uou_careers_stripe();





	function stripe_four_digit_of_card(){

	 	$card = get_user_meta( get_current_user_id() , 'card', true ); 

	 	if( isset($card['last4'])){
	 		return $card['last4'];
	 	}	 		
	 	else{
	 		return '****';
	 	}

	}

