<?php
/**
 * Installation related functions and actions.
 *
 * @author 		Uou career
 * @category 	Admin
 * @package 	Uou career/Classes
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Uou_Careers_Install' ) ) :

/**
 * Uou_Careers_Instrall Class
 */
class Uou_Careers_Install {

	/**
	 * Hook in tabs.
	 */
	
	public function __construct() {
		register_activation_hook( UC_PLUGIN_FILE, array( &$this, 'install' ) );
		register_deactivation_hook( UC_PLUGIN_FILE, array( &$this, 'uninstall' ) );
	}
	/**
	 * Install Uou_Careers_Instrall
	 */
	public function install() {

		$free_subscription_plans = array(
			array(
				'recruiter_quota'       => 5,
				'feature_job_quota' => 3,
				'job_quota'         => 10,
				'blog_quota'        => 10, 
				'membership_price'  => 0,
				'membership_type'   =>  'month',
				'title'             => 'Free',
				'id'                => 1,

			)
		);

		if(version_compare(phpversion(), '5.3.3', '>='))
			$free_subscription_plans = json_encode($free_subscription_plans, JSON_NUMERIC_CHECK );
		else
			$free_subscription_plans = json_encode($free_subscription_plans );
		
		update_option('subscription_plans', $free_subscription_plans);	

		$this->create_pages();
		$this->create_custom_roles();

	}
	function uninstall(){
		delete_option( 'subscription_plans' );
		$this->remove_custom_roles();
		$this->remove_pages();
	}
	private function create_custom_roles(){
        // Add the Agency Admin Role
        add_role(
            'company_admin',
            __( 'Company Admin','uoulib' ),
            array(
                'read' => true,
                'edit_posts' => true,
                'delete_posts' => true,
                'upload_files' =>true
            )
        );

       

        // Add Agent Role
        add_role(
            'recruiter',
            __( 'Recruiter','uoulib' ),
            array(
                'read' => true,
                'edit_posts' => true,
                'delete_posts' => true,
                'upload_files' =>true
            )
        );

        
        // Add Candidate Role which we will need later.
        add_role(
            'candidate',
            __( 'Candidate','uoulib' ),
            array(
                'read' => true,
                'upload_files' =>true
            )
        );
    }
    private function remove_custom_roles(){
    	remove_role('company_admin');
    	remove_role('recruiter');
    	remove_role('candidate');
    }
	private function create_pages(){
		$page_array =  array(
			'login' => array('title'=>'Login', 'shortcode'=>'[uc-login]'),
			'register' => array('title'=>'Register', 'shortcode'=>'[uc-register]'),
			'job-listing' => array('title'=>'Job Listing', 'shortcode'=>'[uc-jobs]'),
			'candidate-listing' => array('title'=>'Candidate Listing', 'shortcode'=>'[uc-candidate]'),
			//'recruiter-listing' => array('title'=>'Recruiter Listing', 'shortcode'=>'[uc-recruiter]'),
			'company-listing' => array('title'=>'Company Listing', 'shortcode'=>'[uc-company]'),
			'partners' => array('title'=>'Partners', 'shortcode'=>'[uc-partners]'),
		);
		foreach ($page_array as $key => $page) {
			$my_page = array(
				'post_title' => $page['title'],
				'post_content' => $page['shortcode'],
				'post_status' => 'publish',
				'post_type' => 'page',
				'post_name' => $key,
			);

			$post_id = wp_insert_post($my_page);
			update_post_meta($post_id, '_wp_page_template', 'page-fullwidth.php');
		}
	}
	private function remove_pages(){
		global $wpdb;
		$page_array =  array(
			'login' => array('title'=>'Login', 'shortcode'=>'[uc-login]'),
			'register' => array('title'=>'Register', 'shortcode'=>'[uc-register]'),
			'job-listing' => array('title'=>'Job Listing', 'shortcode'=>'[uc-jobs]'),
			'candidate-listing' => array('title'=>'Candidate Listing', 'shortcode'=>'[uc-candidate]'),
			//'recruiter-listing' => array('title'=>'Recruiter Listing', 'shortcode'=>'[uc-recruiter]'),
			'company-listing' => array('title'=>'Company Listing', 'shortcode'=>'[uc-company]'),
			'partners' => array('title'=>'Partners', 'shortcode'=>'[uc-partners]'),
		);
		foreach ($page_array as $key => $page) {
			
			$sql =  "SELECT ID FROM $wpdb->posts ".
		        " WHERE $wpdb->posts.post_type = 'page' ".
		        " AND $wpdb->posts.post_name = \"".$key."\" ".
		        " AND ($wpdb->posts.post_status = 'publish') " ;
		    $query = $wpdb->get_results( $sql );
		    foreach ($query as $key1 => $value) {
		    	wp_delete_post($value->ID, true);
		    	delete_post_meta($value->ID, '_wp_page_template', 'page-fullwidth.php');
		    }
		}
	}
}
endif;
return new Uou_Careers_Install();
