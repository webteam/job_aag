jQuery(document).ready(function getUploadEvent(){
	
   setTimeout(function(){
	   jQuery('#userpic').fileapi({
	      url: ajax_object.ajaxurl+'?action=insert_attachment',
	      accept: 'image/*',
	      imageSize: { minWidth: 100, minHeight: 100 },
	      elements: {
	         active: { show: '.js-upload', hide: '.js-browse' },
	         preview: {
	            el: '.js-preview',
	            width: 200,
	            height: 200
	         },
	         progress: '.js-progress'
	      },
	      onSelect: function (evt, ui){
	      	 var file = ui.files[0];
	         if( !FileAPI.support.transform ) {
	            console.log('Your browser does not support Flash :(');
	         }
	         else if( file ){
	            jQuery('#popup').modal({
	               closeOnESC: function(){
	                   jQuery('.js-preview').css({'display':'none'});
	                  return true;
	               },
	               closeOnOverlayClick: false,
	               onOpen: function (overlay){
	                  jQuery(overlay).on('click', '.js-upload', function (){
	                     jQuery.modal().close();
	                     jQuery('.js-preview1').css({'display':'none'});
	                     jQuery('#userpic').fileapi('upload');

	                  });
	                  jQuery('.js-img', overlay).cropper({
	                     file: file,
	                     bgColor: '#fff',
	                     maxSize: [jQuery(window).width()-100, jQuery(window).height()-100],
	                     minSize: [100, 100],
	                     selection: '90%',
	                     onSelect: function (coords){
	                        jQuery('#userpic').fileapi('crop', file, coords);
	                     }
	                  });
	               }
	            }).open();
	         }
	      },
	      emitCompleteEvent : function(){
	      	window.location = '';
	      }
	    
   });
},500);  
});