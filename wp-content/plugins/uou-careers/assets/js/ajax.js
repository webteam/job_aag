    window.FileAPI = {
          debug: false // debug mode
        , staticPath: 'https://localhost/career/wp-content/plugins/careers/assets/js/vendor/fileapi/' // path to *.swf
    };

    jQuery(document).ready(function($) {

       
  if ($.fn.slider) {
        // $('.range-slider .slider').each(function() {
        //     var $this = $(this),
        //         min = $this.data('min'),
        //         max = $this.data('max');

        //     $this.slider({
        //         range: true,
        //         min: min,
        //         max: max,
        //         step: 1,
        //         values: [min, max],
        //         slide: function(event, ui) {
        //             $(this).parent().find('.first-value').text(ui.values[0]);
        //             $(this).parent().find('.last-value').text(ui.values[1]);
        //             $(this).parent().find('.min').val(ui.values[0]).trigger("click");
        //             $(this).parent().find('.max').val(ui.values[1]).trigger("click");
        //         }
        //     });
        // });
    }





    /*
    |--------------------------------------------------------------------------
    | stripe payment hadling and credit card 
    |--------------------------------------------------------------------------
    |
    | 
    |
    */


   $('.credit-card form').card({
        container: $('.card-wrapper'),
    });


    if(typeof stripe_token != 'undefined'){
       Stripe.setPublishableKey(stripe_token);
    }


    $('#checkout').submit(function(event) {



            var qs = (function(a) {
                if (a == "") return {};
                var b = {};
                for (var i = 0; i < a.length; ++i)
                {
                    var p=a[i].split('=');
                    if (p.length != 2) continue;
                    b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
                }
                return b;
            })(window.location.search.substr(1).split('&'));



        var $form = $(this);

        
        $form.find('button').prop('disabled', true);


        var expire = $('.card-expire').val().split('/');

        var packages = qs['package'];
        

        Stripe.card.createToken({
              number: $('.card-number').val(),
              cvc: $('.card-cvc').val(),
              exp_month: parseInt(expire[0]),
              exp_year: parseInt(expire[1]),
              name : $('.card-name').val()
        }, function(status, response){


                if( response.error){
                    
                    console.log('error');

                }else{

                    var token = response.id;

                    $.ajax({
                        url : ajax_object.ajaxurl+'?action=uc_stripe_handle',
                        type: 'POST',                    
                        data: $.param({'response': response , 'package': packages }) ,
                        success: function(c){
                            console.log(c);
                        }
                    });
                }
                


        });

        // Prevent the form from submitting with the default action
        return false;
      });

    



    $('a.repay').on('click',function(e){

        e.preventDefault();

        var data =  $('form#repay-form').serialize();

        $.ajax({
            url : ajax_object.ajaxurl+'?action=uc_stripe_handle_repay',
            type: 'POST',                    
            data:  data,
            success: function(c){
                console.log(c);
            }
        });

    });





    $('#update_card').submit(function(e){


        e.preventDefault();



        var qs = (function(a) {
            if (a == "") return {};
            var b = {};
            for (var i = 0; i < a.length; ++i)
            {
                var p=a[i].split('=');
                if (p.length != 2) continue;
                b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
            }
            return b;
        })(window.location.search.substr(1).split('&'));






        var $form = $(this);

        
        $form.find('button').prop('disabled', true);


        var expire = $('.card-expire').val().split('/');

        var packages = qs['package'];
        

        Stripe.card.createToken({
              number: $('.card-number').val(),
              cvc: $('.card-cvc').val(),
              exp_month: parseInt(expire[0]),
              exp_year: parseInt(expire[1]),
              name : $('.card-name').val()
        }, function(status, response){

                if( response.error){                    
                    console.log('error');
                }else{

                    var token = response.id;

                    $.ajax({
                        url : ajax_object.ajaxurl+'?action=uc_stripe_update_card',
                        type: 'POST', 
                        dataType: 'json',                   
                        data: $.param({'response': response , 'package': packages }) ,
                        success: function(c){
                           if(c.error == 0){
                            location.reload();
                           }
                        }
                    });
                }

        });

    });



    // Uploading files
    
    
});







