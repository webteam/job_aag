;(function($) {

"use strict";

var $body = $('body');
var $head = $('head');
var $header = $('#header');



// Mediaqueries
// ---------------------------------------------------------
var XS = window.matchMedia('(max-width:767px)');
var SM = window.matchMedia('(min-width:768px) and (max-width:991px)');
var MD = window.matchMedia('(min-width:992px) and (max-width:1199px)');
var LG = window.matchMedia('(min-width:1200px)');
var XXS = window.matchMedia('(max-width:480px)');
var SM_XS = window.matchMedia('(max-width:991px)');
var LG_MD = window.matchMedia('(min-width:992px)');



// Touch
// ---------------------------------------------------------
var dragging = false;

$body.on('touchmove', function() {
	dragging = true;
});

$body.on('touchstart', function() {
	dragging = false;
});



// Responsive Tabs
// ---------------------------------------------------------

// $('.responsive-tabs').responsiveTabs();
$("#post_content").attr("ng-model", "user.post_content");
$("#job_content").attr("ng-model", "job.post_content");


}(jQuery));