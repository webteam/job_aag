userModule.controller('job-applicationlist', function($scope,jobService, $filter, $sce, alert, httprequest, $modal,$confirm, $translate){

 	$scope.job = {};


 	$scope.currentPage = 1;
  	$scope.pageSize = 10;
  	$scope.joblist = jobService.getJobs();
  	$scope.company_id = user_object.userData.profile.post_id;
  	if(angular.isDefined($scope.joblist))
  		$scope.dropDownquery = $scope.joblist[0].id;
 	$scope.pageChangeHandler = function(num) {
      //console.log('drinks page changed to ' + num);
  	};	
 	$scope.showData = function(){
 		if($scope.joblist.length > 0)
 			return true;
 		else
 			return false;
 	}
 	    // init the filtered items
   

	$scope.renderHtml = function (htmlCode) {
		return $sce.trustAsHtml(htmlCode);	
  };

  $scope.saveAsBookmark = function(obj){
    var url = '?action=uou_career_save_as_bookmark';
    var params = jQuery.param({ 'id': obj.candidate_id, 'key': 'bookmarked_candidate' });
    httprequest.fetch(url, params).then(function(response){
      if (response.data.errors) {
        var html = "";
        angular.forEach(response.data.errors,function(obj){
          html += obj + "<br/>";
        });
        alert(html);
      }else {
        if (response.data.saved) {
          alert("Bookmarks has been done successfully.");
        }  
      }
    });
  };
});