var buttonOptions = {
 		buttons : ['html', 'formatting', 'bold', 'italic', 'deleted',
	      'unorderedlist', 'orderedlist', 'outdent', 'indent',
       'image', 'file', 'link', 'alignment', 'horizontalrule','video'],
     plugins: ['imagemanager','video'],
     imageUpload: ajax_object.ajaxurl+'?action=upload_editor_image'
};


userModule.controller('upgradePlan', function( $scope, $filter, httprequest, $sce, alert, $translate){
	$scope.updatePlan = function(membership_type){
		url = "?action=uc_update_plan";
		params = jQuery.param({ 'membership_type' : membership_type});
		httprequest.fetch(url, params).then(function(e){
	    	alert(e.data.success);
        });
	};
});
userModule.filter('offset', function() {
  	return function(input, start) {
	    start = parseInt(start, 10);
	    return input.slice(start);
  	};
});

// by tareq 
userModule.controller('showSubscription',function($scope){
    //console.log(subscription.show);
    if( angular.isDefined(subscription.show) ){
        $scope.packages = subscription.show;
        $scope.membership_type = subscription.membership_type;
    }
});

// by robi 
userModule.controller('ctrlLanguage',function ($scope, $translate) {
    $scope.selectedLanguage = $translate.preferredLanguage();
    $scope.changeLanguage = function () {
        $translate.use($scope.selectedLanguage);
    };
});



userModule.controller('jobListing',function($scope, $sce, alert, httprequest, QueryStringToJSON){

 		if(angular.isDefined(listing.jobs)){
 			$scope.jobs = listing.jobs;
 		}
    if(angular.isDefined(listing.isBookmarked)){
      $scope.isBookmarked = Boolean(listing.isBookmarked);
    }
    
    $scope.currentPage = 1;
    $scope.pageSize = ajax_object.posts_per_page;

    // function to submit the form after all validation has occurred
    $scope.pageChangeHandler = function(num) {
        //console.log('drinks page changed to ' + num);
    };  

    $scope.filterItem = {};
    $scope.initilaizeSlider = function($this, min, max){
      $this.slider({
          range: true,
          min: $this.data('min'),
          max: $this.data('max'),
          step: 1,
          values: [min, max],
          slide: function(event, ui) { 
              jQuery(this).parent().find('.first-value').text(ui.values[0]);
              jQuery(this).parent().find('.last-value').text(ui.values[1]);
              jQuery(this).parent().find('.min').val(ui.values[0]).trigger("click");
              jQuery(this).parent().find('.max').val(ui.values[1]).trigger("click");
          }
          
      });
      if(min)
        $this.parent().find('.first-value').text(min);
      if(max)
        $this.parent().find('.last-value').text(max);
    }
    
    var parts = location.href.split('#?'); 
    if(angular.isDefined(parts[1]) ){
      $scope.filterItem = QueryStringToJSON.getJson();
      angular.forEach($scope.filterItem.role, function (id) {
        id = id.replace(/\s/g, '-').toLowerCase();
        jQuery("#"+id).prop('checked', true);;
      });
    }  
    if(angular.isDefined($scope.filterItem.postday) ){
      $this = jQuery("#postday .slider");
      min = $scope.filterItem.postday.min,
      max = $scope.filterItem.postday.max;
      $scope.initilaizeSlider($this, min, max);
      

    }else{
      $this = jQuery("#postday .slider");
      min = $this.data('min'),
      max = $this.data('max');
      $scope.initilaizeSlider($this, min, max);
    }
    
    if(angular.isDefined($scope.filterItem.salary)) {
      $this = jQuery("#salary .slider");
      min = $scope.filterItem.salary.min,
      max = $scope.filterItem.salary.max;
      $scope.initilaizeSlider($this, min, max);
    }else{
      $this = jQuery("#salary .slider");
      min = $this.data('min'),
      max = $this.data('max');
      $scope.initilaizeSlider($this, min, max);
    }
         
      
    
    $scope.saveAsBookmark = function(obj){
      var url = '?action=uou_career_save_as_bookmark';
      if(typeof obj === "object")
        var params = jQuery.param({ 'id': obj.id, 'key': 'bookmarked_job' });
      else
        var params = jQuery.param({ 'id': obj, 'key': 'bookmarked_job' });

      httprequest.fetch(url, params).then(function(response){
        if (response.data.errors) {
          var html = "";
          angular.forEach(response.data.errors,function(obj){
            html += obj + "<br/>";
          });
          alert(html);
        }else {
          if (response.data.saved) {
            if(typeof obj === "object")
              $scope.jobs[$scope.jobs.indexOf(obj)].isBookmarked = true;
            else
              $scope.isBookmarked = true;

            alert("Bookmarks has been done successfully.");
          }  
        }
      });
    };
    $scope.setFilterItem = function(item, filter, type){
      if(type == "list"){
        if(item == "location" ){

          $scope.filterItem.location = filter;
        
        }else if(item == "industry" ){
          
          $scope.filterItem.industry = filter;
          
        }else if(item == "job_type" ){
          
          $scope.filterItem.job_type = filter;
          
        }
      }else if(type == "checkbox"){
        if(item == "role" ){
          if(angular.isUndefined($scope.filterItem.role))
            $scope.filterItem.role = [];
          if (_.contains($scope.filterItem.role, filter)) {
            $scope.filterItem.role = _.without($scope.filterItem.role, filter);
          } else {
            
            $scope.filterItem.role.push(filter);
          
          }
        }
      }else if(type == "slider"){
        if(angular.isUndefined($scope.filterItem.postday))
            $scope.filterItem.postday = [];
        if(item == "postday"){
          $scope.filterItem.postday= {
            min:jQuery("#postday").find('.min').val(),
            max:jQuery("#postday").find('.max').val()
          };
          
        }else if(item == "salary"){
          if(angular.isUndefined($scope.filterItem.salary))
            $scope.filterItem.salary = [];
          $scope.filterItem.salary= {
            min:jQuery("#salary").find('.min').val(), 
            max:jQuery("#salary").find('.max').val()
          };
        }
      }
      return false; 
    }
    $scope.renderHtml = function (htmlCode) {
      return $sce.trustAsHtml(htmlCode);
    };
 });

userModule.controller('companyListing',function($scope, $sce, alert, httprequest){


 		if(angular.isDefined(listing.companies)){
 			$scope.companies = listing.companies;
 		}
    if(angular.isDefined(listing.isBookmarked)){
      $scope.isBookmarked = Boolean(listing.isBookmarked);
    }
    
    $scope.saveAsBookmark = function(obj){
      var url = '?action=uou_career_save_as_bookmark';
      if(typeof obj === "object")
        var params = jQuery.param({ 'id': obj.id, 'key': 'bookmarked_company' });
      else
        var params = jQuery.param({ 'id': obj, 'key': 'bookmarked_company' });

      
      httprequest.fetch(url, params).then(function(response){
        if (response.data.errors) {
          var html = "";
          angular.forEach(response.data.errors,function(obj){
            html += obj + "<br/>";
          });
          alert(html);
        }else {
          if(typeof obj === "object")
              $scope.companies[$scope.companies.indexOf(obj)].isBookmarked = true;
            else
              $scope.isBookmarked = true;
          if (response.data.saved) {
            alert("Bookmarks has been done successfully.");
          }  
        }
      });
    };
    $scope.renderHtml = function (htmlCode) {
      return $sce.trustAsHtml(htmlCode);
    };

 });


userModule.controller('recruiterlisting',function($scope , $sce){


		if(angular.isDefined(listing.recruiters)){
			$scope.recruiters = listing.recruiters;
		}
    $scope.renderHtml = function (htmlCode) {
      return $sce.trustAsHtml(htmlCode);
    };

});

userModule.controller('candidatelisting',function($scope , $sce, alert, httprequest, QueryStringToJSON){

    $scope.currentPage = 1;
    $scope.pageSize = ajax_object.posts_per_page;
    // function to submit the form after all validation has occurred
    $scope.pageChangeHandler = function(num) {
        //console.log('drinks page changed to ' + num);
    };  

		if(angular.isDefined(listing.candidates)){
			$scope.candidates = listing.candidates;
		}
    if(angular.isDefined(listing.isBookmarked)){
      $scope.isBookmarked = Boolean(listing.isBookmarked);
    }
    
    
    var parts = location.href.split('#?'); 
    if(angular.isDefined(parts[1]) ){
      $scope.filterItem = QueryStringToJSON.getJson();
    }

    $scope.saveAsBookmark = function(obj){
      var url = '?action=uou_career_save_as_bookmark';
      if(typeof obj === "object")
        var params = jQuery.param({ 'id': obj.id, 'key': 'bookmarked_candidate' });
      else
        var params = jQuery.param({ 'id': obj, 'key': 'bookmarked_candidate' });

      httprequest.fetch(url, params).then(function(response){
        if (response.data.errors) {
          var html = "";
          angular.forEach(response.data.errors,function(obj){
            html += obj + "<br/>";
          });
          alert(html);
        }else {
          if (response.data.saved) {
            if(typeof obj === "object")
              $scope.candidates[$scope.candidates.indexOf(obj)].isBookmarked = true;
            else
              $scope.isBookmarked = true;
            alert("Bookmarks has been done successfully.");
          }  
        }
      });
    };
    $scope.renderHtml = function (htmlCode) {
      return $sce.trustAsHtml(htmlCode);
    };

});
userModule.controller('bookmarklisting',function($scope , $sce, alert, httprequest){


    if(angular.isDefined(listing.bookmarklist)){
      $scope.bookmarklist = listing.bookmarklist;
    }
    $scope.renderHtml = function (htmlCode) {
      return $sce.trustAsHtml(htmlCode);
    };

});

userModule.controller('partnerslisting',function($scope , $sce){


        if(angular.isDefined(listing.partners)){
            $scope.partners = listing.partners;
        }
        $scope.renderHtml = function (htmlCode) {
          return $sce.trustAsHtml(htmlCode);
        };

});
