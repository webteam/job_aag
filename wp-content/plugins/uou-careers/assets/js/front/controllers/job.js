//job add controlller
userModule.controller('job', function($scope,jobService, $filter, $sce, alert, httprequest, $modal,$confirm, $translate){
 	$scope.job = {};


 	$scope.currentPage = 1;
  	$scope.pageSize = 10;
  	
 	$scope.redactorOptions = buttonOptions;

 	$scope.industrylist = form_taxonomy.taxonomylist.industry;
 	$scope.jobtypelist = form_taxonomy.taxonomylist.job_type;
 	$scope.rolelist = form_taxonomy.taxonomylist.role;
 	$scope.genderlist = form_taxonomy.taxonomylist.gender;
 	$scope.locationlist = form_taxonomy.taxonomylist.location;
 	$scope.nationalitylist = form_taxonomy.taxonomylist.nationality;
 	$scope.careerlevellist = form_taxonomy.taxonomylist.career_level;
 	$scope.years_of_experiencelist = form_taxonomy.taxonomylist.years_of_experience;
 	$scope.currency_option = ajax_object.currency_option;

 	$scope.joblist = jobService.getJobs();
 	// $scope.companylist = user_object.userData.companies;
 	$scope.company_id = user_object.userData.profile.post_id;
 	// function to submit the form after all validation has occurred	
 	$scope.pageChangeHandler = function(num) {
      //console.log('drinks page changed to ' + num);
  	};	
 	$scope.showData = function(){
 		if($scope.joblist.length > 0)
 			return true;
 		else
 			return false;
 	}

	$scope.submitForm = function() {
		
		// check to make sure the form is completely valid

		if ($scope.jobForm.$valid) {
			if($scope.job.id){
				var url = '?action=uc_edit_job';
			}else{
				var url = '?action=uc_create_job';
			}

			$scope.job.post_id =  $scope.company_id;
			//$scope.job.post_meta.company_id = $scope.selectedCompanyOptions.ID;
			if(!$scope.job.post_meta.featured) $scope.job.post_meta.featured = 0;
			var params = jQuery.param({ 'data': $scope.job });

			httprequest.fetch(url, params).then(function(response){
	        	if (response.data.errors) {
                	var html = "";
					angular.forEach(response.data.errors,function(obj){
						html += obj + "<br/>";
					});
                	alert(html);
            	}else {
	                if (response.data.saved) {
		            	if($scope.job.post_meta.featured == 0) $scope.job.post_meta.featured = false;
		            	if($scope.job.id){
	                		angular.forEach($scope.joblist, function(key,obj) {
								if (obj.id === $scope.job.id) {
									$scope.data[key] = $scope.job;
								}
							});

							alert("Job Updated successfully.");
	                	}else{
	                		
		            		$scope.job.id = response.data.ID;
		            		$scope.job.post_meta.recruiter_id = response.data.recruiter_id;
		            		jobService.addJob($scope.job);
		            		alert("Job added successfully.");
		            	}
		            	$scope.$broadcast('show-errors-reset');
		            	$scope.job = {};
		            	jQuery(".redactor_editor").html("");
            		 	
	                }
	                
	            }
	        });
		}

	};
	
	$scope.delete = function(field) {
		$confirm.show().then(function(res) {


      		if(res){

      			
				var url = '?action=uc_delete_job';
				
				var params = jQuery.param({ 'id': field.id });

				httprequest.fetch( url, params).then(function(response){

			    	if (response.data.errors) {

			        	var html = "";
							angular.forEach(response.data.errors,function(obj){
								html += obj + "<br/>";
						});

		            	alert(html);

			    	}else {
			    		
			    		if (response.data.saved) {

			    			jobService.deleteJob(field);
			    			alert("Job deleted successfully");
			            }
			            
					}
		    	
		    	});

		    }


		});
		
	};
	
 	var modal;
 	$scope.recruiterAssignmentList = function(job){

 		url = "?action=uc_recruiters";
 		var params = jQuery.param({ 'company_id' : $scope.company_id });
 		httprequest.fetch(url, params).then(function(response){
 			$scope.acurrentPage = 1;
  			$scope.apageSize = 10;
  			$scope.mpageChangeHandler = function(num) {
		      
		  	};	
	    	$scope.title = "Recruiter Assignment";
	    	$scope.filterjob = job;
	    	$scope.items = response.data.recruiterlist;
			modal = $modal({scope: $scope, template: ajax_object.tpl_url+'modal/assign_recruiter.tpl.html', show: true});
			$scope.showModal = function() {
			    modal.$promise.then(modal.show);
			};
	    });


 	};
 	$scope.assignToRecruiter = function(job, recruiter){
 		
 		url = "?action=uc_assign_recruiter_to_job";
 		var params = jQuery.param({ 'recruiter_id': recruiter.id, 'job_id' : job.id });
 		httprequest.fetch(url, params).then(function(response){
 			$scope.joblist[$scope.joblist.indexOf(job)].post_meta.recruiter_id = recruiter.id;
 			$scope.joblist[$scope.joblist.indexOf(job)].post_meta.recruiter_name = recruiter.recruiter_name;
 			modal.$promise.then(modal.hide);
 		});
 	};
 	$scope.removeAssignedRecruiter = function(job, recruiter_id){
 		
 		url = "?action=uc_remove_assigned_Recruiter_from_job";
 		var params = jQuery.param({ 'recruiter_id': recruiter_id, 'job_id' : job.id });
 		httprequest.fetch(url, params).then(function(response){
 			$scope.joblist[$scope.joblist.indexOf(job)].post_meta.recruiter_id = 0;
 			$scope.joblist[$scope.joblist.indexOf(job)].post_meta.recruiter_name = '';
 			alert(response.data.msg);
 		});
 	};
	$scope.editJob = function(obj){
		if(obj === 'new'){
			$scope.job = {};
			$scope.selectedCompanyOptions = {};
			jQuery(".redactor_editor").html("");

		}else{
			$scope.job = obj;
			$scope.getCountry();
			jQuery(".redactor_editor").html($scope.job.post_content);
    	}
		$scope.showJob = true;
	};
	$scope.cancel = function(){
		$scope.showJob = false;
	};
	$scope.renderHtml = function (htmlCode) {
 			return $sce.trustAsHtml(htmlCode);
    };
    $scope.getTaxonomyList = function(){
    	url = "?action=uc_get_job_taxonomylist";
 		httprequest.fetch(url).then(function(response){
 			console.log(response.data);
 		});
    };
    $scope.checkTrue = function(term_id, values, type){

    	if(angular.isDefined(term_id) && angular.isDefined(values)){

	    	for (var i = 0; i < values.length; i++) {
	    		if(parseInt(values[i]) === parseInt(term_id)){

	    			if(type == 'region'){
	    				$scope.job.taxonomy.region = term_id;
	    			}else if(type == 'country'){
	    				$scope.job.taxonomy.country = term_id;
	    			}
	    			return true;
	    		}
	    	}
	    	return false;
    	}else{
    		return false;
    	}
    };
    
    $scope.addSkill = function(){

    	if(!angular.isDefined($scope.job.post_meta)){
	 		$scope.job.post_meta = {};
	 	}
    	if(!angular.isDefined($scope.job.post_meta.required_skilllist)){
	 		$scope.job.post_meta.required_skilllist = [];
	 	}
		$scope.job.post_meta.required_skilllist.push({
			name : '',
			desc : '',
			level : ''
		});
	};
	$scope.removeSkill = function(obj){
		$scope.job.post_meta.required_skilllist.splice( $scope.job.post_meta.required_skilllist.indexOf(obj), 1 );
	};
    $scope.addAdditionalSkill = function(skill){
    	if(!angular.isDefined($scope.job.post_meta)){
	 		$scope.job.post_meta = {};
	 	}
    	if(!angular.isDefined($scope.job.post_meta.additional_skilllist)){
	 		$scope.job.post_meta.additional_skilllist = [];
	 	}
		var bool = true;
		angular.forEach($scope.job.post_meta.additional_skilllist, function(value){
			if(value.name === skill){
				bool = false;
			}	
		});
		if(skill === ""){
			bool = false;
		}
		if(bool){
			$scope.job.post_meta.additional_skilllist.push({
				name : skill
			});
			$scope.job.additional_skill = "";
		}	
	};
	$scope.removeAdditionalSkill = function(obj){
		$scope.job.post_meta.additional_skilllist.splice( $scope.job.post_meta.additional_skilllist.indexOf(obj), 1 );
	};
	$scope.getCountry = function(){
		$scope.countrylist = [];
    	var url = '?action=uou_get_country';
			
		var params = jQuery.param({ 'term_id': $scope.job.taxonomy.region });

		httprequest.fetch(url, params).then(function(response){
        	$scope.countrylist = response.data.location;
        	$scope.getState();
        });
    };
    $scope.getState = function(){
    	$scope.statelist = [];
    	var url = '?action=uou_get_state';
			
		var params = jQuery.param({ 'term_id': $scope.job.taxonomy.country });

		httprequest.fetch(url, params).then(function(response){
        	$scope.statelist = response.data.location;
   
	    });
    };
    


});
 