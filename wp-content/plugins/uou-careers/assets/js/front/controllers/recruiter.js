//recruiter controller 
userModule.controller('recruiter', function($scope,$filter,httprequest, recruiterService,alert,$sce, $modal, $confirm, $translate)
{
 	$scope.recruiter = {};
 	$scope.items = [];
 	$scope.currentPage = 1;
  	$scope.pageSize = 10;
  	
 	$scope.showRecruiter =false
 	$scope.company_id = user_object.userData.profile.post_id;
 	$scope.company_admin_id = user_object.userData.profile.user_id;

 	

 	$scope.recruiterlist = recruiterService.getRecruiters();

 	// function to submit the form after all validation has occurred
 	$scope.pageChangeHandler = function(num) {
      //console.log('drinks page changed to ' + num);
  	};	
 	$scope.showData = function(){
 		if($scope.recruiterlist.length > 0)
 			return true;
 		else
 			return false;
 	};
	$scope.jobAssignmentList = function(recruiter_id){

 		url = "?action=uc_not_assigned_job";
 		var params = jQuery.param({ 'recruiter_id': recruiter_id, 'company_id' : $scope.company_id });
 		httprequest.fetch(url, params).then(function(response){
 			$scope.mcurrentPage = 1;
  			$scope.mpageSize = 10;
  			$scope.mpageChangeHandler = function(num) {
		      
		  	};	
	    	$scope.title = "Job Assignment";
	    	$scope.dropDownquery = 1;
	    	$scope.recruiter_id = recruiter_id;
	    	$scope.items = response.data.companylist;
			var modal = $modal({scope: $scope, template: ajax_object.tpl_url+'modal/assign_job.tpl.html', show: true});
			$scope.showModal = function() {
			    modal.$promise.then(modal.show);
			};
	    });


 	};
 	
 	$scope.assignToRecruiter = function(job_id, recruiter_id){
 		
 		url = "?action=uc_assign_recruiter_to_job";
 		var params = jQuery.param({ 'recruiter_id': recruiter_id, 'job_id' : job_id });
 		httprequest.fetch(url, params).then(function(response){
 			angular.forEach($scope.items, function(obj){
 				if(obj.ID == job_id){
 					$scope.items[$scope.items.indexOf(obj)].assigned = 1;
 				}
 			});
 			alert(response.data.msg);
 		});
 	};
 	$scope.removeAssignedRecruiter = function(job_id, recruiter_id){
 		
 		url = "?action=uc_remove_assigned_recruiter_from_job";
 		var params = jQuery.param({ 'recruiter_id': recruiter_id, 'job_id' : job_id });
 		httprequest.fetch(url, params).then(function(response){
 			angular.forEach($scope.items, function(obj){
 				if(obj.ID == job_id){
 					$scope.items[$scope.items.indexOf(obj)].assigned = 0;
 				}
 			});
 			alert(response.data.msg);
 		});
 	};
 	
	$scope.save = function() {
		
		// check to make sure the form is completely valid
		if ($scope.recruiterForm.$valid) {
			$scope.recruiter.company_admin_id = $scope.company_admin_id;
			$scope.recruiter.company_id = $scope.company_id;
			$scope.recruiter.role = "recruiter";
			if($scope.recruiter.id){
				var url = '?action=uc_edit_worker';
			}else{
				var url = '?action=uc_create_user';
			}
			
			var params = jQuery.param({ 'data': $scope.recruiter });

			httprequest.fetch(url, params).then(function(response){

				if (response.data.errors) {
					var html = "";
					angular.forEach(response.data.errors,function(obj){
						html += obj + "<br/>";
					});
                	alert(html);
            	}else {
	                if (response.data.saved) {
	                	if($scope.recruiter.id){
	                		angular.forEach($scope.recruiterlist, function(key,obj) {
								if (obj.id === $scope.recruiter.id) {
									data[key].user_login = $scope.recruiter.user_login;
									data[key].user_email = $scope.recruiter.user_email;

								}
							});
							
							alert("recruiter Updated successfully.");
	                	}else{
	                		$scope.recruiter.id = response.data.ID;
		            		recruiterService.addRecruiter($scope.recruiter);
		            		alert( "recruiter added successfully.");
		            	}
		            	$scope.$broadcast('show-errors-reset');
		            	$scope.recruiter = {};
		            	
		            }
	               
    			}
            	
            });
            
		}
	};
	
	$scope.delete = function(field) {
		$confirm.show().then(function(res) {
      		if(res){
				var url = '?action=uc_delete_user';
				
				var params = jQuery.param({ 'id': field.id,'role':'recruiter' });

				httprequest.fetch(url, params).then(function(response){
			    	if (response.data.errors) {
			        	var html = "";
							angular.forEach(response.data.errors,function(obj){
								html += obj + "<br/>";
						});
						alert(html);
		            	
			    	}else {
			    		
			    		if (response.data.saved) {
			    			data = recruiterService.deleteRecruiter(field);
			    			alert("recruiter deleted successfully");
			            }
			           
					}
		    	
		    	});
		    }
		});    	
		
	};
	$scope.editRecruiter = function(obj){
		if(obj === 'new'){
			$scope.recruiter = {};
		}else{
			$scope.recruiter = obj;
		}
		$scope.showRecruiter = true;
	};
	$scope.cancel = function(){
		$scope.showRecruiter = false;
	};
	$scope.renderHtml = function (htmlCode) {
 			return $sce.trustAsHtml(htmlCode);
    };

 });


