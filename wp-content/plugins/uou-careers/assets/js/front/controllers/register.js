userModule.controller('userRegisterController', function( $scope, $filter, httprequest, $sce, alert, $translate){
	$scope.user = {};
 	$scope.saved = false;
    $scope.response_errors = false;
    $scope.accountType = [
        { name: 'Company', value: 'company' },
        { name: 'Candidate', value: 'candidate' },
    ];
    $scope.user.registration_type = $scope.accountType[2];
 	// function to submit the form after all validation has occurred			
	$scope.submitForm = function() {
		
		// check to make sure the form is completely valid
		if ($scope.userForm.$valid) {
			url = "?action=uc_register";
			params = jQuery.param({ 'data': $scope.user });

			httprequest.fetch(url, params).then(function(response){
		    	if (response.data.errors) {
		    		var html = "";
					angular.forEach(response.data.errors,function(obj){
						html += obj + "<br/>";
					});
            		alert(html);
                }else {
	                if (response.data.saved) {
	                   $scope.user = {};
	                    alert("Registration successfully done.");
	                    $scope.$broadcast('show-errors-reset');
	                   	window.location.href = response.data.url;

	                }else{
	                	alert("Registration has not done successfully.");
	                }
	                
            	}
            	
		    });
		}

	};
	
 });
userModule.controller('userLoginController', function( $scope, $filter, httprequest, $sce, alert, $translate){
	$scope.user = {};
 	$scope.saved = false;
    $scope.response_errors = false;
    
 	// function to submit the form after all validation has occurred			
	$scope.submitForm = function() {
		
		// check to make sure the form is completely valid
		if ($scope.userLoginForm.$valid) {
			url = "?action=uc_login";
			$scope.user.security = jQuery("#security").val();
			params = jQuery.param({ 'data': $scope.user });

			httprequest.fetch(url, params).then(function(response){
	    	    if (response.data.loggedin) {
                   $scope.user = {};
                    alert(response.data.message);
                    $scope.$broadcast('show-errors-reset');
                   	window.location.href = response.data.url;

                }else{
                	alert(response.data.message);
                }
	        });
		}

	};
	
 });

