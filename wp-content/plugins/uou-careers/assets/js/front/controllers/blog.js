userModule.controller('blog', function($scope,$filter,httprequest, alert,$sce,blogService,  $confirm, $translate){

 	$scope.post = {};
 	$scope.currentPage = 1;
  	$scope.pageSize = 10;
  	
 	$scope.post.type = "blog";
 	$scope.postlist = blogService.getPosts();
 	$scope.redactorOptions = buttonOptions;
 	// function to submit the form after all validation has occurred
 	$scope.pageChangeHandler = function(num) {
      //console.log('drinks page changed to ' + num);
  	};	
 	$scope.showData = function(){
 		if($scope.postlist.length > 0)
 			return true;
 		else
 			return false;
 	}
 	    // init the filtered items
   

 		
	$scope.submitForm = function() {
		
		// check to make sure the form is completely valid
		if ($scope.postForm.$valid) {
			if($scope.post.id){
				var url = '?action=uou_edit_post';
			}else{
				var url = '?action=uou_save_post';
			}
			$scope.post.type = "post";
			
			var params = jQuery.param({'data': $scope.post});
			httprequest.fetch(url, params).then(function(response){
		    	if (response.data.errors) {
            		var html = "";
					angular.forEach(response.data.errors,function(obj){
						html += obj + "<br/>";
					});
            		alert(html);
	            }else {
		            if (response.data.saved) {
		            	if($scope.post.id){
		            		angular.forEach($scope.postlist, function(obj) {
		            			if (obj.id == $scope.post.id) {
		            				$scope.postlist[$scope.postlist.indexOf(obj)].featured_image = response.data.featured_image;
						    	}
							});
	                		alert("Post Updated successfully.");
							
	                	}else{
	                		$scope.post.id = response.data.ID;
		            		blogService.addPost($scope.post);
		            		alert("Post added successfully.");
		            		
		            	}
                       $scope.$broadcast('show-errors-reset');
                       $scope.post = {};
					   jQuery(".redactor_editor").html("");
					   jQuery('img.img-responsive').attr("src", "");
					}
	               
    			}
		    });
	    }
	};
	
	$scope.delete = function(field) {
		$confirm.show().then(function(res) {
      		if(res){
				var url = '?action=uou_delete_post';
				var params = jQuery.param({ 'id': field.id });

				httprequest.fetch(url, params).then(function(response){
			    	if (response.data.errors) {
			        	var html = "";
							angular.forEach(response.data.errors,function(obj){
								html += obj + "<br/>";
						});
		            	alert(html);
			    	}else {
			    		
			    		if (response.data.saved) {
			    			$scope.postlist = blogService.deletePost(field);
			    			alert("Post deleted successfully");
			            }
			           
					}
		    	
		    	});
			}
    	});
		
	};
	$scope.editPost = function(obj){
		if(obj === 'new'){
			$scope.post = {};
			jQuery(".redactor_editor").html("");
		}else{
			$scope.post = obj;
			jQuery(".redactor_editor").html($scope.post.post_content);
		    jQuery('.preview-div-post').html($scope.post.featured_image);
		}
		$scope.showPost = true;

	};
	$scope.cancel = function(){
		$scope.showPost = false;
	};
	$scope.renderHtml = function (htmlCode) {
		return $sce.trustAsHtml(htmlCode);	
    };
});