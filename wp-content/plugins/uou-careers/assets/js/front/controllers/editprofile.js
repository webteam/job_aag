//Agency controller
userModule.controller('editProfile', function($scope,$filter,httprequest, alert,$sce, $translate){

 	$scope.user = user_object.userData.profile;

 	$scope.genderlist = form_taxonomy.taxonomylist.gender;
 	$scope.locationlist = form_taxonomy.taxonomylist.location;
 	$scope.nationalitylist = form_taxonomy.taxonomylist.nationality;
 	$scope.careerlevellist = form_taxonomy.taxonomylist.career_level;
 	$scope.dgreelist = form_taxonomy.taxonomylist.degree;
 	$scope.years_of_experiencelist = form_taxonomy.taxonomylist.years_of_experience;

 	if(!angular.isDefined($scope.user.post_meta)){
 		$scope.user.post_meta = {};
 	}
 	if(!angular.isDefined($scope.user.post_meta.urllist)){

 		$scope.user.post_meta.urllist = [];
 	}
 	if(!angular.isDefined($scope.user.post_meta.educationlist)){
 		$scope.user.post_meta.educationlist = [];
 	}
 	
 	if(!angular.isDefined($scope.user.post_meta.skilllist)){
 		$scope.user.post_meta.skilllist = [];
 	}

 	$scope.redactorOptions =buttonOptions;
 	setTimeout(function(){
		jQuery("form[name='userForm']").find(".redactor_editor").html($scope.user.post_content);
 	},600);
 	

 	//console.log("robi:"+$scope.user.post_content);
 	// function to submit the form after all validation has occurred			
	$scope.submitForm = function() {
		
		// check to make sure the form is completely valid
		if ($scope.userForm.$valid) {
			
			var url = '?action=uc_edit_user';
			
			var params = jQuery.param({ 'data': $scope.user });

			httprequest.fetch(url, params).then(function(response){
            	if (response.data.errors) {
					var html = "";
					angular.forEach(response.data.errors,function(obj){
						html += obj + "<br/>";
					});
                	alert(html);
            	}else {
	                if (response.data.saved) {
	            		alert("User edited successfully.");
	        		}
	                
				}
		    });
		}

	};
	$scope.addUrl = function(){
		$scope.user.post_meta.urllist.push({
			name : "",
			url: ""
		});

	};
	
	$scope.addEducation = function(){
		$scope.user.post_meta.educationlist.push({
			sname : '',
			qname : '',
			dname : '',
			nname : '',
		});
	};
	
	$scope.addSkill = function(){
		$scope.user.post_meta.skilllist.push({
			name : '',
			desc : '',
			level : ''
		});
	};
	$scope.removeUrl = function(obj){
		$scope.user.post_meta.urllist.splice( $scope.user.post_meta.urllist.indexOf(obj), 1 );
	 };

  	$scope.removeEducation = function(obj){
		$scope.user.post_meta.educationlist.splice( $scope.user.post_meta.educationlist.indexOf(obj), 1 );
	};
	$scope.removeSkill = function(obj){
		$scope.user.post_meta.skilllist.splice( $scope.user.post_meta.skilllist.indexOf(obj), 1 );
	};
	$scope.renderHtml = function (htmlCode) {
 			return $sce.trustAsHtml(htmlCode);
    };
    $scope.checkTrue = function(term_id, values, type){

    	if(angular.isDefined(term_id) && angular.isDefined(values)){
    		for (var i = 0; i < values.length; i++) {
	    		if(parseInt(values[i]) === parseInt(term_id)){

	    			if(type == 'region'){
	    				$scope.user.taxonomy.region = term_id;
	    			}else if(type == 'country'){
	    				$scope.user.taxonomy.country = term_id;
	    			}
	    			return true;
	    		}
	    	}
	    	return false;
    	}else{
    		return false;
    	}
    };
    $scope.getCountry = function(){
    	
    	if(angular.isDefined($scope.user.taxonomy) && angular.isDefined($scope.user.taxonomy.region)){
	    	var url = '?action=uou_get_country';
				
			var params = jQuery.param({ 'term_id': $scope.user.taxonomy.region });

			httprequest.fetch(url, params).then(function(response){
	        	$scope.countrylist = response.data.location;
	        });
	    }    
        
    };
    $scope.getState = function(){
    	if(angular.isDefined($scope.user.taxonomy) && angular.isDefined($scope.user.taxonomy.country)){
	    	var url = '?action=uou_get_state';
				
			var params = jQuery.param({ 'term_id': $scope.user.taxonomy.country });

			httprequest.fetch(url, params).then(function(response){
	        	$scope.statelist = response.data.location;
	   
		    });
		}    
    };
    setTimeout(function(){
		jQuery(".user-form").find(".redactor_editor").html($scope.user.post_meta.content);
		$scope.getCountry();
	
 	},100);
 	var handle = setInterval(function(){
 		
 		if(angular.isDefined($scope.user.taxonomy) && angular.isDefined($scope.user.taxonomy.country)){
 			$scope.getState();
 			clearInterval(handle);
 		}
		
 	},100);
	$scope.renderHtml = function (htmlCode) {
		setTimeout( function(){
			return $sce.trustAsHtml(htmlCode);
		}, 500 );
 		return $sce.trustAsHtml(htmlCode);	
    };
 });
