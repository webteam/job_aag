
// service definitions
userModule.service('httprequest', function($http) {
  var alreadyLoading = {};
  return {
    fetch: function(url, params, fileParams) {
      if ( url in alreadyLoading ) {
        return alreadyLoading[url];
      }
     // jQuery("#spinner").show();
      return alreadyLoading[url] = $http({
            method: 'POST',
            url: ajax_object.ajaxurl+url ,
            data: params,
            file: fileParams,
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).then(function(response){
         // jQuery("#spinner").hide();
          delete alreadyLoading[url];
          return response;
      });
    }
  };
});

userModule.service( 'alert', function( $alert ) {
  return function( msg ) {
    $alert( { title: " ", content: msg, animation: 'fadeZoomFadeDown', type: 'material', show : true, duration : 8 } );
  };
});


//blog Service 
userModule.service('$confirm', function($modal, $rootScope, $q) {
  var scope = $rootScope.$new();
  var deferred;
  scope.title = 'Delete';
  scope.content = 'Are you sure?';
  scope.answer = function(res) {
    deferred.resolve(res);
    confirm.hide();
  }
  
  var confirm = $modal({template: ajax_object.tpl_url+'modal/confirm.tpl.html', scope: scope, show: false});
  var parentShow = confirm.show;
  confirm.show = function() {
    deferred = $q.defer();
    parentShow();
    return deferred.promise;
  }
  
  return confirm;
});
userModule.service('blogService', function() {
    var postList = user_object.userData.postlist;

  var addPost = function(newObj) {
        postList.push(newObj);
        return postList;
  }

  var getPosts = function(){
        return postList;
  }
  var deletePost = function(scope) {
    postList.splice(postList.indexOf(scope),1);
    return postList;
  }

  return {
      addPost: addPost,
      getPosts: getPosts,
      deletePost: deletePost
  };
});

//company service provider
userModule.service('portfolioService', function() {
  var portfolioList  = [];
      var portfolioList = user_object.userData.portfoliolist;

    var addPortfolio = function(newObj) {
        portfolioList.push(newObj);
        return portfolioList;
    }

    var getPortfolios = function(){
        return portfolioList;
    }
    var deletePortfolio = function(scope) {
      portfolioList.splice(portfolioList.indexOf(scope),1);
      return portfolioList;
  }

  return {
        addPortfolio: addPortfolio,
        getPortfolios: getPortfolios,
        deletePortfolio:deletePortfolio
  };
});

 //recruiter Service 
userModule.service('recruiterService', function() {
    var recruiterList = user_object.userData.recruiters;

  var addRecruiter = function(newObj) {
        recruiterList.push(newObj);
        return recruiterList;
  }

  var getRecruiters = function(){
        return recruiterList;
  }
  var deleteRecruiter = function(scope) {
    recruiterList.splice(recruiterList.indexOf(scope),1);
    return recruiterList;
  }

  return {
      addRecruiter: addRecruiter,
      getRecruiters: getRecruiters,
      deleteRecruiter: deleteRecruiter
  };
});

//job service
userModule.service('QueryStringToJSON', function() {
  var getJson = function() {
    var parts = location.href.split('#?');      
    var pairs = parts[1].split('&');
        
    var result = {};
    pairs.forEach(function(pair) {
      pair = pair.split('=');

      if(pair[0].search("%5B%5D") != -1){
        
        if(angular.isUndefined(result[pair[0].replace("%5B%5D", "")]))
          result[pair[0].replace("%5B%5D", "")] = [];
        result[pair[0].replace("%5B%5D", "")].push(decodeURIComponent(pair[1].replace(/\+/g, ' ') || ''));
      
      }else if(pair[0].search("%5B") != -1){
        obj = pair[0].split('%5B');
        if(angular.isUndefined(result[obj[0]]))
          result[obj[0]] = {};
        if(obj[1].replace("%5D", "") == "min"){
          result[obj[0]].min = pair[1];
        }else{
          result[obj[0]].max = pair[1];
        }
        
      }else{

          result[pair[0]] = decodeURIComponent(pair[1].replace(/\+/g, ' ') || '');
      }
          
          
    });

    return JSON.parse(JSON.stringify(result));
  }

  
  return {
    getJson: getJson 
  };
});

//job service
userModule.service('jobService', function() {
  var jobList = user_object.userData.jobs;

  var addJob = function(newObj) {
      jobList.push(newObj);
      return jobList;
  }

  var getJobs = function(){
      return jobList;
  }
  var deleteJob = function(scope) {
    jobList.splice(jobList.indexOf(scope),1);
    return jobList;
  }

  return {
    addJob: addJob,
    getJobs: getJobs,
    deleteJob : deleteJob
  };
});