// directive definitions
userModule.directive('staticInclude', function($http, $templateCache, $compile) {
    return function(scope, element, attrs) {
        var templatePath = attrs.staticInclude;
        $http.get(templatePath, { cache: $templateCache }).success(function(response) {
            var contents = element.html(response).contents();
            $compile(contents)(scope);
        });
    };
});
userModule.directive('txUploader', ['$compile',function($compile) {
        return {
            restrict: "A",
            scope : {
              src : '=src'
            },
            require: "ngModel",
            link: function(scope, element, attrs, ngModel) {

                var file_frame;

                
                element.bind("click", function(e) {
                   e.preventDefault();


                   

                   if ( file_frame ) {
                      file_frame.open();
                      return;
                    }

                   

                    
                    file_frame = wp.media.frames.file_frame = wp.media({
                      title: jQuery( this ).data( 'uploader_title' ),
                      button: {
                        text: jQuery( this ).data( 'uploader_button_text' ),
                      },
                      multiple: false  
                    });


                    file_frame.on( 'select', function() {              
                      attachment = file_frame.state().get('selection').first().toJSON();
                    
                      
                      returnModel = {'attachment_id':attachment.id, 'url': attachment.url};
                      ngModel.$setViewValue(returnModel);
                      scope.src= attachment.url;
                      scope.$apply('src');
                    });

                    file_frame.open();

                });

            }
        };
    }]);

userModule.directive('showErrors', function($timeout) {
  return {
    restrict: 'A',
    require: '^form',
    link: function (scope, el, attrs, formCtrl) {
      // find the text box element, which has the 'name' attribute
      var inputEl   = el[0].querySelector("[name]");
      // convert the native text box element to an angular element
      var inputNgEl = angular.element(inputEl);
      // get the name on the text box
      var inputName = inputNgEl.attr('name');
      
      // only apply the has-error class after the user leaves the text box
      var blurred = false;
      inputNgEl.bind('blur', function() {
        blurred = true;
        el.toggleClass('has-error', formCtrl[inputName].$invalid);
      });
      
      scope.$watch(function() {
        return formCtrl[inputName].$invalid
      }, function(invalid) {
        // we only want to toggle the has-error class after the blur
        // event or if the control becomes valid
        if (!blurred && invalid) { return }
        el.toggleClass('has-error', invalid);
      });
      
      scope.$on('show-errors-check-validity', function() {
        el.toggleClass('has-error', formCtrl[inputName].$invalid);
      });
      
      scope.$on('show-errors-reset', function() {
        $timeout(function() {
          el.removeClass('has-error');
        }, 0, false);
      });
    }
  }
});

userModule.filter('dataFilter', [function () {
    return function (dataList, filterItem){

        if (  !angular.isUndefined(dataList) && 
              !angular.isUndefined(filterItem) && 
              ( 
                (!angular.isUndefined(filterItem.location) && (filterItem.location !== "")) || 
                (!angular.isUndefined(filterItem.industry) && (filterItem.industry !== "")) || 
                (!angular.isUndefined(filterItem.job_type) && (filterItem.job_type !== "")) ||
                (!angular.isUndefined(filterItem.role) && (filterItem.role.length > 0)) ||
                (!angular.isUndefined(filterItem.skills) && (filterItem.skills.length > 0)) ||
                !angular.isUndefined( filterItem.postday ) ||
                !angular.isUndefined( filterItem.salary )
                
              ) 
              
        ) {
            
            var tempDataList = [], check = 0;
            location.hash = "?"+jQuery.param(filterItem);

            if(!angular.isUndefined(filterItem.location) && filterItem.location != ""){
              angular.forEach(dataList, function (data) {
                angular.forEach(data.location, function (item) {

                    var item_match =  new RegExp("^.*"+filterItem.location+".*$","gi"); 
                    if( item.name.match(item_match) ){
                      if(tempDataList.indexOf(data) == -1){
                        tempDataList.push(data);
                        check = 1;
                      }  
                    }

                  });
              });
            }
            
            if(!angular.isUndefined(filterItem.industry) && filterItem.industry != ""){
              if( check > 0 ){
                var temp = [];
                angular.forEach(tempDataList, function (data) {
                  angular.forEach(data.industry, function (item) {
                    var item_match =  new RegExp("^.*"+filterItem.industry+".*$","gi"); 
                    if( item.name.match(item_match) ){
                      temp.push(data);
                        check = 1;
                    }
                  });
                });
                tempDataList = temp;
              }else{
                
                angular.forEach(dataList, function (data) {
                  angular.forEach(data.industry, function (item) {
                      var item_match =  new RegExp("^.*"+filterItem.industry+".*$","gi"); 
                      if( item.name.match(item_match) ){
                        tempDataList.push(data);
                        check = 1;
                      }
                  });
                });
              }
            }

            if(!angular.isUndefined(filterItem.job_type) && filterItem.job_type != ""){  
              if( check > 0 ){
                var temp = [];
                angular.forEach(tempDataList, function (data) {
                  angular.forEach(tempDataList.job_type, function (item) {
                      var item_match =  new RegExp("^.*"+filterItem.job_type+".*$","gi"); 
                      if( item.name.match(item_match) ){
                        temp.push(data);
                          check = 1;
                      }
                  });
                });
                tempDataList = temp;
              }else{
                angular.forEach(dataList, function (data) {
                  angular.forEach(data.job_type, function (item) {
                      var item_match =  new RegExp("^.*"+filterItem.job_type+".*$","gi"); 
                      if( item.name.match(item_match) ){
                        tempDataList.push(data);
                          check = 1;
                      }
                  });
                });
              }
            }  

            if(!angular.isUndefined(filterItem.role) && filterItem.role.length > 0){

              if( check > 0 ){ 
                var temp = [];

                angular.forEach(tempDataList, function (data) {
                  angular.forEach(data.role, function (item) {
                    angular.forEach(filterItem.role, function (role) {
                      var item_match =  new RegExp("^.*"+role+".*$","gi"); 
                      if( item.name.match(item_match) ){
                        if(temp.indexOf(data) == -1){
                          temp.push(data);
                          check = 1;
                        }  
                      }
                    });
                  });
                });
                tempDataList = temp;
              }else{

                angular.forEach(dataList, function (data) {
                  angular.forEach(data.role, function (item) {
                    angular.forEach(filterItem.role, function (role) {
                      var item_match =  new RegExp("^.*"+role+".*$","gi"); 
                      if( item.name.match(item_match) ){
                        if(tempDataList.indexOf(data) == -1){
                          tempDataList.push(data);
                          check = 1;
                        }    
                      }
                      
                    });  
                  });
                });
              }
          }
          if(!angular.isUndefined( filterItem.postday ) && filterItem.postday.min > 0 && filterItem.postday.max <= 60){
            if( check > 0 ){ 
                var temp = [];

                angular.forEach(tempDataList, function (data) {
                  if ( data.postday >= filterItem.postday.min && data.postday <= filterItem.postday.max ) {
                    if(temp.indexOf(data) == -1){
                        temp.push(data);
                        check = 1;
                    }
                  }
                });
                tempDataList = temp;
              }else{
                angular.forEach(dataList, function (data) {
                  if ( parseFloat(data.postday) >= filterItem.postday.min && parseFloat(data.postday) <= filterItem.postday.max ) {
                    if(tempDataList.indexOf(data) == -1){
                        tempDataList.push(data);
                        check = 1;
                    }
                  }
                });
              }

          }

          if(!angular.isUndefined( filterItem.salary ) && filterItem.salary.min > 0 && filterItem.salary.max  <=  100000){
            if( check > 0 ){ 
                var temp = [];

                angular.forEach(tempDataList, function (data) {
                  if ( data.salary >= filterItem.salary.min && data.salary <= filterItem.salary.max ) {
                    if(temp.indexOf(data) == -1){
                        temp.push(data);
                        check = 1;
                    }
                  }
                });
                tempDataList = temp;
              }else{
                angular.forEach(dataList, function (data) {
                  if ( parseFloat(data.salary) >= filterItem.salary.min && parseFloat(data.salary) <= filterItem.salary.max ) {
                    if(tempDataList.indexOf(data) == -1){
                        tempDataList.push(data);
                        check = 1;
                    }
                  }
                });
              }
          }

          if(!angular.isUndefined(filterItem.skills) && filterItem.skills.length > 0){
              if( check > 0 ){
                var temp = [];
                angular.forEach(tempDataList, function (data) {
                  angular.forEach(data.skills, function (item) {
                    var item_match =  new RegExp("^.*"+filterItem.skills+".*$","gi"); 
                    if( item.name.match(item_match) ){
                      temp.push(data);
                        check = 1;
                    }
                  });
                });
                tempDataList = temp;
              }else{
                
                angular.forEach(dataList, function (data) {
                  angular.forEach(data.skills, function (item) {
                      var item_match =  new RegExp("^.*"+filterItem.skills+".*$","gi"); 
                      if( item.name.match(item_match) ){
                        tempDataList.push(data);
                        check = 1;
                      }
                  });
                });
              }
        }
            
          return tempDataList;
          
        } else {
            return dataList;
        }
    };
}]);

