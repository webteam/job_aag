var userModule = angular.module("userModule", ['mgcrea.ngStrap','mgcrea.ngStrap.modal',
'angular-redactor','angularUtils.directives.dirPagination','ngAnimate', 'pascalprecht.translate', 'angular-loading-bar', 'ngSanitize']);

userModule.config(function ($translateProvider) {
    $translateProvider.translations('en', {
      TITLE: 'Hello',
      MENU_PUBLIC_PROFILE: 'Public Profile',
      MENU_PROFILE: 'Profile',
      MENU_RESUME: 'Resume',
      MENU_MY_SETTINGS: 'My Settings',
      MENU_SUBSCRIPTION: 'Subsciption',
      MENU_LOGOUT: 'Logout',
      MENU_ENGLISH: 'English',
      MENU_DENMARK: 'Denmark',
     



      LABEL_FEATURE : 'Featured?',
      LABEL_EMAIL : 'Email:',
      LABEL_TITLE: 'Title',
      LABEL_MEMBER_QUOTE: 'Member Quota',
      LABEL_FEATURE : 'Featured?',
      LABEL_NAME: 'Name:',
      LABEL_POTRAIT: 'User Protrait',
      LABEL_EMAIL : 'Email:',
      LABEL_USERNAME : 'Username:',
      LABEL_PASSWORD : 'Password:',
      LABEL_COMPANY_NAME: 'Company Name',
      LABEL_WEBSITE: 'Website:',
      LABEL_DESCRIPTION: 'Description:',
      LABEL_PORTFOLIO_TITLE: 'Portfolio Title',
      LABEL_PORTFOLIO_DESCRIPTION: 'Portfolio Description',
      LABEL_IMAGE: 'Image',
      LABEL_FACEBOOK: 'Facebook',
      LABEL_TWITTER: 'Twitter',
      LABEL_GOOGLE: 'Google',
      LABEL_LINKEDIN: 'Linkedin',
      LABEL_YOUTUBE: 'Youtube',
      LABEL_INSTAGRAM: 'Instagram',




      EMAIL_REQUIRED: 'The user\'s email is required',
      EMAIL_INVALID: 'The email address is invalid',




      NO_RECRUITER_FOUND: 'There are no recruiter added yet.',
      NO_COMPANY_FOUND: 'There are no company added yet.',
      



      END_CONTRACT: 'End Contract',
      PENDING_REQUEST: 'Pending Request',
      ASK_TO_JOIN: 'Ask To Join',
      



      BUTTON_JOIN: 'Join',
      BUTTON_REJECT: 'Reject',
      BUTTON_CANCEL: 'Cancel',
      BUTTON_COMPANY: 'Add New Company',
      BUTTON_UPDATE_PROFILE: 'Update Profile',


    });
    $translateProvider.translations('de', {
      TITLE: 'Hello',
      MENU_PUBLIC_PROFILE: 'De Public Profile',
      MENU_PROFILE: 'De Profile',
      MENU_RESUME: 'De Resume',
      MENU_MY_SETTINGS: 'De My Settings',
      MENU_SUBSCRIPTION: 'De Subsciption',
      MENU_LOGOUT: 'De Logout',
      MENU_DENMARK: 'De Denmark',
      MENU_ENGLISH: 'De English',
    });
    $translateProvider.preferredLanguage('en');
});
// var plans = angular.module('SubsPlans',[]);
// var plans = angular.bootstrap(document.getElementById("SubsPlans"), ['SubsPlans']);
