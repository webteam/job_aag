
// by tareq 

plans.controller('showSubscription',function($scope){
    //console.log(subscription.show);
    if( angular.isDefined(subscription.show) ){
        $scope.packages = subscription.show;
        $scope.membership_type = subscription.membership_type;
    }





});


var subPlans = angular.module('CreatePlans',[ 'mgcrea.ngStrap']);

    subPlans.directive('txContenteditable', ['$compile', function($compile) {
        return {
            restrict: "A",
            scope : {
              old : '@'
            },
            require: "ngModel",
            link: function(scope, element, attrs, ngModel) {

                element.bind("click", function(e) {
                    e.preventDefault();
                });

                element.attr("contenteditable", true);

                function read() {
                    ngModel.$setViewValue(element.html());
                }

                ngModel.$render = function() {

                        element.html(ngModel.$viewValue || '' );

                };

                ngModel.$isEmpty('tareq');

                element.bind("keydown", function(e) {
                    if (e.keyCode == 13) {
                        document.execCommand('insertHTML', false, '<br><br>');
                        return false;
                    }
                });

                element.bind("blur keyup change", function(e) {
                    scope.$apply(read);
                });
            }
        };
    }]);



(function($){
   // write code here
 	var subs_form = $('#subscription_plan,#email_settings').find('form').find('input[type=submit]');
 	subs_form.css({
 		'display':'none'
 	});
 	$('#email_settings').find('.form-table').find('tr').eq(0).css({'display':'none'});
 	$('#subscription_plan').find('.form-table').find('tr').eq(0).css({'display':'none'});
 	
 
})(jQuery);





	subPlans.filter('dashed', function () {
	        return function (text) {				
				var str = text.replace(/\s+/g, '-');
				return str.toLowerCase();
	        };
	});





	subPlans.controller('MakeSubscription',['$scope','$http','$alert',function( $scope, $http ,$alert ){



	    $scope.plan = {};

		$scope.allPlans = [];

	    	$http({
	            method: 'POST',
	            url: ajax_object.ajaxurl+'?action=uc_get_plans' ,	        
	            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
		    }).then( function( response ){	

		    	if( response.data != "null" ){

		     		$scope.allPlans = response.data;
		    	}


		    });










	
     	$scope.savePlan = function(){


     		if($scope.allPlans.length === 0){
				var id = 1;
			}else{
				var id = $scope.allPlans[$scope.allPlans.length-1].id  + 1;
			}
			//$scope.plan.id = id;
			
			$scope.allPlans.push({
				'id': id,
				'title' : $scope.plan.title,
				'member_quota' : $scope.plan.member_quota,
				'job_quota' : $scope.plan.job_quota,
				'blog_quota' : $scope.plan.blog_quota,
				'feature_job_quota' : $scope.plan.feature_job_quota,
				'tab_profile' : $scope.plan.tab_profile,
				'membership_type' : $scope.plan.membership_type,
				'membership_price' : $scope.plan.membership_price,
				'details' : false
			});
			$scope.plan = {};
		}

	   	$scope.removePlan = function(scope) {

	          $scope.allPlans.splice($scope.allPlans.indexOf(scope),1);
	    };

	    $scope.storePlans = function(){

	    	$http({
	            method: 'POST',
	            url: ajax_object.ajaxurl+'?action=uc_store_subscription_data' ,
	            data: jQuery.param({ 'data': $scope.allPlans }),
	            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
		    }).then( function( response ){	

		          $alert({
		            content: response.data.msg,
		            animation: 'fadeZoomFadeDown',
		            type: 'material',
		            duration: 8
		          });

		    	

		    });


	    }	



}]);

var emailSettings = angular.module('EmailSettings',['mgcrea.ngStrap','mgcrea.ngStrap.modal','colorpicker.module']);
emailSettings.controller('emailController', ['$scope','$http','$alert',function( $scope, $http ,$alert ){
	$scope.uou_email_template_options = {};
	
	$scope.tabs = ['Email Options','New Account Notification', 'Contract Email', 'Job Apply', 'Job Assignment'];
	$scope.tabs.index = 0;
	$scope.tabs.active = function(){
		return $scope.tabs[$scope.tabs.index];
	};
	$scope.indexChanged = function(index){
		$scope.tabs.index = index;
		console.log($scope.tabs.index);
	};

	$http({
        method: 'POST',
        url: ajax_object.ajaxurl+'?action=uc_get_email_options' ,	        
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then( function( response ){	

    	if( response.data != "null" ){

     		$scope.uou_email_template_options = response.data;
    	}


    });
    $scope.storeEmailSettings = function(){

	    	$http({
	            method: 'POST',
	            url: ajax_object.ajaxurl+'?action=uc_add_email_options' ,
	            data: jQuery.param({ 'data': $scope.uou_email_template_options }),
	            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
		    }).then( function( response ){	

		          $alert({
		            content: response.data.msg,
		            animation: 'fadeZoomFadeDown',
		            type: 'material',
		            duration: 8
		          });

		    	

		    });
	}	
	
	

}]);
angular.bootstrap(document.getElementById("emailsettings"), ['EmailSettings']);
