<?php get_header(); ?>


<div class="container percentage-width">
	<div class="row">
		<div class="col-md-12">




			<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">

			  <li role="presentation" class="active"><a href="#profile" role="tab" data-toggle="tab">Profile</a></li>		  
			  <li role="presentation"><a href="#blog" role="tab" data-toggle="tab">Blog</a></li>		  
			  <li role="presentation"><a href="#portfolio" role="tab" data-toggle="tab">Portfolio</a></li>
			  <li role="presentation"><a href="#contact" role="tab" data-toggle="tab">Contact</a></li>
		    
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane active" id="profile">


				<?php  	if ( have_posts() ):  
				// while ( have_posts() ) : 
							the_post(); $post_id= $post->ID; $author_id= $post->post_author; ?>

						<div class="col-md-4">
						
								<?php 	if(  has_post_thumbnail() ) the_post_thumbnail( 'full' , array('class' => 'thumbnail') );  ?>

								<?php

									$current_user = wp_get_current_user($author_id);
									
									$professional_title   = get_post_meta(get_the_ID() , 'professional_title' , true );
									$resume_content   = get_post_meta(get_the_ID() , 'resume_content' , true );
									
									


									$address           = get_post_meta(get_the_ID() , 'location' , true );
									$urllist           = json_decode( get_post_meta(get_the_ID() , 'urllist' , true ) );
									$educationlist           = json_decode( get_post_meta(get_the_ID() , 'educationlist' , true ) );
									$skilllist           = json_decode( get_post_meta(get_the_ID() , 'skilllist' , true ) );
									$experiencelist           = json_decode( get_post_meta(get_the_ID() , 'experiencelist' , true ) );
									
									
									
									// $facebook          = get_post_meta(get_the_ID() , '_agency_facebook' , true );
									// $twitter           = get_post_meta(get_the_ID() , '_agency_twitter' , true );
									// $google            = get_post_meta(get_the_ID() , '_agency_google' , true );
									// $linkedin          = get_post_meta(get_the_ID() , '_agency_linkedin' , true );
									// $youtube           = get_post_meta(get_the_ID() , '_agency_youtube' , true );
									// $instagram         = get_post_meta(get_the_ID() , '_agency_instagram' , true );
									

								  ?>

							
										
							

							<table class="table table-striped">



							    <?php  if( !empty($professional_title) ):  ?>
							    <tr><td></td><td><h6><?php  echo $professional_title;  ?></h6></td></tr>
								<?php  endif;  ?>
						

							    <?php  if( !empty($address) ):  ?>
							    <tr><td>Address</td><td> <?php  echo $address;  ?></td></tr>
								<?php  endif;  ?>


							</table>




						</div>
						<div class="col-md-8">
							<h1><?php  the_title();  ?></h1>

							    <?php  if( !empty($agency_slogan) ):  ?>
										<p><?php  echo $agency_slogan;  ?></p>
								<?php  endif;  ?>

							<p> <?php  the_content();  ?></p>


							<?php  if( !empty($resume_content) ):  ?>
								<h2>Resume Content</h2>
						    	<?php  echo $resume_content;  ?>
							<?php  endif;  ?>


							<?php  if( !empty($educationlist) ):  ?>
							<h2>Educations</h2>
							<?php foreach ($educationlist as $list) : ?>
								<article class="education-items">
									<h1><?php echo $list->qname; ?> | <span><?php echo $list->sname; ?></span></h1>
									<div><?php echo $list->dname; ?></div>
								</article>
							<?php endforeach; ?>
							<?php  endif;  ?>


							<?php  if( !empty($skilllist) ):  ?>
							<h2>Skills</h2>
							<ul class="skill-lists">
							<?php foreach ($skilllist as $list) : ?>
								<li><?php echo $list->name; ?></li>
							<?php endforeach; ?>
							</ul>
							<?php  endif;  ?>

							<?php  if( !empty($experiencelist) ):  ?>
							<h2>Experiences</h2>
							<?php foreach ($experiencelist as $list) : ?>
								<article class="education-items">
									<h1><?php echo $list->jtname; ?> | <span><?php echo $list->ename; ?></span></h1>
									<div><?php echo $list->dname; ?></div>
								</article>
							<?php endforeach; ?>
							<?php  endif;  ?>

							<?php  if( !empty($urllist) ):  ?>
							<h2>URL(s)</h2>
							<ul class="skill-lists">
							<?php foreach ($urllist as $list) : ?>
								<li><a href="<?php echo $list->url; ?>"><?php echo $list->name; ?></a></li>
							<?php endforeach; ?>
							</ul>
							<?php  endif;  ?>
						</div>


				<?php  // endwhile;   
					else:
					    echo 'no post was found';
					endif;
				?>



		  	
		  </div>

		  

		

		  <div role="tabpanel" class="tab-pane" id="blog">

		  		<?php  $candidate_blog_posts = get_posts(array(
		  					'post_type' => 'blog',
		  					'author'    => $author_id,		  					

		  				));

		  		foreach ($candidate_blog_posts as $blog_posts) : ?>

		  			  			<article class="blog-post with-thumb">

		  			  				<?php
		  			  					$thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id( $blog_posts->ID ) );

		  			  					if(!empty($thumbnail)) :
		  			  				?>
		  			  					<div class="thumb"><a href="<?php echo get_the_permalink($blog_posts->ID); ?>"><img class="post-image" src="<?php echo $thumbnail; ?>" alt=""></a></div>

		  			  				<?php endif; ?>
		  			  				
									<h6 class="title"><a href="<?php echo get_the_permalink($blog_posts->ID); ?>"><?php echo $blog_posts->post_title; ?></a></h6>

									<span class="meta"></span>

									<p class="description">
										<?php
											apply_filters('the_content', $blog_posts->post_content);
											$content =  apply_filters('the_content', $blog_posts->post_content); 

											$trimmed_content = wp_trim_words( $content, 50, '<a href="'. get_permalink($blog_posts->ID) .'"> ...Read More</a>' );
											
											echo $trimmed_content;
										?>
									</p>

									<div class="clearfix"></div>

								</article>
		  			  				
		  		<?php endforeach; ?>
		  	
		  </div>


		<div role="tabpanel" class="tab-pane" id="portfolio">
			<h1>Portfolio</h1>
		  		<?php  

		  				$candidate_portfolios = get_posts(array(
		  					'post_type' => 'portfolio',
		  					'author'    => $author_id,		  					

		  				)); 

		  			  foreach( $candidate_portfolios as $portfolio ) { 	  			  				
		  			  					

		  			  			
		  			  				$content =  apply_filters('the_content', $portfolio->post_content); 

									$trimmed_content = wp_trim_words( $content, 140 );
						?>

						<div class="col col-md-6">
						    
						    <?php  echo get_the_post_thumbnail( $portfolio->ID, 'full', array('class' => 'thumbnail') );  ?>
						    
							<h6><?php  echo $portfolio->post_title;  ?></h6>
							<hr>
							<p><?php echo $trimmed_content; ?></p>
						</div>


						

						<?php		
					}
		  		?>


		</div>


		  <div role="tabpanel" class="tab-pane" id="contact">
		  		<h1>Contact form</h1>
		  		<br>

				<form class="col col-md-4" 	method="post" action="">
					<input type="hidden" name="to" class="form-control"  value="<?php  echo $current_user->user_email;  ?>"> 
					<label>From</label>
					<input type="text" name="from" class="form-control"  placeholder="From: ">
					<label>Subject</label>
					<input type="text" name="subject" class="form-control"  placeholder="Subject: ">
					<label>Message</label>
					<textarea name="message" placeholder="Message"></textarea>

					<br>
					<button type="submit" class="btn btn-info">Submit</button>
					<?php wp_nonce_field('handle_contact_form', 'nonce_contact_form');?>
				</form>

		  </div>





		<div class="clearfix"></div>
		</div>


	</div>
			
	</div><!--  end of row  -->
</div><!--  end of container -->


<?php

wp_reset_postdata();
//get_sidebar();
get_footer();