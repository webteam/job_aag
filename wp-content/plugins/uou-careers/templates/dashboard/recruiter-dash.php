<?php
get_career_header();
$recruiter = get_the_user('recruiter');
$recruiter->the_post();
get_recruiter_profile($post->ID, $post->post_title, $post->post_content);


?>

<div id="page-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="responsive-tabs horizontal dashboard-horizontal-tabs">
          <ul class="nav nav-tabs">
            <!-- <li><a href="#profile-edit">Edit Profile</a></li> -->
            <li><a href="#job">Job List</a></li>
            <li><a href="#job-applications">Job Applications</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="job" ng-controller="job" >
              <h3 class="tab-title">
                Add Job
              </h3>
              <div ng-show="showJob" class="cssSlideUp"  static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/recruiter/job/form.html"></div>
              <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/recruiter/job/list.html"></div>
            </div>
            <div class="tab-pane" id="job-applications" ng-controller="job-applicationlist" >
                                          <h3 class="tab-title">
                                                Job Applications
                                          </h3>
                                          <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/job-applicationlist/list.html"></div>
                                         
                                    </div>
          </div>
        </div>
      </div> <!-- end .page-content -->
    </div>
  </div> <!-- end .container -->
</div> <!-- end #page-content -->




<?php get_career_footer(); ?>