<?php
get_career_header();
$candidate = get_the_user('candidate');
$candidate->the_post();
get_candidate_profile($post->ID, $post->post_title, $post->post_content);

?>
<div id="page-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="responsive-tabs horizontal dashboard-horizontal-tabs">
          <ul class="nav nav-tabs">            
            <li class="active"><a href="#resume">Post your resume</a></li>
          </ul>
          <div class="tab-content">
                


            <div class="tab-pane active" id="resume" ng-controller="userResume">
                  <h3 class="tab-title">Post your resume</h3>
                  <div ng-animate="'fade'" static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/candidate/resume.html"></div>
            </div>

                
                
          </div>
        </div>
      </div> <!-- end .page-content -->
    </div>
  </div> <!-- end .container -->
</div> <!-- end #page-content -->


<?php
get_career_footer();