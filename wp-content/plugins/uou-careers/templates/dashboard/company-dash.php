<?php
get_career_header();

$company = get_the_user('company');
$company->the_post();
get_company_profile($post->ID, $post->post_title, $post->post_content);
get_subscription_plans();
$billingPeriod = billingPeriodArray();

$tpath = UOU_CAREERS_URL.'/assets/js/vendor/angular-utils-pagination/dirPagination.tpl.html';

?>
<div id="page-content">
      <div class="container">
            <div class="row">
                  <div class="careers-content">

                  
                        <div class="responsive-tabs horizontal dashboard-horizontal-tabs">
                              <ul class="nav nav-tabs">
                                          <!-- <li class="active"><a href="#my-plan-tab">Subscription</a></li> -->
                                          <li class="active"><a href="#recruiter">Add Recruiter</a></li>
                                          <li><a href="#job">Add Job</a></li>
                                          <li><a href="#job-applications">Job Applications</a></li>
                                          <?php  do_action('uc_add_company_profile_admin_tab_name');   ?>
                              </ul>
                              <div class="tab-content bg-white">

                                    <div class="tab-pane active" id="recruiter" ng-controller="recruiter" >
                                          <h3 class="tab-title">
                                            Add Recruiter
                                          </h3>
                                          <div ng-show="showRecruiter" class="cssSlideUp" static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/company/recruiter/form.html"></div>
                                          <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/company/recruiter/list.html"></div>
                                         
                                    </div>
                                    
                                    <div class="tab-pane" id="job" ng-controller="job" >
                                          <h3 class="tab-title">
                                                Add Job
                                          </h3>
                                           <div ng-show="showJob" class="cssSlideUp" static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/company/job/form.html"></div>
                                          <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/company/job/list.html"></div>
                                         
                                    </div>
                                    <div class="tab-pane" id="job-applications" ng-controller="job-applicationlist" >
                                          <h3 class="tab-title">
                                                Job Applications
                                          </h3>
                                          <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/job-applicationlist/list.html"></div>
                                         
                                    </div>
                                    <?php  do_action('uc_add_company_profile_admin_tab_pane');  ?>

                              </div>
                        </div>
 
                   </div> <!-- end .page-content -->
            </div>

      </div> <!-- end .container -->

</div> <!-- end #page-content -->



<?php
get_career_footer();