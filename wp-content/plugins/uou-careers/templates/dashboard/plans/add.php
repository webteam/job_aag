<?php
	get_career_header();

	// $payments_data = get_option('payments');
	// $payment_option = $payments_data['uc_payment_method'] ? $payments_data['uc_payment_method'] : ' ';

	global $careers;

	$payment_option  = $careers->payment_option;
	


	// generate a token from server side to work with client side . 
	// used localized

	do_action('uc_subscription_token_'.$payment_option);

?>




<div class="container">


		<div class="subscription-nav">
			<a class="btn btn-primary active" href="<?php  echo bloginfo('url');?>/dashboard/subscription">subscription</a>
			<a class="btn btn-primary" href="<?php  echo bloginfo('url');?>/dashboard/subscription/invoices">Invoices</a>  		
			<a class="btn btn-primary" href="<?php  echo bloginfo('url');?>/dashboard/subscription/updates">Update</a>	  		
			<a class="btn btn-primary" href="<?php  echo bloginfo('url');?>/dashboard/subscription/cancel">Cancel</a>
		</div>


	<div class="clearfix"></div>
	<?php  do_action('uc_payment_form_'.$payment_option);?>		
</div>

	
<?php
	get_career_footer();