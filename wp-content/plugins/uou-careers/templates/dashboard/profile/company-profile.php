<?php
get_career_header();
$company = get_the_user('company');
$company->the_post();

$settingsData = get_company_profile($post->ID, $post->post_title, $post->post_content);
$content = $post->post_content;
$editor_id = 'post_content';
$settings = array("ng-model"=> "user.post_content");

 get_subscription_plans();
 
 $billingPeriod = billingPeriodArray();
 //$currentPlan = get_user_meta(get_current_user_id(), 'membership_type', true);

$company_id = $post->ID;

//echo "<pre>";print_r($current_user);echo "</pre>";

?>
<div id="page-content">
      <div class="container">
            <div class="row">
                  <div class="careers-content">

                  
                        <div class="responsive-tabs horizontal dashboard-horizontal-tabs">
                              <ul class="nav nav-tabs">
                                          <!-- <li class="active"><a href="#my-plan-tab">Subscription</a></li> -->
                                          <li class="active"><a href="#profile-edit">Edit Profile</a></li>
                                          <li><a href="#blog">Blog</a></li>
                                          <!-- <li><a href="#portfolio">Portfolio</a></li>
                                         -->

                                          <?php  do_action('uc_add_company_profile_admin_tab_name');   ?>
                              </ul>
                              <div class="tab-content bg-white">


                                   <div class="tab-pane  active" id="profile-edit" ng-controller="editProfile">
                                          <h3 class="tab-title">Edit Profile</h3>
                                          <div ng-animate="'fade'" static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/company/edit.html"></div>
                                    </div>


                                    <div class="tab-pane" id="blog" ng-controller="blog" >
                                          <h3 class="tab-title">
                                                Blog
                                          </h3>
                                          <div ng-show="showPost" class="cssSlideUp" ng-animate=" 'slide' " static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/blog/new-post.html"></div>
                                          <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/blog/post-list.html"></div>
                                         
                                    </div>
                                    <!--  <div class="tab-pane" id="portfolio" ng-controller="portfolio">
                                          <h3 class="tab-title">Portfolio
                                          </h3>
                                          <div ng-show="showPortfolio" ng-animate=" 'slide' " static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/portfolio/new-portfolio.html"></div>
                                          <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/portfolio/portfolio-list.html"></div>
                                   
                                    </div> -->
                                   

                                    <?php  do_action('uc_add_company_profile_admin_tab_pane');  ?>

                              </div>
                        </div>
 
                   </div> <!-- end .page-content -->
            </div>

      </div> <!-- end .container -->

</div> <!-- end #page-content -->



<?php
get_career_footer();