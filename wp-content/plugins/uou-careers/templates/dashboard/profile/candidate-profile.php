<?php
get_career_header();


$candidate = get_the_user('candidate');
$candidate->the_post();

$settingsData = get_candidate_profile($post->ID, $post->post_title, $post->post_content);

//$company = get_attached_company_for_agency($post->ID);

$candidate_id = $post->ID;

//echo "<pre>";print_r($current_user);echo "</pre>";

?>
<div id="page-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="responsive-tabs horizontal dashboard-horizontal-tabs">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#profile-edit">Edit Profile</a></li>
            <li><a href="#blog">Blog</a></li>
          </ul>
          <div class="tab-content">
                
            <div class="tab-pane active" id="profile-edit" ng-controller="editProfile">
              <h3 class="tab-title">Edit Profile</h3>
              <div ng-animate="'fade'" static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/candidate/edit.html"></div>
            </div>
           <div class="tab-pane" id="blog" ng-controller="blog" >
                  <h3 class="tab-title">
                        Blog
                       
                  </h3>
                  <div ng-show="showPost" class="cssSlideUp" ng-animate=" 'slide' " static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/blog/new-post.html"></div>
                  <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/blog/post-list.html"></div>
                 
            </div>
                                    

                
                
          </div>
        </div>
      </div> <!-- end .page-content -->
    </div>
  </div> <!-- end .container -->
</div> <!-- end #page-content -->


<?php
get_career_footer();