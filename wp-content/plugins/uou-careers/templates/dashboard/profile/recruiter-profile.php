<?php
get_career_header();
$recruiter = get_the_user('recruiter');
$recruiter->the_post();
get_recruiter_profile($post->ID, $post->post_title, $post->post_content);


?>

<div id="page-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="responsive-tabs horizontal dashboard-horizontal-tabs">
          
          <ul class="nav nav-tabs">
            <li class="active"><a href="#profile-edit">Edit Profile</a></li>
            <li><a href="#blog">Blog</a></li>
            <!-- <li><a href="#portfolio">Portfolio</a></li> -->
          </ul>

          <div class="tab-content">
            <div class="tab-pane active" id="profile-edit" ng-controller="editProfile">
              <h3 class="tab-title">Edit Profile</h3>

              <div ng-animate="'slide'" static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/recruiter/edit.html"></div>

              </div>


           <div class="tab-pane" id="blog" ng-controller="blog" >
                                          <h3 class="tab-title">
                                                Blog
                                          </h3>
                                          <div ng-show="showPost" class="cssSlideUp" ng-animate=" 'slide' " static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/blog/new-post.html"></div>
                                          <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/blog/post-list.html"></div>
                                         
                                    </div>
                                     <!-- <div class="tab-pane" id="portfolio" ng-controller="portfolio">
                                          <h3 class="tab-title">Portfolio
                                                <button class="btn btn-success btn-right" ng-click="editPortfolio('new')">
                                                <span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;Add New Portfolio
                                          </button>
                                          </h3>
                                          <div ng-show="showPortfolio" ng-animate=" 'slide' " static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/portfolio/new-portfolio.html"></div>
                                          <div static-include = "<?php echo UOU_CAREERS_URL;?>/assets/tpl/portfolio/portfolio-list.html"></div>
                                   
                                    </div> -->
          </div>
        </div>

      </div> <!-- end .page-content -->
    </div>
  </div> <!-- end .container -->
</div> <!-- end #page-content -->




<?php get_career_footer(); ?>