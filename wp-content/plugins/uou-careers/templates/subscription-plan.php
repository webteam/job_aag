<div class="modal fade" id="subscriptionModal" tabindex="-1" role="dialog" aria-labelledby="subscriptionPlanModalLabel" aria-hidden="true" >
  	<div class="modal-dialog"  ng-app="subscriptionPlan" ng-controller="subscriptionPlanController">
    		<div class="modal-content">
			<div class="modal-header  btn-primary">
			        	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			        	<h4 class="modal-title">Subscription Plan</h4>
			</div>
	      		<div class="modal-body">
	        			<div class="subscription-plan-error alert alert-warning hidden"></div>
	        			<form class="form-horizontal">
	        				<fieldset>
	            				
	            				<div class="form-group">
							<label class="col-md-3 control-label" for="meta-field-title">Subscription Plan:</label>
							 <div class="col-md-5">
								<input type="text" ng-model="subscriptionPlanForm.name" ng-required="true" />
								<input type="submit"  class="btn btn-warning" value="Add" ng-click="addSubscriptoionPlan()">
							</div>	
						</div>		
						
						   <!-- <pre>form = {{subscriptionPlans | json}}</pre>   -->
						
					</fieldset>	
						
				
				</form>


				 <accordion close-others="oneAtATime" ng-model="subscriptionPlans">
						 <accordion-group ng-repeat="node in subscriptionPlans"  is-open="status.open">
						    	<accordion-heading>
						    		{{node.title}}
						    	 	<i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"></i>
						    	 	<i class="pull-right glyphicon glyphicon-minus" ng-click="deleteSubscription($index)"></i>
	        						</accordion-heading>
						      	<table width="100%">
						      		<tr><th >Title</th><td editable-text="node.title">{{node.title}}</td></tr>
						  		<tr><th >Agent Quota</th><td editable-text="node.agent_quota">{{node.agent_quota}}</td></tr>
						  		<tr><th >Jobs Quota</th><td editable-text="node.jobs_quota">{{node.jobs_quota}}</td></tr>
						  		<tr><th >Featured Quota</th><td editable-text="node.featured_post_quota">{{node.featured_post_quota}}</td></tr>
						  		<tr><th >Membership Type</th><td editable-select="node.membership_type" e-ng-options="s.value as s.text for s in billingPeriods">{{ (billingPeriods | filter:{value: node.membership_type})[0].text || 'Not set' }}
</td></tr>
						  		<tr><th >Membership Price</th><td editable-text="node.membership_price">{{node.membership_price}}</td></tr>
						      	</table>

						      	
						</accordion-group>
					   
				</accordion>

			</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-success" ng-click="updateSubcription()">Save</button>
		        		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		        	</div>
    		</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
