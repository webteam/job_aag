<?php
//if(array_key_exists('reg_form_submit',$_POST)){
//// Handle Registration Form Submit
//$uou_signup_errors = array();
//print_r($_POST);
//}
?>
<span class="success"></span>
<div ng-app="userModule" ng-controller="userRegisterController" ng-cloak>
    <form name="userForm" ng-submit="submitForm()" class="form-horizontal" novalidate >
        <div class="alert alert-info ng-alert" ng-show="response_errors">
            <button href="#" type="button" class="close" ng-click="response_errors=false">&times;</button>
            <ul>
                  <li ng-repeat="(k,v) in response_errors">{{v}}</li>
            </ul>
        </div>
        <div class="alert alert-info ng-alert" ng-show="saved">
            <button href="#" type="button" class="close" ng-click="saved=false">&times;</button>
            <p>登録完了しました。</p>
        </div>
       
        <div class="form-group"   show-errors>
            <label for="email">メールアドレス</label>
            <input type="email" ng-model="user.email" name="email" class="form-control" id="email" placeholder="メールアドレスを入力して下さい" required />
            <p ng-if="userForm.email.$error.required" class="help-block">ただしメールアドレスを入力してください。</p>
        </div>
        <div class="form-group"   show-errors>
            <label for="username">ユーザ名</label>
            <input type="text" ng-model="user.username" name="username" class="form-control" id="username" placeholder="ユーザ名を入力してください" required />
            <p ng-if="userForm.username.$error.required" class="help-block">ユーザ名を入力されてません。</p>
        </div>
        <div class="form-group"   show-errors>
            <label for="password">パスワード</label>
            <input type="password" ng-model="user.password" name="password" class="form-control" id="password" placeholder="パスワードを入力してください" required />
            <p ng-if="userForm.password.$error.required" class="help-block">パスワードを入力されてません。</p>
        </div>

        <div class="form-group">
            <label for="registration_type"><!--Account Type-->口座の種類</label>
            <select class="form-control" ng-model="user.registration_type" name="registration_type" ng-options="s.name for s in accountType"></select>
        </div>
        <div class="form-group"   show-errors>
            <label for="name">会社/候補名</label>
            <input type="text" ng-model="user.name" name="name" class="form-control" placeholder="名前を入力して下さい" required />
            <p ng-if="userForm.name.$error.required" class="help-block">会社/候補名を選んでください。</p>
        </div>
        <button type="submit" class="btn btn-default" ng-disabled="userForm.$invalid">送信</button>
        
    </form>
</div>