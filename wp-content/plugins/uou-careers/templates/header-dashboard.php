<!doctype html>
<html lang="en">
<head>
            <meta charset="<?php bloginfo('charset') ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <title><?php wp_title('|',true,'right'); ?> <?php bloginfo('name'); ?></title>

            <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

            <!-- Stylesheets -->
            <link href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>

           

            <!-- Main Style -->
           <!--  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"> -->

            <!--[if IE 9]>
            <script src="js/media.match.min.js"></script>
            <![endif]-->
 <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>  ng-app="userModule">
          

                    
                       <header id="header">
                                    <div class="header-top-bar">
                                                <div class="container">
                                                            <!-- Logo -->
                                                            <a href="#" class="logo"><i class="fa fa-list"></i> Dashboard</a>
                                                            <!-- Dashboard Navigation -->
                                                            <nav class="dashboard-nav">
                                                                <ul>
                                                                    <li> <a href="<?php  echo get_users_public_profile(); ?>" translate="MENU_PUBLIC_PROFILE"></a></li>
                                                                    <li> <a href="<?php  echo home_url();  ?>/dashboard/my-profile" translate="MENU_PROFILE"></a></li> 
                                                                   
                                                                    <?php  if( get_user_role() == 'candidate'):  ?>
                                                                             <li> <a href="<?php  echo home_url();  ?>/dashboard" translate="MENU_RESUME"></a></li>        
                                                                    <?php  else:  ?>
                                                                        <li> <a href="<?php  echo home_url();  ?>/dashboard" translate="MENU_MY_SETTINGS"></a></li>
                                                                        <li> <a href="<?php  echo home_url();  ?>/dashboard/subscription" translate="MENU_SUBSCRIPTION"></a></li>


                                                                    <?php  endif;  ?>
                                                                    <li> <a href="<?php echo wp_logout_url( home_url() ); ?>" translate="MENU_LOGOUT"></a></li>
                                                                  
                                                                </ul>
                                                            </nav>
                                                </div>
                                    </div> <!-- end .header-top-bar -->

                                    <div class="header-page-title">
                                                <div class="container">
                                                            <h1></h1>
                                                </div>
                                    </div> <!-- end .header-nav-bar -->
                        </header> <!-- end #header -->

                        <div class="container">
                              <div class="row">
                                    
