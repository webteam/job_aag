<?php
get_header();
$user_role = get_user_role();
if( $user_role =="agency_admin" ){

} elseif (  $user_role =="company_admin"  ) {
    # code...
} elseif (  $user_role =="hr"  ) {
    # code...
} elseif (  $user_role =="agent"  ) {
    # code...
} 
?>
<h1>Create Jobs</h1>
<br/><br/>
<form action="" id="create_job" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="post" value="<?php  echo get_current_user_id();  ?>"/>
            <input type="hidden" name="group_id" value="<?php  echo $post->ID;  ?>"/>
            <div class="form-group">
                        <label for="job_title" class="control-label col-xs-2">Job Title</label>     
                        <div class="col-xs-10">     
                                    <input type="text" name="job_title" />
                        </div>
            </div>
            <div class="form-group">     
                         <label for="job_content"  class="control-label col-xs-2">Job Description</label> 
                         <div class="col-xs-10">    
                                    <?php   
                                            $editor_id = 'job_content';
                                             wp_editor( "", $editor_id ); 
                                    ?>
                        </div>            
            </div>
            <div class="form-group">
                        <label for="job_location" class="control-label col-xs-2">Job Location</label>
                        <div class="col-xs-10">
                                    <input type="text" name="job_location"/>
                        </div>            
            </div>
            <div class="form-group">
                        <label for="job_application" class="control-label col-xs-2">Application</label>
                        <div class="col-xs-10">
                                    <input type="text" name="job_application" />
                        </div>
            </div>
            <div class="form-group">
                        <label for="company_name" class="control-label col-xs-2">Company Name</label>
                        <div class="col-xs-9">
                                    <input type="text" name="company_name" />
                        </div>
            </div>
            <div class="form-group">
                        <label for="company_website" class="control-label col-xs-2">Company Website</label>
                        <div class="col-xs-10">
                                    <input type="text" name="company_website" />
                        </div>
            </div>
            <div class="form-group">
                        <label for="company_tagline" class="control-label col-xs-2">Company Tagline</label>
                        <div class="col-xs-10">   
                                    <input type="text" name="company_tagline" />
                        </div>
            </div>
            
            <div class="form-group">
                        <label for="job_expire" class="control-label col-xs-2">Job Expires</label>
                        <div class="col-xs-10">
                                    <input type="text" name="job_expire"  class="hasDatepicker datepicker" placeholder="YYYY-MM-DD"/>
                        </div>
             </div>
             <div class="form-group">
                        <input type="submit" class="btn btn-primary" id="create_job_submit" value="create"/>
            </div>            

</form>

    <br/><br/>
<?php
    get_footer();
?> 
