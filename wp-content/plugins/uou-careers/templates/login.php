<?php if (is_user_logged_in()) { ?>
    	<a class="login_button" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php } else { ?>
    	
	        	
			<div ng-controller="userLoginController" ng-cloak>
				<div class="alert alert-danger status" role="alert" style="display:none;">
				  	<a href="#" class="alert-link"></a>
				</div>
				<form name="userLoginForm" ng-submit="submitForm()" class="form-horizontal" novalidate >
					
			        <div class="form-group"   show-errors>
			            <label for="username">Username</label>
			            <input type="text" ng-model="user.username" name="username" class="form-control mt10" id="username" placeholder="Enter Username" required />
			            <p ng-if="userLoginForm.username.$error.required" class="help-block">Your username is required.</p>
			        </div>
					<div class="form-group"   show-errors>
			           	<label for="password">Password</label>
			            <input type="password" ng-model="user.password" name="password" class="form-control" id="password" placeholder="Enter Password" required />
			            <p ng-if="userLoginForm.password.$error.required" class="help-block">Enter a valid password.</p>
			        </div>
						<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
					<a class="lost" href="<?php echo wp_lostpassword_url(); ?>">Lost your password?</a>
					
					<br>

					<input class="submit_button btn btn-default" id="uc-login" type="submit" value="Login" name="submit">
						
					
				
				</form>	
			</div>	
	<?php } ?>
	
