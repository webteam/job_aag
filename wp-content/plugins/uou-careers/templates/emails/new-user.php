<?php do_action( 'uou_email_header', $email_heading ); ?>

<p><?php printf( __( "Thanks for creating an account on %s. Your username is <strong>%s</strong>.", 'uou' ), esc_html( $blogname ), esc_html( $user_login ) ); ?></p>


<p><?php printf( __( 'You can access your account area to view your overall information and modify you as you like here: %s.', 'uou' ),  $link ); ?></p>

<?php do_action( 'uou_email_footer' ); ?>