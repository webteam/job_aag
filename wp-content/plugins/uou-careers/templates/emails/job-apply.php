<?php do_action( 'uou_email_header', $email_heading ); ?>

<p><?php printf( __( "A job has been applied by %s.", 'uou' ), esc_html( $user_login ) ); ?></p>

<?php do_action( 'uou_email_footer' ); ?>