<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>

                            </td>
                        </tr>
                        <tr>
                            <td border="0" cellpadding="10" cellspacing="0" width="100%">
                                <?php echo wpautop( wp_kses_post( wptexturize( apply_filters( 'uou_email_footer_text', get_option( 'uou_email_footer_text' ) ) ) ) ); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body> 
</html>     