<?php do_action( 'uou_email_header', $email_heading ); ?>

<p><?php printf( __( "A job has been assigned to you %s.", 'uou' ), esc_html( $user_login ) ); ?></p>
<p><?php printf( __( "Job %s.", 'uou' ), esc_html( $job->post_title ) ); ?></p>
<p><?php printf( __( "Job Description %s.", 'uou' ), esc_html( $job->post_content ) ); ?></p>
<p><?php printf( __( "Job Link here %s.", 'uou' ), $link ); ?></p>

<?php do_action( 'uou_email_footer' ); ?>