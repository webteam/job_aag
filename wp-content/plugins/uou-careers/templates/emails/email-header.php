<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
// Load colours
$bg 		= get_option( 'uou_email_bg_color' );
$body		= get_option( 'uou_email_body_bg_color' );
$base 		= get_option( 'uou_email_base_color' );
$text 		= get_option( 'uou_email_text_color' );
$img 		= get_option( 'uou_email_header_image' );
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo get_bloginfo( 'name' ); ?></title>
	</head>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 	<head>
  		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  		<title><?php echo get_bloginfo( 'name' ); ?></title>
  		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body style="margin: 0; padding: 0;">
		<table border="1" cellpadding="0" cellspacing="0" width="100%" style="background-color: <?php echo esc_attr( $bg );?>;">
		  	<tr>
		   		<td>
		   			<table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="background-color:<?=$base?>;border-collapse: collapse;">
						<tr>
						  	<td align="center" style="padding: 40px 0 30px 0;">
						   		<?php
		                			if ( $img = get_option( 'uou_email_header_image' ) ) {
		                				echo '<p style="margin-top:0;"><img src="' . esc_url( $img ) . '" alt="' . get_bloginfo( 'name' ) . '" /></p>';
		                			}
		                		?>
		                		
                                <h1 style="color:<? echo $text; ?>;"><?php echo $email_heading; ?></h1>
							</td>
						</tr>
						<tr>
						  	<td style="background-color:<?= esc_attr( $body )?>;">
						   		
						