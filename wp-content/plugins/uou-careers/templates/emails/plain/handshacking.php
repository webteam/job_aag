<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo $email_heading . "\n\n";

echo sprintf( __( "%s named agency/company sent a invitation to give them responsibility. Agency username is <strong>%s</strong>.", 'uou' ), $user_login, $user_login ) . "\n\n";
echo sprintf(__("You invitation key is : %s", 'uou'), $verification_code). "\n\n";
echo sprintf( __( 'You can accept this offer by  click here: %s.', 'uou' ), $link ) . "\n\n";

echo "\n****************************************************\n\n";

echo apply_filters( 'uou_email_footer_text', get_option( 'uou_email_footer_text' ) );