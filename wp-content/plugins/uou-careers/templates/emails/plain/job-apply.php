<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo $email_heading . "\n\n";

echo sprintf( __( "A job has been applied by %s. ", 'uou' ), $user_login ) . "\n\n";


echo "\n****************************************************\n\n";

echo apply_filters( 'uou_email_footer_text', get_option( 'uou_email_footer_text' ) );