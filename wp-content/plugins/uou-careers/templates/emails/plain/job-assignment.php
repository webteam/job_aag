<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo $email_heading . "\n\n";
echo sprintf( __( "A job has been assigned to you %s.", 'uou' ), $user_login ) . "\n\n";

echo sprintf( __( "A job has been assigned to you %s.", 'uou' ),  $job->post_title ) . "\n\n";
echo sprintf( __( "A job has been assigned to you %s.", 'uou' ),  $job->post_content ) . "\n\n";

echo "\n****************************************************\n\n";

echo apply_filters( 'uou_email_footer_text', get_option( 'uou_email_footer_text' ) );