<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo $email_heading . "\n\n";

echo sprintf( __( "Thanks for creating an account on %s. Your username is <strong>%s</strong>.", 'uou' ), $blogname, $user_login ) . "\n\n";

echo sprintf( __( 'You can access your account area to view your settings and change your password here: %s.', 'uou' ), get_permalink( wc_get_page_id( 'myaccount' ) ) ) . "\n\n";

echo "\n****************************************************\n\n";

echo apply_filters( 'uou_email_footer_text', get_option( 'uou_email_footer_text' ) );