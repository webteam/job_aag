<?php do_action( 'uou_email_header', $email_heading ); ?>

<p><?php printf( __( "agency/company sent a invitation to give them responsibility. Agency/Company username is <strong>%s</strong>.", 'uou' ), esc_html( $user_login ) ); ?></p>
<p><?php printf(__("You invitation key is : %s", 'uou'), $verification_code);?></p>
<p><?php printf( __( 'You can accept this offer by  click here: %s.', 'uou' ),  $link ); ?></p>

<?php do_action( 'uou_email_footer' ); ?>