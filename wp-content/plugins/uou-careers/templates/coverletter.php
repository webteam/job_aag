<?php
if ( post_password_required() )
	return;
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		
		<div class="comment-list">
			<?php
				wp_list_comments( array(
						'style'       => 'li',
						'short_ping'  => true,
						'avatar_size' => 100,
						'reply_text'        => '',
						'type'=>'comment',
						'callback' =>'coverletters' 
					) 
				);
			?>
		</div> 

		<?php endif;?>

	<?php 
		if ( is_user_logged_in() ) {
		    $user_id = get_current_user_id();
		    $user_info = get_userdata(get_current_user_id());
    		$role = implode(', ', $user_info->roles);

		    $post = $wp_query->post;
		    $args = array(
		        'order' =>  'DESC',
		        'user_id' => $user_id,
		        'post_id' => $post->ID
		    );
		    $comments = get_comments($args);
		    if(count($comments) < 1 && $role == "candidate"){
				$req = get_option( 'require_name_email' );
		    	$aria_req = ( $req ? " aria-required='true'" : '' );
				$comment_args = array(
					'fields' => apply_filters(
						'comment_form_default_fields', array(
							'author' => '<p class="comment-form-author">' .
								'<label for="author">' . __( 'Your Name' ) . '</label> ' .
								( $req ? '<span class="required">*</span>' : '' ) .
								'<input id="author" name="author" type="text" value="' .
								esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />' .
								'</p><!-- #form-section-author .form-section -->',
							'email'  => '<p class="comment-form-email">' .
								'<label for="email">' . __( 'Your Email' ) . '</label> ' .
								( $req ? '<span class="required">*</span>' : '' ) .
								'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
								'" size="30"' . $aria_req . ' />' .
								'</p><!-- #form-section-email .form-section -->',
							'url'    => ''
						)
					),
					'comment_field' => '<p>' . __( 'Cover Letter' ) . '</p>' .
						'<p class="application-form-application">' .
						'<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>' .
						'</p>'.
						'<p><a tx-uploader class="btn btn-danger" ng-model="post.cv" href="#">Upload Your CV</a>'.
                  		' <a class="btn btn-danger removepic" ng-if="post.cv" ng-click="post.cv = \'\'" href="#">Remove</a>'.
                  		'<div>{{post.cv.url}}<input type="hidden" name="cv" value="{{post.cv.url}}" ></div></p>'
                  		,
				    'comment_notes_after' => '',
				    'title_reply' => 'Job Apply',
				    'label_submit'=>'Apply',
				    'id_submit'=>'apply-job',
				    'must_log_in'=> '',
				    'logged_in_as'=>'',
				    'id_form' => 'applicationform',
				);

				comment_form($comment_args); 

			}
		}

	?>

</div><!-- #comments -->