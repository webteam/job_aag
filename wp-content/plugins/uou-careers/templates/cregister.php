<?php
//if(array_key_exists('reg_form_submit',$_POST)){
//// Handle Registration Form Submit
//$uou_signup_errors = array();
//print_r($_POST);
//}
?>
<div class="white-container mt30">
    <span class="success"></span>
    <form role="form" method="post" action="" name="candidate-signup-form" id="uc-candidate-register-form">
       
                <div class="form-group">
                    <label for="fname">First Name</label>
                    <input type="text" name="fname" class="form-control" id="fname" placeholder="Enter First Name">
                </div>
                
                <div class="form-group">
                    <label for="lname">Last Name</label>
                    <input type="lname" name="lname" class="form-control" id="lname" placeholder="Enter Last Name">
                </div>
                
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                </div>

                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control" id="username" placeholder="Username">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>

                <div class="form-group">
                    <label for="registration_type">Upload your resume</label>
                    <input type="file" name="resume" />
                </div>

                <button type="submit" id="uc-candidate-register" name="reg_form_submit" class="btn btn-default">Submit</button>
    </form> 
</div>