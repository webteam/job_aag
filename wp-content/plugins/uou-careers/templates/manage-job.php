<?php
global $posts;
get_header();


if ( have_posts() ) {
	$associates = get_the_assocate_agency($posts[0]->ID);
	 while ( have_posts() ) { the_post();

	        echo '<h1>'.get_the_title().'</h1>';
	        the_content();
	}
	echo "<br/><br/><table width='100%'><tr><th>Agency Name</th><th>Contract Started</th></tr>";
	foreach ($associates as $key => $value) {
		echo '<tr><td>'.$value->post_title.'</td><td>'.date("Y-m-d", $value->start_date).'</td><tr>';
	}
	echo "</table>";
} else {
    echo 'no post was found';
}
wp_reset_postdata();


get_sidebar();

get_footer();
