<?php

$args = array(
	'post_type' => 'company',
	'posts_per_page' => -1
);
$posts = get_posts( $args );

$partners = array();


	foreach ($posts as  $post) {

		$post_id = $post->ID;

		$company_job_posts = get_posts(array(
			'post_type' => 'job',
			'meta_query' => array(
		       array(
		           'key' => 'company_id',
		           'value' => $post_id,
		           'compare' => '=',
		       )
	   		)
		));

		$count = count($company_job_posts);


		$partners[] = array(

			'id'                => $post_id,
			'title'             => $post->post_title,
			'content'			=> substr(strip_tags($post->post_content), 0, 140),
			'permalink'			=> home_url("/company/$post->post_name"),
			'jobs_count'		=> $count,
			'thumbnails' 		=> wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) )
		);

	}


	wp_localize_script( 'ng-controllers', 'listing', array( 'partners' => $partners ) );


?>
<div class="row">
	<div class="col-sm-12 page-content parnters">

		<div class="row">

			<div>
				<div ng-controller="partnerslisting">

					<div class="white-container candidates-search">
						<input type="text" class="form-control" placeholder="パートナーさんを検索" ng-model="search">
					</div>

					<div class="col-sm-6 col-md-4 col-lg-3" ng-repeat="partner in partners| filter:search">
						<div class="partners-item">
							<div class="img">
								<img src="{{ partner.thumbnails }}" alt="">

								<div class="overlay">
									<h6>{{ partner.title }}</h6>
									<p>{{ partner.content }}</p>
									<p><strong>Industry:</strong> <a href="#">Audio</a>, <a href="#">Sound</a>, <a href="#">Music</a></p>
								</div>
							</div>

							<div class="meta clearfix">
								<span>{{ partner.jobs_count }} Jobs</span>
								<a href="{{ partner.permalink }}" class="btn btn-default fa fa-link"></a>
								<!-- <a href="#" class="btn btn-default fa fa-search"></a> -->
							</div>
						</div>
					</div>
						
				</div>
			</div>
			<div class="clearfix"></div>
	</div>
</div>