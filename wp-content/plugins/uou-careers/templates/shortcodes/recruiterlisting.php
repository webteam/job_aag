<?php

$args = array(
	'post_type' => 'recruiter',
	'posts_per_page' => -1
);
$posts = get_posts( $args );


$recruiters = array();

foreach ($posts as  $post) {
	$post_id = $post->ID;
	$city_state = get_post_meta($post_id , '_agency_contact_city_state' , true );
	$country    = get_post_meta($post_id , '_agency_contact_country' , true );
	$location   = $city_state . ', ' . $country;

	$thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) );

	$recruiters[] = array(

		'id'         => $post_id,
		'title'      => $post->post_title,
		'content'    => $post->post_content,
		'thumb_src'  => (!empty($thumb_src)) ? $thumb_src : careers_theme_profile_placeholder_image_url(),
		'recruiter_url'  => home_url( '/recruiter/' .$post->post_name),
		'post_day'   => get_the_time('j', $post_id),
		'post_month' => get_the_time('M', $post_id),
		'location'   => $location

	);
}

wp_localize_script( 'ng-controllers', 'listing', array( 'recruiters' => $recruiters ) );

?>
<div class="pt30 pb30">
	<div>
		<div ng-controller="recruiterlisting">
			
			<div class="white-container candidates-search">
				<input type="text" class="form-control" placeholder="<!--Search for recruiter listing..-->1234" ng-model="search">
			</div>
			<div class="title-lines">
				<h3 class="mt0">Available Recruiter</h3>
			</div>

				
			<div class="jobs-item with-thumb" ng-repeat="recruiter in recruiters| filter:search">
				
				<div class="thumb"><a href="{{ recruiter.recruiter_url }}"><img src="{{ recruiter.thumb_src }}" alt=""></a></div>
				<div class="clearfix visible-xs"></div>
				<div class="date">{{ recruiter.post_day }} <span>{{ recruiter.post_month }}</span></div>
				<h6 class="title"><a href="{{ recruiter.recruiter_url }}">{{ recruiter.title }}</a></h6>
				<span class="meta">{{ recruiter.location }}</span>
				
				<ul class="top-btns">
					<li><a href="{{ recruiter.recruiter_url }}" class="btn btn-gray fa fa-link"></a></li>
				</ul>

				<p class="description" ng-bind-html="recruiter.content | limitTo:230"></p>

				<div class="clearfix"></div>
			</div>
				
		</div>
	</div>
</div>