<?php

$args = array(
	'post_type' => 'candidate',
	'posts_per_page' => -1
);
$posts = get_posts( $args );


$candidates = array();

foreach ($posts as  $post) {
	$post_id = $post->ID;
	$locations     = get_the_terms( $post_id, 'location' );

	$arr           = array();

    foreach ($locations as $location) {
      if($location->parent != 0) {
        $arr[] = $location;
      }
    }

    if($arr[0]->term_id == $arr[1]->parent) {
      $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
    } else {
      $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
    }
    
    $thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) );

    if(empty($thumb_src)) {
    	$thumb_src = careers_theme_profile_placeholder_image_url();
    }

	$candidates[] = array(

		'id'            => $post_id,
		'title'         => $post->post_title,
		'content'       => truncate($post->post_content, 230),
		'thumb_src'     => $thumb_src,
		'candidate_url' => home_url( '/candidate/' .$post->post_name),
		'post_day'      => get_the_time('j', $post_id),
		'post_month'    => get_the_time('M', $post_id),
		'location'      => $state_country

	);
}

wp_localize_script( 'ng-controllers', 'listing', array( 'candidates' => $candidates ) );

?>
<div class="pt30 pb30">
	<div>
		<div ng-controller="candidatelisting">
			
			<div class="white-container candidates-search clearfix">
				<div class="col-sm-6">
					<input type="text" class="form-control" placeholder="candidate name" ng-model="search.title">
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control" placeholder="xxxxxxxxx" ng-model="search.location">
				</div>
			</div>
			
			<div class="title-lines">
				<h3 class="mt0">Available Candidates</h3>
			</div>

				
			<div class="jobs-item with-thumb" ng-repeat="candidate in candidates | filter:search:strict">
				
				<div class="thumb"><a href="{{ candidate.candidate_url }}"><img ng-src="{{ candidate.thumb_src }}" alt=""></a></div>
				<div class="clearfix visible-xs"></div>
				<div class="date">{{ candidate.post_day }} <span>{{ candidate.post_month }}</span></div>
				<h6 class="title"><a href="{{ candidate.candidate_url }}">{{ candidate.title }}</a></h6>
				<span class="meta">{{ candidate.location }}</span>

				<ul class="top-btns">
					<li><a href="" class="btn btn-gray fa fa-star" ng-click="saveAsBookmark(candidate)"></a></li>
					<li><a href="{{ candidate.candidate_url }}" class="btn btn-gray fa fa-link"></a></li>
				</ul>

				<p class="description" ng-bind-html="candidate.content | limitTo:230"></p>

				<div class="clearfix"></div>
			</div>
				
		</div>
	</div>
</div>