<?php

$args = array(
	'post_type' => 'company',
	'posts_per_page' => -1
);
$posts = get_posts( $args );


$companies = array();

foreach ($posts as  $post) {
	$companies[] = array(

		'id'          => $post->ID,
		'title'       => $post->post_title,
		'content'     => $post->post_content,
		'thumb_src'   => wp_get_attachment_thumb_url( get_post_thumbnail_id( $post->ID ) ),
		'company_url' => home_url( '/company/' .$post->post_name),
		'location'    => get_post_meta($post->ID, '_user_contact_address', true)

	);
}

wp_localize_script( 'ng-controllers', 'listing', array( 'companies' => $companies ) );

?>

<div >
	<div ng-controller="companylisting">

		<input type="text" class="form-control" placeholder="search for companylisting" ng-model="search">

		<div class="title-lines">
			<h3 class="mt0">Available Companies</h3>
		</div>


			
		<div class="companies-item with-thumb" ng-repeat="company in companies| filter:search">
			
			<div class="thumb"><a href="{{ company.company_url }}"><img src="{{ company.thumb_src }}" alt=""></a></div>
			<h6 class="title"><a href="{{ company.company_url }}">{{ company.title }}</a></h6>
			<span class="meta">{{ company.location }}</span>
			<p class="description">{{ company.content }}</p>

			<div class="clearfix"></div>
		</div>
			
	</div>
</div>