<?php get_header(); ?>


<div class="container percentage-width">
	<div class="row">
		<div class="col-md-12">




			<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		  <li role="presentation" class="active"><a href="#profile" role="tab" data-toggle="tab">Profile</a></li>
		  <li role="presentation"><a href="#job" role="tab" data-toggle="tab">Job</a></li>
		  <li role="presentation"><a href="#blog" role="tab" data-toggle="tab">Blog</a></li>
		  <li role="presentation"><a href="#contact" role="tab" data-toggle="tab">Contact</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane active" id="profile">


				<?php  	if ( have_posts() ):  
				// while ( have_posts() ) : 
							the_post(); $post_id= $post->ID; $author_id= $post->post_author; ?>

						<div class="col-md-4">
								<?php 	if(  has_post_thumbnail() ) the_post_thumbnail( 'full' , array('class' => 'thumbnail') );  ?>
								<?php  
									// echo "POST META<br>";
									// var_dump(get_post_meta($post_id));
									// echo "USER META<br>";
									// var_dump(get_user_meta($author_id));
									// echo "<br>";
									// echo "<br>";

									$foundation_year     = get_post_meta(get_the_ID() , '_agency_contact_foundation_year' , true );
									$website             = get_post_meta(get_the_ID() , 'agency_website' , true );
									$legal_entity        = get_post_meta(get_the_ID() , '_agency_contact_legal_entity' , true );
									$contact_turnover    = get_post_meta(get_the_ID() , '_agency_contact_turnover' , true );
									$total_employee      = get_post_meta(get_the_ID() , '_agency_contact_employee_total' , true );
									$contact_phone       = get_post_meta(get_the_ID() , '_agency_contact_phone' , true );
									$contact_fax         = get_post_meta(get_the_ID() , '_agency_contact_fax' , true );
									$contact_post_code   = get_post_meta(get_the_ID() , '_agency_contact_post_code' , true );
									$address             = get_post_meta(get_the_ID() , '_agency_contact_address' , true );
									$opening_mon_fri     = get_post_meta(get_the_ID() , '_agency_opening_hours_monday_friday' , true );
									$opening_sat         = get_post_meta(get_the_ID() , '_agency_opening_hours_saturday' , true );
									$opening_sun         = get_post_meta(get_the_ID() , '_agency_opening_hours_sunday' , true );
									
									$check = get_post_meta( get_the_ID() , 'contact_email' , true );
									if(!empty($check))
										$agent_contact_email = get_post_meta(get_the_ID() , 'contact_email' , true );
									else
										$agent_contact_email = get_userdata($author_id)->user_email;

									$agency_slogan       = get_post_meta(get_the_ID() , 'agency_slogan' , true );
									
									$facebook            = get_post_meta(get_the_ID() , '_agent_facebook' , true );
									$twitter             = get_post_meta(get_the_ID() , '_agent_twitter' , true );
									$google              = get_post_meta(get_the_ID() , '_agent_google' , true );
									$linkedin            = get_post_meta(get_the_ID() , '_agent_linkedin' , true );
									$youtube             = get_post_meta(get_the_ID() , '_agent_youtube' , true );
									$instagram           = get_post_meta(get_the_ID() , '_agent_instagram' , true );


								  ?>

							
										
							

							<table class="table table-striped">



							    <?php  if( !empty($foundation_year) ):  ?>
							    <tr><td>Foundation year</td><td> <?php  echo $foundation_year;  ?></td></tr>
								<?php  endif;  ?>

							    <?php  if( !empty($foundation_year) ):  ?>
							    <tr><td>Legal Entity</td><td> <?php  echo $legal_entity;  ?></td></tr>
								<?php  endif;  ?>

							    <?php  if( !empty($contact_turnover) ):  ?>
							    <tr><td>Turnover</td><td> <?php  echo $contact_turnover;  ?></td></tr>
								<?php  endif;  ?>

							    <?php  if( !empty($total_employee) ):  ?>
							    <tr><td>Total Employee</td><td> <?php  echo $total_employee;  ?></td></tr>
								<?php  endif;  ?>


							    <?php  if( !empty($contact_phone) ):  ?>
							    <tr><td>Phone</td><td> <?php  echo $contact_phone;  ?></td></tr>
								<?php  endif;  ?>

							    <?php  if( !empty($contact_fax) ):  ?>
							    <tr><td>Fax</td><td> <?php  echo $contact_fax;  ?></td></tr>
								<?php  endif;  ?>

							    <?php  if( !empty($contact_post_code) ):  ?>
							    <tr><td>Zip</td><td> <?php  echo $contact_post_code;  ?></td></tr>
								<?php  endif;  ?>

						

							    <?php  if( !empty($address) ):  ?>
							    <tr><td>Address</td><td> <?php  echo $address;  ?></td></tr>
								<?php  endif;  ?>

							    <?php  if( !empty($website) ):  ?>
							    <tr><td>Website</td><td> <a href="<?php  echo $website;  ?>"> <?php  echo $website;  ?> </a></td></tr>
								<?php  endif;  ?>


								<tr><td colspan="2">Opening hour</td></tr>

							    <?php  if( !empty($opening_mon_fri) ):  ?>
							    <tr><td>Fri - Mon</td><td> <?php  echo $opening_mon_fri;  ?></td></tr>
								<?php  endif;  ?>


							    <?php  if( !empty($opening_sat) ):  ?>
							    <tr><td>Sat</td><td> <?php  echo $opening_sat;  ?></td></tr>
								<?php  endif;  ?>	



							    <?php  if( !empty($opening_sun) ):  ?>
							    <tr><td>Sun</td><td> <?php  echo $opening_sun;  ?></td></tr>
								<?php  endif;  ?>	

								
								<tr><td colspan="2">

								<?php  if(!empty($facebook) ): ?> <a class="social" href="<?php  echo $facebook;  ?>"><i class="fa fa-facebook"></i></a><?php  endif;  ?>
								<?php  if(!empty($twitter) ): ?> <a class="social" href="<?php  echo $twitter;  ?>"><i class="fa fa-twitter"></i></a><?php  endif;  ?>
								<?php  if(!empty($google) ): ?> <a class="social" href="<?php  echo $google;  ?>"><i class="fa fa-google-plus"></i></a><?php  endif;  ?>
								<?php  if(!empty($linkedin) ): ?> <a class="social" href="<?php  echo $linkedin;  ?>"><i class="fa fa-linkedin"></i></a><?php  endif;  ?>
								<?php  if(!empty($youtube) ): ?> <a class="social" href="<?php  echo $youtube;  ?>"><i class="fa fa-youtube"></i></a><?php  endif;  ?>
								<?php  if(!empty($instagram) ): ?> <a class="social" href="<?php  echo $instagram;  ?>"><i class="fa fa-instagram"></i></a><?php  endif;  ?>
								

								</td></tr>

							   
		



							</table>




						</div>
						<div class="col-md-8">
							<h1><?php  the_title();  ?></h1>

							    <?php  if( !empty($agency_slogan) ):  ?>
										<p><?php  echo $agency_slogan;  ?></p>
								<?php  endif;  ?>

							<p> <?php  the_content();  ?></p>
						</div>


				<?php  // endwhile;   
					else:
					    echo 'no post was found';
					endif;
				?>



		  	
		  </div>
		  <div role="tabpanel" class="tab-pane" id="job">

		  		<?php  $agency_job_posts = get_posts(array(
		  					'post_type' => 'job',
		  					'meta_query' => array(
						       array(
						           'key' => 'agent_id',
						           'value' => $post_id,
						           'compare' => '=',
						       )
						    )
						)); 

		  			  foreach ($agency_job_posts as $job_posts) {

		  			  			// Company
								$company_id   = get_post_meta( $job_posts->ID, 'company_id', true );
								$company_post =  get_post( $company_id );
								$company_name = $company_post->post_title;
								$company_url  = home_url( '/company/' .$company_post->post_name);

								// Agency
								$agency_id   = get_post_meta( $job_posts->ID, 'agency_id', true );
								$agency_post =  get_post( $agency_id );
								$agency_name = $agency_post->post_title;
								$agency_url  = home_url( '/agency/' .$agency_post->post_name);



				  			  	$expire = get_post_meta( $job_posts->ID ,'expire' , true);
								$future = strtotime($expire); 
								$days = round(($future - time()) / 86400);

								// Agency / Company thumb
								if( empty( $company_id ) && ! empty( $agency_id ) ) {

									$thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $agency_id ) );
									$thumb_url = $agency_url;

								} else {
									
									$thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $company_id ) );
									$thumb_url = $company_url;

								}





			
								if( $days >= 0){ ?>

									<div class="jobs-item with-thumb">
			
										<div class="thumb"><a href="<?php echo $thumb_url; ?>"><img src="<?php echo $thumb_src; ?>" alt=""></a></div>
										<h4 class="title"><a href="<?php echo get_the_permalink($job_posts->ID); ?>"><?php  echo $job_posts->post_title;  ?> <small> <?php  echo $days; if($days > 1 ) echo ' days'; else 'day';  ?>  left </small></a></h4>
										<span class="meta"><?php echo get_post_meta($job_posts->ID, 'location', true); ?></span>

										<p class="description">
											<?php  $content =  apply_filters('the_excerpt', $job_posts->post_content); 

											$trimmed_content = wp_trim_words( $content, 50, '<a href="'. get_permalink($job_posts->ID) .'"> ...Read More</a>' );
											
											echo $trimmed_content;
											?>
										</p>
										<div class="clearfix"></div>
									</div>

									<?php

								}


		  			  }
		  		?>


		  	
		  </div>
		  <div role="tabpanel" class="tab-pane" id="blog">

		  		<?php  $agent_blog_posts = get_posts(array(
		  					'post_type' => 'blog',
		  					'author'    => $author_id,		  					

		  				));

		  			  foreach ($agent_blog_posts as $blog_posts) : ?>

		  			  			<?php
		  			  				$post_id    = $blog_posts->ID;
									$post_class = 'agent-blog-post';
									$post_link  = $blog_posts->guid;
									$post_title = $blog_posts->post_title;
		  			  			?>

		  			  			<article class="blog-post with-thumb">

		  			  				<?php
		  			  					$thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id( $blog_posts->ID ) );

		  			  					if(!empty($thumbnail)) :
		  			  				?>
		  			  					<div class="thumb"><a href="<?php echo get_the_permalink($blog_posts->ID); ?>"><img class="post-image" src="<?php echo $thumbnail; ?>" alt=""></a></div>

		  			  				<?php endif; ?>
		  			  				
									<?php echo apply_filters( 'agent_blog_post_title_html', sprintf( '<h4><a href="%s" id="agent-blog-post-%s" class="%s" title="%s">%s</a></h4>', $post_link, $post_id, $post_class, $post_title, $post_title ), $post_id); ?>

									<span class="meta"></span>

									<p class="description">
										<?php
											apply_filters('the_content', $blog_posts->post_content);
											$content =  apply_filters('the_content', $blog_posts->post_content); 

											$trimmed_content = wp_trim_words( $content, 50, '<a href="'. get_permalink($blog_posts->ID) .'"> ...Read More</a>' );
											
											echo $trimmed_content;
										?>
									</p>

									<div class="clearfix"></div>

								</article>
		  			  				
		  		<?php endforeach; ?>
		  	
		  </div>


		  <div role="tabpanel" class="tab-pane" id="contact">
		  		<h1>Contact form</h1>
		  		<br>
				<form class="col col-md-4" 	method="post" action="">
					<input type="hidden" name="to" class="form-control"  value="<?php  echo $agent_contact_email;  ?>"> 
					<label>From</label>
					<input type="text" name="from" class="form-control"  placeholder="From: ">
					<label>Subject</label>
					<input type="text" name="subject" class="form-control"  placeholder="Subject: ">
					<label>Message</label>
					<textarea name="message" placeholder="Message"></textarea>


					<br>
					<button type="submit" class="btn btn-info">Submit</button>
					<?php wp_nonce_field('handle_contact_form', 'nonce_contact_form');?>
				</form>

		  </div>

			<div class="clearfix"></div>
		</div>




	</div>	
	</div><!--  end of row  -->
</div><!--  end of container -->


<?php

wp_reset_postdata();
//get_sidebar();
get_footer();