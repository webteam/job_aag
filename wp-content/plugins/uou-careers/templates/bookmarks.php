<?php get_header(); 
if ( ! is_user_logged_in() ) :
	echo "Please login to see your bookmarked itemlist.";
else :
	$userID = get_current_user_id();
	$array = array('bookmarked_job','bookmarked_candidate','bookmarked_company');
	$bookmarklist = array();
	foreach ($array as $key => $value) {
		$split = explode('_', $value);
		$bookmarkedlist = get_user_meta($userID, $value, true);
		if ( !empty( $bookmarkedlist ) ) {
			$args = array(
				'post_type' => $split[1],
				'posts_per_page' => -1,
				'post__in' => $bookmarkedlist
			);
			$posts = get_posts( $args );


				foreach ($posts as  $post) {
					$post_id = $post->ID;
					$locations     = get_the_terms( $post_id, 'location' );
					$arr           = array();

					if( ! empty( $locations ) ) {
						foreach ($locations as $location) {
					      if($location->parent != 0) {
					        $arr[] = $location;
					      }
					    }

					    if($arr[0]->term_id == $arr[1]->parent) {
					      $state_country   = $arr[1]->name . ', ' . $arr[0]->name;
					    } else {
					      $state_country   = $arr[0]->name . ', ' . $arr[1]->name;
					    }
					}

					if( $post->post_type === 'job') {
						$company_id      = get_post_meta( $post_id, 'company_id', true );
						$thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $company_id ) );	
					} else {
						$thumb_src = wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) );
					}

				    if(empty($thumb_src)) {
				    	$thumb_src = careers_theme_profile_placeholder_image_url();
				    }
				    
					$bookmarklist[$value][] = array(

						'id'            => $post_id,
						'title'         => $post->post_title,
						'content'       => truncate($post->post_content, 230),
						'thumb_src'     => $thumb_src,
						'url' => home_url( '/'.$split[1].'/' .$post->post_name),
						'post_day'      => get_the_time('j', $post_id),
						'post_month'    => get_the_time('M', $post_id),
						'location'      => $state_country,
						'type' 			=> 'search_'.$split[1]

					);
				}
			}

			wp_localize_script( 'ng-controllers', 'listing', array( 'bookmarklist' => $bookmarklist ) );
		}
		


	?>
	<div class="pt30 pb30">
		<div>
			<div ng-controller="bookmarklisting">
				
				<div class="white-container bookmarks-search clearfix">
					<div class="col-sm-6">
						<select ng-model="search.type">
			              <option value="search_job">Job</option>
			              <option value="search_candidate">Candidate</option>
			              <option value="search_company">Company</option>
			            </select>
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" placeholder="title" ng-model="search.title">
					</div>
				</div>
				
				<div class="title-lines">
					<h3 class="mt0">Bookmarks</h3>
				</div>

				<div ng-repeat="bookmarks in bookmarklist">	
					<div class="jobs-item with-thumb" ng-repeat="bookmark in bookmarks  | filter:search:strict">
						<div class="thumb"><a href="{{ bookmark.url }}"><img ng-src="{{ bookmark.thumb_src }}" alt=""></a></div>
						<div class="clearfix visible-xs"></div>
						<div class="date">{{ bookmark.post_day }} <span>{{ bookmark.post_month }}</span></div>
						<h6 class="title"><a href="{{ bookmark.candidate_url }}">{{ bookmark.title }}</a></h6>
						<span class="meta">{{ bookmark.location }}</span>

						<ul class="top-btns">
							<li><a href="{{ bookmark.url }}" class="btn btn-gray fa fa-link"></a></li>
						</ul>

						<p class="description" ng-bind-html="bookmark.content"></p>

						<div class="clearfix"></div>
					</div>
				</div>	
			</div>
		</div>
	</div>

<?php
endif;
wp_reset_postdata();
//get_sidebar();
get_footer();
?>