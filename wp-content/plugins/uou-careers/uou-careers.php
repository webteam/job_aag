<?php
/**
 * Plugin Name: uou careers
 * Plugin URI:  http://uou.ch
 * Description: Complete job management app on top of WordPress
 * Author:      Tareq Jobayere
 * Author URI:  http://tarex.github.io
 * Version:     0.1
 * Text Domain: uou-careers
 * Domain Path: /languages/
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
    exit;



  function _log( $message ) {
    if( WP_DEBUG === true ){
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
    }
  }






if ( ! class_exists( 'UouCareers' ) ) :


Class UouCareers{

    public $version = '0.0.1';

    protected static $_instance = null;

    public $careers_options;

    public $payment_option;


    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    public function __construct(){

      define( 'UOU_CAREERS_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
      define( 'UOU_CAREERS_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
      define( 'UC_PLUGIN_FILE', __FILE__ );
     




       include( 'includes/admin/class-uou-careers-admin.php' );
       include( 'includes/admin/WPFlashMessages.php' );

       //  add_action('admin_notices', array( $this , 'uc_careers_admin_notices') );



        include( 'includes/class-uou-careers-install.php' );
        include( 'includes/uou-careers-stripe.php' );
        include( 'includes/class-uou-careers-basic-process.php' );
        include( 'includes/class-uou-careers-user-permission.php' );
        include( 'includes/class-uou-careers-post-types.php' );
        include( 'includes/class-uou-careers-router.php' );
        include( 'includes/class-uou-careers-load-template.php' );
        include( 'includes/class-uou-careers-shortcodes.php' );
        include( 'includes/class-uou-careers-ajax-post.php' );
        include( 'includes/uou-careers-template-functions.php' );
        include( 'includes/class-uou-careers-emails.php' );
        include( 'includes/class-uou-careers-contact-form.php' );



        // Atmf
        // require_once( UOU_CAREERS_DIR . '/includes/class-uou-atmf-ajax-admin-request.php' );


        //global $tada;
        //$this->careers_options = $tada;

      //  add_filter( 'uou_subscribe_option_data', array( $this, 'subscribe_option_data' ),10 );



        $payments_data = get_option('payments');
        $payment_option = $payments_data['uc_payment_method'] ? $payments_data['uc_payment_method'] : ' ';

        $this->payment_option =  $payment_option;


       
        // Actions
        add_action( 'plugins_loaded', array( $this, 'uc_load_careers_textdomain' ) );
        add_action( 'switch_theme', 'uc_flush_rewrite_rules', 15 );



        add_action('admin_enqueue_scripts', array( $this , 'uou_admin_load_scripts' ) );
        add_action( 'wp_enqueue_scripts', array( $this ,'uc_load_scripts' ) );
        $email_functions = array(
            'uou_new_user',
            'uou_job_apply',
            'uou_handshacking',
            'uou_job_assignment'
        );
        foreach ( $email_functions as $function )
            add_action( $function, array( $this, 'set_email' ), 10, 10 );
        
        
    }


   
    public function set_email(){
        $this->mailer();

        $args = func_get_args();
        do_action_ref_array( current_filter() . '_notification', $args );
    }
    public function uc_load_careers_textdomain() {

        load_plugin_textdomain( 'uou-careers', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    }


    public function mailer() {
        return Uou_Careers_Emails::instance();
    }



    public function uc_careers_admin_notices(){
        echo '<div class="updated">

        <h1> Careers Plugin Notice </h1>

        </div>';
    }




    public function uou_admin_load_scripts(){

          

            wp_enqueue_script( 'searchjs',  UOU_CAREERS_URL."/assets/js/admin-meta.js" );

            wp_register_style( 'bootstrap-css', UOU_CAREERS_URL. '/assets/css/bootstrap-admin.css', array(), false, 'all' );
            wp_enqueue_style( 'bootstrap-css' );

            wp_register_style( 'colorpicker-css', UOU_CAREERS_URL. '/assets/js/vendor/angular-colorpicker/colorpicker.css', array(), false, 'all' );
            wp_enqueue_style( 'colorpicker-css' );

            wp_register_style( 'ui-css', '//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css', array(), false, 'all' );
            wp_enqueue_style( 'ui-css' );

            wp_register_script( 'qui', '//code.jquery.com/ui/1.11.0/jquery-ui.js', array(), false, true );
            wp_enqueue_script( 'qui' );

            // wp_register_script( 'bootstrap', UOU_CAREERS_URL. '/assets/js/bootstrap.min.js', array(), false, true );
            // wp_enqueue_script( 'bootstrap' );

            wp_register_script( 'atmf-angular', UOU_CAREERS_URL. '/assets/js/vendor/angular/angular.js', array(), false, true );
            wp_enqueue_script( 'atmf-angular' );

            wp_register_script( 'atmf-angular-sanitize', UOU_CAREERS_URL. '/assets/js/vendor/angular-sanitize/angular-sanitize.js', array(), false, true );
            wp_enqueue_script( 'atmf-angular-sanitize' );

            wp_register_script( 'angular-bootstrap', UOU_CAREERS_URL. '/assets/js/vendor/angular-bootstrap/ui-bootstrap-tpls.js', array(), false, true );
            wp_enqueue_script( 'angular-bootstrap' );

            wp_register_style( 'angular-xeditable-css', UOU_CAREERS_URL. '/assets/js/vendor/xeditable/xeditable.css', array(), false, 'all' );
            wp_enqueue_style( 'angular-xeditable-css' );

            wp_register_script( 'angular-xeditable', UOU_CAREERS_URL. '/assets/js/vendor/xeditable/xeditable.js', array(), false, true );
            wp_enqueue_script( 'angular-xeditable' );
            
            wp_register_script( 'angular-strap', '//cdnjs.cloudflare.com/ajax/libs/angular-strap/2.1.2/angular-strap.min.js', array(), false, true );
            wp_enqueue_script( 'angular-strap' );
            
            wp_register_script( 'angular-strap-tpl', '//cdnjs.cloudflare.com/ajax/libs/angular-strap/2.1.2/angular-strap.tpl.min.js', array(), false, true );
            wp_enqueue_script( 'angular-strap-tpl' );

            wp_register_script( 'angular-colorpicker', UOU_CAREERS_URL. '/assets/js/vendor/angular-colorpicker/angular-colorpicker.js', array(), false, true );
            wp_enqueue_script( 'angular-colorpicker' );
           
            wp_register_script( 'ng-app',  UOU_CAREERS_URL."/assets/js/back/app.js", array(), false, true );
            wp_enqueue_script( 'ng-app' );

            wp_register_script( 'ng-controllers',  UOU_CAREERS_URL."/assets/js/back/controllers.js" , array(), false, true);
            wp_enqueue_script( 'ng-controllers' );

            wp_localize_script('searchjs', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )  );
            
            wp_enqueue_style( 'wp-color-picker' );
            $this->createtaxonomylocationifnotexist();
    }




    public function uc_load_scripts(){



        
        wp_register_style( 'bootstrap-css', UOU_CAREERS_URL. '/assets/css/bootstrap.min.css', array(), false, 'all' );
        wp_enqueue_style( 'bootstrap-css' );

        wp_register_style( 'fontawesome', UOU_CAREERS_URL. '/assets/js/vendor/font-awesome/font-awesome.css', array(), false, 'all' );
        wp_enqueue_style( 'fontawesome' );


        wp_register_style( 'card-css', UOU_CAREERS_URL. '/assets/js/vendor/card/card.css', array(), false, 'all' );
        wp_enqueue_style( 'card-css' );
        
        wp_register_style( 'uc-career-animate', UOU_CAREERS_URL. '/assets/css/animate.css', array(), false, 'all' );
        wp_enqueue_style( 'uc-career-animate' );


        wp_register_style( 'uc-career-css', UOU_CAREERS_URL. '/assets/css/career.css', array(), false, 'all' );
        wp_enqueue_style( 'uc-career-css' );

        wp_register_style( 'redactor-css', UOU_CAREERS_URL. '/assets/css/redactor.css', array(), false, 'all' );
        wp_enqueue_style( 'redactor-css' );

        wp_register_script( 'stripe', 'https://js.stripe.com/v2/', array(), false, false );
        wp_enqueue_script( 'stripe' );
    
        wp_enqueue_script( 'jquery' );

        wp_register_script( 'redactor-js', UOU_CAREERS_URL. '/assets/js/redactor.js', array(), false, false );
        wp_enqueue_script( 'redactor-js' );

        wp_register_script( 'redactor-plugin-js', UOU_CAREERS_URL. '/assets/js/vendor/angular-redactor/imagemanager.js', array(), false, false );
        wp_enqueue_script( 'redactor-plugin-js' );
        wp_register_script( 'redactor-video-plugin-js', UOU_CAREERS_URL. '/assets/js/vendor/angular-redactor/video.js', array(), false, false );
        wp_enqueue_script( 'redactor-video-plugin-js' );

        wp_enqueue_media();
        wp_register_script( 'jqueryba', UOU_CAREERS_URL. '/assets/js/jquery.ba-outside-events.min.js', array(), false, true );
        wp_enqueue_script( 'jqueryba' );
         
        wp_register_script( 'jqueryinview', UOU_CAREERS_URL. '/assets/js/jquery.inview.min.js', array(), false, true );
        wp_enqueue_script( 'jqueryinview' );
         
        wp_register_script( 'jqueryresponsive', UOU_CAREERS_URL. '/assets/js/jquery.responsive-tabs.js', array(), false, true );
        wp_enqueue_script( 'jqueryresponsive' );
         
        wp_register_script( 'script', UOU_CAREERS_URL. '/assets/js/script.js', array(), false, true );
        wp_enqueue_script( 'script' );

        wp_register_script( 'bootstrap', UOU_CAREERS_URL. '/assets/js/vendor/bootstrap/bootstrap.js', array(), false, true );
        wp_enqueue_script( 'bootstrap' );

        wp_register_script( 'card-js', UOU_CAREERS_URL. '/assets/js/vendor/card/card.js', array(), false, true );
        wp_enqueue_script( 'card-js' );

        wp_register_script( 'ajaxHandle', UOU_CAREERS_URL. '/assets/js/ajax.js', array(), false, true );
        wp_enqueue_script( 'ajaxHandle' );

        
        wp_register_script( 'uc-angular', UOU_CAREERS_URL. '/assets/js/vendor/angular/angular.js', array(), false, true );
        wp_enqueue_script( 'uc-angular' );

        wp_register_script( 'angular-cookies', UOU_CAREERS_URL. '/assets/js/vendor/angular-cookies/angular-cookies.min.js', array(), false, true );
        wp_enqueue_script( 'angular-cookies' );

        wp_register_script( 'atmf-angular-loading-bar', UOU_CAREERS_URL. '/assets/js/vendor/angular-loading-bar/loading-bar.js', array(), false, true );
        wp_enqueue_script( 'atmf-angular-loading-bar' );

        wp_register_script( 'angular-translate', UOU_CAREERS_URL. '/assets/js/vendor/angular-translate/angular-translate.min.js', array(), false, true );
        wp_enqueue_script( 'angular-translate' );

        wp_register_script( 'atmf-angular-sanitize', UOU_CAREERS_URL. '/assets/js/vendor/angular-sanitize/angular-sanitize.js', array(), false, true );
        wp_enqueue_script( 'atmf-angular-sanitize' );

        wp_register_script( 'atmf-angular-animate', UOU_CAREERS_URL. '/assets/js/vendor/angular-animate/angular-animate.js', array(), false, true );
        wp_enqueue_script( 'atmf-angular-animate' );

        wp_register_script( 'angular-bootstrap', UOU_CAREERS_URL. '/assets/js/vendor/angular-bootstrap/ui-bootstrap-tpls.js', array(), false, true );
        wp_enqueue_script( 'angular-bootstrap' );

        wp_register_script( 'angular-strap', UOU_CAREERS_URL.'/assets/js/vendor/angular-strap/angular-strap.js', array(), false, true );
        wp_enqueue_script( 'angular-strap' );
        
        wp_register_script( 'angular-strap-tpl', UOU_CAREERS_URL.'/assets/js/vendor/angular-strap/angular-strap.tpl.js', array(), false, true );
        wp_enqueue_script( 'angular-strap-tpl' );

        wp_register_script( 'uc-dirpagination', UOU_CAREERS_URL. '/assets/js/vendor/angular-utils-pagination/dirPagination.js', array(), false, true );
        wp_enqueue_script( 'uc-dirpagination' );
        wp_localize_script( 'uc-dirpagination', 'tpl_object', array('tplurl' => UOU_CAREERS_URL.'/assets/tpl/dirPagination/dirPagination.tpl.html' ) );

        wp_register_script( 'angular-redactor', UOU_CAREERS_URL. '/assets/js/vendor/angular-redactor/angular-redactor.js', array(), false, true );
        wp_enqueue_script( 'angular-redactor' );

        wp_register_script( 'ng-app',  UOU_CAREERS_URL."/assets/js/front/app.js", array(), false, true );
        wp_enqueue_script( 'ng-app' );

        wp_register_script( 'ng-services',  UOU_CAREERS_URL."/assets/js/front/services/services.js" , array(), false, true);
        wp_enqueue_script( 'ng-services');

        wp_register_script( 'ng-directives',  UOU_CAREERS_URL."/assets/js/front/directives/directives.js" , array(), false, true);
        wp_enqueue_script( 'ng-directives' );
        
        wp_register_script( 'ng-controllers',  UOU_CAREERS_URL."/assets/js/front/controllers/controllers.js" , array(), false, true);
        wp_enqueue_script( 'ng-controllers' );

        wp_register_script( 'ng-editprofile',  UOU_CAREERS_URL."/assets/js/front/controllers/editprofile.js" , array(), false, true);
        wp_enqueue_script( 'ng-editprofile' );
        
        wp_register_script( 'ng-recruiter',  UOU_CAREERS_URL."/assets/js/front/controllers/recruiter.js" , array(), false, true);
        wp_enqueue_script( 'ng-recruiter' );
        wp_register_script( 'ng-job-applicationlist',  UOU_CAREERS_URL."/assets/js/front/controllers/job-applicationlist.js" , array(), false, true);
        wp_enqueue_script( 'ng-job-applicationlist' );

        wp_register_script( 'ng-job',  UOU_CAREERS_URL."/assets/js/front/controllers/job.js" , array(), false, true);
        wp_enqueue_script( 'ng-job' );
        
        wp_register_script( 'ng-blog',  UOU_CAREERS_URL."/assets/js/front/controllers/blog.js" , array(), false, true);
        wp_enqueue_script( 'ng-blog' );

        wp_register_script( 'ng-register',  UOU_CAREERS_URL."/assets/js/front/controllers/register.js" , array(), false, true);
        wp_enqueue_script( 'ng-register' );
        $generall_option = get_option('general');
        $posts_per_page = get_option('posts_per_page');
        
        wp_localize_script( 'ajaxHandle', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'posts_per_page' => $posts_per_page, 'currency_option'=>$generall_option['uou_currency_sign'], 'tpl_url'=> UOU_CAREERS_URL."/assets/tpl/")) ;
        
        $this->localize_taxonomy();
    }
    public function createtaxonomylocationifnotexist(){
        include('includes/countryList.php');
        //echo "d";exit;
        //print_r($countryList);exit;
        foreach ($countryList as $key => $value) {
            if(!($parent_term = term_exists( $key, 'location'))){
                $parent_term = wp_insert_term(
                    $key, // the term 
                    'location', // the taxonomy
                    array(
                      'description'=> 'Region '. strtolower($key),
                      'slug' => strtolower($key),
                      'parent'=> 0
                    )
                );
            }
            $parent_term_id = $parent_term['term_id'];
            if(isset($parent_term_id)){
                foreach ($value as $key1 => $value1) {
                    $val = is_array($value1)? $key1 : $value1;
                    if(!($country_term = term_exists( $val, 'location', $parent_term_id))){
                        $country_term = wp_insert_term(
                            $val, // the term 
                            'location', // the taxonomy
                            array(
                                'description'=> 'Country '. strtolower($val),
                                'slug' => strtolower($val),
                                'parent'=> $parent_term_id
                            )
                        );
                    }
                    $country_term_id = $country_term['term_id'];
                    if(isset($country_term_id)){
                        if(is_array($value1)){
                            foreach ($value1 as $key2 => $value2) {
                                if(!($state_term = term_exists( $value2, 'location', $country_term_id))){
                                    $state_term = wp_insert_term(
                                        $value2, // the term 
                                        'location', // the taxonomy
                                        array(
                                            'description'=> 'State '. strtolower($value2),
                                            'slug' => strtolower($value2),
                                            'parent'=> $country_term_id
                                        )
                                    );
                                }
                            }
                        }     
                    }
                }
                
            }
        }    
    }
    function getChildTaxonomy($parent){
        global $wpdb;
        $sql = "SELECT $wpdb->terms.name, $wpdb->term_taxonomy.* FROM $wpdb->term_taxonomy ".
                " INNER JOIN $wpdb->terms ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id WHERE $wpdb->term_taxonomy.parent = $parent ORDER BY $wpdb->term_taxonomy.taxonomy";

        $query = $wpdb->get_results( $sql );
        $terms = array();
        $i = 0;
        foreach ($query as $value) {
            $terms[$i]['term_id'] = $value->term_id;
            $terms[$i]['term_name'] = $value->name;
            $terms[$i]['parent'] = $value->parent;
            $i++;
        }
        return $terms;
    }
    function localize_taxonomy(){
        global $wpdb;
        $sql = "SELECT $wpdb->terms.name, $wpdb->term_taxonomy.* FROM $wpdb->term_taxonomy ".
                " INNER JOIN $wpdb->terms ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id WHERE $wpdb->term_taxonomy.parent = 0 ORDER BY $wpdb->term_taxonomy.taxonomy";

        $query = $wpdb->get_results( $sql );
        $terms = array();
        $i = 0;
        foreach ($query as $value) {
            $terms[$value->taxonomy][$i]['term_id'] = $value->term_id;
            $terms[$value->taxonomy][$i]['term_name'] = $value->name;
            $terms[$value->taxonomy][$i]['parent'] = $value->parent;
            if($value->taxonomy == "industry")
                $terms[$value->taxonomy][$i]['childs'] = $this->getChildTaxonomy($value->term_id);
            $i++;
        }
        wp_localize_script('ng-controllers', 'form_taxonomy', array( 'taxonomylist' => $terms )  );
    }
}

endif;



function UC() {
    return UouCareers::instance();
}

// Global for backwards compatibility.
$GLOBALS['careers'] = UC();


