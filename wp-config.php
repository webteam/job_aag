<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'itaa_aajob');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'itaa_aajob');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'aag888');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0gU~2R]+m?vE}ne(/-3ee=gt{2M*= $U}aG}Q~CD@*Bqi]1jKQ 0TKS_R2+1i;:`');
define('SECURE_AUTH_KEY',  '-hEm5#.f3p{^TuJKuBxX3xG.$ioh4WCa]O4sz>M*/?T6HA9C}Yz qLl6~^%a+ 4z');
define('LOGGED_IN_KEY',    ' XRg|1@wkd3@-^tGJhE<hpnGq dd>DH>fA6s)|x6^wEipb?xi.&/5*jbZbm=DJ#d');
define('NONCE_KEY',        '@Bwaoan.Q,;B,D10.JI}d|EUwYBadD3-K-MX1.<AWi|ft<q&RAyWn:MfR2_OxZx{');
define('AUTH_SALT',        'z{G+aXxZ/ZUaU|34$OUdnv[b5Hk~`Z80UEn]1z{)=oV*{yS?rI;(+TS~u1x9/xY{');
define('SECURE_AUTH_SALT', '.8h]h,D7Q{NQ#eS1dq+EY(,W8&G?H?M2W*8k9Kv|>QwWCs[e,G9OVRC2vJRPeK`,');
define('LOGGED_IN_SALT',   ')Kqc!TjoMXo@r}:fdtohpdeBo_DEA%rh%S+%xkE|v^^ FbVEL5EIG>|]XR{K 8g7');
define('NONCE_SALT',       '4Pulp+3-Bpz_sKQu^.+hZRdTz{em*]lgtPanIOz<&4h-fTshSIk^51l4|~b*6_`$');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
